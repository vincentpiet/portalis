open Nethttpd_types
open Nethttpd_services
open Nethttpd_engine
open Utils
open StdLabels
open Users

module Context = Context.Make(Glogic.CDescr)(Glogic.Src)(Glogic.Wll)
module Ext = Context.Ext
module Query = Context.Root.Query


(* -------------------------------------------------------------- *)
(* Globals *)
let port = ref 0

let creator = ref ""
let activation_date = ref (rfc3339_now ())
let last_update = ref "none"
let id = ref "no_name"

let logfile = ref "/tmp/portalis.log"
let ignore_rights = ref false

let data_dir = ref "/tmp"
let context_name = ref None



(* -------------------------------------------------------------- *)
(* exception handling *)
exception No_context
(* in module Users:
   - No_such_role of string
   - Not_enough_rights
   - No_such_key of string
   - Expired_key of string
   - Registered_key of string
 *)

type param_error = Missing_parameter of string | Empty_parameter of string
exception Parameter_error of param_error list

exception Wrong_creator of string

let xml_of_exn = fun e ->
  let xml_msg msg = [Xml.Element ("message", [], [Xml.PCData msg])] in
  match e with
  | No_context -> xml_msg "No context is loaded."
  | No_such_role str -> xml_msg (str ^ " is not a valid role name.")
  | Not_enough_rights -> xml_msg "Not enough rights"
  | No_such_key str -> xml_msg ("Key " ^ str ^ " is not registered for this service.")
  | Expired_key str -> xml_msg ("Key " ^ str ^ " has expired.")
  | Registered_key str -> xml_msg ("Key " ^ str ^ " is already registered.")
  | Parameter_error errors ->
    let f = fun err -> match err with
      | Missing_parameter str -> xml_msg ("parameter " ^ str ^ " is missing")
      | Empty_parameter str -> xml_msg ("parameter " ^ str ^ " is empty")
    in
    List.flatten (List.map f errors)
  | No_such_file str -> xml_msg ("File " ^ str ^ " was not found on the server.")
  | Wrong_creator str -> xml_msg ("The given address: " ^ str ^ " does not match the creator email.")
  | Todo str -> xml_msg ("TODO: implement " ^ str ^ " command.")
  | _ -> xml_msg (Printexc.to_string e)



(* -------------------------------------------------------------- *)
(* Common sanity checks *)

let check_param = fun param (cgi: Netcgi.cgi_activation) ->
  if not (cgi # argument_exists param) then
    Some (Missing_parameter param)
  else if (cgi # argument_value param) = "" then
    Some (Empty_parameter param)
  else
    None

let assert_params = fun params (cgi: Netcgi.cgi_activation) ->
  let checks = List.map (fun p -> check_param p cgi) params in
  let errors = List.filter (fun x -> is_some x) checks in
  let errors' = List.map val_of_option errors in
  if errors' <> [] then raise (Parameter_error errors')



(* -------------------------------------------------------------- *)
(* Helper functions for Camelis *)

let rec wait_for_camelis_updates () =
  (* we'd like to use Unix.sleep, but its minimum is a 1 second sleep,
   * which is way too long here . So we use Unix.select in a
   * non-standard way to pause for 10 ms. *)
  let _ = Unix.select [] [] [] 0.01 in
  let n = Context.nb_updates () in
  if n > 0
  then wait_for_camelis_updates ()
  else ()


let awaken = fun f ->
  wait_for_camelis_updates ();
  ignore (f ())


let save_context = fun () ->
  match !context_name with
  | None -> raise No_context
  | Some name ->
    awaken (fun () -> Context.save $ !data_dir // name);
    last_update := rfc3339_now ();
    logline !logfile ("Context " ^ name ^ " saved.")



let nb_objects = fun () ->
  Ext.cardinal (Context.extent Query.top)

let descr_of_oid = fun oid ->
  let preview = match Context.origin_obj oid with
    | Context.Object.Internal (pre, args) -> pre
    | Context.Object.External (s, r) -> Context.Src.preview s r
  in
  match preview with
    | Source.String d -> ["name", xml_string d]
    | Source.Picture (f, d) -> ["picture", xml_string f; "name", xml_string d]

let string_of_feat f = Syntax.stringizer (Context.print f)

let string_of_query q = Syntax.stringizer (Context.print_query q)

let parse_of_string p s =
  let (__strm: _ Stream.t) = Syntax.from_string s in
  let x = try p __strm
    with
    | Stream.Failure ->
      raise (Stream.Error "Syntax error: unexpected beginning") in
  let _ = try Stream.empty __strm
    with
    | Stream.Failure -> raise (Stream.Error "Syntax error: unexpected end")
  in x

let feat_of_string = parse_of_string Context.parse

let query_of_string = parse_of_string Context.parse_query

let update_of_string = parse_of_string Context.parse_update

(* Given a working query 'wq', return the list of increments (with their
   cardinal), the local objects and the extent of feature 'feat' *)
let ls' = fun wq feat ->
  let links_options = [] in
  let view = Context.links wq links_options feat in
  let incrs = List.fold_left
    ~f:(fun res propr ->
      let logs, supp = propr.Context.propr_logs, propr.Context.propr_supp in
      List.fold_left
        ~f:(fun res' log -> (log, supp) :: res')
        ~init:res
        logs
    )
    ~init:[]
    view.Context.proprs in
  let sort_by_count (feat1, card1) (feat2, card2) =
    Common.compare_pair (compare, Query.compare) (card2, feat1) (card1, feat2) in
  let cmp = sort_by_count in
  let incrs2 = List.sort ~cmp incrs in
  let incrs3 = List.map (fun (log, supp) -> (string_of_query log, supp)) incrs2 in
  (incrs3, view.Context.locals, view.Context.extent)

let ls wq feat = ls' (query_of_string wq) (query_of_string feat)



(* -------------------------------------------------------------- *)
(* conversion to Xml of Camelis objects *)

let xml_of_feat = fun f ->
  Xml.Element ("feature", ["name", xml_string $ string_of_feat f], [])

let xml_of_oid = fun oid ->
  Xml.Element ("object",
               ("oid", string_of_int oid) :: (descr_of_oid oid),
               [])

let xml_of_ext = fun e ->
  Xml.Element ("extent", [],
               List.map ~f:xml_of_oid (Ext.elements e))

let xml_of_incr = fun (i, card) ->
  Xml.Element ("increment", [ "name", xml_string i;
                              "card", string_of_int card
                            ], [])



(* -------------------------------------------------------------- *)
(* Helper functions for building xml answers *)

let xml_ok = fun ?(attrs = []) ?(children = []) tag ->
  let attrs' = ("status", "ok") :: ("lastUpdate", !last_update) :: attrs in
  Xml.Element (tag, attrs', children)


let xml_error = fun tag xml_msg ->
  Xml.Element (tag,
               ["status", "error"],
               xml_msg)



(* -------------------------------------------------------------- *)
(* Compute and send result to HTTP queries *)

let ping_service = fun tag cgi ->
  let attrs = [ "creator", !creator;
                "activationDate", !activation_date;
                "serviceId", !id;
                "contextLoaded", string_of_bool $ is_some !context_name;
                "nbObject", string_of_int $ nb_objects ();
                "pid", string_of_int $ Unix.getpid ();
              ] in
  xml_ok tag ~attrs


let ping_users_service = fun tag (cgi:Netcgi.cgi_activation) ->
  let xml_users = List.map xml_of_user (user_list ()) in
  xml_ok tag ~attrs:["creator", !creator] ~children:xml_users


let set_role_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let admin_key = cgi # argument_value "adminKey" in
  let user_key = cgi # argument_value "userKey" in
  let new_role_str = cgi # argument_value "role" in
  let _ = assert_rights admin_key Admin in
  let new_role = role_of_string new_role_str in
  let _ = update_user_role user_key new_role in
  xml_ok tag ~children: [xml_of_key user_key]


let register_key_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let admin_key = cgi # argument_value "adminKey" in
  let user_key = cgi # argument_value "userKey" in
  let timeout = if cgi # argument_exists "timeout" then
      float_of_string (cgi # argument_value "timeout")
    else
      default_key_timeout in
  let _ = assert_new_key user_key in
  let new_role_str = cgi # argument_value "role" in
  let _ = assert_rights admin_key Admin in
  let new_role = role_of_string new_role_str in
  let _ = add_user user_key new_role timeout in
  xml_ok tag ~children: [xml_of_key user_key]


let delkey_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let admin_key = cgi # argument_value "adminKey" in
  let user_key = cgi # argument_value "userKey" in
  let _ = assert_rights admin_key Admin in
  let _ = remove_user user_key in
  xml_ok tag


let reset_creator_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let creator_mail = cgi # argument_value "email" in
  let newkey = cgi # argument_value "newKey" in
  if creator_mail = !creator then
    let _ = add_user newkey Admin default_key_timeout in
    xml_ok tag
  else
    raise (Wrong_creator creator_mail)


(* -------------------------------------------------------------- *)
(* Admin commands *)

let reset_context_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let _ = Context.init ~log:false (string_of_option !context_name) in
  xml_ok tag

let import_context_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let ctxfile = cgi # argument_value "ctxfile" in
  let _ = assert (Filename.check_suffix ctxfile ".ctx") in
  let _ = assert_file_exists $ !data_dir // ctxfile in
  let ctxname = Filename.chop_extension ctxfile in
  let basename = !data_dir // ctxname in
  (* import in current context *)
  let options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells; `Rules; `Actions] in
  let _ = awaken (fun () -> Context.import options basename) in
  (* if this is a new context, give it a name *)
  let _ = if !context_name == None then context_name := Some ctxname in
  (* save as a LIS file *)
  let _ = save_context () in
  (* cheerful answer to send *)
  xml_ok tag ~attrs:["contextName", string_of_option !context_name;
                     "ctxfile", ctxfile]

let import_lis_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let lisfile = cgi # argument_value "lisfile" in
  let _ = assert (Filename.check_suffix lisfile ".lis") in
  let _ = assert_file_exists $ !data_dir // lisfile in
  (* update context name, then load from lisfile *)
  let ctxname = Filename.chop_extension lisfile in
  let _ = context_name := Some ctxname in
  let _ = Context.load (!data_dir // ctxname) in
  (* done! *)
  xml_ok tag ~attrs:["contextName", string_of_option !context_name;
                     "lisfile", lisfile]

let import_file_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let basename = cgi # argument_value "file" in
  let recursive = if cgi # argument_value "recursive" = "true" then [`Recursive] else [] in
  let parts = if cgi # argument_value "parts" = "true" then [`Parts] else [] in
  let file = !data_dir // basename in
  let _ = assert_file_exists file in
  let options = recursive @ parts in
  (* TODO: allow use of url instead of Filename. *)
  let _ = Context.init_source file options Context.Object.Descr.empty in
  xml_ok tag


(* -------------------------------------------------------------- *)
(* Collaborator commands *)

let export_lis_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let lisfile = (!data_dir) // (string_of_option !context_name) ^ ".lis" in
  let data = Netencoding.Base64.encode (string_of_file lisfile) in
  xml_ok tag ~attrs:["data", data]


let export_ctx_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let is_partial = (cgi # argument_exists "query") in
  let q = if is_partial
    then query_of_string (cgi # argument_value "query")
    else Query.top in
  let ctxfile = Filename.temp_file (string_of_option !context_name) "-export.ctx" in
  let dbname = Filename.chop_extension ctxfile in
  let all_options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells;
                     `Rules; `Actions] in
  let _ = Context.export q all_options dbname in
  let data = Netencoding.Base64.encode (string_of_file ctxfile) in
  let _ = Sys.remove ctxfile in
  xml_ok tag ~attrs:["data", data]


let del_objects_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let ext_oids = Ext.union_r (List.map Ext.singleton oids) in
  let del_oid oid = try Context.update_obj oid Context.Rem with _ -> () in
  let del_source = fun url () -> Context.final_source url in
  let _ = awaken (fun () -> Ext.iter del_oid ext_oids) in
  let _ = awaken (fun () -> Context.fold_sources_under del_source () ext_oids) in
  let _ = wait_for_camelis_updates () in
  let xml_extent = [xml_of_ext (Context.extent Query.top)] in
  xml_ok tag ~children:xml_extent


let del_feature_from_req_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = cgi # argument_value "query" in
  let features = List.map (fun x -> "not " ^ x # value) (cgi # multiple_argument "feature") in
  let req_for_oids = cgi # argument_value "reqForOids" in
  let get_feat_tree = cgi # argument_value "tree" in
  (* Retrieve objects and update their descriptions *)
  let _incrs, _locals, ext = ls req_for_oids "all" in
  let u = update_of_string (String.concat " and " features) in
  let _ = Ext.iter (fun oid -> Context.update_obj oid (Context.Extr u)) ext in
  (* Show feature tree if asked for *)
  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  (* send result *)
  xml_ok tag ~children:xml_incrs


let del_feature_from_oid_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = cgi # argument_value "query" in
  let features = List.map (fun x -> "not " ^ x # value) (cgi # multiple_argument "feature") in
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let get_feat_tree = cgi # argument_value "tree" in
  let u = update_of_string (String.concat " and " features) in
  let _ = List.iter (fun oid -> Context.update_obj oid (Context.Extr u)) oids in
  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  xml_ok tag ~children:xml_incrs


let del_feature_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = cgi # argument_value "query" in
  let features = List.map (fun x -> x # value) (cgi # multiple_argument "feature") in
  let delete_feat f = Context.del_feature (feat_of_string f) in
  let _ = List.iter delete_feat features in
  let incrs, _locals, ext = ls wq "all" in
  let xml_incrs = Xml.Element ("increments", [], List.map xml_of_incr incrs) in
  xml_ok tag ~children:[xml_incrs]




(* -------------------------------------------------------------- *)
(* Publisher commands *)
let set_features_from_req_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = cgi # argument_value "query" in
  let features = List.map (fun x -> x # value) (cgi # multiple_argument "feature") in
  let req_for_oids = cgi # argument_value "reqForOids" in
  let get_feat_tree = cgi # argument_value "tree" in
  (* Retrieve objects and update their descriptions *)
  let _incrs, _locals, ext = ls req_for_oids "all" in
  let u = update_of_string (String.concat " and " features) in
  let _ = Ext.iter (fun oid -> Context.update_obj oid (Context.Extr u)) ext in
  (* Show feature tree if asked for *)
  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  (* send result *)
  xml_ok tag ~children:xml_incrs


let set_features_from_oids_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let wq = cgi # argument_value "query" in
  let features = List.map (fun x -> x # value) (cgi # multiple_argument "feature") in
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let get_feat_tree = cgi # argument_value "tree" in

  let u = update_of_string (String.concat " and " features) in
  let _ = List.iter (fun oid -> Context.update_obj oid (Context.Extr u)) oids in

  let xml_incrs = if get_feat_tree = "true" then
      let incrs, _locals, _ext = ls wq "all" in
      [Xml.Element ("increments", [], List.map xml_of_incr incrs)]
    else
      [] in
  xml_ok tag ~children:xml_incrs



let create_object_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let object_name = cgi # argument_value "name" in
  let features = List.map (fun x -> x # value) (cgi # multiple_argument "feature") in
  let qdescr = query_of_string (String.concat " and " features) in
  let pre = if not (cgi # argument_exists "picture")
    then Source.String object_name
    else let picture = !data_dir // (cgi # argument_value "picture") in
         Source.Picture (picture, object_name) in
  let args = [] in
  let d = Context.Object.Descr.of_query qdescr in
  let u = update_of_string "all" in
  let _ = Context.make_obj pre args d u in
  let _ = wait_for_camelis_updates () in
  let xml_extent = xml_of_ext (Context.extent qdescr) in
  xml_ok tag ~children:[xml_extent]


let add_features_service = todo_service


let add_axiom_service = fun tag (cgi:Netcgi.cgi_activation) ->
  let premise = feat_of_string (cgi # argument_value "premise") in
  let concl_str = String.capitalize (cgi # argument_value "conclusion") in
  let conclusion = feat_of_string concl_str  in
  let _ = Context.add_axiom premise conclusion in
  let _ = awaken (fun () -> Context.add_feature conclusion) in
  let _ = wait_for_camelis_updates () in
  let get_extent = cgi # argument_value "extent" in
  let _incrs, _locals, ext = ls concl_str "all" in
  let xml_extent = if get_extent = "true" then [xml_of_ext ext] else [] in
  xml_ok tag ~children:xml_extent




(* -------------------------------------------------------------- *)
(* Reader commands *)

let load_service = fun tag (cgi: Netcgi.cgi_activation) ->
  (* derive expected filename from serviceId *)
  let parsed_id = List.rev (Str.split (Str.regexp ":") !id) in
  let basename = List.hd parsed_id in
  let path_part = (List.nth parsed_id 1) // basename in
  let lisfile = !data_dir // path_part // (basename ^ ".lis") in
  let ctxfile = !data_dir // path_part // (basename ^ ".ctx") in
  match Sys.file_exists lisfile, Sys.file_exists ctxfile with
  | true, _ ->
    let _ = context_name := Some basename in
    let _ = Context.load (!data_dir // path_part // basename) in
    xml_ok tag ~attrs:["contextName", basename]
  | false, true ->
    (* Loading from ctx: first reset, then import ctx, then save *)
    let _ = Context.init ~log:false (string_of_option !context_name) in
    let options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells; `Rules; `Actions] in
    let _ = awaken (fun () -> Context.import options $ !data_dir // path_part // basename) in
    let _ = context_name := Some basename in
    let _ = save_context () in
    xml_ok tag ~attrs:["contextName", basename]
  | false, false ->
    raise $ No_such_file (basename ^ ".lis or " ^ basename ^ ".ctx")

let intent_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let get_extent = cgi # argument_value "extent" in
  let my_extent = Ext.union_r (List.map Ext.singleton oids) in
  (* compute intent of given oids *)
  let my_intent = Context.intent my_extent in
  let xml_intent = Xml.Element ("intent", [], List.map xml_of_feat my_intent) in
  (* Compute extent of the intent -- this is a Galois connection, remember? *)
  let iquery = String.concat " and " (List.map string_of_feat my_intent) in
  let my_extent' = Context.extent (query_of_string iquery) in
  let xml_extent = if get_extent = "true" then [xml_of_ext my_extent'] else [] in
  xml_ok tag ~children:(xml_intent :: xml_extent)


let ls_features_service = fun tag (cgi : Netcgi.cgi_activation) ->
  let wq = cgi # argument_value "wq" in
  let feature = cgi # argument_value "feature" in
  let get_extent = cgi # argument_value "extent" in
  let incrs, _locals, ext = ls wq feature in
  let xml_incrs = Xml.Element ("increments", [], List.map xml_of_incr incrs) in
  let xml_extent = if get_extent = "true" then [xml_of_ext ext] else [] in
  xml_ok tag ~children:(xml_incrs :: xml_extent)

let get_tree_from_zoom_service = todo_service
let get_features_from_req_service = todo_service


let get_valued_features_for_oid = fun featnames oid ->
  let intent = List.map string_of_feat (Context.intent (Ext.singleton oid)) in
  let filters = List.map (fun s -> Str.regexp ("\\(" ^ s ^ "\\)\\( is \\| = \\| in \\)\\(.+\\)")) featnames in
  let rec split_feat = fun filters feat -> match filters with
    | [] -> None
    | x :: xs when Str.string_match x feat 0 ->
      Some (Str.matched_group 1 feat, Str.matched_group 3 feat)
    | x :: xs -> split_feat xs feat in
  let split_feats = List.map (split_feat filters) intent in
  let xml_of_featval = fun (name, value) ->
    let value' = if value.[0] = '"'
      then String.sub value 1 ((String.length value) - 2)
      else value in
    Xml.Element ("property", ["name", name; "value", value'], []) in
  let rec to_xml = fun l -> match l with
    | [] -> []
    | None :: xs -> to_xml xs
    | Some x :: xs -> (xml_of_featval x) :: (to_xml xs) in
  Xml.Element ("object", ["oid", (string_of_int oid)], (to_xml split_feats))


let get_valued_features_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let _ = assert_params ["oid"] in         (* TODO: change to handle oid | req *)
  let oids = List.map (fun x -> int_of_string x#value) (cgi # multiple_argument "oid") in
  let featnames = List.map (fun x -> x # value) (cgi # multiple_argument "featname") in
  let featvalues = List.map (get_valued_features_for_oid featnames) oids in
  xml_ok tag ~children:featvalues



let extent_service = fun tag (cgi: Netcgi.cgi_activation) ->
  let query = cgi # argument_value "query" in
  let _incrs, _locals, ext = ls query "all" in
  xml_ok tag ~children:[xml_of_ext ext]




(* -------------------------------------------------------------- *)
(* Dispatch services to URLs *)

(* list of services; each is a 5-uplet:
 *   URL,
 *   handler function,
 *   minimum role,
 *   list of required params,
 *   UpdateContext | UpdateUsers | NoChange
 *)
(* Note that presence of a "userKey" parameter is automatically checked
   if role <> No_right *)

type effect = UpdateContext | UpdateUsers | NoChange

let services = [ "/ping", ping_service, No_right, [], NoChange;
                 "/pingUsers", ping_users_service, Admin, [], NoChange;
                 "/setRole", set_role_service, No_right, ["adminKey"; "userKey"; "role"], UpdateUsers; (* TODO: change *key param names *)
                 "/registerKey", register_key_service, No_right, ["adminKey"; "userKey"], UpdateUsers; (* TODO: change *key param names *)
                 "/delKey", delkey_service, No_right, ["adminKey"; "userKey"], UpdateUsers; (* TODO: change *key param names *)
                 "/resetCreator", reset_creator_service, No_right, ["email"; "newKey"], UpdateUsers;
                 (* Admin commands *)
                 "/resetCamelis", reset_context_service, Admin, [], UpdateContext;
                 "/importCtx", import_context_service, Admin, ["ctxfile"], UpdateContext;
                 "/importLis", import_lis_service, Admin, ["lisfile"], UpdateContext;
                 "/importFile", import_file_service, Admin, ["file"], UpdateContext;
                 (* Collaborator commands *)
                 "/exportLis", export_lis_service, Collaborator, [], NoChange;
                 "/exportCtx", export_ctx_service, Collaborator, [], NoChange;
                 "/delObjects", del_objects_service, Collaborator, ["oid"], UpdateContext;
                 "/delFeatureFromReq", del_feature_from_req_service, Collaborator, ["query"; "feature"; "reqForOids"], UpdateContext;
                 "/delFeatureFromOid", del_feature_from_oid_service, Collaborator, ["query"; "feature"; "oid"], UpdateContext;
                 "/delFeature", del_feature_service, Collaborator, ["query"; "feature"], UpdateContext;
                 (* Publisher commands *)
                 "/setFeaturesFromReq", set_features_from_req_service, Publisher, ["query"; "feature"; "reqForOids"], UpdateContext;
                 "/setFeaturesFromOids", set_features_from_oids_service, Publisher, ["query"; "feature"; "oid"], UpdateContext;
                 "/createObject", create_object_service, Publisher, ["name"; "feature"], UpdateContext;
                 "/addFeatures", add_features_service, Publisher, [], UpdateContext;                   (* TODO *)
                 "/addAxiom", add_axiom_service, Publisher, ["premise"; "conclusion"], UpdateContext;
                 (* Reader commands *)
                 "/load", load_service, Reader, [], NoChange;
                 "/intent", intent_service, Reader, ["oid"], NoChange;
                 "/zoom", ls_features_service, Reader, ["wq"; "feature"], NoChange;
                 "/getTreeFromZoom", get_tree_from_zoom_service, Reader, [], NoChange; (* TODO *)
                 "/getFeaturesFromReq", get_features_from_req_service, Reader, [], NoChange; (* TODO *)
                 "/getValuedFeatures", get_valued_features_service, Reader, ["userKey"; "featname"], NoChange;
                 "/extent", extent_service, Reader, ["query"], NoChange;
               ]

(* Wrapper around service handlers *)
(* It checks for required params. It checks for access rights. It also
   catches exception fired by the handler, and send an xml
   representation of the error. *)
(* WARNING: the parameter for checking access rights is hardcoded to "userKey" *)
(* Also checks if the context has to be saved on disk *)
let wrap_handler = fun url handler role params update_kind env (cgi: Netcgi.cgi_activation) ->
  (* remove leading "/" from url to get tagname *)
  let tag = (Str.string_after url 1) ^ "Response" in
  try
    if role <> No_right then begin
      assert_params ("userKey"::params) cgi;
      (* assert_param "userKey" cgi; *)
      assert_rights (cgi # argument_value "userKey") role
    end
    else
      assert_params params cgi;
    let _ = logline !logfile (cgi # url ~with_query_string:`Env ()) in
    let _ = update_user_expiration (cgi # argument_value "userKey") in
    let xml = handler tag cgi in
    let _ = logline !logfile (url ^ " compute OK.") in
    let _ = if update_kind = UpdateContext then begin
      last_update := rfc3339_now ();
      save_context ();
    end in
    let _ = send_xml env cgi xml in
    logline !logfile (url ^ " all done.")
  with
  | e ->
    let xml = xml_error tag (xml_of_exn e) in
    send_xml env cgi xml


let srv =
  let make_service = fun (url, f, role, params, update_kind) ->
    url,
    dynamic_service { dyn_handler = wrap_handler url f role params update_kind;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = Some "/";
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  uri_distributor $ List.map make_service services



(* -------------------------------------------------------------- *)
(* Boilerplate code to create and launch web server *)

let serve_connection ues fd =
  let config =
    new Nethttpd_engine.modify_http_engine_config
      ~config_input_flow_control:true
      ~config_output_flow_control:true
      Nethttpd_engine.default_http_engine_config in
  let pconfig =
    new Nethttpd_engine.buffering_engine_processing_config in
  Unix.set_nonblock fd;
  ignore(Nethttpd_engine.process_connection config pconfig fd ues srv)


let rec accept ues srv_sock_acc =
  let acc_engine = srv_sock_acc # accept() in
  Uq_engines.when_state ~is_done:(fun (fd,fd_spec) ->
    if srv_sock_acc # multiple_connections then (
      serve_connection ues fd;
      accept ues srv_sock_acc
    ) else
      srv_sock_acc # shut_down())
    ~is_error:(fun _ -> srv_sock_acc # shut_down())
    acc_engine


let start port =
  let ues = Unixqueue.create_unix_event_system () in
  let opts =
    { Uq_engines.default_listen_options with
      Uq_engines.lstn_reuseaddr = true } in
  let lstn_engine =
    Uq_engines.listener
      (`Socket(`Sock_inet(Unix.SOCK_STREAM, Unix.inet_addr_any, port) ,opts)) ues in
  Uq_engines.when_state ~is_done:(accept ues) lstn_engine;

  Printf.printf "Listening on port %d\n" port;
  flush stdout;
  Unixqueue.run ues



(* -------------------------------------------------------------- *)
(* Testing defaults *)

let set_test_defaults = fun () ->
  (* import /tmp/planets.ctx *)
  let options = [`Axioms; `Features; `Sources; `Extrinsic; `Wells; `Rules; `Actions] in
  let _ = awaken (fun () -> Context.import options "/tmp/planets") in
  let _ = context_name := Some "planets" in
  (* save as a LIS file *)
  let _ = save_context () in
  (* add a few user keys *)
  List.iter (fun (x,y,z) -> add_user x y z) [ "abc123ef45", Admin, default_key_timeout;
                                              "987fed65cb", Reader, default_key_timeout ]



(* -------------------------------------------------------------- *)
(* Parse command-line arguments and launches server *)

let usage = String.concat " "
  [ "Usage:"; Sys.argv.(0);
    "-port portnum";
    "-creator email";
    "-key adminKey";
    "-serviceId string";
    "[-log filename]";
    "[-despot]";
    "[-test]";
    "\n"
  ]

let speclist =
  let add_creator = fun k -> add_user k Admin default_key_timeout in
  Arg.align
    [ ("-port",      Arg.Set_int port,           " port number the server will listen");
      ("-creator",   Arg.Set_string creator,     " email of the sevice creator (that's YOU)");
      ("-key",       Arg.String add_creator,     " key of the admin who owns the service");
      ("-serviceId", Arg.Set_string id,          " service identification string (e.g. \"http://localhost:8060::planet:planet\")");
      ("-datadir",   Arg.Set_string data_dir,    " path to the directory holding context data on server (defaults to " ^ !data_dir ^ ")");
      ("-log",       Arg.Set_string logfile,     " path to the log file (defaults to " ^ !logfile ^ ")");
      ("-despot",    Arg.Set ignore_rights,      " ignore users right (use for debugging purposes)");
      ("-test",      Arg.Unit set_test_defaults, " set some internal values to defaults for testing");
    ]

let anon_fun = fun x -> raise $ Arg.Bad ("Bad argument : " ^ x)

(* TODO: check id is well formed? *)
let check_args = fun () ->
  let warn cond msg = if cond then Printf.printf "[Warning] %s\n" msg else () in
  let check cond msg = if cond then raise $ Arg.Bad msg else () in
  let in_argv str = array_mem str Sys.argv in
  let warnings = [ not (in_argv "-datadir"), "datadir not specified, defaulting to " ^ !data_dir;
                   not (in_argv "-log"), "log not specified, defaulting to " ^ !logfile;
                 ] in
  let checks = [ not (in_argv "-port"), "Unspecified port number";
                 not (valid_email !creator), "Malformed creator email address";
                 not (in_argv "-serviceId"), "Unspecified service id";
                 not (in_argv "-key"), "Unspecified key";
               ] in
  List.iter (uncurry check) checks;
  List.iter (uncurry warn) warnings

let main = fun () ->
  try
    let _ = Arg.parse speclist anon_fun usage in
    check_args ();
    Netsys_signal.init ();
    start !port
  with Arg.Bad msg ->
    print_endline $ "Error: " ^ msg;
    print_newline ();
    Arg.usage speclist usage

let _ = main ()
