(* -------------------------------------------------------------- *)
(* Access rights, users/key management *)

open Utils


type user_role = | No_right             (* cannot do anything *)
                 | Reader               (* can read context *)
                 | Collaborator         (* can add but not delete *)
                 | Publisher            (* can edit his context(s) *)
                 | Admin                (* can edit context and users *)
                 | General_admin        (* can do everything *)

type userkey = string                   (* hexadecimal number as a string *)

type date = float

type user = userkey * (user_role * date)


exception No_such_role of string
exception Not_enough_rights
exception No_such_key of string
exception Expired_key of string
exception Registered_key of string


let default_key_timeout = 3. *. 60. *. 60. (* 3 hours, in seconds *)

let users:user list ref = ref []


let string_of_role r = match r with
  | General_admin -> "generalAdmin"
  | Admin -> "admin"
  | Publisher -> "publisher"
  | Collaborator -> "collaborator"
  | Reader -> "reader"
  | No_right -> "none"


let role_of_string s = match (String.lowercase s) with
  | "generaladmin" -> General_admin
  | "admin" -> Admin
  | "publisher" -> Publisher
  | "collaborator" -> Collaborator
  | "reader" -> Reader
  | "none"
  | "" -> No_right
  | _ -> raise (No_such_role s)


(* -------------------------------------------------------------- *)
(* Core accessors *)
let user_list = fun () -> !users


let get_user = fun key ->
  try
    List.assoc key (user_list ())
  with
    Not_found -> raise $ No_such_key key


let update_user_role = fun key new_role ->
  try
    let _, expire_date = List.assoc key (user_list ()) in
    let new_user = key, (new_role, expire_date) in
    users := new_user :: (List.remove_assoc key !users)
  with
    Not_found -> raise $ No_such_key key


let update_user_expiration = fun key ->
  try
    let role, _old_expire = List.assoc key (user_list ()) in
    let new_user = key, (role,  now () +. default_key_timeout) in
    users := new_user :: (List.remove_assoc key !users)
  with
    (* when key is not found, just do nothing *)
    (* this happens with eg. the ping command *)
    Not_found -> ()


let add_user = fun key role timeout ->
  let open Netstring_str in
  if (string_match (regexp "[a-fA-F0-9]+$") key 0) == None then
    failwith "Malformed key"
  else if List.mem_assoc key !users then
    ()
  else
    let expire_date = now () +. timeout in
    users := (key, (role, expire_date)) :: !users


let remove_user = fun key ->
  users := List.remove_assoc key !users


(* -------------------------------------------------------------- *)
(* Other functions, should not touch !users *)

let assert_rights = fun key role ->
  try
    let user_role, expire_date = List.assoc key (user_list ()) in
    let _ = if now () > expire_date then raise (Expired_key key) in
    match compare user_role role with
    | -1 -> raise Not_enough_rights
    |_ -> ()
  with
  | Not_found -> raise $ No_such_key key
  | Expired_key _ as e -> remove_user key; raise e


let assert_new_key = fun key ->
  if List.mem_assoc key !users then raise (Registered_key key)

let xml_of_user = fun user ->
  let key, (role, expire_date) = user in
  let expire_date_str = Netdate.mk_internet_date expire_date in
  Xml.Element ("user", ["key", key;
                        "role", (string_of_role role);
                        "expires", expire_date_str], [])

let xml_of_key = fun key ->
  let role, expire_date = get_user key in
  let expire_date_str = Netdate.mk_internet_date expire_date in
  Xml.Element ("user", ["key", key;
                        "role", (string_of_role role);
                        "expires", expire_date_str], [])
