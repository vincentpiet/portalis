package fr.irisa.lis.portalis.admin.db;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.Application;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.UserCore;




public interface DataBase {

	public UserCore[] getUsers();
	public Application[] getApplications();
	public Site getPortalisSite();
	public void init();
	public void save() throws DataBaseException;
	
	public ServiceCoreInterface getService(String name) throws PortalisException;
	public LoginResult getUserInfo(String ident, String password)
			throws DataBaseException;

}