package fr.irisa.lis.portalis.admin.db;

@SuppressWarnings("serial")
public class DataBaseException extends Exception implements
		java.io.Serializable {

	public DataBaseException(String mess) {
		super(mess);
	}

	public DataBaseException() {
	}

	public DataBaseException(String mess, Throwable e) {
		super(mess, e);
	}
}
