package fr.irisa.lis.portalis.admin.db;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.Application;
import fr.irisa.lis.portalis.shared.admin.data.LoginErr;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.RightProperty;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.UserCore;

public class DataBaseSimpleImpl implements DataBase {
	
	private static final Logger LOGGER = Logger.getLogger(DataBaseSimpleImpl.class.getName());

	private final UserCore[] testUsers = new UserCore[RightValue.values().length];

	private static final Map<String, UserCore> users = new HashMap<String, UserCore>();
	private static final Map<String, Application> applications = new HashMap<String, Application>();
	private static Site portalisSite = new Site(applications);
;

	@Override
	public UserCore[] getUsers() {
		Collection<UserCore> values = users.values();
		return values.toArray(new UserCore[values.size()]);
	}

	@Override
	public LoginResult getUserInfo(String ident, String password)
			throws DataBaseException {
		UserCore userCore = users.get(ident);
		LoginErr err = null;
		if (userCore == null) {
			err = LoginErr.unknown;
		} else if (userCore.getPassword().equals(password)) {
			err = LoginErr.ok;
		} else {
			err = LoginErr.incorrectPassword;
		}
		return new LoginResult(userCore, err);
	}

	@Override
	public Application[] getApplications() {
		Collection<Application> values = applications.values();
		return values.toArray(new Application[values.size()]);
	}

	@Override
	public ServiceCoreInterface getService(String fullName) throws PortalisException {
		String appliName = ServiceCore.extractAppliName(fullName);
		String serviceName = ServiceCore.extractServiceName(fullName);
		ServiceCoreInterface reponse = null;
		for (Application appli : applications.values()) {
			if (appli.getId().equals(appliName)) {
				for (ServiceCore service : appli.getServices()) {
					if (service.getServiceName().equals(serviceName)) {
						reponse = service;
						break;
					}
				}
				break;
			}
		}
		return reponse;
	}

	@Override
	public Site getPortalisSite() {
		return portalisSite;
	}

	@Override
	public void init() {
		
		// ****************************** users *********************************
		users.put(UserCore.ANONYMOUS_USER.getEmail(), UserCore.ANONYMOUS_USER);
		users.put(UserCore.ANONYMOUS_ADMIN.getEmail(), UserCore.ANONYMOUS_ADMIN);

		// ================ bekkers =================
		UserCore bekkers = new UserCore("bekkers@irisa.fr", "bekkers", "yves",
				true);
		users.put(bekkers.getEmail(), bekkers);

		// ================ sigonneau =================
		UserCore sigonneau = new UserCore("benjamin@dromaludaire.info",
				"sigonneau", "benjamin", false);
		users.put(sigonneau.getEmail(), sigonneau);
		
		// ================ piet =================
		UserCore piet = new UserCore("vincent.piet@irisa.fr", "piet",
				"vincent", false);
		users.put(piet.getEmail(), piet);
		

		// ****************************** applications *********************************
		// ================ planets =================
		Application planets = new Application("planets");
		applications.put(planets.getId(), planets);
		ServiceCore planets_planets = new ServiceCore(planets.getId(),
				"planets");
		planets.add(planets_planets);

		ServiceCore planets_planetPhoto = new ServiceCore(planets.getId(),
				"planetsPhoto");
		planets.add(planets_planetPhoto);

		// ================ formalConcepts =================
		Application formalConcepts = new Application("formalConcepts");
		applications.put(formalConcepts.getId(), formalConcepts);
		ServiceCore formalConcepts_colors = new ServiceCore(
				formalConcepts.getId(), "colors");
		formalConcepts.add(formalConcepts_colors);
		ServiceCore formalConcepts_number = new ServiceCore(
				formalConcepts.getId(), "number");
		formalConcepts.add(formalConcepts_number);

		// ================ illustres =================
		Application illustres = new Application("illustres");
		applications.put(illustres.getId(), illustres);
		ServiceCore illustres_bd = new ServiceCore(illustres.getId(), "bd");
		illustres.add(illustres_bd);

		ServiceCore illustres_comics = new ServiceCore(illustres.getId(),
				"comics");
		illustres.add(illustres_comics);

		// ================ handicap =================
		Application handicap = new Application("handicap");
		applications.put(handicap.getId(), handicap);
		ServiceCore handicap_handicap = new ServiceCore(handicap.getId(),
				"handicap");
		handicap.add(handicap_handicap);

		// ================ patrimoine =================
		Application patrimoine = new Application("patrimoine");
		applications.put(patrimoine.getId(), patrimoine);
		ServiceCore patrimoineBretagne = new ServiceCore(patrimoine.getId(),
				"patrimoineBretagne");
		patrimoine.add(patrimoineBretagne);

		// ================ thabor =================
		Application thabor = new Application("thabor");
		applications.put(thabor.getId(), thabor);
		ServiceCore thabor_arbres_thabor = new ServiceCore(thabor.getId(),
				"arbres_thabor");
		thabor.add(thabor_arbres_thabor);

		// ****************************** rights *********************************
		setUniformRightValue(UserCore.ANONYMOUS_USER, RightValue.ADMIN);
		setUniformRightValue(sigonneau, RightValue.ADMIN);
		setUniformRightValue(piet, RightValue.ADMIN);

		int i = 0;
		for (RightValue rightValue : RightValue.values()) {
			String email = rightValue.toString() + "@irisa.fr";
			boolean admin = rightValue.equals(RightValue.GENERALADMIN) ? true
					: false;
			testUsers[i] = new UserCore(email, rightValue.toString(),
					rightValue.toString(), admin);
			users.put(email, testUsers[i]);
			Set<RightProperty> rights = new HashSet<RightProperty>();
			if (!rightValue.equals(RightValue.GENERALADMIN)) {
				for (Application application : applications.values()) {
					for (ServiceCore service : application.getServices()) {
						rights.add(new RightProperty(email, service
								.getFullName(), rightValue));
					}
				}
				testUsers[i].setRights(rights);
			}
			i++;
		}

		dumpDataBaseInfo();
	}

	private void setUniformRightValue(UserCore userCore, RightValue rightValue) {
		Set<RightProperty> rights = new HashSet<RightProperty>();
		for (Application application : applications.values()) {
			for (ServiceCore service : application.getServices()) {
				rights.add(new RightProperty(userCore.getEmail(), service
						.getFullName(), rightValue));
			}
		}
		userCore.setRights(rights);
	}

	public static void dumpDataBaseInfo() {
		for (String key : users.keySet()) {
			LOGGER.config("db user : "+users.get(key).getEmail());
		}
		for (Application application : applications.values()) {
			for (ServiceCore service : application.getServices()) {
				LOGGER.config("db service : "+service.getFullName());
			}
		}
	}

	@Override
	public void save() throws DataBaseException {
		try {
			XmlUtil.writeFile(XmlUtil.parseXMLString(portalisSite.toString())
					.getOwnerDocument(), "/srv/webapps/site.xml");
		} catch (Exception e) {
			throw new DataBaseException(e.getMessage());
		}

	}

}
