package fr.irisa.lis.portalis.admin.bd;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.portalis.admin.db.DataBaseException;
import fr.irisa.lis.portalis.admin.db.DataBaseSimpleImpl;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;

public class TestBd {

	static final Logger LOGGER = Logger.getLogger(TestBd.class.getName());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @BeforeClass : TestBd       |"
				+ "\n            ---------------------------------------------------\n");
		PortalisService.getInstance().init("localhost", 8080, "portalis");
	}
	@Test
	public void testAdminPropertiesFilesArePresent() {
			LOGGER.info("--------------> @Test : testAdminPropertiesFilesArePresent\n");
		try {
			assertTrue(AdminProprietes.test());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void test() {
		DataBaseSimpleImpl bd = new DataBaseSimpleImpl();
		bd.init();
		try {
			bd.save();
		} catch (DataBaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
