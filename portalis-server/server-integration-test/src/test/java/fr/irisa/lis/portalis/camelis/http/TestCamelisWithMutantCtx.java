package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.AddAxiomReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelFeatureReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelObjectsReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCamelisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.XAddAxiomReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisWithMutantCtx {

	private static final Logger LOGGER = Logger.getLogger(TestCamelisWithCtx.class.getName());
	private static Session camelisSession;

	private static void installFreshCtx() throws Exception {
		// ----- copy fresh ctx file
		// TODO: remove hardcoded path and filename
		LOGGER.fine("Copying fresh planets.ctx to the planets:planets service");
		File oldLis = new File(CoreTestConstants.WEBAPPS_DIR + "/planets/planets/planets.lis");
		if (oldLis.exists()) {
			boolean success = oldLis.delete();
			if (!success) throw new Exception("Deleting .lis file in TestCamelisWithMutantCtx @Before failed.");
		}
		UtilTest.installTestResource(
			"planets-test.ctx",
			CoreTestConstants.WEBAPPS_DIR + "/planets/planets/planets.ctx");
	}

	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = "localhost";
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisWithMutantCtx ************");

			if (CargoLauncher.tomcatIsActif()) {
				CargoLauncher.stop();
			}
			CargoLauncher.start();

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> Login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email, password);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession, camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS, null, null);
			assertTrue("Le démarrage de Camelis n'a pas marché", startReponse!=null);
			ActiveLisService activeLisService = startReponse.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis"+startReponse, activeLisService!=null);

			LOGGER.info("\nPortalisCtx ========================================> installFreshCtx <============================");
			// We need to initially import the context here
			// otherwise, the contextLoaded property won't be set
			// and most xml-lis-server operations will fail with a No_context exception
			// thus making the tests fail mmiserably
			installFreshCtx();
			camelisSession = Session.createCamelisSession(loginSession, activeLisService);
			String ctxFile = "planets/planets/planets.ctx";

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			ImportCtxReponse importReponse = CamelisHttp.importCtx(camelisSession, ctxFile);
			assertTrue("Import failed", importReponse != null);
			assertTrue("importCtx problem: " + importReponse, importReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
}


	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		resetTestContext();
		UtilTest.killAllCamelisProcessAndClearUsers();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisWithMutantCtx **********");
	}

	public static void resetTestContext() throws Exception {
		LOGGER.info("\nPortalisCtx ========================================> resetTestContext <============================");
		ResetCamelisReponse resetReponse = CamelisHttp.resetCamelis(camelisSession);
		assertNotNull("Reseting failed", resetReponse != null);
		assertTrue("Problem during resetCamelis: " + resetReponse, resetReponse.isOk());
		
		installFreshCtx();
		
		LOGGER.info("\nPortalisCtx ========================================> importCtx planets/planets/planets.ctx <============================");
		String ctxFile = "planets/planets/planets.ctx";
		ImportCtxReponse importReponse = CamelisHttp.importCtx(camelisSession, ctxFile);
		assertTrue("Ctx import failed", importReponse != null);
		assertTrue("problem during importCtx: " + importReponse, importReponse.isOk());
	}
	
	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
		resetTestContext();
	}

	@After
	public void tearDown() throws Exception {
	}


	
	@Test
	public void testResetCamelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testResetCamelis ***********************************\n");
		try {
			ResetCamelisReponse resetReponse = CamelisHttp.resetCamelis(camelisSession);
			assertNotNull(resetReponse);
			assertTrue("resetCamelis should not return an error: " + resetReponse, resetReponse.isOk());
			
			LOGGER.info("\nPortalisCtx ========================================> check empty extent <============================");
			ExtentReponse extReponse = CamelisHttp.extent(camelisSession, "all");
			LisExtent receivedExt = extReponse.getExtent();
			assertTrue("Extent should be empty: " + extReponse,
						receivedExt != null && receivedExt.card() == 0);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testDelObjects() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testDelObjects ***********************************\n");
		try {
			int[] oids = { 1, 6 };
			DelObjectsReponse reponse = CamelisHttp.delObjects(camelisSession, oids);
			assertNotNull(reponse);
			assertTrue("delObjects should not return an error: " + reponse, reponse.isOk());
			// -------------- check extent -----------------
			LisExtent receivedExt = reponse.getExtent();
			assertNotNull(receivedExt);
			assertTrue("Object with oid == 1 not deleted", !receivedExt.contains(1));
			assertTrue("Object with oid == 6 not deleted", !receivedExt.contains(6));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	
	@Test
	public void testDelFeature() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testDelFeature ***********************************\n");
		try {
			String query = "all";
			String[] features = { "Distance", "Size" };
			DelFeatureReponse reponse = CamelisHttp.delFeature(camelisSession, query, features);
			assertNotNull(reponse);
			assertTrue("delFeature should not return an error: " + reponse, reponse.isOk());
			// -------------- check increments -----------------
			LisIncrementSet receivedIncrs = reponse.getIncrements();
			assertNotNull(receivedIncrs);
			for (String feat: features) {
				assertTrue("Feature " + feat + " not deleted", !receivedIncrs.contains(feat));
			}
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
		
	
	@Test
	public void testAddAxiom() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testAddAxiom ***********************************\n");
		try {
			AddAxiomReponse reponse = CamelisHttp.addAxiom(camelisSession, "satellite", "Misc");
			assertNotNull(reponse);
			assertTrue("addAxiom should not return an error: " + reponse, reponse.isOk());
			ZoomReponse zrep = CamelisHttp.zoom(camelisSession, "all", "all");
			LisIncrementSet incrs = zrep.getIncrements();
			assertTrue("Increment satellite not masked", !incrs.contains("satellite"));
			assertTrue("Increment Misc not listed", incrs.contains("Misc"));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	
	@Test
	public void testAddAxiomX() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testAddAxiomX ***********************************\n");
		try {
			AddAxiomReponse reponse = CamelisHttp.addAxiom(camelisSession, "satellite", "Misc", true);
			XAddAxiomReponse xreponse = (XAddAxiomReponse) reponse;
			assertNotNull(xreponse);
			assertTrue("addAxiom should not return an error: " + xreponse, xreponse.isOk());
			
			// check returned extent
			LisObject[] objs = { new LisObject(1, "earth"),
								 new LisObject(2, "jupiter"),
								 new LisObject(3, "mars"),
								 new LisObject(5, "neptune"),
								 new LisObject(6, "pluto"),
								 new LisObject(7, "saturn"),
					  			 new LisObject(8, "uranus") };
			LisExtent expectedExt = new LisExtent(objs);
			assertTrue("Received extent does not match expected extent",
						expectedExt.equals(xreponse.getExtent()));

			// check that satellite is masked and Misc is listed in increments
			ZoomReponse zrep = CamelisHttp.zoom(camelisSession, "all", "all");
			LisIncrementSet incrs = zrep.getIncrements();
			assertTrue("Increment satellite not masked", !incrs.contains("satellite"));
			assertTrue("Increment Misc not listed", incrs.contains("Misc"));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
}
