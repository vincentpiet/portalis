package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisLauncherHttpRequest1 {private static final Logger LOGGER = Logger.getLogger(TestCamelisLauncherHttpRequest1.class.getName());


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = "localhost";
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		CargoLauncher.start();
		LOGGER.info(""
				+ "\n            ----------------------------------------------------------------"
				+ "\n            |         @BeforeClass : TestCamelisLauncherHttpRequest1       |"
				+ "\n            ----------------------------------------------------------------\n");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		CargoLauncher.stop();
		LOGGER.info(""
				+ "\n            ---------------------------------------------------------------"
				+ "\n            |         @AfterClass : TestCamelisLauncherHttpRequest1        |"
				+ "\n            ----------------------------------------------------------------\n");
	}

	@Before
	public void setUp() throws Exception {
		UtilTest.killAllCamelisProcessAndClearUsers();
		LOGGER.info("--------------> @Before : CargoLauncher.start\n");
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("--------------> @After : CargoLauncher.stop\n");
	}

	@Test
	public void testBootTomcat() {
		LOGGER.info("--------------> @Test : testBootTomcat\n");
		try {
			LoginReponse loginReponse = AdminHttp.login(CoreTestConstants.MAIL_YVES, CoreTestConstants.PASSWORD_YVES);
			assertTrue("La réponse doit être sans erreur, loginReponse ="+loginReponse, loginReponse.isOk());
			Session sess = loginReponse.getSession();
			LOGGER.fine("camelisSession = "+sess.getPortalisSessionId());
			PidReponse pidReponse = AdminHttp.lsof(sess, PortalisService.getInstance().getPort());
			assertTrue("La réponse doit être sans erreur, pidReponse ="+pidReponse, pidReponse.isOk());
			assertTrue("Il doit y avoir un processus sur le port "+
					PortalisService.getInstance().getPort() + "\nPids = " + pidReponse.getPidsAsString()
					, pidReponse.getPids().size()>0);
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testBootTomcatErrNoSession() {
		LOGGER.info("--------------> @Test : testBootTomcatErrNoSession\n");
		try {
			Session sess = Session.createPortalisSession(CoreTestConstants.AU1);
			int port = PortalisService.getInstance().getPort();
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(sess, port);
			boolean free = pidReponse.getPids().size() == 0;
			assertFalse("Tomcat n'a pas été démarré sur le port 8080", !free);

			LOGGER.info("testBootTomcat() aprés startup, isFree8080=" + free);
			assertTrue("La réponse doit être en erreur, pidReponse ="+pidReponse, !pidReponse.isOk());
			
			final String TEMOIN = ErrorMessages.TEMOIN_NOT_A_REGISTERED_PORTALIS_SESSION;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' pidReponse =\n" + pidReponse,
					pidReponse.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelis() {
		LOGGER.info("--------------> @Test : testStartCamelis\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();
			
			// ********************* login *********************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email, password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="+loginReponse, loginReponse.isOk());
			Session sessUser = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", sessUser!=null);


			// ******************* startCamelis ****************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(sessUser,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="+startReponse, startReponse.isOk());
			ActiveLisService camelisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un camelisService", camelisService!=null);
			assertEquals(host, camelisService.getHost());
			
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testLoginHttp() {
		LOGGER.info("--------------> @Test : testLoginHttp sur le port "+CoreTestConstants.PORT1+"\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();
			LoginReponse reponse = AdminHttp.login(email, password);
			assertTrue("La reponse ne doit pas être en erreur : "+reponse, reponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}




}
