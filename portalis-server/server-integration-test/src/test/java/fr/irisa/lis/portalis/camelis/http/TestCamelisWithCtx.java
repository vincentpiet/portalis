package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.data.Property;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.GetValuedFeaturesReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.XIntentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.XZoomReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;



public class TestCamelisWithCtx {private static final Logger LOGGER = Logger.getLogger(TestCamelisWithCtx.class.getName());
	private static Session camelisSession;

	private static void installFreshCtx() throws Exception {
		// ----- copy fresh ctx file
		// TODO: remove hardcoded path and filename
		LOGGER.fine("Copying fresh planets.ctx to the planets:planets service");
		File oldLis = new File(CoreTestConstants.WEBAPPS_DIR + "/planets/planets/planets.lis");
		if (oldLis.exists()) {
			boolean success = oldLis.delete();
			if (!success) throw new Exception("Deleting .lis file in TestCamelisWithMutantCtx @Before failed.");
		}
		UtilTest.installTestResource(
			"planets-test.ctx",
			CoreTestConstants.WEBAPPS_DIR + "/planets/planets/planets.ctx");
	}

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = "localhost";
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisWithCtx ************");

			if (CargoLauncher.tomcatIsActif()) {
				CargoLauncher.stop();
			}
			CargoLauncher.start();

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> Login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email, password);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession, camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS, null, null);
			assertTrue("Le démarrage de Camelis n'a pas marché", startReponse!=null);
			ActiveLisService activeLisService = startReponse.getActiveLisService();
			assertTrue("pas de service en retour du démarage de Camelis"+startReponse, activeLisService!=null);

			LOGGER.info("\nPortalisCtx ========================================> installFreshCtx <============================");
			installFreshCtx();
			camelisSession = Session.createCamelisSession(loginSession, activeLisService);
			String ctxFile = "planets/planets/planets.ctx";
			
			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			ImportCtxReponse importReponse = CamelisHttp.importCtx(camelisSession, ctxFile);
			assertTrue("L'import n'a pas marché", importReponse!=null);
			assertTrue("problème d'importCtx : "+importReponse, importReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
}


	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		UtilTest.killAllCamelisProcessAndClearUsers();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisWithCtx **********");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testExtentReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testExtentReponse ***********************************\n");
		try {
			String lisQuery = "Medium";
			ExtentReponse reponse = CamelisHttp.extent(camelisSession, lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"+ reponse,
						receivedExtent != null && receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			LisObject[] expectedObjs = { new LisObject(5, "neptune"),
					  					 new LisObject(8, "uranus") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n"+receivedExtent+"does not match expected extent\n"+expectedExtent,
						expectedExtent.equals(receivedExtent));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testIntentReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testIntentReponse ***********************************\n");
		try {
			String[] oids = { "1", "2", "5", "8" };
			IntentReponse reponse = CamelisHttp.intent(camelisSession, oids);
			assertNotNull(reponse);
			LisIntent receivedIntent = reponse.getIntent();
			// intent({1,2,5,8}) should contain 3 features: Distance, satellite, Size
			LisIntent expectedIntent = new LisIntent();
			expectedIntent.add(new Property("Distance"));
			expectedIntent.add(new Property("satellite"));
			expectedIntent.add(new Property("Size"));
			assertTrue(String.format("Received intent %s\ndoes not match expected intent %s", receivedIntent, expectedIntent),
						expectedIntent.equals(receivedIntent));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testXIntentReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testXIntentReponse ***********************************\n");
		try {
			String[] oids = { "1", "2", "5", "8" };
			XIntentReponse reponse = (XIntentReponse) CamelisHttp.intent(camelisSession, oids, true);
			assertNotNull(reponse);

			// -------------- check intent -----------------
			LisIntent receivedIntent = reponse.getIntent();
			// intent({1,2,5,8}) should contain 3 features: Distance, satellite, Size
			LisIntent expectedIntent = new LisIntent();
			expectedIntent.add(new Property("Distance"));
			expectedIntent.add(new Property("satellite"));
			expectedIntent.add(new Property("Size"));
			assertTrue("Received intent does not match expected intent",
						expectedIntent.equals(receivedIntent));
			
			// -------------- check extent -----------------
			LisExtent receivedExt = reponse.getExtent();
			assertTrue("There should be 7 objects in the extent: " + reponse,
						receivedExt != null && receivedExt.card() == 7);
			// the extent should be as follow:
			LisObject[] objs = { new LisObject(1, "earth"),
								 new LisObject(2, "jupiter"),
								 new LisObject(3, "mars"),
								 new LisObject(5, "neptune"),
								 new LisObject(6, "pluto"),
								 new LisObject(7, "saturn"),
					  			 new LisObject(8, "uranus") };
			LisExtent expectedExt = new LisExtent(objs);
			assertTrue("Received extent does not match expected extent",
						expectedExt.equals(receivedExt));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testZoomReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testZoomReponse ***********************************\n");
		try {
			String wq = "Medium";
			String feat = "all";
			ZoomReponse reponse = CamelisHttp.zoom(camelisSession, wq, feat);
			assertNotNull(reponse);
			assertTrue("Zoom should not return an error:"+ reponse, reponse.isOk());
			LisIncrementSet receivedIncrs = reponse.getIncrements();
			assertTrue("There should be 4 increments: "+ reponse,
						receivedIncrs != null && receivedIncrs.card() == 4);
			// the increments should be as follow:
			LisIncrement[] incrs = { new LisIncrement("Distance", 2),
					  				 new LisIncrement("satellite", 2),
					  				 new LisIncrement("Size", 2),
					  				 new LisIncrement("Basket", 1) };
			LisIncrementSet expectedIncrs = new LisIncrementSet(incrs);
			assertTrue("Received increments does not match expected increments",
						expectedIncrs.equals(receivedIncrs));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testXZoomReponse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testXZoomReponse ***********************************\n");
		try {
			String wq = "Medium";
			String feat = "all";
			XZoomReponse reponse = (XZoomReponse) CamelisHttp.zoom(camelisSession, wq, feat, true);
			assertNotNull(reponse);
			assertTrue("Zoom should not return an error:"+ reponse, reponse.isOk());
			// -------------- check increments -----------------
			LisIncrementSet receivedIncrs = reponse.getIncrements();
			assertTrue("There should be 4 increments: "+ reponse,
						receivedIncrs != null && receivedIncrs.card() == 4);
			// the increments should be as follow:
			LisIncrement[] incrs = { new LisIncrement("Distance", 2),
					  				 new LisIncrement("satellite", 2),
					  				 new LisIncrement("Size", 2),
					  				 new LisIncrement("Basket", 1) };
			LisIncrementSet expectedIncrs = new LisIncrementSet(incrs);
			assertTrue("Received increments does not match expected increments",
						expectedIncrs.equals(receivedIncrs));
			// -------------- check extent -----------------
			LisExtent receivedExt = reponse.getExtent();
			assertTrue("There should be 2 objects in the extent: " + reponse,
						receivedExt != null && receivedExt.card() == 2);
			// the extent should be as follow:
			LisObject[] objs = { new LisObject(5, "neptune"),
					  			 new LisObject(8, "uranus") };
			LisExtent expectedExt = new LisExtent(objs);
			assertTrue("Received extent does not match expected extent",
						expectedExt.equals(receivedExt));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	
	@Test
	public void testGetValuedFeatures() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetValuedFeatures ***********************************\n");
		try {
			int[] oids = {1, 2};
			String[] feats = {"foo", "bar"};
			GetValuedFeaturesReponse reponse = CamelisHttp.getValuedFeatures(camelisSession, oids, feats);
			assertNotNull(reponse);
			assertTrue("getValuedFeatures should not return an error: " + reponse, reponse.isOk());
			
			// check response is correct
			assertEquals(reponse.getValue(1, "foo"), "toto");
			assertEquals(reponse.getValue(1, "bar"), "42");
			assertEquals(reponse.getValue(2, "foo"), "tata");
			assertEquals(reponse.getValue(2, "bar"), "1618");
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
}
