package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.Set;
import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.data.UserCore;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class UtilTest {private static final Logger LOGGER = Logger.getLogger(UtilTest.class.getName());
	

	public static Session startOneCamelis(Session loginSession)
            throws PortalisException, IOException, InterruptedException {
		StartReponse startReponse = AdminHttp.startCamelis(loginSession,
				CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
		assertTrue("Le retour ne doit pas être en erreur startReponse="
				+ startReponse, startReponse.isOk());

		ActiveLisService activeLisService = startReponse.getActiveLisService();
		int port = activeLisService.getPort();
		Session sess1 = Session.createCamelisSession(loginSession, activeLisService);

		PingReponse pingReponse = CamelisHttp.ping(sess1);
		LOGGER.fine("startOneCamelis() session-in =" + loginSession + "session-out ="
				+ sess1 + "pingReponse =" + pingReponse);
		assertTrue("Le retour ne doit pas être en erreur pingReponse="
				+ pingReponse, pingReponse.isOk());
		assertEquals(activeLisService, pingReponse.getFirstActiveLisService());
		assertEquals(port, pingReponse.getFirstActiveLisService().getPort());
		return sess1;
	}
	
	public static Session startOneCamelis(Session loginSession, String serviceFullName)
            throws PortalisException, IOException, InterruptedException {
		StartReponse startReponse = AdminHttp.startCamelis(
				loginSession, serviceFullName, null, null);
		assertTrue("Le retour ne doit pas être en erreur :"+startReponse, startReponse.isOk());

		ActiveLisService activeLisService = startReponse.getActiveLisService();
		int port = activeLisService.getPort();
		Session sess1 = Session.createCamelisSession(loginSession, activeLisService);

		PingReponse pingReponse = CamelisHttp.ping(sess1);
		LOGGER.fine(pingReponse.toString());
		assertTrue("Le retour ne doit pas être en erreur :"+pingReponse, pingReponse.isOk());
		assertEquals(activeLisService, pingReponse.getFirstActiveLisService());
		assertEquals(port, pingReponse.getFirstActiveLisService().getPort());
		
		LOGGER.fine("sess "+loginSession);
		LOGGER.fine("sess1 "+sess1);
		return sess1;
	}

	public static Session loginAndStartCamelis(String email, String password)
            throws PortalisException, IOException, InterruptedException {
		LOGGER.fine("\n\n              ===========================================> login <============================");
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email, password);
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());

		LOGGER.fine("\n\n              ===========================================> startCamelis <============================");
		Session sessUser = loginReponse.getSession();
		assertTrue("Il doit y avoir une session", sessUser != null);
		Session sess1 = UtilTest.startOneCamelis(sessUser);
		return sess1;
	}

	public static void registerUser(Session sess, String userKey,
			RightValue rightValue) throws PortalisException {
		RegisterKeyReponse camelisUserResponse = CamelisHttp.registerKey(sess,
				userKey, rightValue);
		LOGGER.info("CamelisUserResponse = " + camelisUserResponse);
		assertTrue("Le retour ne doit pas être en erreur\nreponse = "
				+ camelisUserResponse, camelisUserResponse.isOk());
		assertTrue("Un utilisateur doit être là. La réponse est :\n"
				+ camelisUserResponse, camelisUserResponse.getUser() != null);

		assertEquals(rightValue, camelisUserResponse.getUser().getRole());
	}



	public static StartReponse startCamelis(Session sess, int nb) throws PortalisException {
		StartReponse reponse = AdminHttp.startCamelis(sess,
				CoreTestConstants.SERVICE_PLANETS_PHOTO + nb, null, null);
		assertTrue("La reponse ne doit pas être en erreur, reponse=" + reponse,
				reponse.isOk());
		assertTrue("Le service Camelis ne doit pas être nul",
				reponse.getActiveLisService() != null);

		PortsActifsReponse actifs = AdminHttp.getActifsPorts(sess);
		assertTrue("actifs ne doit pas être null", actifs != null);
		assertTrue("actifs ne doit pas être en erreur, il vaut "+actifs, actifs != null);
		assertEquals(nb, actifs.getListPortsActifs().size());
		return reponse;
	}

	public static StartReponse startCamelis(Session sess, int nb, int port) throws PortalisException {
		StartReponse startReponse = AdminHttp.startCamelis(sess, port,
				CoreTestConstants.SERVICE_PLANETS_PHOTO + nb, null, null);
		assertTrue("La reponse ne doit pas être nulle, startReponse=" + startReponse,
				startReponse != null);
		assertTrue("La reponse ne doit pas être en erreur, startReponse=" + startReponse,
				startReponse.isOk());
		assertTrue("Le service Camelis ne doit pas être nul",
				startReponse.getActiveLisService() != null);

		PortsActifsReponse actifs = AdminHttp.getActifsPorts(sess);
		assertTrue("actifs ne doit pas être null", actifs != null);
		assertTrue("actifs ne doit pas être en erreur, il vaut "+actifs, actifs != null);
		assertEquals(nb, actifs.getListPortsActifs().size());
		return startReponse;
	}

	public static void isPresent(int pid, PidReponse reponse) {
		Set<Integer> pidList = reponse.getPids();
		assertTrue(
				"Le processus " + pid + " devrait être présent dans la liste "
						+ reponse.getPidsAsString(), pidList.contains(pid));
	}
	
	public static LoginReponse loginHttpAndCheck(String email,
			String password) throws PortalisException {
		LoginReponse reponse = AdminHttp.login(email, password);
		assertTrue("la réponse doit être sans erreur, elle vaut :"+reponse, reponse.isOk());

		Session session = null;
		try {
			session = reponse.getSession();
		} catch (PortalisException e) {
			fail("Il doit y avoir une session");
		}
		assertEquals(PortalisService.getInstance().getHost(),session.getHost());
		assertEquals(PortalisService.getInstance().getPort(),session.getPort());
		ActiveUser activeUser = session.getActiveUser();
		assertTrue(activeUser!=null);
		UserCore UserCore = activeUser.getUserCore();
		assertTrue(UserCore!=null);
		assertEquals(email,UserCore.getEmail());

		return reponse;
	}
	
	private static void copyFile(File sourceFile, File destFile) throws IOException {
	    if (!destFile.exists()) destFile.createNewFile();
	    FileChannel source = null;
	    FileChannel destination = null;
	    try {
	        source = new FileInputStream(sourceFile).getChannel();
	        destination = new FileOutputStream(destFile).getChannel();
	        destination.transferFrom(source, 0, source.size());
	    } finally {
	        if (source != null) source.close();
	        if (destination != null) destination.close();
	    }
	}
	
	private static File getTestResource(String name) {
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		return new File(cl.getResource(name).getPath());
	}
	
	public static void installTestResource(String src, String dest) throws IOException {
		copyFile(getTestResource(src), new File(dest));
	}

	public static void killAllCamelisProcess() throws PortalisException {
		LoginReponse loginReponse = AdminHttp.login(CoreTestConstants.MAIL_YVES, CoreTestConstants.PASSWORD_YVES);
		assertNotNull(loginReponse);
		assertTrue("loginReponse ne doit doit pas être en erreur :"+ loginReponse, loginReponse.isOk());
		Session sess = loginReponse.getSession();
		
		PingReponse pingReponse = AdminHttp.killAllCamelisProcess(sess);
		assertNotNull(pingReponse);
		assertTrue("pingReponse ne doit doit pas être en erreur :"+ pingReponse, pingReponse.isOk());
		
	}

	public static void killAllCamelisProcessAndClearUsers() throws PortalisException, RequestException {
		LoginReponse loginReponse = AdminHttp.login(CoreTestConstants.MAIL_YVES, CoreTestConstants.PASSWORD_YVES);
		assertNotNull(loginReponse);
		assertTrue("loginReponse ne doit doit pas être en erreur :"+ loginReponse, loginReponse.isOk());
		Session sess = loginReponse.getSession();
		
		PingReponse pingReponse = AdminHttp.killAllCamelisProcess(sess);
		assertNotNull(pingReponse);
		assertTrue("pingReponse ne doit doit pas être en erreur :"+ pingReponse, pingReponse.isOk());
		
		VoidReponse voidReponse = AdminHttp.clearActifUsers(sess);
		assertNotNull(voidReponse);
		assertTrue("voidReponse ne doit doit pas être en erreur :"+ voidReponse, voidReponse.isOk());
	}
}
