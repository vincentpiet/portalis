package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoProprietes;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestPortalisClear {private static final Logger LOGGER = Logger.getLogger(TestPortalisClear.class.getName());
	
	private static final String tomcatContainerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String tomcatContainerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String tomcatContainer = tomcatContainerPath + tomcatContainerName;


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestPortalisClear ************");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass TestPortalisClear ******************************");
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testProperties() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testProperties ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();
			
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());
			
			LOGGER.info("\nPortalisCtx ========================================> lsof <============================");
			int port = PortalisService.getInstance().getPort();
			PidReponse pidReponse = AdminHttp.lsof(loginSession, port);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			Set<Integer> pids = pidReponse.getPids();
			assertTrue("Il doit y avoir un processus sur le port "+
					port + "\nPids = " + pidsToString(pids)
					, pids.size()>0);

			LOGGER.info("\nPortalisCtx ========================================> propertyFilesCheck <============================");
			VoidReponse voidReponse = AdminHttp.propertyFilesCheck(loginSession);
			assertTrue("La réponse ne doit pas être en erreur : "+voidReponse, voidReponse.isOk());
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		} finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}
	
	@Test
	public void testAdminPropertiesFilesArePresent() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testAdminPropertiesFilesArePresent ***********************************\n");
		try {
			assertTrue(AdminProprietes.test());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testNoActifsPorts() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testNoActifsPorts ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();

			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session sessUser = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", sessUser != null);

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <============================");
			PortsActifsReponse portsActifsReponse = AdminHttp.getActifsPorts(sessUser);
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(0, actifs.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		} finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}

	
	@Test
	public void testTomcatIsActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testTomcatIsActif ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();

			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session sessUser = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", sessUser != null);
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());
			
			LOGGER.info("\nPortalisCtx ========================================> lsof <============================");
			int port = PortalisService.getInstance().getPort();
			PidReponse pidReponse = AdminHttp.lsof(sessUser, port);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			Set<Integer> pids = pidReponse.getPids();
			assertTrue("Il doit y avoir un processus sur le port "+
					port + "\nPids = " + pidsToString(pids)
					, pids.size()==1);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		} finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}

	@Test
	public void testStartTomcat() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartTomcat ***********************************\n");
		try {
			CargoLauncher.start();
			UtilTest.killAllCamelisProcess();
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());

			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}  finally {
			try {
				UtilTest.killAllCamelisProcess();
			} catch (PortalisException e) {
				fail("On n'arrive pas à tuer les processus");
			}
			CargoLauncher.stop();
		}
	}

	private static String pidsToString(Set<Integer> pids) {
		StringBuffer buff = new StringBuffer("[");
		int i = 0;
		for (Integer pid : pids) {
			buff.append(i > 0 ? ", " : "").append(pid);
			i++;
		}
		return buff.toString();
	}


}
