package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Set;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestCamelisLauncherHttpRequest2 {private static final Logger LOGGER = Logger.getLogger(TestCamelisLauncherHttpRequest2.class.getName());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = "localhost";
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisLauncherHttpRequest2 ************");
		CargoLauncher.start();
		UtilTest.killAllCamelisProcessAndClearUsers();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisLauncherHttpRequest2 **********");
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @After ***********************************");
		UtilTest.killAllCamelisProcessAndClearUsers();
	}

	@Test
	public void testGetSite() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetSite ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* getSite *********************
			SiteReponse siteReponse = AdminHttp.getSite(loginSession);

			assertTrue("La réponse doit être sans erreur, siteReponse ="
					+ siteReponse, siteReponse.isOk());
			LOGGER.info("siteReponse = " + siteReponse.toString());

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testLsofCmdOk() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLsofCmdOk ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			// ********************* lsof *******************
			PidReponse pidReponse = AdminHttp.lsof(loginSession, startReponse.getActiveLisService().getPort());
			assertTrue("La réponse doit être sans erreur, PidReponse ="
					+ pidReponse, pidReponse.isOk());

			// ********************* getActifsPorts *******************
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(loginSession);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());

			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();

			for (PortActif actif : actifs) {
				boolean trouve = false;
				int actifPid = actif.getPid();
				for (int pid : pidReponse.getPids()) {
					if (actifPid == pid) {
						trouve = true;
						break;
					}
				}
				assertTrue(String.format("portsActifsReponse = %s, Le processus %d n'est pas dans %s",
						portsActifsReponse, actifPid, pidReponse), trouve);
			}
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testLsofHttpOk() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLsofHttpOk ***********************************\n");
		String email = CoreTestConstants.UC1.getEmail();
		String password = CoreTestConstants.UC1.getPassword();
		try {
			String host = PortalisService.getInstance().getHost();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			// ********************* lsof *******************
			PidReponse pidReponse = AdminHttp.lsof(loginSession, startReponse.getActiveLisService().getPort());
			assertTrue("La réponse doit être sans erreur, PidReponse ="
					+ pidReponse, pidReponse.isOk());

			// ********************* getActifsPorts *******************
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(loginSession);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());

			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();

			for (PortActif actif : actifs) {
				boolean trouve = false;
				int actifPid = actif.getPid();
				for (int pid : pidReponse.getPids()) {
					if (actifPid == pid) {
						trouve = true;
						break;
					}
				}
				assertTrue(String.format("Le processus %d n'est pas dans %s",
						actifPid, portsActifsReponse), trouve);
			}
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testLsofNoSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLsofNoSession ***********************************\n");
		try {
			int anyPort = 7777;
			LOGGER.info("\nPortalisCtx ========================================> anonymousLogin <============================");
			LoginReponse loginReponse = AdminHttp.anonymousLogin();
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();

			
			LOGGER.info("\nPortalisCtx ========================================> lsof <============================");
			PidReponse in = AdminHttp.lsof(loginSession, anyPort);

			assertTrue("La réponse ne doit pas être en erreur, elle vaut :"
					+ in, in.isOk());
			assertEquals(0, in.getPids().size());
		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testPingPortCamelisListening() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testPingPortCamelisListening ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			// *********************** ping ********************
			PingReponse pingReponse = CamelisHttp.ping(Session
					.createCamelisSession(loginSession, ActiveLisService));
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			assertEquals(loginSession.getActiveUser().getUserCore().getEmail(),
					ActiveLisService.getCreator());
			assertEquals(CoreTestConstants.SERVICE_PLANETS_PHOTO,
					ActiveLisService.getFullName());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCalmelisDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisDefaultPort ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					serviceName, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());

			// ******************* getActifsPorts ****************
			PortsActifsReponse portActifReponse = AdminHttp.getActifsPorts(loginSession);
			assertTrue("La réponse doit être sans erreur, portActifReponse ="
					+ portActifReponse, portActifReponse.isOk());
			Set<PortActif> actifs = portActifReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(1, actifs.size());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			PortActif portActif = new PortActif(host, startReponse
					.getActiveLisService().getPort(), ActiveLisService.getPid());
			assertTrue("le port actif " + portActif
					+ " doit être dans la liste des actfs",
					actifs.contains(portActif));
			assertEquals(serviceName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.UC1.getEmail(),
					ActiveLisService.getCreator());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPort ***********************************\n");

		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			String serviceFullName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 12;
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					serviceFullName, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			int port1 = ActiveLisService.getPort();

			// ******************* getActifsPorts ****************
			PortsActifsReponse portActifReponse = AdminHttp.getActifsPorts(loginSession);
			assertTrue("La réponse doit être sans erreur, portActifReponse ="
					+ portActifReponse, portActifReponse.isOk());
			Set<PortActif> actifs = portActifReponse.getListPortsActifs();
			assertTrue("La liste d'actifs actifs ne doit pas être nulle",
					actifs != null);
			assertEquals(1, actifs.size());
			PortActif actif = new PortActif(host, port1,
					ActiveLisService.getPid());
			assertTrue(actif + " devrait être dans la liste des actifs",
					actifs.contains(actif));

			assertEquals(port1, ActiveLisService.getPort());
			assertEquals(serviceFullName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.UC1.getEmail(),
					ActiveLisService.getCreator());
		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCalmelisArgumentsNeeded() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisArgumentsNeeded ***********************************\n");
		try {
			String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO;
			StartReponse reponse = AdminHttp.startCamelis(
					Session.createPortalisSession(ActiveUser
							.createAnonymousActiveAdmin()), null, serviceName,
					null, null);

			LOGGER.info("StartReponse = " + reponse);

			assertTrue("La réponse doit être en erreur, elle vaut :" + reponse,
					!reponse.isOk());
			
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nAttention pushDocument() non exécuté");
		}
	}

	@Test
	public void testStartCamelisSpecifiedPortAllreadyInUse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPortAllreadyInUse ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// *************************** login ***********************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			Session loginSession = loginReponse.getSession();

			// ************************ startCamelis ********************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse1 = UtilTest.startCamelis(loginSession, 1);
			LOGGER.info("StartReponse1 = " + startReponse1);
			int port1 = startReponse1.getActiveLisService().getPort();

			// ************************ startCamelis ********************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse2 = AdminHttp.startCamelis(loginSession,
					port1, CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			LOGGER.info("StartReponse2 = " + startReponse2);

			assertTrue("La réponse doit être en erreur, elle vaut :"
					+ startReponse2, !startReponse2.isOk());


		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisNoSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisNoSession ***********************************\n");
		try {

			// *************************** login ***********************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.UC1.getEmail(),
					CoreTestConstants.UC1.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ************************ startCamelis ********************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			Session sess = Session.createPortalisSession(CoreTestConstants.AU1);
			StartReponse startReponse = AdminHttp.startCamelis(sess,
					CoreTestConstants.PORT,
					CoreTestConstants.SERVICE_FULL_NAME1, null, null);
			assertTrue("startReponse doit être en erreur, il vaut "
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_NOT_A_REGISTERED_PORTALIS_SESSION;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisIncorrectServiceName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisIncorrectServiceName ***********************************\n");

		LOGGER.fine("\n\n              ===========================================> login <============================");
		try {
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
				CoreTestConstants.UC1.getEmail(),
				CoreTestConstants.UC1.getPassword());
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session loginSession = loginReponse.getSession();

		LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
		String serviceName = "service-1";
		StartReponse startReponse = AdminHttp.startCamelis(loginSession,
				CoreTestConstants.PORT, serviceName, null, null);
		assertTrue("startReponse doit être en erreur, il vaut " + startReponse,
				!startReponse.isOk());
		final String TEMOIN = ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME;
		assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
				+ "' La réponse est :\n" + startReponse,
				startReponse.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessIdDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdDefaultPort ***********************************\n");

		// *************************** login ***********************
		LOGGER.fine("\n\n              ===========================================> login <============================");
		try {
		LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
				CoreTestConstants.UC1.getEmail(),
				CoreTestConstants.UC1.getPassword());
		assertTrue("La réponse doit être sans erreur, loginReponse ="
				+ loginReponse, loginReponse.isOk());
		Session loginSession = null;
			loginSession = loginReponse.getSession();

		// ************************ startCamelis ********************
		LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
		String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 12;
		StartReponse startReponse = AdminHttp.startCamelis(loginSession,
				CoreTestConstants.PORT1, serviceName, null, null);
		assertTrue("startReponse ne doit pas être en erreur, il vaut "
				+ startReponse, startReponse.isOk());
		assertEquals(CoreTestConstants.PORT1, startReponse.getActiveLisService()
				.getPort());

		// ************************ GetCamelisProcessId ********************
		LOGGER.fine("\n\n              ========================================> GetCamelisProcessId <=========================");
		PidReponse pidReponse = AdminHttp.getCamelisProcessId(loginSession, null);
		assertTrue("pidReponse ne doit pas être en erreur, il vaut "
				+ pidReponse, pidReponse.isOk());
		LOGGER.info("PidReponse=" + pidReponse.toString());

		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testGetActifsPorts() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetActifsPorts ***********************************\n");
		try {

			// ************************** login ************************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.UC1.getEmail(),
					CoreTestConstants.UC1.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ************************ startCamelis ********************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse1 = UtilTest.startCamelis(loginSession, 1);
			assertTrue("La réponse doit être sans erreur, startReponse1 ="
					+ startReponse1, startReponse1.isOk());
			PortActif actif1 = new PortActif(
					PortalisService.getInstance().getHost(), startReponse1
							.getActiveLisService().getPort(), startReponse1
							.getActiveLisService().getPid());
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse2 = UtilTest.startCamelis(loginSession, 2,
					CoreTestConstants.PORT);
			assertTrue("La réponse doit être sans erreur, startReponse2 ="
					+ startReponse2, startReponse2.isOk());
			PortActif actif2 = new PortActif(
					PortalisService.getInstance().getHost(), CoreTestConstants.PORT,
					startReponse2.getActiveLisService().getPid());
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse3 = UtilTest.startCamelis(loginSession, 3);
			assertTrue("La réponse doit être sans erreur, startReponse3 ="
					+ startReponse3, startReponse3.isOk());
			PortActif actif3 = new PortActif(
					PortalisService.getInstance().getHost(), startReponse3
							.getActiveLisService().getPort(), startReponse3
							.getActiveLisService().getPid());

			// ************************ getActifsPorts ********************
			LOGGER.fine("\n\n              ========================================> getActifsPorts <=========================");
			PortsActifsReponse reponse = AdminHttp.getActifsPorts(loginSession);
			assertTrue("La réponse ne doit pas avoir d'erreur, elle vaut :"
					+ reponse, reponse.isOk());

			Set<PortActif> actifs = reponse.getListPortsActifs();
			assertEquals(3, actifs.size());

			assertTrue(actif1 + " devrait être dans la liste des actifs",
					actifs.contains(actif1));
			assertTrue(actif2 + " devrait être dans la liste des actifs",
					actifs.contains(actif2));
			assertTrue(actif3 + " devrait être dans la liste des actifs",
					actifs.contains(actif3));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName() + " rencontrée : "
					+ e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testGetActifsPortsNoActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetActifsPortsNoActif ***********************************\n");
		try {

			// ************************** login ************************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.UC1.getEmail(),
					CoreTestConstants.UC1.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ************************ getActifsPorts ********************
			LOGGER.fine("\n\n              ========================================> getActifsPorts <=========================");
			PortsActifsReponse reponse = AdminHttp.getActifsPorts(loginSession);
			assertTrue("La réponse ne doit pas avoir d'erreur, elle vaut :"
					+ reponse, reponse.isOk());

			Set<PortActif> actifs = reponse.getListPortsActifs();
			assertEquals(0, actifs.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName() + " rencontrée : "
					+ e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testLoginSucess() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLoginSucess ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("la réponse ne doit pas être en erreur "+loginReponse, loginReponse.getStatus().equals(XmlIdentifier.OK));
			assertEquals(CoreTestConstants.UC1, loginReponse.getSession()
					.getActiveUser().getUserCore());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testGetCamelisProcessIdOnBadPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessIdOnBadPort ***********************************\n");
		try {
			LOGGER.fine("\n\n              ========================================> login <=========================");
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertEquals(CoreTestConstants.UC1, loginReponse.getSession()
					.getActiveUser().getUserCore());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			int port1 = ActiveLisService.getPort();
			
			// ******************* stopCamelis ****************
			LOGGER.fine("\n\n              ========================================> stopCamelis <=========================");
			PingReponse pingReponse = AdminHttp.stopCamelis(loginSession);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			
			
			
			// ********************** GetCamelisProcessId *******************
			LOGGER.fine("\n\n              ========================================> GetCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(loginSession, port1);
			assertTrue("La réponse ne pas doit être en erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			assertTrue("Il ne doit pas y avoir de processus, la réponse est :\n" + pidReponse,
					pidReponse.getPids().size() == 0);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testGetPidFromSystem() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetPidFromSystem ***********************************\n");
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);
			int port = loginSession.getPort();

			// ******************* startCamelis ****************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			int port1 = ActiveLisService.getPort();
			assertEquals(host, ActiveLisService.getHost());

			// ********************** ping *******************
			LOGGER.fine("\n\n              ========================================> ping <=========================");
			PingReponse pingReponse = CamelisHttp.ping(Session
					.createCamelisSession(loginSession,
							ActiveLisService));
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());

			// ********************** getCamelisProcessId *******************
			LOGGER.fine("\n\n              ========================================> getCamelisProcessId <=========================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(loginSession,
					port1);
			assertEquals(port, loginSession.getPort());
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			ActiveLisService ActiveLisService1 = pingReponse.getFirstActiveLisService();
			assertTrue("ActiveLisService1 ne doit pas être null",
					ActiveLisService1 != null);
			Set<Integer> pids = pidReponse.getPids();
			Integer[] tabPids = pids.toArray(new Integer[pids.size()]);
			assertEquals(tabPids[0].intValue(), ActiveLisService1.getPid());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testFindPortAndStartCamelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testFindPortAndStartCamelis ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// *************************** login ***********************
			LOGGER.fine("\n\n              ===========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			Session loginSession = loginReponse.getSession();

			// ************************ startCamelis ********************
			LOGGER.fine("\n\n              ========================================> startCamelis <=========================");

			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);

			assertTrue("startReponse doit être sans erreur, il vaut "
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStopCalmelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStopCalmelis ***********************************\n");

		String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					serviceName, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();

			// *********************** ping ********************
			Session camelisSession = Session.createCamelisSession(loginSession, ActiveLisService);
			PingReponse pingReponse = CamelisHttp.ping(camelisSession);
			assertTrue("La réponse ne doit pas être en erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			assertEquals(ActiveLisService, pingReponse.getFirstActiveLisService());

			// ******************* stopCamelis ****************
			PingReponse pingReponse1 = AdminHttp.stopCamelis(loginSession,
					ActiveLisService.getPort());
			assertTrue("La réponse doit être sans erreur, pingReponse1 ="
					+ pingReponse1, pingReponse1.isOk());
			LOGGER.info("pingReponse1 = " + pingReponse1.toString());

			// *********************** ping ********************
			PingReponse pingReponse2 = CamelisHttp.ping(camelisSession);
			assertTrue("La réponse doit être en erreur, pingReponse ="
					+ pingReponse2, !pingReponse2.isOk());
			final String TEMOIN = ErrorMessages.ERROR_DURING_HTTP_REQUEST;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + pingReponse2,
					pingReponse2.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testGetCamelisProcessId() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testGetCamelisProcessId ***********************************\n");

		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			int port1 = ActiveLisService.getPort();
			assertEquals(host, ActiveLisService.getHost());

			// ***************** getCamelisProcessId ****************
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(loginSession,
					port1);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			LOGGER.info("PidReponse=" + pidReponse.toString());

			// ********************** ping ********************
			Session camelisSession = Session.createCamelisSession(loginSession, ActiveLisService);
			PingReponse pingReponse = CamelisHttp.ping(camelisSession);
			assertTrue(pingReponse != null);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			ActiveLisService ActiveLisService1 = pingReponse.getFirstActiveLisService();

			assertEquals(ActiveLisService, ActiveLisService1);
			assertEquals(ActiveLisService.getPid(), ActiveLisService1.getPid());

			UtilTest.isPresent(ActiveLisService.getPid(), pidReponse);
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testUsingAnExpiredSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testUsingAnExpiredSession ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse = AdminHttp.logout(loginSession);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse, voidReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être en erreur, startReponse ="
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_PAS_DE_SESSION_EN_COURS;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

}
