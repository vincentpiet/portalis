package fr.irisa.lis.portalis.camelis.http;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.joda.time.DateTime;
import org.joda.time.Duration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoProprietes;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.data.UserCore;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportLisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.SetRoleReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class CamelisHttpTest {
	
	private static final Logger LOGGER = Logger.getLogger(CamelisHttpTest.class.getName());

	private static final String tomcatContainerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String tomcatContainerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String tomcatContainer = tomcatContainerPath
			+ tomcatContainerName;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = "localhost";
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		if (CargoLauncher.tomcatIsActif()) {
			CargoLauncher.stop();
		}
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass CamelisHttpTest ************");
		CargoLauncher.start();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		UtilTest.killAllCamelisProcess();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass CamelisHttpTest **********");
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
		UtilTest.killAllCamelisProcessAndClearUsers();
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @After ***********************************");
	}

	@Test
	public void test8080IsBusy() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test test8080IsBusy ***********************************\n");
		try {
			LoginReponse loginReponse = AdminHttp.login(
					CoreTestConstants.MAIL_YVES,
					CoreTestConstants.PASSWORD_YVES);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session sess = loginReponse.getSession();
			int port = PortalisService.getInstance().getPort();
			PidReponse pidReponse = AdminHttp.lsof(sess, port);
			assertTrue("La réponse doit être sans erreur, pidReponse ="
					+ pidReponse, pidReponse.isOk());
			Set<Integer> pids = pidReponse.getPids();
			assertTrue("Il doit y avoir un processus sur le port " + port
					+ "\nPids = " + pidsToString(pids), pids.size() > 0);
			VoidReponse logoutReponse = AdminHttp.logout(sess);
			assertTrue("La réponse doit être sans erreur, logoutReponse ="
					+ logoutReponse, logoutReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testTomcatIsActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testTomcatIsActif ***********************************\n");
		try {
			assertTrue(tomcatContainer + " doit être actif",
					CargoLauncher.tomcatIsActif());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testImportCtx() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportCtx ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			Session camelisSession = Session.createCamelisSession(loginSession,
					ActiveLisService);
			String ctxFile = "planets/planets/planets.ctx";
			ImportCtxReponse importReponse = CamelisHttp.importCtx(
					camelisSession, ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testLSIALA() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLSIALA ***********************************\n");
		/* test le broadcast vers les camelis lors d'un nouveau login */
		try {
			String email1 = CoreTestConstants.UC1.getEmail();
			String password1 = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse1 = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse1, loginReponse1.isOk());
			Session adminSession1 = loginReponse1.getSession();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession1 = loginReponse1.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession1,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			Session camelisSession1 = Session.createCamelisSession(
					loginSession1, ActiveLisService);
			String ctxFile = "planets/planets/planets.ctx";
			ImportCtxReponse importReponse = CamelisHttp.importCtx(
					camelisSession1, ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user1) <============================");
			String lisQuery = "Medium";
			ExtentReponse reponse = CamelisHttp.extent(camelisSession1,
					lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ reponse, receivedExtent != null
					&& receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			LisObject[] expectedObjs = { new LisObject(5, "neptune"),
					new LisObject(8, "uranus") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent));

			LOGGER.info("\nPortalisCtx ========================================> logout 1 <============================");
			VoidReponse voidReponse1 = AdminHttp.logout(adminSession1);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse1, voidReponse1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");

			String email2 = CoreTestConstants.UC2.getEmail();
			String password2 = CoreTestConstants.UC2.getPassword();
			LoginReponse reponse2 = AdminHttp.login(email2, password2);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			Session camelisSession2 = Session.createCamelisSession(
					reponse2.getSession(), ActiveLisService);

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user2) <============================");
			ExtentReponse extentReponse2 = CamelisHttp.extent(camelisSession2,
					lisQuery);
			assertNotNull(extentReponse2);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ extentReponse2, extentReponse2.isOk());
			LisExtent receivedExtent2 = extentReponse2.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ extentReponse2, receivedExtent2 != null
					&& receivedExtent2.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent2));

			LOGGER.info("\nPortalisCtx ========================================> logout 2 <============================");
			VoidReponse voidReponse2 = AdminHttp.logout(camelisSession2);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse2, voidReponse2.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testLSIALABis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLSIALA ***********************************\n");
		/* test le broadcast vers les camelis lors d'un nouveau login */
		try {
			String email1 = CoreTestConstants.UC1.getEmail();
			String password1 = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse1 = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse1, loginReponse1.isOk());
			Session adminSession1 = loginReponse1.getSession();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession1 = loginReponse1.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession1,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			Session camelisSession1 = Session.createCamelisSession(
					loginSession1, ActiveLisService);
			String ctxFile = "planets/planets/planets.ctx";
			ImportCtxReponse importReponse = CamelisHttp.importCtx(
					camelisSession1, ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user1) <============================");
			String lisQuery = "Medium";
			ExtentReponse reponse = CamelisHttp.extent(camelisSession1,
					lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ reponse, receivedExtent != null
					&& receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			LisObject[] expectedObjs = { new LisObject(5, "neptune"),
					new LisObject(8, "uranus") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent));

			LOGGER.info("\nPortalisCtx ========================================> logout 1 <============================");
			VoidReponse voidReponse1 = AdminHttp.logout(adminSession1);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse1, voidReponse1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");

			LoginReponse reponse2 = AdminHttp.login(email1, password1);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			Session camelisSession2 = Session.createCamelisSession(
					reponse2.getSession(), ActiveLisService);

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user2) <============================");
			ExtentReponse extentReponse2 = CamelisHttp.extent(camelisSession2,
					lisQuery);
			assertNotNull(extentReponse2);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ extentReponse2, extentReponse2.isOk());
			LisExtent receivedExtent2 = extentReponse2.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ extentReponse2, receivedExtent2 != null
					&& receivedExtent2.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent2));

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse2 = AdminHttp.logout(camelisSession2);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse2, voidReponse2.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testLLSIAA() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLLSIAA ***********************************\n");
		/* test le brocast des utilisateurs actifs lors d'un startCamelis */
		try {
			String email1 = CoreTestConstants.UC1.getEmail();
			String password1 = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");
			String email2 = CoreTestConstants.UC2.getEmail();
			String password2 = CoreTestConstants.UC2.getPassword();
			LoginReponse reponse2 = AdminHttp.login(email2, password2);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			Session loginSession2 = null;
			try {
				loginSession2 = reponse2.getSession();
			} catch (PortalisException e) {
				fail("Il doit y avoir une session");
			}
			ActiveUser activeUser2 = loginSession2.getActiveUser();
			assertTrue(activeUser2 != null);
			UserCore UserCore2 = activeUser2.getUserCore();
			assertTrue(UserCore2 != null);
			assertEquals(email2, UserCore2.getEmail());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession1 = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession1,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			Session camelisSession1 = Session.createCamelisSession(
					loginSession1, ActiveLisService);
			String ctxFile = "planets/planets/planets.ctx";
			ImportCtxReponse importReponse = CamelisHttp.importCtx(
					camelisSession1, ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user1) <============================");
			String lisQuery = "Medium";
			ExtentReponse reponse = CamelisHttp.extent(camelisSession1,
					lisQuery);
			assertNotNull(reponse);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ reponse, reponse.isOk());
			LisExtent receivedExtent = reponse.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ reponse, receivedExtent != null
					&& receivedExtent.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			LisObject[] expectedObjs = { new LisObject(5, "neptune"),
					new LisObject(8, "uranus") };
			LisExtent expectedExtent = new LisExtent(expectedObjs);
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent));

			LOGGER.info("\nPortalisCtx ========================================> extent Medium (user2) <============================");
			Session camelisSession2 = Session.createCamelisSession(
					loginSession2, ActiveLisService);

			ExtentReponse extentReponse2 = CamelisHttp.extent(camelisSession2,
					lisQuery);
			assertNotNull(extentReponse2);
			assertTrue("l'extension ne doit doit pas être en erreur :"
					+ extentReponse2, extentReponse2.isOk());
			LisExtent receivedExtent2 = extentReponse2.getExtent();
			assertTrue("Il doit y avoir deux éléments dans l'extension :"
					+ extentReponse2, receivedExtent2 != null
					&& receivedExtent2.card() == 2);
			// extent(medium) should contain two objects: neptune and uranus
			assertTrue("Received extent\n" + receivedExtent
					+ "does not match expected extent\n" + expectedExtent,
					expectedExtent.equals(receivedExtent2));

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse1 = AdminHttp.logout(camelisSession1);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse1, voidReponse1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> logout <============================");
			VoidReponse voidReponse2 = AdminHttp.logout(camelisSession2);
			assertTrue("La réponse doit être sans erreur, voidReponse ="
					+ voidReponse2, voidReponse2.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testRegisterKeyAlreadyRegistered() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKeyAlreadyRegistered ***********************************\n");
		try {
			String email1 = CoreTestConstants.UC1.getEmail();
			String password1 = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse1 = UtilTest.loginHttpAndCheck(email1,
					password1);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse1, loginReponse1.isOk());
			Session loginSession1 = loginReponse1.getSession();
			LOGGER.fine("portalisSession1 :n" + loginSession1);

			LOGGER.info("\nPortalisCtx ========================================> second login <============================");
			String email2 = CoreTestConstants.UC2.getEmail();
			String password2 = CoreTestConstants.UC2.getPassword();
			LoginReponse reponse2 = AdminHttp.login(email2, password2);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse2, reponse2.isOk());

			Session loginSession2 = null;
			try {
				loginSession2 = reponse2.getSession();
			} catch (PortalisException e) {
				fail("Il doit y avoir une session");
			}
			ActiveUser activeUser2 = loginSession2.getActiveUser();
			assertTrue(activeUser2 != null);
			UserCore UserCore2 = activeUser2.getUserCore();
			assertTrue(UserCore2 != null);
			assertEquals(email2, UserCore2.getEmail());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			StartReponse startReponse = AdminHttp.startCamelis(loginSession1,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			Session camelisSession1 = Session.createCamelisSession(
					loginSession1, ActiveLisService);
			String ctxFile = "planets/planets/planets.ctx";
			ImportCtxReponse importReponse = CamelisHttp.importCtx(
					camelisSession1, ctxFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> registerKey <============================");
			Session camelisSess1 = Session.createCamelisSession(loginSession1,
					ActiveLisService);
			RegisterKeyReponse camelisUserResponse = CamelisHttp.registerKey(
					camelisSess1, activeUser2.getPortalisSessionId(),
					RightValue.READER);
			LOGGER.info("CamelisUserResponse = " + camelisUserResponse);
			assertTrue("Le retour doit être en erreur\nreponse = "
					+ camelisUserResponse, !camelisUserResponse.isOk());

			final String TEMOIN = ErrorMessages.TEMOIN_IS_ALREADY_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + camelisUserResponse,
					camelisUserResponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testImportCtxNoFileName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportCtxNoFileName ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> importCtx <============================");
			Session camelisSession = Session.createCamelisSession(loginSession,
					ActiveLisService);
			ImportCtxReponse importReponse = CamelisHttp
					.importCtx(camelisSession);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testImportLis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportLis ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.fine("[testImportLis] login\n");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.fine("[testImportLis] startCamelis\n");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.fine("[testImportLis] importLis\n");
			Session camelisSession = Session.createCamelisSession(loginSession,
					ActiveLisService);
			String lisFile = "planets/planets/planets1.lis";
			ImportLisReponse importReponse = CamelisHttp.importLis(
					camelisSession, lisFile);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testImportLisNoFileName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testImportLisNoFileName ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.fine("[testImportLisNoFileName] login\n");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.fine("[testImportLisNofileName] startCamelis\n");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			LOGGER.fine("[testImportLisNoFileName] importLis\n");
			Session camelisSession = Session.createCamelisSession(loginSession,
					ActiveLisService);
			ImportLisReponse importReponse = CamelisHttp
					.importLis(camelisSession);
			assertTrue("La réponse doit être sans erreur, importReponse ="
					+ importReponse, importReponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testLoadContext() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLoadContext ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.fine("[testLoadContext] login\n");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.fine("[testLoadContext] startCamelis\n");
			int camelisPort = CoreTestConstants.PORT1;
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PLANETS,
					null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService camelisService = startReponse
					.getActiveLisService();
			assertTrue("camelisService ne doit pas être null",
					camelisService != null);

			LOGGER.fine("[testLoadContext] loadContext\n");
			Session camelisSession = Session.createCamelisSession(loginSession,
					camelisService);
			LoadContextReponse reponse = CamelisHttp
					.loadContext(camelisSession);
			assertTrue("La réponse doit être sans erreur, loadContextReponse ="
					+ reponse, reponse.isOk());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStart1ProcessSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStart1ProcessSpecifiedPort ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int camelisPort = CoreTestConstants.PORT1;
			Session sess1 = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(sess1,
					camelisPort, CoreTestConstants.SERVICE_PLANETS_PHOTO, null,
					null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveService ActiveLisService = startReponse.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null",
					ActiveLisService != null);

			String creator = CoreTestConstants.AU1.getUserCore().getEmail();
			String idAppli = ServiceCore
					.extractAppliName(CoreTestConstants.SERVICE_PLANETS_PHOTO);
			String idService = ServiceCore
					.extractServiceName(CoreTestConstants.SERVICE_PLANETS_PHOTO);
			ServiceCoreInterface serviceCore = new ServiceCore(idAppli,
					idService);
			assertTrue(!ActiveLisService.equals(new ActiveLisService(
					serviceCore.getFullName(), creator, PortalisService
							.getInstance().getHost(), camelisPort, new Date(),
					null, 5656, 654)));

			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(sess1);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(1, actifs.size());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testChangementDeSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testChangementDeSession ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("Le retour ne doit pas être en erreur :" + startReponse,
					startReponse.isOk());

			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			int port = ActiveLisService.getPort();
			Session session2 = Session.createCamelisSession(loginSession,
					ActiveLisService);

			PingReponse pingReponse1 = CamelisHttp.ping(session2);
			assertTrue("Le retour ne doit pas être en erreur :" + pingReponse1,
					pingReponse1.isOk());
			assertEquals(ActiveLisService,
					pingReponse1.getFirstActiveLisService());
			assertEquals(port, pingReponse1.getFirstActiveLisService()
					.getPort());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisServicesOnPortalis <============");
			PingReponse pingReponse2 = AdminHttp
					.getActiveLisServicesOnPortalis(loginSession);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse2, pingReponse2.isOk());
			List<ActiveLisService> lesServices1 = pingReponse2.getLesServices();
			assertEquals(1, lesServices1.size());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisServicesOnPortalis <============");
			Session session3 = Session
					.createAnonymousAdminCamelisSession(ActiveLisService);
			PingReponse pingReponse = AdminHttp
					.getActiveLisServicesOnPortalis(session3);
			assertTrue("La réponse doit être en erreur, pingReponse ="
					+ pingReponse, !pingReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_NOT_A_REGISTERED_PORTALIS_SESSION;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + pingReponse,
					pingReponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStart3Processes() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStart3Processes ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			Session session = loginReponse.getSession();
			UtilTest.startOneCamelis(session,
					CoreTestConstants.SERVICE_PLANETS_PHOTO);

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			int port2 = CoreTestConstants.PORT2;
			StartReponse startReponse = AdminHttp.startCamelis(session, port2,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			Session.createCamelisSession(session,
					startReponse.getActiveLisService());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			UtilTest.startOneCamelis(session,
					CoreTestConstants.SERVICE_PLANETS_PHOTO);

			LOGGER.info("\nPortalisCtx ========================================> getCamelisServicesOnPortalis <============");
			PingReponse pingReponse1 = AdminHttp
					.getActiveLisServicesOnPortalis(session);
			assertTrue("La réponse doit être sans erreur, pingReponse ="
					+ pingReponse1, pingReponse1.isOk());
			List<ActiveLisService> lesServices1 = pingReponse1.getLesServices();
			assertEquals(3, lesServices1.size());

			LOGGER.info("\nPortalisCtx ========================================> getActifsPorts <==========================");
			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(session);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(3, actifs.size());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testKillAllProcesses() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testKillAllProcesses ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			Session sess = loginReponse.getSession();
			StartReponse rep1 = AdminHttp.startCamelis(sess,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, rep1 =" + rep1,
					rep1.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse rep2 = AdminHttp.startCamelis(sess,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, rep2 =" + rep2,
					rep2.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse rep3 = AdminHttp.startCamelis(sess,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("La réponse doit être sans erreur, rep3 =" + rep3,
					rep3.isOk());

			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(loginReponse.getSession());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(3, actifs.size());

			assertTrue("les deux réponses doivent être différentes rep1="
					+ rep1 + "\nrep1=" + rep2, !rep1.equals(rep2));
			assertTrue("les deux réponses doivent être différentes",
					!rep1.equals(rep3));
			assertTrue("les deux réponses doivent être différentes",
					!rep2.equals(rep3));

			AdminHttp.stopCamelis(sess);

			PortsActifsReponse portsActifsReponse2 = AdminHttp
					.getActifsPorts(loginReponse.getSession());
			Set<PortActif> actifs2 = portsActifsReponse2.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs2 != null);
			assertEquals(0, actifs2.size());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testPingSuccessDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testPingSuccessDefaultPort ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			Session loginSession = loginReponse.getSession();
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue(
					"Le retour ne doit pas être en erreur : " + startReponse,
					startReponse.isOk());

			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null, startReponse ="
					+ startReponse, ActiveLisService != null);

			Session camelisSession = Session.createCamelisSession(loginSession,
					ActiveLisService);
			LOGGER.info("\nPortalisCtx ========================================> CamelisHttp.ping <============================");
			PingReponse pingReponse = CamelisHttp.ping(camelisSession);
			assertTrue("Le retour ne doit pas être en erreur : " + pingReponse,
					pingReponse.isOk());

			assertEquals(ActiveLisService,
					pingReponse.getFirstActiveLisService());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisProcessId <============================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(loginSession,
					ActiveLisService.getPort());
			assertTrue("Le retour ne doit pas être en erreur : " + pidReponse,
					pidReponse.isOk());

			assertTrue(pidReponse.getPids().contains(
					pingReponse.getFirstActiveLisService().getPid()));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testPingSuccessSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testPingSuccessSpecifiedPort ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("loginReponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session session = loginReponse.getSession();

			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			StartReponse startReponse = AdminHttp.startCamelis(session,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue("startReponse ne doit pas être en erreur : "
					+ startReponse, startReponse.isOk());

			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("ActiveLisService ne doit pas être null, startReponse ="
					+ startReponse, ActiveLisService != null);

			LOGGER.info("\nPortalisCtx ========================================> CamelisHttp.ping <============================");
			Session sess = Session.createCamelisSession(session,
					ActiveLisService);
			PingReponse pingReponse = CamelisHttp.ping(sess);
			assertTrue("pingReponse ne doit pas être en erreur : "
					+ pingReponse, pingReponse.isOk());

			assertEquals(ActiveLisService,
					pingReponse.getFirstActiveLisService());

			LOGGER.info("\nPortalisCtx ========================================> getCamelisProcessId <============================");
			PidReponse pidReponse = AdminHttp.getCamelisProcessId(session,
					ActiveLisService.getPort());
			assertTrue("Le retour ne doit pas être en erreur : " + pidReponse,
					pidReponse.isOk());
			assertTrue(pidReponse.getPids().contains(
					pingReponse.getFirstActiveLisService().getPid()));

			LOGGER.info("\nsession is " + session);
			LOGGER.info("\nsess is " + sess);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}

	}

	/*
	 * ------------------------------------- testRegisterKey
	 * ------------------------------------------
	 */

	@Test
	public void testRegisterKeyNoPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKeyNoPort ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			Session session = loginReponse.getSession();
			Session sess1 = UtilTest.startOneCamelis(session);

			RightValue rightValue = RightValue.NONE;

			// ******************* registerKey ****************
			RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
					CoreTestConstants.USER_KEY, rightValue);
			LOGGER.info("testRegisterKeyNoPort()" + reponse);

			assertTrue("Le retour ne doit pas être en erreur", reponse.isOk());
			assertTrue("Un utilisateur doit être là. La réponse est :\n"
					+ reponse, reponse.getUser() != null);

			assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
			assertEquals(rightValue, reponse.getUser().getRole());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testRegistetestRegisterKey2rKey2() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKey2 ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			Session sess1 = UtilTest.loginAndStartCamelis(email, password);

			LOGGER.fine("testRegisterKey2() session = " + sess1);

			LOGGER.info("\nPortalisCtx ========================================> registerKey 1 <============================");
			RightValue rightValue = RightValue.ADMIN;
			RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
					CoreTestConstants.USER_KEY, rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour ne doit pas être en erreur, reponse ="
					+ reponse, reponse.isOk());
			assertTrue("Un utilisateur doit être là. La réponse est :\n"
					+ reponse, reponse.getUser() != null);

			assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
			assertEquals(rightValue, reponse.getUser().getRole());

			LOGGER.info("\nPortalisCtx ========================================> registerKey 2 <============================");
			RightValue rightValue1 = RightValue.NONE;
			RegisterKeyReponse reponse1 = CamelisHttp.registerKey(sess1,
					CoreTestConstants.USER_KEY, rightValue1);
			LOGGER.info("SetRoleReponse = " + reponse1);
			assertTrue("Le retour doit être en erreur. reponse = " + reponse1,
					!reponse1.isOk());
			assertTrue("Un utilisateur ne pas doit être là. La réponse est :\n"
					+ reponse1, reponse1.getUser() == null);

			final String TEMOIN = ErrorMessages.TEMOIN_IS_ALREADY_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse1,
					reponse1.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testRegisterKeyWithTimeout() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testRegisterKeyWithTimeout ***********************************\n");
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// login
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("Login error : " + loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("No session available", loginSession != null);

			// startCamelis
			Session session = loginReponse.getSession();
			Session sess1 = UtilTest.startOneCamelis(session);

			// registerKey
			RightValue rightValue = RightValue.NONE;
			int timeout = 300; // 5 minutes, arbitrary
			RegisterKeyReponse reponse = CamelisHttp.registerKey(sess1,
					CoreTestConstants.USER_KEY, rightValue, timeout);
			LOGGER.info("[testRegisterKeyWithTimeout] response = " + reponse);

			assertTrue("RegisterKey returned an error: " + reponse,
					reponse.isOk());
			assertTrue("No user in response: " + reponse,
					reponse.getUser() != null);

			assertEquals(CoreTestConstants.USER_KEY, reponse.getUser().getKey());
			assertEquals(rightValue, reponse.getUser().getRole());

			// is the expiration date really 5 minutes from now?
			DateTime now = new DateTime();
			DateTime exp = new DateTime(reponse.getUser().getExpires());
			Duration diff = new Duration(now, exp);
			boolean accurateTimeout = diff.isShorterThan(Duration
					.standardSeconds(timeout + 1))
					&& diff.isLongerThan(Duration.standardSeconds(timeout - 1));
			assertTrue("Timeout was not set correctly", accurateTimeout);

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	/*
	 * ------------------------------------- setRole
	 * ------------------------------------------
	 */

	@Test
	public void testSetRoleOk1() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleOk1 ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			Session sess1 = UtilTest.loginAndStartCamelis(email, password);

			RightValue rightValue0 = RightValue.NONE;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(sess1, userKey, rightValue0);

			RightValue rightValue1 = RightValue.ADMIN;

			SetRoleReponse setRoleReponse = CamelisHttp.setRole(sess1,
					CoreTestConstants.USER_KEY, rightValue1);
			LOGGER.info("SetRoleReponse = " + setRoleReponse);
			assertTrue("Le retour ne doit pas être en erreur",
					setRoleReponse.isOk());
			assertTrue("Un utilisateur doit être là. La réponse est :\n"
					+ setRoleReponse, setRoleReponse.getUser() != null);

			assertEquals(rightValue1, setRoleReponse.getUser().getRole());

			assertEquals(CoreTestConstants.USER_KEY, setRoleReponse.getUser()
					.getKey());
			assertEquals(rightValue1, setRoleReponse.getUser().getRole());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Ignore("Until role checking is implemented")
	@Test
	public void testSetRoleNoAdminKey() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoAdminKey ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			Session sess1 = UtilTest.loginAndStartCamelis(email, password);

			RightValue rightValue0 = RightValue.NONE;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(sess1, userKey, rightValue0);

			RightValue rightValue = RightValue.READER;

			SetRoleReponse reponse = CamelisHttp.setRole(sess1,
					CoreTestConstants.USER_KEY, rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNoUserKey() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoUserKey ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			Session sess1 = UtilTest.loginAndStartCamelis(email, password);

			RightValue rightValue0 = RightValue.NONE;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(sess1, userKey, rightValue0);

			RightValue rightValue = RightValue.EDITOR;
			SetRoleReponse reponse = CamelisHttp.setRole(sess1, null,
					rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNoRole() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoRole ***********************************\n");

		try {

			String role = null;
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			assertTrue(
					"Le retour ne doit pas être en erreur : " + startReponse,
					startReponse.isOk());

			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();

			// ********************* ping *********************
			Session sess = Session.createCamelisSession(loginSession,
					ActiveLisService);
			PingReponse pingReponse = CamelisHttp.ping(sess);
			LOGGER.fine(pingReponse.toString());
			assertTrue("Le retour ne doit pas être en erreur",
					pingReponse.isOk());

			assertEquals(ActiveLisService,
					pingReponse.getFirstActiveLisService());

			// ********************* setRole *********************
			SetRoleReponse reponse = CamelisHttp.setRole(sess,
					CoreTestConstants.USER_KEY, role);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_INCORRECT;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNoRole1() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNoRole1 ***********************************\n");

		try {
			RightValue role = null;
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			Session sess1 = UtilTest.startOneCamelis(loginSession);

			RightValue rightValue0 = RightValue.COLLABORATOR;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(sess1, userKey, rightValue0);

			SetRoleReponse reponse = CamelisHttp.setRole(sess1,
					CoreTestConstants.USER_KEY, role);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_MISSING;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleBadRole() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleBadRole ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			Session sess1 = UtilTest.startOneCamelis(loginSession);

			RightValue rightValue0 = RightValue.COLLABORATOR;
			String userKey = CoreTestConstants.USER_KEY;

			UtilTest.registerUser(sess1, userKey, rightValue0);

			String role = "noRole";
			SetRoleReponse reponse = CamelisHttp.setRole(sess1,
					CoreTestConstants.USER_KEY, role);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur\nreponse = " + reponse,
					!reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_INCORRECT;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n"
					+ Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNotRegisteredAdminKey() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleNotRegisteredAdminKey ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			Session sess1 = UtilTest.startOneCamelis(loginSession);

			RightValue rightValue = RightValue.READER;
			SetRoleReponse reponse = CamelisHttp.setRole(sess1,
					CoreTestConstants.USER_KEY, rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleUserUnkown() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleUserUnkown ***********************************\n");

		try {
			String userKey = "123456654321";
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			Session sess1 = UtilTest.startOneCamelis(loginSession);

			RightValue rightValue = RightValue.NONE;

			SetRoleReponse reponse = CamelisHttp.setRole(sess1, userKey,
					rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			assertEquals(CamelisXmlName.ERROR, reponse.getStatus());
			final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testSetRoleNotRegisteredUser() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testSetRoleOk2 ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ********************* startCamelis *********************
			Session sess1 = UtilTest.startOneCamelis(loginSession);

			RightValue rightValue = RightValue.ADMIN;

			SetRoleReponse reponse = CamelisHttp.setRole(sess1,
					CoreTestConstants.USER_KEY, rightValue);
			LOGGER.info("SetRoleReponse = " + reponse);
			assertTrue("Le retour doit être en erreur", !reponse.isOk());
			assertTrue("Un utilisateur ne doit pas être là. La réponse est :\n"
					+ reponse, reponse.getUser() == null);

			final String TEMOIN = ErrorMessages.TEMOIN_IS_NOT_REGISTERED;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + reponse,
					reponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testLogin() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testLogin ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse reponse = AdminHttp.login(email, password);
			assertTrue("la réponse doit être sans erreur, elle vaut :"
					+ reponse, reponse.isOk());

			Session session = reponse.getSession();
			assertTrue(session != null);
			int port = PortalisService.getInstance().getPort();
			String host = PortalisService.getInstance().getHost();
			assertEquals(host, session.getHost());
			assertEquals(port, session.getPort());
			ActiveUser activeUser = session.getActiveUser();
			assertTrue(activeUser != null);
			UserCore userdata = activeUser.getUserCore();
			assertTrue(userdata != null);
			assertEquals(email, userdata.getEmail());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}

	}

	@Test
	public void testStartCalmelisDefaultPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisDefaultPort\n");
		String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					serviceName, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());

			PortsActifsReponse portsActifsReponse2 = AdminHttp
					.getActifsPorts(loginReponse.getSession());
			assertTrue(
					"La réponse doit être sans erreur, portsActifsReponse2 ="
							+ portsActifsReponse2, portsActifsReponse2.isOk());
			Set<PortActif> actifs = portsActifsReponse2.getListPortsActifs();
			assertTrue("Liste d'actifs null", actifs != null);
			assertEquals(1, actifs.size());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			PortActif portActif = new PortActif(host, startReponse
					.getActiveLisService().getPort(), ActiveLisService.getPid());
			assertTrue("le port actif " + portActif
					+ " doit être dans la liste des actfs",
					actifs.contains(portActif));
			assertEquals(serviceName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.UC1.getEmail(),
					ActiveLisService.getCreator());
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisSpecifiedPort() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPort ***********************************\n");

		try {
			String host = PortalisService.getInstance().getHost();
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			String serviceFullName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 12;
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					serviceFullName, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			assertTrue("Il doit y avoir un ActiveLisService",
					ActiveLisService != null);
			assertEquals(host, ActiveLisService.getHost());

			assertTrue("La réponse ne doit pas être en erreur, elle vaut :"
					+ startReponse, startReponse.isOk());

			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(loginReponse.getSession());
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertTrue("La liste d'actifs actifs ne doit pas être nulle",
					actifs != null);
			assertEquals(1, actifs.size());
			assertEquals(serviceFullName, ActiveLisService.getFullName());
			assertEquals(CoreTestConstants.UC1.getEmail(),
					ActiveLisService.getCreator());
		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCalmelisArgumentsNeeded() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCalmelisArgumentsNeeded ***********************************\n");
		try {

			// *************************** startCamelis ***********************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <============================");
			String serviceName = XmlIdentifier.SERVICE_NAME();
			StartReponse reponse = AdminHttp.startCamelis(
					Session.createPortalisSession(CoreTestConstants.AU1), null,
					serviceName, null, null);

			LOGGER.info("StartReponse = " + reponse);

			assertTrue("La réponse doit être en erreur, elle vaut :" + reponse,
					!reponse.isOk());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nAttention pushDocument() non exécuté");
		}
	}

	@Test
	public void testStartCamelisSpecifiedPortAllreadyInUse() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisSpecifiedPortAllreadyInUse ***********************************\n");

		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// *************************** login ***********************
			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			Session loginSession = loginReponse.getSession();

			// ************************ startCamelis ********************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			StartReponse startReponse1 = UtilTest.startCamelis(loginSession, 1);
			LOGGER.info("StartReponse1 = " + startReponse1);
			int port1 = startReponse1.getActiveLisService().getPort();

			// ************************ startCamelis ********************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			StartReponse startReponse2 = AdminHttp.startCamelis(loginSession,
					port1, CoreTestConstants.SERVICE_PLANETS_PHOTO, null, null);
			LOGGER.info("StartReponse2 = " + startReponse2);

			assertTrue("La réponse doit être en erreur, elle vaut :"
					+ startReponse2, !startReponse2.isOk());

			PortsActifsReponse portsActifsReponse = AdminHttp
					.getActifsPorts(loginSession);
			assertTrue("La réponse doit être sans erreur, portsActifsReponse ="
					+ portsActifsReponse, portsActifsReponse.isOk());
			Set<PortActif> actifs = portsActifsReponse.getListPortsActifs();
			assertEquals(1, actifs.size());
			String host = PortalisService.getInstance().getHost();

			PortActif actif = new PortActif(host, port1, startReponse1
					.getActiveLisService().getPid());
			assertTrue(actif + " devrait être dans la liste des actifs",
					actifs.contains(actif));

		} catch (PortalisException e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisNoSession() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisNoSession ***********************************\n");
		try {

			// *************************** login ***********************
			LOGGER.info("\nPortalisCtx ========================================> login <============================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.UC1.getEmail(),
					CoreTestConstants.UC1.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ************************ startCamelis ********************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			Session sess = Session.createPortalisSession(CoreTestConstants.AU1);
			StartReponse startReponse = AdminHttp.startCamelis(sess,
					CoreTestConstants.PORT,
					CoreTestConstants.SERVICE_FULL_NAME1, null, null);
			assertTrue("startReponse doit être en erreur, il vaut "
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_NOT_A_REGISTERED_PORTALIS_SESSION;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartCamelisIncorrectServiceName() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartCamelisIncorrectServiceName\n");
		// *************************** login ***********************
		LOGGER.info("\nPortalisCtx ========================================> login <============================");
		try {
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(
					CoreTestConstants.UC1.getEmail(),
					CoreTestConstants.UC1.getPassword());
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = null;
			loginSession = loginReponse.getSession();

			// ************************ startCamelis ********************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			String serviceName = "service-1";
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					CoreTestConstants.PORT, serviceName, null, null);
			assertTrue("startReponse doit être en erreur, il vaut "
					+ startReponse, !startReponse.isOk());
			final String TEMOIN = ErrorMessages.TEMOIN_INCORRECT_SERVICE_NAME;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + startReponse,
					startReponse.messagesContains(TEMOIN));
		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStopCalmelis() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStopCalmelis\n");
		String serviceName = CoreTestConstants.SERVICE_PLANETS_PHOTO + 55;
		try {
			String email = CoreTestConstants.UC1.getEmail();
			String password = CoreTestConstants.UC1.getPassword();

			// ********************* login *********************
			LOGGER.info("\nPortalisCtx ========================================> login <=========================");
			LoginReponse loginReponse = UtilTest.loginHttpAndCheck(email,
					password);
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();
			assertTrue("Il doit y avoir une session", loginSession != null);

			// ******************* startCamelis ****************
			LOGGER.info("\nPortalisCtx ========================================> startCamelis <=========================");
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					serviceName, null, null);
			assertTrue("La réponse doit être sans erreur, startReponse ="
					+ startReponse, startReponse.isOk());
			LOGGER.info("StartReponse = " + startReponse.toString());
			ActiveLisService ActiveLisService = startReponse
					.getActiveLisService();
			int port1 = ActiveLisService.getPort();

			// *********************** ping ********************
			LOGGER.info("\nPortalisCtx ========================================> ping <=========================");
			Session camelisSess = Session.createCamelisSession(loginSession,
					ActiveLisService);
			PingReponse pingReponse = CamelisHttp.ping(camelisSess);
			assertTrue("La réponse ne doit pas être en erreur, pingReponse ="
					+ pingReponse, pingReponse.isOk());
			assertEquals(ActiveLisService,
					pingReponse.getFirstActiveLisService());

			// ******************* stopCamelis ****************
			LOGGER.info("\nPortalisCtx ========================================> stopCamelis <=========================");
			PingReponse pingReponse1 = AdminHttp.stopCamelis(loginSession,
					port1);
			assertTrue("La réponse doit être sans erreur, pingReponse1 ="
					+ pingReponse1, pingReponse1.isOk());
			LOGGER.info("pingReponse1 = " + pingReponse1.toString());

			// *********************** ping ********************
			LOGGER.info("\nPortalisCtx ========================================> ping <=========================");
			PingReponse pingReponse2 = CamelisHttp.ping(camelisSess);
			assertTrue("La réponse doit être en erreur, pingReponse ="
					+ pingReponse2, !pingReponse2.isOk());
			final String TEMOIN = ErrorMessages.ERROR_DURING_HTTP_REQUEST;
			assertTrue("Les messages devraient contenir le témoin '" + TEMOIN
					+ "' La réponse est :\n" + pingReponse2,
					pingReponse2.messagesContains(TEMOIN));

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	private static String pidsToString(Set<Integer> pids) {
		StringBuffer buff = new StringBuffer("[");
		int i = 0;
		for (Integer pid : pids) {
			buff.append(i > 0 ? ", " : "").append(pid);
			i++;
		}
		return buff.toString();
	}

}
