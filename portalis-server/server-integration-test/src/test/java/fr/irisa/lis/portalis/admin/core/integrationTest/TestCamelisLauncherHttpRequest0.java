package fr.irisa.lis.portalis.admin.core.integrationTest;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoProprietes;
import fr.irisa.lis.portalis.camelis.http.UtilTest;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public class TestCamelisLauncherHttpRequest0 {
	
	

	private static final String tomcatContainerPath = CargoProprietes.cargoProp
			.getProperty("tomcatPath");
	private static final String tomcatContainerName = CargoProprietes.cargoProp
			.getProperty("tomcatName");
	private static final String tomcatContainer = tomcatContainerPath + tomcatContainerName;private static final Logger LOGGER = Logger.getLogger(TestCamelisLauncherHttpRequest0.class.getName());

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		final String host = "localhost";
		final int portNum = 8080;
		final String portalisAppli = "portalis";
		PortalisService.getInstance().init(host, portNum, portalisAppli);		
		LOGGER.info("\nPortalisCtx **************************************************** @BeforeClass TestCamelisLauncherHttpRequest0 ************");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @AfterClass ******************************");
		CargoLauncher.start();
		UtilTest.killAllCamelisProcess();
		CargoLauncher.stop();
		LOGGER.info("\nPortalisCtx **************************************************** @fin AfterClass TestCamelisLauncherHttpRequest0 **********");
	}

	@Before
	public void setUp() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @Before **********************************");
		CargoLauncher.start();
		UtilTest.killAllCamelisProcessAndClearUsers();
		CargoLauncher.stop();
	}

	@After
	public void tearDown() throws Exception {
		LOGGER.info("\nPortalisCtx **************************************************** @After ***********************************");
	}

	@Test
	public void testTomcatIsNotActif() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testTomcatIsNotActif ***********************************\n");
		try {
			assertTrue(tomcatContainer+" ne doit pas être actif", !CargoLauncher.tomcatIsActif());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testStartTomcat() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testStartTomcat ***********************************\n");
		try {
			CargoLauncher.start();
			assertTrue(tomcatContainer+" doit être actif", CargoLauncher.tomcatIsActif());

		} catch (Exception e) {
			fail(e.getClass().getCanonicalName()
					+ " rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		} finally {
			CargoLauncher.stop();
		}
	}



	@Test
	public void testAdminPropertiesFiles() {
		LOGGER.info("\nPortalisCtx **************************************************** @Test testAdminPropertiesFiles ***********************************\n");
		try {
			CargoLauncher.start();

			LOGGER.info("\nPortalisCtx ========================================> anonymousLogin <============================");
			LoginReponse loginReponse = AdminHttp.anonymousLogin();
			assertTrue("La réponse doit être sans erreur, loginReponse ="
					+ loginReponse, loginReponse.isOk());
			Session loginSession = loginReponse.getSession();

			LOGGER.info("\nPortalisCtx ========================================> adminPropertyFilesCheck <============================");
			VoidReponse reponse = AdminHttp.propertyFilesCheck(loginSession);
			assertTrue("La réponse ne doit pas avoir d'erreur, reponse ="+reponse,reponse.isOk());
		} catch (Exception e) {
			fail(e.getMessage() + "\nStack :\n" + Util.stack2string(e));
		} finally {
			CargoLauncher.stop();
		}
	}

}