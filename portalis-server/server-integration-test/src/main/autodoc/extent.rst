Pour le service considéré demande l'extension correspondant à la requête donnée.

Paramêtres
----------

============  ===================================================================  ========  =========================
Name          Description                                                          Required  Defaults
============  ===================================================================  ========  =========================
userKey       user's key                                                           *yes*
request       Camelis request                                                      *yes*
============  ===================================================================  ========  =========================

Résultat
--------

``ExtentReponse`` **extends** ``LisReponse`` **extends** ``VoidReponse``

