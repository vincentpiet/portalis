<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>
<%@ page import="fr.irisa.lis.portalis.system.linux.LinuxCmd"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.LogReponse"%>

<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "getLog.jsp";
private static final Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin."+jspName);%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");
	
	LOGGER.fine("\nPortalisCtx ====== "+jspName+" ====== "+session.getId());

	LogReponse reponse = null;
	try {
		String camelisSessionId = (String) session
		.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		reponse = AdminCmd.getLog();
	} catch (Exception e) {
		String state = XmlIdentifier.ERROR;
		String mess = (e.getMessage() == null ? "Erreur "+jspName+" : "
		+ e.getClass().getName() : e.getMessage());
		LOGGER.severe(mess+Util.stack2string(e));
		reponse = new LogReponse(state, mess);
	}
	LOGGER.fine("reponse = " + reponse);
%>
<%=reponse%>
