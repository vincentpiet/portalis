<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.DateFormat"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.server.core.ServerConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Application"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveSite"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ServiceCore"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.ActiveLisService"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>
<%@ page import="fr.irisa.lis.portalis.system.linux.LinuxCmd"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse"%>

<%!static final String COLOR = "#990000";%>
<html>
<body>
	<jsp:include page="${request.getContextPath()}/clientAdmin/header.html" />
	<h1 align="center">
		Portalis
		<%=LinuxCmd.getPortalisVersion()%></h1>
	<h2 align="center">Services disponibles</h2>

	<%
		response.setHeader("Expires", "0");
		String email = request.getParameter("email");
		String password = request.getParameter("password");
		if (email != null && email.length() != 0 && password != null
				&& password.length() != 0) {

			LoginReponse reponse = null;
			String camelisSessionId = null;
			camelisSessionId = (String) session
					.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
			reponse = AdminCmd
					.login(session.getId(), email,
							request.getParameter(XmlIdentifier.PASSWORD()),
							session);
			if (!reponse.isOk()) {
				// return clientLogin.jsp
			}
	%>
	<p align="right">Logout</p>

	<table border="1"
		style="border-color: <%=COLOR%>; margin-left: auto; margin-right: auto;">
		<tr>
			<th>Nom de service</th>
			<th>host</th>
			<th>port</th>
			<th>creator</th>
			<th>Date de création</th>
			<th>pid</th>
			<th>Mise à jour</th>
			<th>NbObjet</th>
			<th>Admin</th>
		</tr>

		<%
			ActiveSite siteRep = AdminCmd.getActiveSite().getSite();
				for (Application appli : siteRep.getApplications().values()) {
		%>
		<tr>
			<td colspan="9" align="center"
				style="font-weight: bold; color: <%=COLOR%>"><%=appli.getId()%></td>
		</tr>
		<%
			for (ServiceCore serviceCore : appli.getServices()) {
		%>
		<tr>
			<td colspan="8" style="font-weight: bold"><%=serviceCore.getFullName()%></td>
			<td>start service</td>
		</tr>

		<%
			List<ActiveLisService> lesServicesActifs = new ArrayList<ActiveLisService>();

						for (ActiveLisService activeService : siteRep
								.getActiveLisServices()) {
							if (serviceCore.getFullName().equals(
									activeService.getFullName())) {
								lesServicesActifs.add(activeService);
							}
						}
		%>
		<tr>
			<td rowspan="<%=lesServicesActifs.size()%>"
				style="vertical-align: middle; font-weight: bold"><%=serviceCore.getFullName()%></td>
			<%
				for (ActiveLisService activeService : lesServicesActifs) {
			%>
			<td><%=activeService.getHost()%></td>
			<td><%=activeService.getPort()%></td>
			<td><%=activeService.getCreator()%></td>
			<td><%=Util.prettyDate(activeService
									.getActivationDate())%></td>
			<td><%=activeService.getPid()%></td>
			<td><%=Util.prettyDate(activeService
									.getLastUpdate())%></td>
			<td><%=activeService.getNbObject()%></td>
			<td>Stop</td>
		</tr>

		<%
			}

					}
				}
		%>
	</table>

	<%
		} else {
	%>

	<table border="1"
		style="border-color: <%=COLOR%>; margin-left: auto; margin-right: auto;">
		<tr>
			<th>Nom de service</th>
			<th>host</th>
			<th>port</th>
			<th>creator</th>
			<th>Date de création</th>
			<th>pid</th>
			<th>Mise à jour</th>
			<th>NbObjet</th>
		</tr>

		<%
			ActiveSite siteRep = AdminCmd.getActiveSite().getSite();
				for (Application appli : siteRep.getApplications().values()) {
		%>
		<tr>
			<td colspan="8" align="center"
				style="font-weight: bold; color: <%=COLOR%>"><%=appli.getId()%></td>
		</tr>
		<%
			for (ServiceCore serviceCore : appli.getServices()) {
						List<ActiveLisService> lesServicesActifs = new ArrayList<ActiveLisService>();

						for (ActiveLisService activeService : siteRep
								.getActiveLisServices()) {
							if (serviceCore.getFullName().equals(
									activeService.getFullName())) {
								lesServicesActifs.add(activeService);
							}
						}
						if (lesServicesActifs.size() == 0) {
		%>

		<tr>
			<td style="font-weight: bold"><%=serviceCore.getFullName()%></td>
		</tr>

		<%
			} else {
		%>
		<tr>
			<td rowspan="<%=lesServicesActifs.size()%>"
				style="vertical-align: middle; font-weight: bold"><%=serviceCore.getFullName()%></td>
			<%
				for (ActiveLisService activeService : lesServicesActifs) {
			%>
			<td><%=activeService.getHost()%></td>
			<td><%=activeService.getPort()%></td>
			<td><%=activeService.getCreator()%></td>
			<td><%=Util.prettyDate(activeService
										.getActivationDate())%></td>
			<td><%=activeService.getPid()%></td>
			<td><%=Util.prettyDate(activeService
										.getLastUpdate())%></td>
			<td><%=activeService.getNbObject()%></td>
		</tr>

		<%
			}
						}

					}
				}
		%>
	</table>
	<%
		}
	%>
</body>
</html>