<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse"%>

<%@ page import="java.util.logging.Logger"%>

<%!

	private static final String jspName = "logout.jsp";
	static final private Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin." + jspName);
	private PortalisCtx camelisCtx = PortalisCtx.getInstance();

%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.fine("\nPortalisCtx ====== "+jspName+" ====== "+session.getId());

	VoidReponse reponse = null;
	String camelisSessionId = (String) session
	.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
	LOGGER.fine("CamelisSessionId = " + camelisSessionId);
	if (camelisSessionId == null) {
		reponse = new VoidReponse(XmlIdentifier.ERROR, jspName
		+ " : Sorry no user id logged for key "
		+ camelisSessionId);
	} else {
		ActiveUser activeUser = PortalisCtx.getInstance().getUser(
		camelisSessionId);
		if (activeUser == null) {
	String mess = "impossible d'effectuer le logout, la session "
			+ camelisSessionId + " n'est pas connue";
	LOGGER.severe(mess);
	throw new PortalisException(mess);
		}
		String email = camelisCtx.getUser(camelisSessionId)
		.getUserCore().getEmail();
		camelisCtx.userLogout(camelisSessionId);
		LOGGER.fine(new StringBuffer(jspName).append("(")
		.append(camelisSessionId).append(")")
		.append(" email = ").append(email).toString());
		reponse = new VoidReponse();
	}
%>

<%=reponse%>
