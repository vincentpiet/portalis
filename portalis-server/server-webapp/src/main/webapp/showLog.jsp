<?xml version="1.0" encoding="UTF-8" ?>
<%@page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>

<%@ page import="org.w3c.dom.Element"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlUtil"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseHtmlwriter"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.LogReponse"%>

<%@ page import="fr.irisa.lis.portalis.system.linux.LinuxCmd"%>


<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "showLog.jsp";
	static final private Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin." +jspName);%>
<%
	response.setContentType(ClientConstants.CONTENT_TYPE_HTML);
	response.setHeader("Expires", "0");
	
	LOGGER.fine("\nPortalisCtx ====== "+jspName+" ====== "+session.getId());

	LogReponse reponse = null;
	try {
		String camelisSessionId = (String) session
		.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.fine("CamelisSessionId = "+camelisSessionId);
		reponse = AdminCmd.getLog();
	
	} catch (Exception e) {
		String state = XmlIdentifier.ERROR;
		String mess = (e.getMessage() == null ? "Erreur "+jspName+" : "
		+ e.getClass().getName() : e.getMessage());
		LOGGER.severe(mess+Util.stack2string( e));
		reponse = new LogReponse(state, mess);
	}
	LOGGER.fine("reponse = " + reponse);
	Element elemHtml = new AdminReponseHtmlwriter().visit(reponse);
%>
<%=XmlUtil.prettyXmlString(elemHtml)%>
