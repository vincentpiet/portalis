<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page import="fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse"%>

<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "getActiveCamelisServices.jsp";
private static final Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin."+jspName);%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.fine("\nPortalisCtx ====== "+jspName+" ====== "+session.getId());

	PingReponse reponse = null;
	String camelisSessionId = (String) session
	.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
	LOGGER.fine("CamelisSessionId = "+camelisSessionId);
	if (camelisSessionId == null) {
		String mess = String.format(
		"Erreur %s : pas de session en cours", jspName);
		LOGGER.fine(mess);
		reponse = new PingReponse(XmlIdentifier.ERROR, mess);
	} else {

		reponse = AdminCmd.getActiveLisServicesOnPortalis();
		LOGGER.fine("reponse = " + reponse);
	}
%>

<%=reponse%>
