<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>

<%@ page import="fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "stopCamelis.jsp";
static final private Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin." +jspName);%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");
	
	LOGGER.fine("\nPortalisCtx ====== "+jspName+" ====== "+session.getId());

	PingReponse reponse = null;
	try {
		String camelisSessionId = (String) session
		.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.fine("CamelisSessionId = "+camelisSessionId);
		if (camelisSessionId == null) {
	String mess = String.format(
			"Erreur %s : pas de session en cours", jspName);
	LOGGER.fine(mess);
	reponse = new PingReponse(XmlIdentifier.ERROR, mess);
		} else {

	String portNum = request.getParameter(XmlIdentifier.PORT());
	String hostName = request
			.getParameter(XmlIdentifier.HOST());
	
	if (hostName == null) {
		hostName = request.getServerName();
	}
	
	if (portNum != null) {
		reponse = AdminCmd.stopCamelis(camelisSessionId, portNum);
	} else {
		reponse = AdminCmd.killAllCamelisProcess(camelisSessionId);
	}

		}

	} catch (Exception e) {
		String state = XmlIdentifier.ERROR;
		String mess = (e.getMessage() == null ? String.format(
		"Erreur %s : %s", jspName, e.getClass().getName())
		: String.format("Erreur %s : %s", jspName,
				e.getMessage()));
		LOGGER.severe(mess+Util.stack2string( e));
		reponse = new PingReponse(state, mess);
	}
	LOGGER.fine("reponse = " + reponse);
%>
<%=reponse%>
