<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page import="fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse"%>

<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "startCamelis.jsp";
	static final private Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin." +jspName);%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");
	
	LOGGER.fine("\nPortalisCtx ====== "+jspName+" ====== "+session.getId());

	StartReponse reponse = null;
	try {
		String camelisSessionId = (String) session
		.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.fine("CamelisSessionId = "+camelisSessionId);
		if (camelisSessionId == null) {
	String mess = String.format("Erreur %s : pas de session en cours", jspName);
	LOGGER.fine(mess);
	throw new PortalisException(mess);
		}
		
		String serviceName = request.getParameter(XmlIdentifier.SERVICE_NAME());
		String portNum = request.getParameter(XmlIdentifier.PORT());
		String datadir = request.getParameter(XmlIdentifier.DATADIR());
		String logFile = request.getParameter(XmlIdentifier.LOG());

		if (portNum != null) {
	// give a choosen port number
	reponse = AdminCmd.startCamelis(camelisSessionId, portNum, serviceName, datadir, logFile);
		} else {
	// leave portalis choosing port number
	reponse = AdminCmd.startCamelis(camelisSessionId, serviceName, datadir, logFile);
		}
	} catch (PortalisException e) {
		String state = XmlIdentifier.ERROR;
		reponse = new StartReponse(state, e.getMessage());
	} catch (Exception e) {
		String state = XmlIdentifier.ERROR;
		String mess = (e.getMessage() == null ? "Erreur interne : "
		+ e.getClass().getName() : e.getMessage());
		LOGGER.severe(mess+Util.stack2string( e));
		reponse = new StartReponse(state, mess);
	}
	LOGGER.fine("reponse = "+reponse);
%>
<%=reponse%>
