<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.server.core.PortalisCtx"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.admin.data.PortalisService"%>
<%@ page import="java.util.logging.Logger"%>

<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.AdminProprietes"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse"%>
	
<%@ page import="java.io.File"%>
<%@ page import="java.io.InputStream"%>


<%!private static final String jspName = "login.jsp";
	static final private Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin." + jspName);

	public void jspInit() {
		String LogFile = AdminProprietes.portalis.getProperty("admin.camelis.logFilePath");
		File file = new File(LogFile);
		if (file.exists()) {
			file.delete();
		}
		
		String hostName = getInitParameter("portalis.host");
		String port = getInitParameter("portalis.port");
		String portalisAppliName = getServletContext(). getContextPath().substring(1);
		int hostPort = Integer.parseInt(port);
		InputStream loggingConfFile = getServletContext().getResourceAsStream("/WEB-INF/logging.properties");
		PortalisCtx.getInstance().init(hostName, hostPort, portalisAppliName, loggingConfFile);
	}%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.fine("\nPortalisCtx ====== " + jspName + " ====== "
	+ session.getId());

	LoginReponse reponse = null;
	String camelisSessionId = null;
	try {
		camelisSessionId = (String) session
		.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		if (camelisSessionId != null) {
	String email = PortalisCtx.getInstance()
			.getUser(camelisSessionId).getUserCore().getEmail();
	reponse = new LoginReponse(XmlIdentifier.ERROR, jspName
			+ " : Sorry, one user is allready logged : "
			+ email);
		} else {
	String email = request.getParameter(XmlIdentifier.EMAIL());
	reponse = AdminCmd.login(session.getId(), email,
			request.getParameter(XmlIdentifier.PASSWORD()),
			session);
	if (reponse.isOk()) {
		camelisSessionId = reponse.getSession()
				.getPortalisSessionId();
		LOGGER.fine(new StringBuffer(jspName).append("(")
				.append(email).append(")")
				.append(" portalisID = ")
				.append(camelisSessionId).toString());
	}
		}
	} catch (Throwable t) {
		String mess = String.format("%s %s%s", jspName, t.getMessage(),
		Util.stack2string(t));
		LOGGER.severe(mess);
		reponse = new LoginReponse(XmlIdentifier.ERROR, mess);
	}
%>

<%=reponse%>
