<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ClientConstants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.server.core.AdminCmd"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.PidReponse"%>

<%@ page import="java.util.logging.Logger"%>
<%@ page import="java.lang.NumberFormatException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.ErrorMessages"%>

<%!private static final String jspName = "lsof.jsp";
	static final private Logger LOGGER = Logger.getLogger("fr.irisa.lis.portalis.admin." + jspName);%>

<%
	response.setContentType(ClientConstants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.fine("\nPortalisCtx ====== " + jspName + " ====== "
	+ session.getId());

	PidReponse reponse = null;
	String portNum = request.getParameter(XmlIdentifier.PORT());
	try {
		if (portNum == null) {
	throw new PortalisException("manque le parametre "
			+ XmlIdentifier.PORT());
		} else {
	String camelisSessionId = (String) session
			.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
	LOGGER.fine("CamelisSessionId = " + camelisSessionId);
	if (camelisSessionId == null) {
		String mess = String.format(
				"Erreur %s : pas de session en cours", jspName);
		LOGGER.fine(mess);
		throw new PortalisException(mess);
	}

	int port = Integer.parseInt(portNum);

	reponse = AdminCmd.lsof(camelisSessionId, port);
		}
	} catch (PortalisException e) {
		final String TEMOIN = ErrorMessages.TEMOIN_PAS_DE_SESSION_EN_COURS;
		if (e.getMessage().contains(TEMOIN)) {
	reponse = new PidReponse();
		} else {
	String state = XmlIdentifier.ERROR;
	reponse = new PidReponse(state, e.getMessage());
		}
	} catch (NumberFormatException e) {
		String mess = "Numéro de port incorrect : " + portNum;
		LOGGER.warning(mess);
		throw new PortalisException(mess);
	} catch (Exception e) {
		String state = XmlIdentifier.ERROR;
		String mess = (e.getMessage() == null ? "Erreur lsof.jsp : "
		+ e.getClass().getName() : e.getMessage());
		LOGGER.severe(mess+Util.stack2string( e));
		reponse = new PidReponse(state, mess);
	}
%>
<%=reponse%>
