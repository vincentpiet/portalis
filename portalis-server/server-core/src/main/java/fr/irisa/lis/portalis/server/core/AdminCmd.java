package fr.irisa.lis.portalis.server.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ErrorMessages;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Precondition;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.LoginErr;
import fr.irisa.lis.portalis.shared.admin.data.LoginResult;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.reponse.ActiveSiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LogReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VersionReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.admin.validation.CoreServiceNameValidator;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.system.linux.LinuxCmd;
import fr.irisa.lis.portalis.system.linux.LinuxInteractor;
import fr.irisa.lis.portalis.system.linux.PortService;

public class AdminCmd {
	
	private static final Logger LOGGER = Logger.getLogger(AdminCmd.class.getName());

	private static PortalisCtx ctx = PortalisCtx.getInstance();
	

	public final static String WEBAPP_PATH = AdminProprietes.portalis
			.getProperty("admin.webappsDirPath");
	public static final String LOG_FILE_PATH = AdminProprietes.portalis
			.getProperty("admin.camelis.logFilePath");
	private static final File logFile = new File(LOG_FILE_PATH);

	static {
		try {
			// test de la présence de WEBAPP_PATH
			File webapp = new File(WEBAPP_PATH);
			if (!webapp.exists()) {
				LOGGER.warning("pas de répertoire '" + WEBAPP_PATH
						+ "' on essaie de le créer");
				if (!webapp.mkdirs())
					throw new PortalisException("répertoire " + WEBAPP_PATH
							+ " impossible à créer");
			}
			// test de la présence de LOG_FILE_PATH
			File logFile = new File(LOG_FILE_PATH);
			File logPath = logFile.getParentFile();
			if (!logPath.exists()) {
				LOGGER.warning("pas de répertoire '" + logPath.getAbsolutePath()
						+ "' on essaie de le créer");
				if (!logPath.mkdirs())
					throw new PortalisException("répertoire "
							+ logPath.getAbsolutePath() + " impossible à créer");
			}

			ctx.getDataBase().init();
		} catch (Exception e) {
			LOGGER.severe("Erreur lors de l'initialisation de la classes AdminCmd"+Util.stack2string(e));
		}

	}

	public static LogReponse getLog() {
		LogReponse logReponse = new LogReponse();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(logFile));
			String line = reader.readLine();
			while (line != null) {
				logReponse.addLine(line);
				line = reader.readLine();
			}
			reader.close();
		} catch (IOException e) {
			String mess = "problème d'entrée/sortie dans getLog";
			return new LogReponse(mess, e);
		}
		return logReponse;
	}

	public static VoidReponse clearLog() throws PortalisException {
		return LinuxCmd.clearLog(logFile);
	}

	public static SiteReponse getSite(String camelisSessionID) {
		// vérification du rôle
		SiteReponse reponseErr = PortalisCtx.getInstance().roleCheck(
				new SiteReponse(), camelisSessionID, RightValue.ADMIN);
		if (!reponseErr.isOk()) {
			return reponseErr;
		}

		return new SiteReponse(ctx.getDataBase().getPortalisSite());
	}

	public static ActiveSiteReponse getActiveSite(String camelisSessionID) {
		// vérification du rôle
		ActiveSiteReponse reponseErr = PortalisCtx.getInstance().roleCheck(
				new ActiveSiteReponse(), camelisSessionID, RightValue.ADMIN);
		if (!reponseErr.isOk()) {
			return reponseErr;
		}

		return getActiveSite();
	}

	public static ActiveSiteReponse getActiveSite() {
		ActiveSite activeSite = new ActiveSite();
		activeSite.setApplications(ctx.getDataBase().getPortalisSite().getApplications());
		PingReponse pingReponse = getActiveLisServicesOnPortalis();
		activeSite.setActivelisServices(pingReponse.getLesServices());
		return new ActiveSiteReponse(activeSite);
	}

	public static VoidReponse adminPropertyFilesCheck(String camelisSessionID) {
		LOGGER.info("\nPortalisCtx AdminCmd.adminPropertyFilesCheck()");
		// vérification du rôle
		VoidReponse reponseErr = PortalisCtx.getInstance().roleCheck(
				new VoidReponse(), camelisSessionID, RightValue.ADMIN);
		if (!reponseErr.isOk()) {
			return reponseErr;
		}

		try {
			AdminProprietes.test();
		} catch (Throwable t) {
			String mess = " fin testAdminPropertyFileDirect() error ="
					+ t.getMessage();
			LOGGER.severe(mess+Util.stack2string( t));
			return new VoidReponse(mess, t);
		}
		return new VoidReponse();
	}

	public static VersionReponse getCamelisVersion(String camelisSessionID) {

		// vérification du rôle
		VersionReponse reponseErr;
		reponseErr = PortalisCtx.getInstance().roleCheck(new VersionReponse(),
				camelisSessionID, RightValue.ADMIN);
		if (!reponseErr.isOk()) {
			return reponseErr;
		}

		return new VersionReponse(LinuxCmd.getPortalisVersion());

	}

	public static PidReponse getCamelisProcessId(String camelisSessionId,
			String port) {
		Precondition errors = new Precondition();
		int portNum = errors.checkIntArg(port,
				"Le parametre %s doit être un entier, sa valeur est %s",
				XmlIdentifier.PORT(), port);
		if (errors.isOk()) {
			return getCamelisProcessId(camelisSessionId, portNum);
		} else {
			LOGGER.severe(errors.getMessagesAsString());
			return new PidReponse(XmlIdentifier.ERROR, errors.getMessages());
		}
	}

	public static PidReponse getCamelisProcessId(String camelisSessionId,
			int port) {
		LOGGER.info("==== AdminCmd ==== >> getCamelisProcessId(" + port + ")");

		PidReponse pidReponse = null;
		try {
			// vérification du rôle
			pidReponse = PortalisCtx.getInstance().roleCheck(new PidReponse(),
					camelisSessionId, RightValue.ADMIN);
			if (!pidReponse.isOk()) {
				return pidReponse;
			}

			Set<Integer> pids = new HashSet<Integer>();
			int pid = LinuxCmd.getCamelisProcessId(port);
			if (pid != 0)
				pids.add(pid);
			pidReponse.setPids(pids);
			return pidReponse;
		} catch (PortalisException e) {
			String mess = "Problèmes dans getCamelisProcessId()";
			LOGGER.severe(mess+Util.stack2string( e));
			return new PidReponse(mess, e);
		}

	}

	public static PortsActifsReponse getActifsPorts(String camelisSessionID) {
		try {

			// vérification du rôle
			PortsActifsReponse reponseErr = PortalisCtx.getInstance()
					.roleCheck(new PortsActifsReponse(), camelisSessionID,
							RightValue.ADMIN);
			if (!reponseErr.isOk()) {
				return reponseErr;
			}

			PortsActifsReponse reponse = new PortsActifsReponse(
					LinuxCmd.getActifsCamelisPortsOnPortalis());
			LOGGER.info("==== AdminCmd ==== >> getActifsPorts() =\n" + reponse);
			return reponse;
		} catch (PortalisException e) {
			String mess = "Problèmes dans getActifsPorts";
			LOGGER.severe(mess+Util.stack2string( e));
			return new PortsActifsReponse(mess, e);
		}
	}

	public static LoginReponse login(String initKey, String email,
			String password, HttpSession session) {
		try {
			LOGGER.info(new StringBuffer("\nPortalisCtx AdminCmd.login(")
					.append(initKey).append(", ").append(email).append(", ")
					.append(password).append(")").toString());
			// attention le login direct n'utilise les données de connections
			// que pour constuire la nouvelle session
			Precondition precondition = new Precondition();
			precondition.checkJavaArg(initKey != null,
					"Parameter %s is missing", XmlIdentifier.SESSION_ID());
			precondition.checkJavaArg(email != null, "Parameter %s is missing",
					XmlIdentifier.EMAIL());
			precondition.checkJavaArg(password != null,
					"Parameter %s is missing", XmlIdentifier.PASSWORD());
			if (!precondition.isOk()) {
				return new LoginReponse(XmlIdentifier.ERROR,
						precondition.getMessages());
			}
			LoginResult user = ctx.getDataBase().getUserInfo(email, password);
			if (user.getLoginErr().equals(LoginErr.ok)) {
				// démarrage d'un nouvel utilisateur avec une identification de
				// session http comme germe d'identification
				ActiveUser activeUser = new ActiveUser(user.getUserCore(),
						initKey);

				// creation de la session portalis associée
				Session portalisSession = Session
						.createPortalisSession(activeUser);
				ctx.userLogin(activeUser, session);
				return new LoginReponse(portalisSession);
			} else {
				return new LoginReponse(XmlIdentifier.ERROR, user.getLoginErr()
						.toString());
			}

		} catch (Exception e) {
			String mess = "Problèmes au login";
			LOGGER.severe(mess+Util.stack2string( e));
			return new LoginReponse(mess, e);
		}
	}

	public static PidReponse lsof(String camelisSessionID, int port)
			throws PortalisException {
		LOGGER.info("\nPortalisCtx AdminCmd.lsof(" + camelisSessionID + ", "
				+ port + ")");

		PidReponse pidReponse = PortalisCtx.getInstance().roleCheck(
				new PidReponse(), camelisSessionID, RightValue.ADMIN);
		if (!pidReponse.isOk()) {
			return pidReponse;
		}

		Precondition errors = new Precondition();
		errors.checkJavaArg(port > 0, "parametre %s doit être un entier > O",
				XmlIdentifier.PORT());
		if (errors.isOk()) {
			Set<Integer> pidList = LinuxCmd.lsof(port);
			pidReponse = new PidReponse(pidList);
			LOGGER.fine("sortie de lsof(" + port + ") pidList = "
					+ pidReponse.getPidsAsString());
			pidReponse.setJavaPid(LinuxCmd.getMyJavaPid());
			return pidReponse;
		} else {
			return new PidReponse(errors.getMessages());
		}

	}

	public static PingReponse getActiveLisServicesOnPortalis() {
		LOGGER.info("==== AdminCmd ==== >> getActiveLisServicesOnPortalis()");
		try {
			Set<PortActif> PortActifs = LinuxCmd
					.getActifsCamelisPortsOnPortalis();
			List<ActiveLisService> actifs = new ArrayList<ActiveLisService>();
			for (PortActif portActif : PortActifs) {
				int port = portActif.getPort();
				String host = portActif.getServer();
				PingReponse pingReponse = CamelisHttp.ping(host, port);
				if (pingReponse.getStatus().equals(XmlIdentifier.OK)) {
					ActiveLisService ActiveLisService = pingReponse
							.getFirstActiveLisService();
					if (ActiveLisService == null) {
						String mess = "Erreur interne, il devrait y avoir un service camelis : "
								+ pingReponse;
						throw new PortalisException(mess);
					}
					actifs.add(ActiveLisService);
				} else {
					LOGGER.info("==== AdminCmd ==== >> fin de getActiveLisServicesOnPortalis() reponse :\n"
							+ pingReponse);
					return pingReponse;
				}
			}
			PingReponse pingReponse = new PingReponse(actifs);
			LOGGER.info("==== AdminCmd ==== >> fin de getActiveLisServicesOnPortalis() reponse :\n"
					+ pingReponse);
			return pingReponse;
		} catch (Exception e) {
			String mess = "Problèmes dans getActiveLisServicesOnPortalis";
			LOGGER.severe(mess+Util.stack2string( e));
			return new PingReponse(mess, e);
		}
	}

	/*
	 * ------------------------------ startCamelis
	 */

	public static StartReponse startCamelis(String camelisSessionID,
			String serviceName, String webApplicationDataPath, String logFile)
			throws PortalisException {
		int port = PortService.choosePort();

		StartReponse rep = startCamelis(camelisSessionID, port, serviceName,
				webApplicationDataPath, logFile);
		return rep;
	}

	public static StartReponse startCamelis(String camelisSessionID,
			String port, String serviceName, String webApplicationDataPath,
			String logFile) {
		Precondition errors = new Precondition();
		int portNum = errors.checkIntArg(port,
				"Le parametre %s doit être un entier, sa valeur est %s",
				XmlIdentifier.PORT(), port);
		if (!errors.isOk()) {
			LOGGER.warning(errors.getMessagesAsString());
			return new StartReponse(XmlIdentifier.ERROR, errors.getMessages());
		}
		return startCamelis(camelisSessionID, portNum, serviceName,
				webApplicationDataPath, logFile);
	}

	public static StartReponse startCamelis(String camelisSessionID, int port,
			String serviceName, String webApplicationDataPath, String logFile) {
		StartReponse startReponse = new StartReponse();
		try {
			PortalisCtx.getInstance().roleCheck(startReponse, camelisSessionID,
					RightValue.ADMIN);
			if (!startReponse.isOk()) {
				return startReponse;
			}

			LOGGER.info(String.format(
					"==== AdminCmd ==== >> startCamelis(%s, %d, %s, %s, %s)",
					camelisSessionID, port, serviceName,
					webApplicationDataPath, logFile));
			Precondition precond = new Precondition();
			precond.checkJavaArg(
					// TODO validation
					CoreServiceNameValidator.getInstance()
							.validate(serviceName),
					"Incorrect service name : %s", serviceName);
			ActiveUser activeUser = PortalisCtx.getInstance().getUser(
					camelisSessionID);
			precond.checkJavaArg(activeUser != null, "Utilisateur actif null");
			LOGGER.fine(String.format("startCamelis1(%s, %d, %s, %s+Util.stack2string( %s))",
					camelisSessionID, port, serviceName,
					webApplicationDataPath, logFile));
			LOGGER.fine("startCamelis1 precond=" + precond.getMessagesAsString());

			if (!precond.isOk()) {
				LOGGER.warning("erreur de paramêtre : "
						+ precond.getMessagesAsString());
				startReponse = new StartReponse(XmlIdentifier.ERROR,
						precond.getMessages());
			} else {

				final String creatorEmail = activeUser.getUserCore().getEmail();
				final String adminKey = activeUser.getPortalisSessionId();

				if (port == 0) {
					port = PortService.choosePort();
				}

				LOGGER.fine(String.format("startCamelis2(%s, %s, %s+Util.stack2string( %s))",
						camelisSessionID, serviceName, webApplicationDataPath,
						logFile));
				if (logFile == null || logFile.length() == 0) {
					logFile = LOG_FILE_PATH;
					if (logFile == null || logFile.length() == 0) {
						LinuxInteractor
								.throwPortalisException("La propriété logFilePath doit être initialisée");
					}
				}

				LOGGER.fine(String.format("startCamelis3(%s, %s, %s+Util.stack2string( %s))",
						camelisSessionID, serviceName, webApplicationDataPath,
						logFile));
				if (webApplicationDataPath == null
						|| webApplicationDataPath.length() == 0) {
					webApplicationDataPath = WEBAPP_PATH;
					if (webApplicationDataPath == null
							|| webApplicationDataPath.length() == 0) {
						LinuxInteractor
								.throwPortalisException("La propriété webApplicationDataPath doit être initialisée");
					}
				}
				LOGGER.fine(String.format("startCamelis4(%s, %s, %s+Util.stack2string( %s))",
						camelisSessionID, serviceName, webApplicationDataPath,
						logFile));

				checkWebappsDir(webApplicationDataPath);

				String host = PortalisCtx.getInstance().getHost();
				String ActiveLisServiceId = host + ":" + port + "::"
						+ serviceName;
				LOGGER.fine("startCamelis\n(" + adminKey + ", " + creatorEmail
						+ ", " + serviceName + ", " + port + ", "
						+ webApplicationDataPath + ", " + logFile + ")");

				String[][] httpReqArgs = {
						{ XmlIdentifier.CREATOR(), creatorEmail },
						{ XmlIdentifier.PORT(), port + "" },
						{
								XmlIdentifier.KEY(),
								(activeUser != null ? activeUser
										.getPortalisSessionId() : "none") },
						{ XmlIdentifier.SERVICE_ID(), ActiveLisServiceId },
						{ XmlIdentifier.LOG(), logFile },
						{ XmlIdentifier.DATADIR(), webApplicationDataPath } };
				precond.setHttpReqArgs(httpReqArgs);

				if (!precond.isOk()) {
					LOGGER.warning("erreur de paramêtre : "
							+ precond.getMessagesAsString());
					startReponse = new StartReponse(XmlIdentifier.ERROR,
							precond.getMessages());
				} else {

					final String cmd = ServerConstants.CAMELIS_APPLICATION_NAME
							+ precond.getArgsAsLinuxCommandString();

					LinuxInteractor.executeCommand(cmd, false);
					// ici on ne peut pas attendre la fin du processus car, par
					// définition,
					// il ne va pas s'arrèter ...
					// si le processus getPid(port) ne répond pas tout de suite
					// on
					// va
					// essayer plusieurs fois
					// en se donnant un temps d'attente entre chaque essai
					// on abandonne au bout d'un temps

					PingReponse pingReponse = CamelisHttp.ping(
							PortalisCtx.getInstance().getHost(), port);

					ActiveLisService ActiveLisService = pingReponse
							.getFirstActiveLisService();
					if (ActiveLisService == null) {
						String mess = "Erreur interne, il devrait y avoir un ActiveLisServices sur le port "
								+ port;
						LOGGER.fine(mess);
						startReponse = new StartReponse(XmlIdentifier.ERROR, mess);

					} else {

						startReponse = new StartReponse(ActiveLisService);
						ctx.broadCastStartCamelisToUsers(startReponse,
								activeUser);
						LisServerChecker.getInstance().starting(startReponse);
					}
				}
			}
		} catch (Exception e) {
			String mess = e.getMessage();
			if (mess != null) {
				if (mess.contains(ErrorMessages.TEMOIN_ALREADY_IN_USE)) {
					mess = String.format("port %d is already in use", port);
				} else if (mess
						.contains(ErrorMessages.TEMOIN_UNKNOWN_OPTION_CREATOR)) {
					mess = String
							.format("Gros problème : start Camelis n'accepte plus l'option -creator ..., le port est : %d",
									port);
				}
				startReponse = new StartReponse(mess, e);
				LOGGER.severe(mess);
			} else {
				mess = "Echec lancement de Camelis  : " + e.getMessage();

				LOGGER.severe(mess+Util.stack2string( e));
				startReponse = new StartReponse(mess, e);
			}
			LOGGER.severe("fin de startCamelis() Camelis is not started, reponse :"
					+ startReponse);
			return startReponse;
		}
		LOGGER.info("==== AdminCmd ==== >> fin de startCamelis() Camelis is started, reponse :\n"
				+ startReponse);
		return startReponse;
	}

	public static PingReponse stopCamelis(String camelisSessionID, String port)
			throws PortalisException {
		Precondition errors = new Precondition();
		int portNum = errors.checkIntArg(port,
				"Le parametre %s doit être un entier, sa valeur est %s",
				XmlIdentifier.PORT(), port);
		if (!errors.isOk()) {
			LOGGER.warning(errors.getMessagesAsString());
			return new PingReponse(XmlIdentifier.ERROR, errors.getMessages());
		}
		return stopCamelis(camelisSessionID, portNum);
	}

	public static PingReponse stopCamelis(String camelisSessionID, int port)
			throws PortalisException {
		LOGGER.info("==== AdminCmd ==== >> stopCamelis(" + port + ")");
		Precondition errors = new Precondition();
		PingReponse pingReponse = PortalisCtx.getInstance().roleCheck(
				new PingReponse(), camelisSessionID, RightValue.ADMIN);
		if (!pingReponse.isOk()) {
			return pingReponse;
		}

		errors.checkJavaArg(
				port > 0,
				"stopCamelis le port doit être un entier >0,  sa valeur est %S",
				port);
		if (!errors.isOk()) {
			LOGGER.warning("erreur de paramêtre : " + errors.getMessagesAsString());
			return new PingReponse(XmlIdentifier.ERROR, errors.getMessages());
		}
		try {
			String host = PortalisCtx.getInstance().getHost();

			pingReponse = CamelisHttp.ping(host, port);
			LisServerChecker.getInstance().stopping(pingReponse);
			killCamelisProcessByPort(port);
			LOGGER.info("==== AdminCmd ==== >> fin de stopCamelis() Camelis is stopped, reponse :\n"
					+ pingReponse);

			return pingReponse;
		} catch (PortalisException e) {
			String mess = "Problèmes dans stop Camelis";
			LOGGER.severe(mess+Util.stack2string( e));
			return new PingReponse(mess, e);
		}
	}

	public static PingReponse killAllCamelisProcess(String camelisSessionID)
			throws PortalisException {
		LOGGER.fine("-------------------> cleaning up all CamelisProcess\n");
		PingReponse pingReponse = PortalisCtx.getInstance().roleCheck(
				new PingReponse(), camelisSessionID, RightValue.ADMIN);
		if (!pingReponse.isOk()) {
			return pingReponse;
		}
		Set<PortActif> actifs = LinuxCmd.getActifsCamelisPortsOnPortalis();

		for (PortActif actif : actifs) {
			LinuxCmd.killCamelisProcessByPid(actif.getPid());
		}

		pingReponse = getActiveLisServicesOnPortalis();
		LisServerChecker.getInstance().stopping(pingReponse);
		return pingReponse;
	}

	private static void killCamelisProcessByPort(int port)
			throws PortalisException {
		LOGGER.info("==== Linux ==== >> killCamelisProcessByPort(" + port + ")");
		int pid = LinuxCmd.getCamelisProcessId(port);
		if (pid == 0) {
			throw new PortalisException("Pas de processus Camelis sur port "
					+ port);
		}
		LinuxCmd.killCamelisProcessByPid(pid);
	}

	private static void checkWebappsDir(String webApplicationDataPath)
			throws PortalisException {
		if (webApplicationDataPath == null) {
			String mess = "checkWebappsDir() : WebApplicationDataPath is null";
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		File webappsDir = new File(webApplicationDataPath);
		if (!webappsDir.exists()) {
			LOGGER.warning("directory " + webApplicationDataPath
					+ " does not exists, creating it !");
			if (!webappsDir.mkdirs()) {
				String mess = "checkWebappsDir() : Cannot create directory "
						+ webApplicationDataPath;
				LOGGER.severe(mess);
				throw new PortalisException(mess);
			}
		}
	}

	/* gestion des utilisateurs */

}
