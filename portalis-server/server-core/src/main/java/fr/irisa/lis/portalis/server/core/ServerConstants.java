package fr.irisa.lis.portalis.server.core;

import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.system.linux.LinuxCmd;


public final class ServerConstants {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(ServerConstants.class.getName());
	
	public final static String CAMELIS_APPLICATION_NAME = AdminProprietes.portalis
			.getProperty("camelis.application.name");
	
	static {
		LinuxCmd.init(CAMELIS_APPLICATION_NAME);
	}
		
}
