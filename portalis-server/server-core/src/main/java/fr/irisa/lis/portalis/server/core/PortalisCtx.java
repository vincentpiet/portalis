package fr.irisa.lis.portalis.server.core;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Logger;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;

import fr.irisa.lis.portalis.admin.db.DataBase;
import fr.irisa.lis.portalis.admin.db.DataBaseSimpleImpl;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.data.UserCore;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCreatorReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class PortalisCtx {

	private static final Logger LOGGER = Logger.getLogger(PortalisCtx.class
			.getName());

	private DataBase dataBase = new DataBaseSimpleImpl();
	private String host;
	private int port;

	private int httpSessionCount;
	// key is portalisID
	private Map<String, ActiveUser> portalisUsers = new ConcurrentHashMap<String, ActiveUser>();
	private Map<String, HttpSession> httpSessions = new ConcurrentHashMap<String, HttpSession>();

	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static PortalisCtx instance = new PortalisCtx();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static PortalisCtx getInstance() {
		return SingletonHolder.instance;
	}

	private PortalisCtx() {
		super();
		this.httpSessionCount = 0;
		LOGGER.info("\nPortalisCtx initialisé : " + this);
	}

	public void init(String hostName, int port, String portalisAppliName, InputStream loggingConfFile) {
		this.host = hostName;
		this.port = port;
		PortalisService.getInstance().init(hostName, port, portalisAppliName, loggingConfFile);
	}

		public void sessionCreated(HttpSessionEvent se) {
			final HttpSession session = se.getSession();

			// session 5mn
			session.setMaxInactiveInterval(5 * 60);
			synchronized (this) {
				httpSessionCount++;
			}
			String id = session.getId();
			String message = new StringBuffer(
					"\nPortalisCtx New J2EE Session created ").append("ID: ")
					.append(id).append(" ").append("There are now ")
					.append("" + httpSessionCount).append(" live sessions")
					.toString();

			LOGGER.info(message);
		}

		public void sessionDestroyed(HttpSessionEvent se) {
			final HttpSession session = se.getSession();
			String httpSessionId = session.getId();
			destroyBothSessionsByHttpId(httpSessionId);
		}

	private synchronized void destroyBothSessionsByHttpId(String httpSessionId) {
		--httpSessionCount;
		for (String portalisId : httpSessions.keySet()) {
			if (httpSessions.get(portalisId).getId().equals(httpSessionId)) {
				try {
					destroyBothSessionsByPortalisId(portalisId);
				} catch (PortalisException e) {
					LOGGER.severe("error destroying portalisId " + portalisId);
				}
				break;
			}
		}
		String message = new StringBuffer(
				"\nPortalisCtx J2EE Session destroyed" + "ID = ")
				.append(httpSessionId).append(" ").append("There are now ")
				.append("" + httpSessionCount).append(" live sessions")
				.toString();
		LOGGER.info(message);
	}

	public synchronized void clearActiveUsers() throws PortalisException {
		LOGGER.info("\nPortalisCtx clearActiveUsers");
		for (String portalisId : portalisUsers.keySet()) {
			destroyBothSessionsByPortalisId(portalisId);
		}
		portalisUsers.clear();
		httpSessions.clear();
		LOGGER.fine("\nPortalisCtx fin clearActiveUsers");
	}

	public synchronized void userLogout(String portalisId)
			throws PortalisException {
		destroyBothSessionsByPortalisId(portalisId);
	}

	private ActiveUser destroyBothSessionsByPortalisId(String portalisId)
			throws PortalisException {
		LOGGER.info("\nPortalisCtx removing "
				+ portalisId
				+ " user="
				+ (portalisUsers.get(portalisId) != null ? portalisUsers
						.get(portalisId).getUserCore().getEmail() : "null"));
		broadcastUserKeyDeletion(portalisUsers.get(portalisId));
		HttpSession session = httpSessions.get(portalisId);
		// remove the three stateValues
		session.removeAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		httpSessions.remove(portalisId);
		ActiveUser user = portalisUsers.remove(portalisId);
		session.invalidate();
		return user;
	}

	public synchronized void userLogin(ActiveUser activeUser,
			HttpSession session) throws PortalisException {
		LOGGER.info("\nPortalisCtx registering " + activeUser.toShortString());
		String portalisId = activeUser.getPortalisSessionId();
		// set the three stateValues
		session.setAttribute(XmlIdentifier.CAMELIS_SESSION_ID, portalisId);
		portalisUsers.put(portalisId, activeUser);
		httpSessions.put(portalisId, session);
		broadcastUserKeyCreation(activeUser);
	}

	public synchronized ActiveUser getUser(String portalisId) {
		return portalisUsers.get(portalisId);
	}

	public synchronized ActiveUser[] getUsers() {
		return portalisUsers.values().toArray(
				new ActiveUser[portalisUsers.size()]);
	}

	private void broadcastUserKeyCreation(ActiveUser activeUser)
			throws PortalisException {
		LOGGER.fine(String.format(
				"\nPortalisCtx broadcastUserKeyCreation() : %s",
				activeUser.toShortString()));

		// On calcule dynamiquement les services courramment actifs
		List<ActiveLisService> services = AdminCmd
				.getActiveLisServicesOnPortalis().getLesServices();

		// boucle sur tous les services actifs
		for (ActiveLisService service : services) {
			registerUserOnService(activeUser, service);
		}
		LOGGER.info(String.format(
				"\nPortalisCtx fin broadcastUserKeyCreation() : %s",
				activeUser.toShortString()));
	}

	public synchronized void broadCastStartCamelisToUsers(
			StartReponse startReponse, ActiveUser whoIsCreator)
			throws PortalisException {
		LOGGER.info("\nPortalisCtx broadCastStartCamelisToUsers creator "
				+ whoIsCreator.toShortString());

		String creatorKey = whoIsCreator.getPortalisSessionId();
		for (String key : portalisUsers.keySet()) {
			if (!key.equals(creatorKey)) {
				ActiveUser user = portalisUsers.get(key);
				LOGGER.fine("\nPortalisCtx broadCastStartCamelisToUsers user "
						+ user.toShortString());
				registerUserOnService(user, startReponse.getActiveLisService());
			}
		}
		LOGGER.info("\nPortalisCtx fin broadCastStartCamelisToUsers");
	}

	private void registerUserOnService(ActiveUser activeUser,
			ActiveLisService service) throws PortalisException {
		UserCore user = activeUser.getUserCore();
		LOGGER.fine(String.format("\nPortalisCtx registerUserOnService %s %s",
				user.getEmail(), service.getFullName()));
		String userKey = activeUser.getPortalisSessionId();
		String creator = service.getCreator();
		// création d'une session admin temporaire

		Session newCamelisSess = Session
				.createAnonymousAdminCamelisSession(service);

		// on prend la main sur le service
		ResetCreatorReponse resetCreatorReponse = CamelisHttp.resetCreator(
				newCamelisSess, creator);
		if (!resetCreatorReponse.isOk()) {
			String mess = String
					.format("Login error : resetCreator impossible,\nsession is :%s\ncreator is : %s\nmess is : %s",
							newCamelisSess, creator,
							resetCreatorReponse.getMessagesAsString());
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		RegisterKeyReponse registerKeyResponse = CamelisHttp.registerKey(
				newCamelisSess, userKey, user.getRight(service.getFullName()));
		if (!registerKeyResponse.isOk()) {
			String mess = String
					.format("Login error : registerKey impossible,\nsession is :%s\nuserKey is : %s\nmess is : %s",
							newCamelisSess, userKey,
							registerKeyResponse.getMessagesAsString());
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
	}

	private void broadcastUserKeyDeletion(ActiveUser activeUser)
			throws PortalisException {
		LOGGER.fine(String.format(
				"\nPortalisCtx broadcastUserKeyDeletion() activeUser = %s",
				activeUser.toShortString()));
		String userKey = activeUser.getPortalisSessionId();

		// On calcule dynamiquement les services courramment actifs
		List<ActiveLisService> services = AdminCmd
				.getActiveLisServicesOnPortalis().getLesServices();

		// boucle sur tous les services actifs
		for (ActiveLisService service : services) {
			String creator = service.getCreator();
			Session newCamelisSess = Session
					.createAnonymousAdminCamelisSession(service);

			// on prend la main sur le service
			ResetCreatorReponse resetCreatorReponse = CamelisHttp.resetCreator(
					newCamelisSess, creator);
			if (!resetCreatorReponse.isOk()) {
				String mess = String
						.format("Login error : resetCreator impossible,\nsession is :%s\ncreator is : %s\nmess is : %s",
								newCamelisSess, creator,
								resetCreatorReponse.getMessagesAsString());
				LOGGER.severe(mess);
				throw new PortalisException(mess);
			}

			DelKeyReponse delKeyReponse = CamelisHttp.delKey(newCamelisSess,
					userKey);
			if (!delKeyReponse.isOk()) {
				String mess = String
						.format("Login error : setRole impossible,\nsession is :%s\nuserKey is : %s\nmess is : %s",
								newCamelisSess, userKey,
								delKeyReponse.getMessagesAsString());
				LOGGER.severe(mess);
				throw new PortalisException(mess);
			}
		}
		LOGGER.fine(String.format(
				"\nPortalisCtx fin broadcastUserKeyDeletion() activeUser = %s",
				activeUser.toShortString()));
	}

	public <T extends VoidReponse> T roleCheck(T reponse,
			String camelisSessionId, RightValue role) {
		LOGGER.fine("\nPortalisCtx roleCheck(" + camelisSessionId + ", "
				+ role.toString() + ")");
		ActiveUser activeUser = getUser(camelisSessionId);
		if (activeUser == null) {
			String mess = "Erreur interne, pas d'utilisateur pour la clé "
					+ camelisSessionId;
			LOGGER.severe(mess);
			reponse.addMessage(XmlIdentifier.ERROR, mess);
		} else if (!Session.roleChecker.isAuthorized(activeUser.getUserCore(),
				role)) {
			String mess = "no " + role.toString() + " right for "
					+ reponse.getClass().getName() + "";
			LOGGER.warning(mess);
			reponse.addMessage(XmlIdentifier.ERROR, mess);
		}
		return reponse;
	}

	public DataBase getDataBase() {
		return dataBase;
	}

	public void setDataBase(DataBase dataBase) {
		this.dataBase = dataBase;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

}
