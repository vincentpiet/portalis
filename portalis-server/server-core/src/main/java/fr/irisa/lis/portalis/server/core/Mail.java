package fr.irisa.lis.portalis.server.core;

import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail {
	
	private String host = "smtp.inria.fr";
	private Integer port;
	private boolean authentification = false;
	private String password;
	
	public Mail() {}
	
	public Mail(String host) {
		this.host = host;
	}

	public Mail(String host, Integer port) {
		this.host = host;
		this.port = port;
	}

	public Mail(String host, String password) {
		this(host);
		this.authentification = true;
		this.password = password;
	}

	public Mail(String host, Integer port, String password) {
		this(host, port);
		this.authentification = true;
		this.password = password;
	}
	
	/**
	 * Send an email with attached content.
	 * 
	 * @param from Sender's email ID
	 * @param to Recipient's email ID
	 * @param subject text subject
	 * @param content text content
	 * @param file path of file to be attached
	 * @param fileName attachmentName
	 */
	public void send(final String from, String to, String subject, String content,
			String file, String fileName) {


		Properties props = new Properties();
		props.put("mail.smtp.auth", authentification);
		props.put("mail.smtp.starttls.enable", true);
		props.put("mail.smtp.host", host);
		if (port!=null) {
			props.put("mail.smtp.port", port.toString());  // port eventually
		}

		Session session = getSession(from, props);

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(content);

			MimeBodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(file);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);

			message.setContent(multipart);

			Transport.send(message);

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Send a simple email.
	 * 
	 * @param from Sender's email ID
	 * @param to Recipient's email ID
	 * @param subject text subject
	 * @param content text content
	 * @param file path of file to be attached
	 * @param fileName attachmentName
	 */
	public void send(final String from, String to, String subject, String content) {

		// Get system properties
		Properties properties = System.getProperties();

		// Setup mail server
		properties.setProperty("mail.smtp.host", host);
		properties.put("mail.smtp.auth", authentification);

		// Setup port
		if (port!=null) {
			properties.put("mail.smtp.port", port.toString());  // port eventually
		}

		Session session = getSession(from, properties);

		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);

			// Set From: header field of the header.
			message.setFrom(new InternetAddress(from));

			// Set To: header field of the header.
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(
					to));

			// Set Subject: header field
			message.setSubject(subject);

			// Now set the actual message
			message.setText(content);

			// Send message
			Transport.send(message);
			
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}

	private Session getSession(final String from, Properties properties) {
		Session session;
		if (authentification) {
			session = Session.getInstance(properties,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(from, password);
					}
				});
		} else {
			session = Session.getDefaultInstance(properties);
		}
		return session;
	}
}
