.. -*- mode: rst -*-

===============================================
Portalis - outils et principes de développement
===============================================

.. sectionauthor:: Yves Bekkers <bekkers@irisa.fr>
.. sectionauthor:: Benjamin Sigonneau <sigonnea@irisa.fr>


.. only:: html

   :download:`[version PDF] <_build/pdf/toolsAndDeveloppementPrinciples.pdf>`

.. _Camelis:            http://www.irisa.fr/LIS/ferre/camelis/
.. _Objective Caml:   http://caml.inria.fr/ocaml/
.. _Ocaml:   http://caml.inria.fr/ocaml/
.. _GWT:                https://developers.google.com/web-toolkit/
.. _reStructuredText: http://docutils.sourceforge.net/rst.html
.. _reStructuredText quick refererence guide: http://docutils.sourceforge.net/docs/user/rst/quickref.html
.. _reStructuredText directives: http://docutils.sourceforge.net/docs/ref/rst/directives.html
.. _Introduction à reStructuredText: http://docutils.sourceforge.net/sandbox/wilk/french/quickstart-fr.html
.. _Eclipse: http://www.eclipse.org

.. |chantier| image:: images/chantier.gif
   :align: middle
   :width: 100 px


------
Outils
------

Langages de Programation et langages à balises
==============================================

Java, Servlets, pages JSP
-------------------------

Pour de sombres raisons de licence, les systèmes *Linux* possèdent leur propre *Java* appelé *Open jdk*.
Cette version de Java n'est pas recommandée pour les développements car elle est toujours en retard sur
la version officielle de *Java* distribuée librement par *Oracle*.
Pour les développements *Java* il est impératif d'installer *Oracle jdk* et de laisser de coté *Open jdk*.
Voir ''_.

Librairies java tiers utilisées dans Portalis
'''''''''''''''''''''''''''''''''''''''''''''

Les serveurs Portalis sont écrits en Java/J2EE version 7. On utilise exclusivement des librairies open-sources.
Voici une liste des principales librairies utilisées :

- **Junit 4**, **hamcrest-core** : Pour les tests unitaires lancés par *Maven*.
- **common-logging**, **log4J** : Librairies de log dévelopées par le Projet *Apache*.
- **validation-api** : Librairie de validation des données développée par *Hibernate*.
  Sera intégrée dans les prochaines versions de Java. Elle est aussi utilisée par les versions récentes de GWT.
- **dom4j**, **xml-api**, **jaxen** : librairies DOM adaptées à Java.


Objective Caml - Ocaml
----------------------

Les Serveurs d'information logique, Camelis et Portalis sont écrits en Ocaml_ version 12. Les makefile de ces modules
utilisent la librairie *Ocaml-findlib*. Voici les librairies Ocaml uitlisées :

- *ocaml-findlib* : voir ocaml-findlib_. Sous Fedora faire ``yum install ocaml-findlib-devel``.
- *nethttpd* : Sous Fedora faire ``yum install ocaml-ocamlnet-nethttpd-devel``.
- *xml-light* : Sous Fedora faire ``yum install ocaml-xml-light-devel``.
- *camelis* :

.. _ocaml-findlib: http://projects.camlcity.org/projects/dl/findlib-1.2.5/doc/ref-html/r17.html

XML
---

Les protocoles de communication entre serveurs et entre serveurs et clients sont des dialectes XML. Voir :doc:`protocole-serveur-camelis-xml`.

Outils de développement
=======================

Google Web Toolkit - GWT
------------------------

Outil de développement de site WEB. Les clients sont écrits en Java et compilés par *GWT* en *JavaScript+HTML*. 6 versions de compilateurs génèrent le code
Javascript spécifique à chaque navigateur.

Eclipse
-------

Notre outil de développement est *Eclipse*. Nous utilisons actuellement la version *Indigo* V3.7.
La version *Juno* V4.2 plus récente étant trôp lente ...

Eclipse supporte les développements J2EE, avec des éditeurs de pages JSP, des lanceurs d'application WEB.
Il supporte aussi *reStructuredText*, *GWT*, *Maven* et *git*, tous les outils que nous utilisons.

Problème de validation des pages jsp résolu dans Eclipse
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Une page JSP montre des erreurs de validation du genre "type javax.servet not accessible"

Solution
````````
.. _how to convert java project to web project in eclipse: http://www.mkyong.com/java/how-to-convert-java-project-to-web-project-in-eclipse/
.. _Java - How to change context root of a dynamic web project in eclipse: http://stackoverflow.com/questions/2437465/java-how-to-change-context-root-of-a-dynamic-web-project-in-eclipse

Le problème vient de la libairie ``jsp-api`` qui n'est pas disponible

Elle est habituellement fournie par un accès aux librairies de Tomcat.
Il y a plusieurs moyens de la rendre disponible à la compilation.
Une des solutions consiste à donner la nature webProject au projet.
On trouve le moyen de faire cela sur la page `how to convert java project to web project in eclipse`_.
Voir aussi `Java - How to change context root of a dynamic web project in eclipse`_

Voici comment faire :

1. Edit the ``.project`` file

2. Copy ``web project`` following facet ``<buildCommand>`` element and paste within ``java project`` ``.project`` ``buildSpec`` tag

   .. code-block:: xml

      <buildCommand>
         <name>org.eclipse.wst.common.project.facet.core.builder</name>
         <argumejnts>
         </arguments>d
      </buildCommand>

3. Copy ``web project`` following facet ``<nature>`` element and paste within``java project .project natures`` tag

   .. code-block:: xml

      <nature>org.eclipse.wst.common.project.facet.core.nature</nature>

   P.S Be careful of the ``.project`` xml tag, make sure you paste into a correct location.

4. Save it

5. Modify your project properties as follows :

   - Right click on ``Java Project``, click ``properties``.
   - Select ``Project Facets`` and click ``modify project`` button.
   - Click ``Dynamic Web Module`` and ``Java`` checkboxes.
     .. image:: images/toolsAndDeveloppementPrinciples/convert-java-project-to-web-project-in-eclipse.jpg

Finished, Now your java project has been converted into a web project.

Maven
-----

Voir :doc:`maven`.

Gitj
----

Voir :doc:`git-seb`.

Conteneur de servlets Tomcat
----------------------------

Pour supporter nos applications J2EE en exploitation nous utilisons *Tomcat 6* un conteneur de Servlets développé par *Apache*.

Noter que n'importe quel autre conteneur de servlets peut être utilisé. Ils sont nombreux ...

Documentation
=============

Sphinx, ReStructuredText et Docutils
------------------------------------

.. _Sphinx: http://sphinx-doc.org/

Pour la documentation nous utilisons Sphinx_, un outil basé sur le
langage reStructuredText_ et la librairie docutils_ écrite en *Python*
qui fourni des transformateurs de documents *reStructuredText* en divers
formats de publication sur le WEB. *reStructuredText* est un langage
textuel à base de quelques marques et quelques schémas de strutures
auxquels sont associés les outils de la librairie *Docutils*.

Voici une liste non exhaustive de documents présentant rapidement quelques fonctionalités du langage *reStructuredText* :

- `reStructuredText quick refererence guide`_
- `reStructuredText directives`_
- `Introduction à reStructuredText`_ (en français)

Un des avantages de Sphinx est qu'il intègre en natif la coloration syntaxique de code pour les sorties HTML. Tous les langages
de programmation et les langages à balise les plus courants sont supportés. Le nombre de langages acceptés est
impressionnant, voir pygments_.

Pour la liste des langages reconnus voir `Pygments languages`_.

.. _pygments: http://pygments.org/
.. _Pygments languages: http://pygments.org/languages/


Pour les liens internes entre documents différents de Portalis nous
utilisons dans les sources ``.rst`` la directive ``:doc:`` ou la
directive ``:ref:`` de Sphinx.


Maven et Sphinx
'''''''''''''''

.. todo::

   |chantier| Voir le plugin pour Maven sphinx-maven_ qui permet de
   faire générer un site de documentation par Maven. Question : doit-on
   l'intégrer dans notre environnement de développement ?

.. _sphinx-maven: http://tomdz.github.com/sphinx-maven/


Outils d'exploitation
=====================

Conteneur de servlets Tomcat
----------------------------

Pour supporter nos applications J2EE en exploitation nous utilisons *Tomcat 6* un conteneur de Servlets développé par *Apache*.

Noter que n'importe quel autre conteneur de servlets peut être utilisé. Ils sont nombreux ...

----------------------------
Structure du projet Portalis
----------------------------

Schéma général
==============

Portalis comporte un ensemble de modules qui permettent de construire des Clients, des serveurs *Portalis* et des serveurs Camelis.xmlServer.
Voici le schéma général de cette application :

.. image:: images/schemaGeneralPortalis.jpg
   :width: 16 cm

Les serveurs Camelis_ sont construits en `Objective Caml`_ (*Ocaml*) et ont été testé pour la version 12 de *Ocaml*.
Les serveurs Portalis sont construits en *Java* + *J2EE*. Ils ont été testés pour la version 7 de Java.

Les :doc:`communications serveurs/serveurs et clients/serveurs <protocole-serveur-camelis-xml>` se fond dans un dialecte XML.

Nous rappelons ci-dessous les règles de nommage suggérées par *Maven* et que nous reprenons à notre compte pour le nommage des modules de Portalis.

.. include:: textes/reglesDeNommageEnMaven.rst

Structure modulaire du projet Portalis
--------------------------------------

Chaque module de Portalis est un *artifact* au sens *Maven*. Voici la liste des modules qui constituent le projet *Portalis* ainsi que leur identification :

- **root** : l'artifact racine (au sens Maven) du projet Portalis. Il ne génére qu'un fichier ``pom.xml`` qui est propagé par Maven
  dans les divers modules de Portalis au moment du déploiement. IL est alors réutilisé pour le déploiement des autres modules. Dans Maven
  il est déclaré comme le parent de tous les autres modules. Ce qui permet partager entre tous les modules de Portalis un ensemble de propriétés
  communes, propres au déploiement, par Maven, de Portalis.4) Save it.

- **doc** : le module de documentation de Portalis qui utilise la suite docutils_ et le langage reStructuredText_.
- **admin.core** : Contient la définition des objets métiers pour l'administration de Portalis. On y trouve aussi les méthodes de sérialisation et
  désérialisation en XML des instances de ces objets métier.

  Contient aussi une implementation en Java de l'interface d'administration. Chaque méthode implémente l'envoie en Java d'une requéte HTTP et désérialise
  le résultat XML en des instances de classes Java. Chaque méthode comporte comme paramêtres une URL en plus des paramétres inhérents à la commande.

  Contient aussi la définition de l'interface d'accès à la base de données.
- **admin.xmlDatabase** : implémente dans des fichiers XML la rémanence des données d'administration de Portalis. Ce module pourrait être remplacé à
  terme par une base de données relationnelle telle que *Mysql*.
- **admin.XMLserver** : Serveur d'administration pour Portalis sans interface utilisateur. On communique avec lui par des requétes HTTP. Il répond en XML.
- **camelis.core** : Contient la définition des objets métiers pour l'echange d'information entre clients et serveurs Camelis-XML. On y trouve aussi
  les méthodes de sérialisation et désérialisation en XML des instances de ces objets métier.
- **camelis** : Implementation en Java du :doc:`protocole de communication avec les serveurs Camelis-xml <protocole-serveur-camelis-xml>`. Chaque méthode implémente l'envoie en Java
  d'une requéte HTTP et désérialise le résultat XML en des instances de classes Java. Chaque méthode comporte une URL
  et les paramétres inhérents é la commande.
- **camelis.XMLserver** : Serveur de dépot de données Camelis. Les programmes sont écrits en Ocaml_.

.. _docutils: http://docutils.sourceforge.net/

Serveurs d'information logique
==============================


Structure des serveurs Camelis
------------------------------

.. todo::

   |chantier| Mettre ici comment sont construits les serveurs Camelis (librairies Ocaml etc. ...).

Gestion des accès concurents
----------------------------

.. todo::

   |chantier| Mettre ici comment les accès partagés sont rendus surs (gestion de la synchronisation des accès concurents).

Librairies utilisées
--------------------

.. todo::

   |chantier| Mettre ici la liste des librairies Ocaml utilisées.

Serveurs Portalis
=================

les serveurs *Portalis* sont construits en *Java* et à l'aide de servlets_ et de `pages JSP`_.

Pour les test nous utilisons Tomcat_ comme serveur J2EE.

.. _servlets: http://fr.wikipedia.org/wiki/Servlet
.. _pages JSP: http://fr.wikipedia.org/wiki/JavaServer_Pages
.. _Tomcat: http://tomcat.apache.org/



Clients
=======

Client GWT
----------

Gestion des boutons de navigation du navigateur
'''''''''''''''''''''''''''''''''''''''''''''''

Suppose that you use a GWT widget that generates SelectionEvents.
For example the GWT ``TabPanel`` generates ``SelectionEvent<Integer>`` events.
Then, in your GWT application, perforvm the 2 following steps

step 1
``````

Add a history token to the history stack whenever you receive a
``SelectionEvent`` from your ``TabPanel``. One can do that by adding a new ``SelectionHandler``
to the ``TabPanel``. This is simply done as follows :

.. literalinclude:: java/SelectionHandlerExample.java
   :language: java
   :lines: 14-18

step 2
``````

Create an object that implements the ``ValueChangeHandler`` interface,
parses the new token (available by calling ``ValueChangeEvent.getValue()``)
and changes the application state to match.

.. literalinclude:: java/SelectionHandlerExample.java
   :language: java
   :lines: 20-41

Validation des données d'entrée
'''''''''''''''''''''''''''''''

.. todo::

   |chantier| Mettre ici comment est effectuée la validation des données d'entrée.


---------------------------------
Systèmes de développement - Linux
---------------------------------

Le développement est testé pour deux systèmes *Linux*, *Fedora 17* et *Debian Wheezy*.

Installation sous Fedora 17
===========================

.. _forge INRIA Camelis: https://gforge.inria.fr/projects/camelis/
.. _Tomcat 6 & Fedora 14 frustrations: http://caffiendfrog.blogspot.fr/2011/02/tomcat-6-fedora-14-frustrations.html
.. _How to install tomcat6 and make it work on Fedora 16: http://forums.fedoraforum.org/showthread.php?t=277855
.. _Apache Tomcat: http://tomcat.apache.org/
.. _Java Platform (JDK): http://www.oracle.com/technetwork/java/javase/downloads/index.html
.. _ocaml-taglib: http: //sourceforge.net/projects/savonet/files/ocaml-taglib/
.. _Ocaml LablGtk: http://lablgtk.forge.ocamlcore.org/
.. _Gnome: http://www.gnome.org/about/


**Note importante** Toutes les installations décrites ci-dessous doivent, bien entendu, être effectuées en tant que ``root``.


**Descktop kde** : Si vous préférez *Kde* à *Gnome* :

   .. code-block:: sh

      yum install @kde-desktop

**Ocaml, Ocamlfind** : Chargez l'ensemble *Ocaml*, *Ocamlfind* par :

   .. code-block:: sh

      yum install ocaml-findlib-devel

   Puis charger les libraires *Ocaml* requises. (voir `Objective Caml - Ocaml`_).

**camelis** : Pour installer *Camelis* procéder comme suit :

   1. Installer les dépendances de Camelis :

      - *Ocaml* ``ocamlfind`` (fait à l'étape précédente)
      - *Ocaml* ``camlp4`` : A installer par ``yum install ocaml-camlp4-devel.x86_64``
      - *Linux* et *Ocaml* ``taglib`` : Librairies utilisées par les *Tranducers* audio de *Camelis* :

        - *Linux* ``taglib`` : A installer par ``yum install taglib-devel taglib-devel.x86_64``
        - *Ocaml* ``taglib`` : Il n'y a pas de paquet installable par ``yum``. Installer à partir de la
          page `ocaml-taglib`_. Cette page appartient au projet *Liquid-soap*, un ensemble de librairies *Ocaml*
          pour manipuler des média. Elle est trompeuse :

          - il y a en haut un lien pour télécharger *Liquidsoap* (mais on n'en veut pas) ;
          - plus bas, les liens vers les différentes versions de *ocaml-taglib*
            (mais il n'y a que les numéros de versions, pas le nom). Prendre la dernière,
            au jour où nous parlons, il s'agissait de la version ``V 0.2.0``.

          *Attention* le fichier ``README`` donne des informations incomplètes ... Il ne dis pas d'exécuter
          ``./configure`` pour produire le fichier ``makefile``. Donc, installer *ocaml-taglib* par :

          .. code-block:: sh

             ./configure
             make all
             make install

          Les fichers ``taglib.cmxs`` et ``taglib.cmxa`` n'ayant pas été produits par cette instalation,
          Nous devons les produire. On cherche d'abord où sont installés les bindings pour *taglib* :

          .. code-block:: sh

             ocamlfind query taglib

          On se déplace dans le répertoire de la librairie de *Ocaml-taglib* puis
          on produit les fameux ``cmxs``

          .. code-block:: sh

             cd $(ocamlfind query taglib)
             ocamlopt -ccopt -L. -shared -linkall -o taglib.cmxs taglib.cmxa


      - *Ocaml* ``Lablgtk2`` : Librairie `Ocaml LablGtk`_ utilisée par l'interface *Glis*.
        Cette librairie est une intervface sur la librairie *Linux GTK+* du projet Gnome_. Installer d'abord la librairie
        *Linux GTK+* comme suit :

        .. code-block:: sh

           yum install gtk+-devel.x86_64 gtk+extra-devel.x86_64

        Puis, aprés avoir charger la librairie *Ocaml LablGtk* depuis son site, intallez là comme suit :

        .. code-block:: sh

           ./configure && make world

      - *Linux* ``jhead`` : Librairie utilisée par les transducers JPEG de *Camelis* pour lire les tag dans les images ``jpeg``.
        Installer `jhead`` par la commande :

        .. code-block:: sh

           yum install jhead

      - ``Logfun`` Une librairie *Ocaml* de *Logiques* pour *Camelis* faite par l'équipe LIS. S'installe automatiquement
        en même temps que *Camelis*

   2. Charger *Camelis* depuis la `forge INRIA Camelis`_. Choisissez la dernière version ``camelis-1.5.tar.gz`` puis :

   3. En tant qu'utilisateur ``root``, décompresser ce fichier dans le répertoire ``/usr/local``.
      Ainsi *Camelis* est installé dans le répertoire ``/usr/local/camelis-1.5``.

   4. Se placer dans le répertoire ``/usr/local/camelis-1.5`` puis faire ``make`` suivi de ``make install``.

   5. Optionnellement vous pouvez installer l'interface *Glis*. Pour cela, se placer dans le répertoire
      ``/usr/local/camelis-1.5/interface`` puis faire ``make`` suivi de ``make install``.

**git** : Si vous voulez utiliser *Git* en dehors d'*Eclipse* :

.. code-block:: sh

   yum install git

**qgit** : Si vous en sentez le besoin, un navigateur pour les dépos *Git* :
Installation de Orale JDK
.. code-block:: sh

   yum instal qgit

**maven2** : Si vous voulez utiliser *Maven2* en dehors d'*Eclipse* :

.. code-block:: sh

   yum install maven2

**Java** :  Voir la section `Installation de Java : Oracle JDK`_

**Tomcat** : **Attention** ne pas installer *Tomcat* par ``yum``.
   Il est préférable d'installer une version de développement dans votre espace utilisateur.

   ``yum`` sous *Fedora 17* installe *Tomcat6* qui n'est pas correct en développement.
   Ce package est préparé de manière générique pour tout *Linux* et ne marche pas pour *Linux* *Fedora* ...
   Voir `Tomcat 6 & Fedora 14 frustrations`_ mais surtout `How to install tomcat6 and make it work on Fedora 16`_
   pour vous convaincre de ne pas utiliser la commande ``yum install tomcat6``.

   Pour notre part nous avons installé la version ``tomcat-7.0.32``. Procédez comme suit pour une installation :

   1. Chargez le zip de *Tomcat 7* sur le site `Apache Tomcat`_.
   2. Décompresser le fichier ``apache-tomcat-7.0.32.tar.gz`` (ou une version plus récente)
      dans le répertoire ``/usr/local``. Ainsi, pour nous, Tomcat est installé dans le répertoire
      ``/home/$MY_USER/apache-tomcat-7.0.32``.
   3. Dans le répertoire ``/usr/bin`` creez le ficher exécutable ``tomcat7`` avec le contenu suivant :

      .. code-block:: sh

         #!/bin/sh
         CATALINA_HOME=/home/bekkers/apache-tomcat-7.0.32
         JAVA_HOME=/usr/lib/jvm/j2sdk1.7-oracle/jre
         export CATALINA_HOME JAVA_HOME
         case $1 in
            start)
              if [ -x ${CATALINA_HOME}/bin/startup.sh ]; then
                 echinstallation de orale jdko "Starting Tomcat..."
                 ${CATALINA_HOME}/bin/startup.sh
              fi
              exit
              ;;
           stop)
              if [ -x ${CATALINA_HOME}/bin/shutdown.sh ]; then
                 echo "Stopping Tomcat..."
                 ${CATALINA_HOME}/bin/shutdown.sh
              fi
              exit
              ;;
           restart)
              $0 stop; $0 start;
              ;;
           "")Installation de Orale JDK
              $0 stinstallation de orale jdkart
              exit
              ;;
           *)
              echo "tomcat [start|stop|restart]"
              exit
              ;;
         esac

      Ajustez les variables ``$CATALINA_HOME`` et ``$JAVA_HOME`` en fonction de votre installation.
      Dans notre sytème nous avions les valeurs :

     .. code-block:: sh

           CATALINA_HOME=/home/bekkers/apache-tomcat-7.0.32
           JAVA_HOME=/usr/lib/jvm/j2sdk1.7-oracle/jre


   4. Exécutez *Tomcat7* par la commande ``tomcat7 start[stop]``

   .. _Comment voir si Tomcat est lancé:

   6. Pour voir si *Tomcat7* est bien lancé essayer les 3 méthodes suivantes

      a) Tester le port 8080 par la commande ``netstat`` :

         .. code-block:: console

            $ netstat -anp | grep 8080
            tcp6       0      0 :::8080                 :::*

      b) Tester le port 8080 par la commande ``lsof`` :

         .. code-block:: console

            $ lsof -i tcp:8080 -Fp
            P5434

         5434 est numéro de processus qui supporte *Tomcat*.

      c) Si vous avez mis en place le système tel que proposé ci-dessus, une application
         d'aministration sera lancée automatiquement au lancement de *Tomcat*.
         Utilisez alors un navigateur et visitez l'adresse ``http://localhost:8080/``.
         La page d'accueil du system d'administration de *Tomcat* doit alors s'afficher :

         .. image:: images/toolsAndDeveloppementPrinciples/tomcat.jpg

**Eclipse** :: **Attention** ne pas installer *Eclipse* par ``yum``.

   *Yum* charge *Eclipse* *juno* (V4.2) malheureusement connu comme beaucoup plus lent que les versions précédentes
   d'*Eclipse*. Nous recommandons de charger *Eclipse* *indigo* (V3.7)
   directement sur le site d'*Eclipse*. Prendre *EInstallation de Orale JDKclipse J2EE* puis mettre les plugins:

   - **maven2** Le Plugin des développements *Maven2* sous *Eclipse*
   - **reStructuredText editor** un éditeur avec coloration syntaxique pour *reStructuredText*.
   - **GPE Google Plugin for Eclipse** Pour le développement de clients *GWT*
   - **ObjectAId** pour la décompilations de classes en diagrammes de classes *UML*.
   - **git** Si vous voulez utiliser *Git* sous *Eclipse*, mais cela n'est pas indispensable.

.. _Rendre accessibles à Maven les librairies SLF4J:

   *Attention* Le plugin *Maven2* utilise la libairie `Simple Logging Facade for Java (SLF4J)`_ mais ne la charge pas !
   Procédez comme suit pour son installation sous *Java* d'*Oracle*.

      1. Charger la dernière version sur le site de *SLF4J*. En ce qui nous concerne il s'agissait de la
         version ``slf4j-1.7.2``.

      2. Décompresser la librairie sous /usr/lib/jvm/j2sdk1.7-oracle. Ainsi elle se trouve installée sous
         ``/usr/local/slf4j-1.7.2``

      3. Supposez votre *Oracle jdk* installé en ``$MON_JAVA_DIR``. Exécutez alors les commandes suivantes :

      .. code-block:: sh

         cp /usr/local/slf4j-1.7.2/slf4j-api-1.7.2.jar $MON_JAVA_DIR/jre/lib/ext
         cp /usr/local/slf4j-1.7.2/slf4j-simple-1.7.2.jar $MON_JAVA_DIR/jre/lib/ext

   .. todo::

      |chantier| Voir ici la possibilité de charger un plugin d'*Eclipse* pour éditer les programmes *Ocaml*

.. _Simple Logging Facade for Java (SLF4J): http://www.slf4j.org/index.html


Installation sous Debian (et Ubuntu)
====================================

Cette section décrit l'installation de l'environnement de développement
pour Portalis sous Debian GNU/Linux. Cette section se base sur une
installation minimale de Debian Wheezy. Les instructions ci-dessous
devraient être utilisables sous Ubuntu sans difficulté majeure (mais pas
testé).

On suppose un utilisateur normal, pouvant utiliser ``sudo`` quand
nécessaire. On travaille dans le répertoire ``~/code/``.

Installation des bibliothèques, compilateurs et outils nécessaires
------------------------------------------------------------------


Outils de base : éditeur, zip, unzip, git, make, maven, eclipse

.. code-block:: console

  $ sudo apt-get install emacs zip unzip vim-gtk make git maven2 eclipse


Ocaml et bibliothèques nécessaires pour la compilation de camelis et de
camelis.server :

.. code-block:: console

  $ sudo apt-get install ocaml ocaml-findlib \
                         libtaglib-ocaml-dev libxml-light-ocaml-dev \
                         liblablgtk2-ocaml-dev \
                         libocamlnet-ocaml-dev libnethttpd-ocaml-dev

Sphinx (pour compiler la doc) :

.. code-block:: console

  $ sudo apt-get install python-sphinx rst2pdf

Beaucoup de bibliothèques Java sont venues automatiquement avec
l'installation d'Eclipse. Il en manque encore quelques-unes : GWT, SLF4J

.. code-block:: console

  $ sudo apt-get install libgwt-dev-java libslf4j-java


Installation de Java : Oracle JDK
---------------------------------

L'installation d'Eclipse a déjà entraîné l'installation d'OpenJDK. La
version d'Oracle du JDK ne peut être distribuée par Debian en raison de
problèmes de licence. Heureusement, il existe une procédure pour
installer le JDK d'Oracle proprement.


On commence par télécharger le JDK chez Oracle, puis on crée des paquets
Debian avec. Enfin, on installe le JDK depuis ces nouveaux paquets :

.. code-block:: console

  $ sudo apt-get install java-package
  $ cd /tmp
  $ wget --no-cookies --header "Cookie: gpw_e24=hello" http://download.oracle.com/otn-pub/java/jdk/7u9-b05/jdk-7u9-linux-x64.tar.gz
  $ make-jpkg jdk-7u9-linux-x64.tar.gz
  $ sudo dpkg -i oracle-j2sdk1.7_1.7.0+update9_amd64.deb


On se retrouve donc avec plusieurs JDK installés. On va sélectionner le
JDK d'Oracle comme version par défaut :

.. code-block:: console

  $ sudo update-java-alternatives --list
  j2sdk1.7-oracle 317 /usr/lib/jvm/j2sdk1.7-oracle
  java-1.6.0-openjdk-amd64 1061 /usr/lib/jvm/java-1.6.0-openjdk-amd64
  java-gcj-4.7 /usr/lib/jvm/java-gcj-4.7
  $ sudo update-java-alternatives --set j2sdk1.7-oracle

Voilà, c'est fait. Mais où donc est-ce installé ? Facile :

.. code-block:: console

  $ cat /usr/share/doc/oracle-j2sdk1.7/README.Debian
  Package for Java(TM) JDK, Standard Edition, Oracle(TM)
  ---

  This package has been automatically created with java-package (0.50+nmu1).
  All files from the original distribution should have been installed in
  the directory /usr/lib/jvm/j2sdk1.7-oracle. Please take a look at this directory for
  further information.



Installation de Tomcat
----------------------

La version packagée par Debian de Tomcat ne fonctionne pas immédiatement
avec Eclipse. En conséquence, il est plus simple d'installer Tomcat dans
le répertoire de l'utilisateur depuis les sources upstream.

On commence par désactiver le lancement du Tomcat du système, si il est
déjà installé :

.. code-block:: console

  $ sudo /etc/init.d/tomcat7 stop
  $ sudo update-rc.d -f tomcat7 disable

Ensuite, on installe localement la version de Tomcat qui sera utilisée
par Eclipse : il suffit de télécharger puis de dézipper le dernier
tarball de *Tomcat* sur le site d'*Apache*. Nous avons testé *Tomcat7*
avec succès.

Enfin, il faut configurer Eclipse, cf :ref:`config_eclipse`.



Compilation et installation de Camelis
--------------------------------------

Toutes les dépendances ont été installées lors de l'étape précédente. La
compilation et l'installation de Camelis ne posent donc pas de problèmes
particuliers.

.. code-block:: console

  $ cd ~/code
  $ wget https://gforge.inria.fr/frs/download.php/31201/camelis-1.5.tar.gz
  $ tar xf camelis-1.5.tar.gz
  $ cd camelis-1.5
  $ make -C logfun/ byte opt
  $ sudo make -C logfun install
  $ make byte opt
  $ sudo make install

À ce stade, le cœur de Camelis est installé. Nous n'avons besoin de rien
d'autre dans le cadre de Portalis. Cependant, il peut être intéressant
d'installer l'interface classique de Camelis à des fins de test :

.. code-block:: console

  $ make -C interface
  $ sudo ln -sf $(pwd)/interface/glis.exe /usr/local/bin/camelis
  $ glis.exe


Récupération de Portalis
------------------------

.. code-block:: console

  $ cd ~/code/
  $ git clone https://bitbucket.org/bekkers/portalis.git

Et vogue la galère.


.. _config_eclipse:

Configuration d'Eclipse
-----------------------

Utiliser le bon JDK
'''''''''''''''''''


**Attention** Vous devez vérifier qu'*Eclipse* utilise bien *Oracle JDK*. Pour faire cette vérification procéder comme suit :

- Sous *Eclipse* ouvrir ``Window>Preferences>Java>Installed JRE``
- Faire ``Remove`` de tout ce qui est *Open jdk*
- Ajouter *Oracle jdk*


Faire fonctionner Eclipse et Tomcat
'''''''''''''''''''''''''''''''''''

1. Sous *Eclipse* déclarer le serveur *Tomcat* en utilisant le menu ``Window>Preferences>Server>Runtime Environments``
   ajouter votre serveur dans les propriétés d'*Eclipse*.

.. _Configurer la propriété Server Path d'un serveur:

2. Configurer la propriété ``Server Location`` de ce nouveau serveur afin qu'il sâche où déployer
   les applications en test. Procéder comme suit :

   - Ouvrir une fenêtre ``servers`` par le menu ``Window>Show View>Servers``
   - Effacer tous les serveurs de cette vue
   - Ajouter votre serveur dans cette vue s'il n'y est pas. S'il y est déjà, enlever lui toutes les applications
     (sinon vous ne pourrez modifier sa propriété ``Server Location``).
   - Double cliquer sur votre serveur, une fenêtre d'édition des propriétés du server s'ouvre dans la vue ``Editeurs`` d'*Eclipse*.
   - Cocher la case ``Ùse Tomcat Installation`` puis instancier la propriété ``Server Path`` en navigant vers votre serveur,
     enfin instancier la propriété ``Deploy Path`` en naviguant vers le répertoire ``wtpwebapps`` de votre serveur
     (il doit être au même niveau que son propre répertoire ``webapp``. C'est là qu'*Eclipse* déploira vos applications.

.. _Configuration correcte des projets Eclipse-WEB Dynamique-Maven:

3. Vérifier que le ficher ``.project`` à la racine de vos projets *Eclipse/WEB Dynamique/Maven* possèdent les définitions suivantes :

   .. literalinclude:: xml/eclipse.project.xml
      :language: xml


4. Vérifier les propriétés suivantes de votre projet (pour éditer les propriétés d'un projet
   faire un clic droit sur la racine du projet, dans la fenêtre qui s'ouvre choisir l'option ``Properties``, la
   fenêtre d'édition des propriétés du projet s'ouvre alors, regarder les propriétés suivantes de votre projet *Eclipse* :

   - Dans l'option ``Properties>Web Project Settings`` le champ ``Context Root`` doit contentir le nom de votre application WEB.
     Dans *Eclipse* en général c'est le même que le nom du projet *Eclipse*.
   
   - Dans l'option ``Properties>Project Facets`` les facets suivantes doivent être cochées

     . ``Dynamic Web Module``
     . ``Java``

   - Dans l'option ``Properties>Targeted Runtime`` votre serveur doit être coché

     .. _Définir les répertoires à copier pour le déployement en test d'un projet WEB dynamique:

   - Dans l'option ``Properties>Deployment Assembly`` vous pouvez voir les répertoires qui sont copiés par *Eclipse* au lancement
     de *Tomcat* depuis Eclipse.
     
     Pour un projet *Maven* web dynamique, vous devez voir impérativement les lignes
     
     ==================  =================== =================================================
     Source              Deploy Path
     ==================  =================== =================================================
     Maven dependancies  WEB-INF/lib         Librairies tières de votre application
     src/main/java       WEB-INF/class       les codes compilés     
     src/main/resources  WEB-INF/class       les ressources (fichiers \*.properties ...)     
     src/main/webapp     /                   les ressources statiques de votre application WEB 
                                             images, fichiers HTML, ...    
     ==================  =================== =================================================
     
     Pour placer les dépendances *Maven* (librairies thièrs utilisées par votre projet web dynamique), dans la page
     ``Properties>Deployment Assembly`` cliquer sur le bouton ``Add`` sélectionner ``Java Build Path Entries`` puis
     cliquer sur le bouton ``Next`` sélectionner ``Maven dependancies``, cliquer ``Finish``.

   - Dans l'option ``Properties>Java Build Path`` :

     .. _Configurez le classpath d'un projet Java-Maven:

     * Dans l'onglet ``Source`` vérifier que les répertoires ``Source Repertory`` et ``Output Repertory`` de votre projet
       ``Java Web dynamique`` sont correctement définis et en accord avec l'organisation d'un projet *Maven*.

     .. _Utiliser un JDK d'Oracle:

     * Dans l'onglet ``Librairies`` vérifier que la librairie ``JRE System Library`` du projet est bien celle d'un JDK d'*Oracle*.


-------------------------------------------------------
Quelques problèmes rencontrés et les solutions adoptées
-------------------------------------------------------

Questions à propos d'*Eclipse*
==============================

1. Vous ne réussisez pas à visualiser le code source des librairies Système.

   Voir `Utiliser un JDK d'Oracle`_.

#. Les compilations, intallations, déploiements *Maven* sous *Eclipse* génèrent le message d'erreur suivant dans la fenêtre ``Console`` :

   .. code-block:: text

      SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
      SLF4J: Defaulting to no-operation (NOP) logger implementation
      SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.

   Voir `Rendre accessibles à Maven les librairies SLF4J`_.

#. Vous n'êtes plus certains de savoir où sont déployées vos applications web dynamiques que vous testez depuis *Eclipse*

   Voir `Configurer la propriété Server Path d'un serveur`_.

#. Vous avez l'impression que *Maven* ou *Eclise* ne génèrent pas le code objet de votre projet WEB dynamique.

   Voir `Configuration correcte des projets Eclipse-WEB Dynamique-Maven`_.

#. Le fichier ``.project`` de votre projet est correct mais *Eclipse* ne génère pas le code objet *Java* là où vous l'attendez

   Voir `Configurez le classpath d'un projet Java-Maven`_

#. Au moment des tests, vous constatez que *Eclipse* génère bien un projet WEB dynamique dans le répertoire ``webapps``
   de votre *Tomcat* de développement mais celui-ci est vide ou incomplet.

   Voir `Définir les répertoires à copier pour le déployement en test d'un projet WEB dynamique`_.

#. Votre projet *Eclipse* que vous croyez *WEB dynamique* ne possède pas la propriété ``Deployment Assembly``

   Voir `Configuration correcte des projets Eclipse-WEB Dynamique-Maven`_.

#. Les pages JSP de votre projet *Web dynamique* *Eclipse* affichent une erreur ``javax.servlet cannot be resolved to a type``.

   Dans ce cas votre projet n'a plus les librairies *Tomcat*. Pour les ajouter procédez comme suit :
   
   1) Dans les propriétés du projet (*click droit sur la racine du projet*) choisissez l'option ``Properties>Java Build Path`` onglet ``Libraries``
   2) Cliquer sur le bouton ``Add Library``
   3) Choisissez la librairie ``Server Runtime`` puis faire suivant et choisissez le server *Tomcat* que vous utilsez pour le développement.
   
#. Vous voulez avoir les sources de vos dépendances *Maven* pour avoir une aide en ligne complète dans *Eclipse*.

   Dans le menu *Eclipse* ``Window>Properties>Maven`` Cocher les cases ``Download Artefact Sources``
   et ``Download Artefact Javadocs``.
   
#. Comment faire pour s'assurer qu'*Eclipse* soit lancé une machine JVM particulière.

   Avant de lancer *Eclipse* initialiser l'option ``-vm`` dans le fichier de configuration ``eclipse.ini`` comme suit :
   
   1. L'option ``-vm`` et sa valeur (chemin vers une JVM) doivent être sur des lignes séparées.
   2. La valeur doit être le chemin absolu ou relatif vers l'exécutable Java, et pas seulement dans le répertoire de base Java.
   3. L'option ``-vm`` doit apparaître avant l'option ``-vmargs``, puisque toutes les options après ``-vmargs`` sont
      passées directement à la JVM.
      
   Voici un exemple de contenu de fichier ``eclipse.ini`` avec l'option ``-vm`` positionnée :
   
   .. code-block:: text

      -startup
      ../../../plugins/org.eclipse.equinox.launcher_1.2.0.v20110502.jar
      --launcher.library
      ../../../plugins/org.eclipse.equinox.launcher.cocoa.macosx.x86_64_1.1.100.v20110502
      -product
      org.eclipse.epp.package.jee.product
      --launcher.defaultAction
      openFile
      -showsplash
      org.eclipse.platform
      --launcher.XXMaxPermSize
      256m
      --launcher.defaultAction
      openFile
      -vm
      C:\Java\JDK\1.6\bin\javaw.exe
      -vmargs
      -Dosgi.requiredJavaVersion=1.5
      -XX:MaxPermSize=256m
      -Xms40m
      -Xmx512m
   
#. Comment savoir où se trouvent les installations d'*Eclipse* sur votre système ?

   Chercher les occurences du fichier ``eclipse.ini`` qui se trouvent toujours dans les installations d'*Eclipse*
   à coté de l'exécutable ``eclipse``. Par exemple, sous *Linux* taper la commande suivante en ligne de commande :
   
   .. code-block:: console

      $ locate eclipse.ini

Questions à propos de *Maven*
=============================

1. Les compilations, intallations, déploiements *Maven* sous *Eclipse* génèrent le message d'erreur suivant dans la fenêtre ``Console`` :

   .. code-block:: text

      SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
      SLF4J: Defaulting to no-operation (NOP) logger implementation
      SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.

   Voir `Rendre accessibles à Maven les librairies SLF4J`_.

#. Vous voulez avoir les sources de vos dépendances *Maven* pour avoir une aide en ligne complète dans *Eclipse*.

   Dans le menu *Eclipse* ``Window>Preferences>Maven`` cocher les cases ``Download Artefact Sources`` et ``Download Artefact Javadocs``.

#. Comment capter dans des fichiers de propriétés la version du fichier ``pom.xml`` (ie la valeur de la balise ``<version>``) ?

   Procéder comme suit :
   
   a. Le fichier de propriétés ``*.properties`` doit contenir la ligne suivante :
   
      .. code-block:: properties

         myapp.version=${pom.version}    
 
   #. dans le fichier ``pom.xml`` de votre module, indiquer que le fichier sera filtré par *Maven* :
   
      .. literalinclude:: xml/maven.build.properties.file.filter.xml
         :language: xml
           
      Quand *Maven* construira votre application, il remplacera tout ``${...}`` contenus dans
      vos fichiers de propriétés par leur valeur connue de *Maven*.
      Par défaut, ``${pom.version}``  définit la version du fichier ``pom.xml``.
      
   #. Dans votre code *Java*, vous aurez juste besoin de charger le fichier de
      propriétés et de récupérer la valeur de la propriété ``myApp.version``.

#. Comment obtenir directement en *Java* la version d'un artefact *Maven* ?

   Vous devez utiliser ``getClass().getPackage().getImplementationVersion()`` pour obtenir 
   les informations de version qui devraient être stockées dans le fichier ``MANIFEST.MF`` du package ``.jar``.
   Cependant *Maven* ne les range pas par défaut dans ses packages *Jar*.
   
   Afin que *Maven* stock ses informations de version dans ses fichiers *Jar*, il faut modifier l'élément
   ``<archive> <configuration>`` du plugin ``maven-jar-plugin``
   de votre ficher ``pom.xml`` . Initialiser les options 
   ``addDefaultImplementationEntries`` et ``addDefaultSpecificationEntries`` à ``true``, comme suit :
   
   .. literalinclude:: xml/maven.build.manifest.xml
      :language: xml
      
.. _mettre le jar d'un war dans le dépot usager:

#. Comment mettre le ``jar`` d'un module ``war`` dans le dépot usager ?

   Modifier le fichier ``pom.xml`` du projet comme suit :
   
   a. Spécifiez le packaging ``war`` en haut du fichier ``pom.xml``.
   
   #. Ajoutez les lignes suivantes dans la section ``<build>`` du fichier ``pom.xml`` :
   
      .. literalinclude:: xml/maven.build.compile.jar.xml
         :language: xml
         
   Maintenant, si vous taper ``mvn package`` ou ``mvn install`` en ligne de commande et vous obtenez le ``jar`` en même
   temps que le ``war`` dans le répertoire ``target/`` ainsi que dans le dépot usager.

#. Comment inclure les sources *Java* dans les déploiements de vos applications sur les dépots distants de *Maven* ?

   Dans la section ``<build>`` du fichier ``pom.xml``, configurer le plug-in ``maven-source-plugin`` comme suit :
   
   .. literalinclude:: xml/maven.build.source.xml
      :language: xml

      
   Ainsi, vous trouverez une archive ``<monArchive>-<version>-sources.jar`` en plus de l'archive
      ``<monArchive>-<version>.jar`` pour chaque modules de votre application.
      
#. Comment partager les progammes de test avec un autre module ?

   Procéder comme suit :

   a. Dans la section ``<build>`` du fichier ``pom.xml``, du module de base, configurer le plug-in ``maven-source-plugin`` comme suit :
   
      .. literalinclude:: xml/maven.build.testcode.generate.xml
         :language: xml

   #. Dans la section ``<dependencies>`` du fichier ``pom.xml``, du module dependant, ajouter la dépendance suivante :
   
      .. literalinclude:: xml/maven.build.testcode.use.xml
         :language: xml
         
         
#. Comment copier une ressources d'un projet dans un autre pour eviter d'avoir plusieurs versions d'une même ressources ?

   Utilisez le plugin ``maven-resources-plugin`` comme suit :
   
   .. literalinclude:: xml/copy.resources.xml
      :language: xml

#. Maven ne lance pas les classes de test junit qui se trouvent dans le répertoire ``src/test/java``.

   Par défaut Maven utilise les conventions d'appellation suivantes lorsqu'il cherche les tests à exécuter:

   - Test\*
   - \*Test
   - \*TestCase

   Vos classes de test ne suivent pas ces conventions. Vous devez les renommer ou configurer Maven Surefire Plugin
   pour utiliser un autre modèle pour les classes de test.
   
   

Questions à propos de *Tomcat*
==============================

1. Comment savoir si *Tomcat* est lancé (par défaut il se lance sur le port 8080)

   Voir `Comment voir si Tomcat est lancé`_.

#. Au moment des tests, vous constatez que *Eclipse* génère bien un projet WEB dynamique dans le répertoire ``webapps``
   de votre *Tomcat* de développement mais celui-ci est vide ou incomplet.

   Voir `Définir les répertoires à copier pour le déployement en test d'un projet WEB dynamique`_.
   
#. Au moment des tests, vous constatez que *Eclipse* génère bien un projet WEB dynamique dans le répertoire ``webapps``
   de votre *Tomcat* de développement mais celui-ci ne comporte pas les lib.

   Voir `Définir les répertoires à copier pour le déployement en test d'un projet WEB dynamique`_.
   
#. Comment faire pour qu'un projet *Maven* créer avec le modèle ``maven.achetype.webapp`` soit visible par *Eclipse* comme
   un projet lançable par ``run on server``.
   
   Voir `Configuration correcte des projets Eclipse-WEB Dynamique-Maven`_.
   
#. Comment faire une nouvelle version (release ou snapshot) d'un projet *Maven* ?

   Procéder comme suit :

   a. Parcourir tous les pom du projet en partant de la racine, changer le numéro de version :
   
      - Exemple pour une création de release passer de ``0.0.5-SNAPSHOT`` à ``0.0.6``
      - Exemple pour une création de snapshot passer de ``0.0.5-SNAPSHOT`` à ``0.0.6-SNAPSHOT``
   
      Pour la racine du projet, c'est le numéro de version de l'artifact qu'il faut changer. Pour tous
      les modules tous les modules fils de la racine du projet, récursivement changer le numéro de version du
      parent du module considéré en accord avec la version de la racine.
   
      Noter que le suffixe ``-SNAPSHOT`` est une convention de *Maven* pour différencier les noms de release
      des noms de snapshot.
   
   #. Tant qu'il reste des dépendances internes du type *module B dépends de module A* (``A <- B``) telle que
      ``A`` ne dépend que de modules internes déjà déployés et ``A`` n'a pas encore été déployer faire : 
   
      - au sein du module ``A`` faire ``mvn clean deploy``
      - dans tous les éléments ``<dependency>`` des poms des modules ``Bi``, dépendants de ``A``,
        changer la version de ``A``.
      - vérifier que tous les tests qui utilisent ``A`` fonctionnent correctement.
        
   #. Si certains autres de vos projets *Maven* dépendent de modules du présent projet, changer le numéro
      de version de toutes les dépendances et si des tests vos autres projets utilisent des modules du
      présent projet, vérifier que ces tests marchent toujours.
       
Questions à propos de reStructuredText et Sphinx
================================================

1. Au moment de la construcion du site par *Sphinx* vous rencontrez l'exception: ``No module named rst2pdf``

   .. code-block:: console

      $ Extension error:
      $ Could not import extension rst2pdf.pdfbuilder (exception: No module named rst2pdf)

   **Solution** : Il faut ajouter au fichier *Python* ``conf.py`` de configuration de *Sphinx* les lignes suivantes :

   .. code-block:: python

      import pkg_resources
      pkg_resources.require("rst2pdf") # get latest version
      import rst2pdf

   **Note**: Solution trouvée sur le site :

   http://blog.gmane.org/gmane.comp.python.sphinx.devel/month=20091001/page=5
   
Questions à propos de log4j
===========================

1. Je reçois le message ``log4j:WARN No appenders could be found for logger``

   While using log4j in your application, sometimes you might encounter the following message:

   .. code-block:: console

      log4j:WARN No appenders could be found for logger(somePackageName.someClassName).
      log4j:WARN Please initialize the log4j system properly.

   The reason why you see this message is that your *log4j* configuration file
   (i.e. ``log4j.xml`` or ``log4j.properties``) is NOT found in the classpath.
   Placing the log4j configuration file in the applications classpath should solve the issue. 

   