.. -*- mode: rst -*-

===================================
Androlis - Spécifications générales
===================================

.. sectionauthor:: Vincent Piet <vincent.piet@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/androlis-spec.pdf>`

.. admonition:: Androlis est en cours de développement, voici sa TODO list :

  * **Implémenter** implémenter les pages de navigation.
  * Durant le développement et pour la mise au point : **utiliser** un système de log à base de la librairie log4j_
  * **utiliser** l'asynchronisme des requêtes HTTP/XML pour laisser l'interface acive pendant l'interprétation d'une requête.

.. _Camelis:        http://www.irisa.fr/LIS/ferre/camelis/
.. _`Objective Caml`: http://caml.inria.fr/ocaml/
.. _`concept formel`: http://en.wikipedia.org/wiki/Formal_concept_analysis
.. _log4j:            http://code.google.com/p/android-logging-log4j/

Présentation
============

Portalis
--------

**Portalis** est une application WEB répartie qui permet à des utilisateurs distants de se partager,
de manière collaboratrive, des serveurs d'information logique LIS. Chaque serveur d'information met en oeuvre un **service**.
Un service est un ensemble d'objets et d'attributs d'un domaine d'intérêt particulier. Portalis permet de naviger intelligemment
au sein des services. Une étape de navigation sélectionne un ensemble d'objets ainsi que les attributs qui les caractérisent. Il s'agit d'un `concept formel`_

Concrêtement, **Portalis** est composé d'un serveur d'administration, appelé **serveur Portalis**, et d'un ensemble de serveurs d'information,
à raison de un serveur d'information par service considéré.
Pour l'instant, les serveurs d'information accessibles sont des serveurs de type **Camelis_**. Ces derniers sont implémentés en `Objective Caml`_. 
Voici le schéma de cette application :

.. image:: images/schemaGeneralPortalis.jpg
   :width: 16 cm

Androlis
--------

**Androlis** est un client Androïd pour l'application répartie **Portalis** ainsi qu'à ses serveurs d'information.

Les clients Androlis communiquent avec le serveur Portalis ainsi qu'avec les serveurs Camelis, à travers **internet** via le protocole **HTTP**.
Les réponses des serveurs sont retournées en XML.

Caractéristiques d'Androlis
===========================

Androlis est une interface usager composée de pages. Les pages permettent une navigation intelligente au sein de l'information maintenu par les serveurs.
Les pages principales d'Androlis sont :

- Page de login
- Page de choix d'un service
- Pages d'affichage des objets couramment sélectionnés
  - affichage liste
  - affichage icon ou image
  - affichage carte
  - affichage détail (de type fiche)
- Pages d'affichages des attributs
  - affichage liste
  - affichage nuage de points
- Page d'affichage des requêtes

La plupart des pages sont aussi des pages d'édition.

Organisation d'un projet Android
================================
Les projets android combinent deux langages : java et xml. Le langage xml est utilisé pour tout ce qui est 
description d'interface statique, description de données, description d'informations relatives à l'affichage...
Le java apporte le dynamisme (grâce par exemple aux listeners) et toutes la partie calculatoire. 

L'ensemble formé par une interface et les calculs sous-jacents s’appelle une activité.


Description de code
===================

XML
---
Les fichiers xml sont :
   _pom.xml
   _AndroidManifest.xml
   _des fichiers contenu dans le répertoire des ressources : res

Le contenu du fichier pom.xml est détaillé dans le document de reprise de code.

Android manifest
~~~~~~~~~~~~~~~~
Le fichier AndroidManifest.xml contient des informations sur : 
   _la compatibilité de l'application avec les différentes versions d'android, 
   _des autorisations de lecture et d'écriture nécessaires à l'utilisation de log4j décrit dans le
    document de reprise de code, 
   _une description de application contenant les activités.

Nous avons apporté quelques modifications dans la description de l'application :
l'ajout de la règle android:theme="@style/CustomTheme" qui nous permet d'appliquer un thème personnel,  
ainsi que l'ajout de la règle android:windowSoftInputMode="stateHidden" qui nous permet de cacher le clavier par défaut 
lors de l'ouverture d'activités contenant des textes éditables. C'est un comportement qui nous semble préférable 
car donnant accès à l’ensemble des fonctionnalités à l'utilisateur, alors que dans le cas inverse 
elles auraient été cachées par le clavier.

Contenu du répertoire res
~~~~~~~~~~~~~~~~~~~~~~~~~
Le répertoire res contient les répertoires:
   _drawable,
   _layout,
   _menu,
   _values,
ainsi qu'un certain nombre de répertoires préfixés par ces noms. 
Ces répertoires préfixés contiennent des ressources adaptés aux différentes tailles d'écran. 
Ce fonctionnement est décrit à l'adresse :
http://developer.android.com/guide/practices/screens_support.html.

Drawable
^^^^^^^^
Le répertoire drawable contient essentiellement les images utilisées dans l'application. 
Il contient également des fichiers xml de design décrivant l'aspect des boutons ou d'une ligne 
dont les bords sont atténués (le séparateur présent dans l'activité request).

Layout
^^^^^^
Le répertoire layout contient des fichiers préfixés par activity.
Ces fichiers contiennent l'interface graphique statique de l'application. 
Pour chacun des fichiers, activity est suivi par le nom de l'activité qu'il décrit.

Il contient également les fichiers décrivant l'entête de l'application, préfixé par header.

Enfin il contient des fichiers décrivant les différentes représentation d'objets contenu dans des listes : 
des objets simples, des noms générique et des objets cliquables.

Menu
^^^^
Le répertoire menu contient des fichiers relatifs aux menus contextuels. Il décrit les différentes options du menu.

Values
^^^^^^
Le répertoire values contient les fichiers color, dimens, strings, styles, et themes 
permettant d'utiliser pour des noms symboliques, à la place de valeurs.

L'intérêt de ce mécanisme est de permettre par la suite d'utiliser un autre fichier, par exemple values-fr/string.xml,
redéfinnissant ces valeurs dans des cas particulier, ici lorsque l'OS est en français. Les valeurs contenus dans ce fichier 
remplacent alors celle du fichier par défaut.

Java
----
Les fichiers java sont contenus dans le répertoire src dans quatre packages. 

com.androlis
~~~~~~~~~~~~
Le package com.androlis contient les fichiers java de chacune des activités. 
Ils permettent de lier une interface à une activité, d'apporter du dynamisme en modifiant des données 
dans des conteneurs ou en déclarant des listeners, d'appeler l'activité suivante avec éventuellement des données.

com.android.factory
~~~~~~~~~~~~~~~~~~~
Ce package contient une classe java Factory permettant d'accèder et de mémoriser l'était de variables statiquement.
En effet, si android défini le notion de contexte, il n'est en revanche pas possible d'y stocker durablement une information.
Il n'est possible de passer une variable d'une activitée à une autre quand la passant explicitement au moment de changer d'activité.
L'intétêt de cette classe est donc de pouvoir accèder à ces variables peut importe l'activité courante sans la contrainte
la passer explicitement durant chaque changement d'activités.

com.android.list
~~~~~~~~~~~~~~~~
Le package com.android.list contient une ré-implémentation du composant list d'android. 
C'est en effet le seul moyen d'obtenir une liste filtrable contenant des noms générique pour classifier les objets, ou des checkboxs.

La classe List_MyCustomAdapter.java
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
La fonction de filtrage nécessite deux conteneurs, un pour l'ensemble des données servant de référentiel 
(dans notre classe c'est la liste _originalData), et un pour les données affichés (dans notre classe c'est la liste _filteredData). 
La liste des données filtrées se mettra à jour en fonction de la recherche de l'utilisateur.

La fonction getView est appelé implicitement dès qu'un objet apparait à l'écran 
(suite au lancement de l'activité contenant la liste ou à un scroll sur la liste), 
afin d'afficher chacun des objets avec la vue qui lui correspond.

La classe getFilter décrit le fonctionnement du filtre, assure l'opération de publication du résultat 
(dans notre cas l'affectation des listes calculées aux listes filtrées et une notification appelant explicitement getView), 
et contient une description du filtre utilisé.

com.android.log4j
~~~~~~~~~~~~~~~~~
Le package com.android.log4j contient une classe permettant de configurer le système Log4j. 
Il n'est en effet pas possible de le configurer au moyen d'un fichier xml de propriété. 
Plus des détails sont donnés dans le document de reprise de code.

com.android.map
~~~~~~~~~~~~~~~
Le package com.android.map contient une classe DemoIconProvider utile pour l'utilisation de la librairie android-maps-extensions.
L'intégration et l'usage de cette librairie est expliqué dans le document de reprise de code.
Ce fichier particulier lie les images m1 à m5 du fichier drawable aux différents seuils de points qu'ils agrègent, et s'occupe de l'affichage
d'informations lorsque l'on clique sur ces agrégateurs.

com.androlis.requestMemorization
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Le package com.androlis.requestMemorization contient le fichier assurant la mémorisation des requête, 
permettant la navigation vers les requêtes précédentes. Le mécanisme qui y est décrit met en œuvre deux piles, 
la première contenant les requêtes passés et actuellement sélectionnée, la seconde les requêtes futures. 
Si une nouvelle requête est ajoutée, les requêtes futures sont alors oubliées.

com.androlid.spinner
~~~~~~~~~~~~~~~~~~~~
Le package com.androlis.spinner contient le fichier Spinner_MyCustomSpinner,  permettant d'initialiser le spinner pour chacune des
activités. Elle contient une méthode qui prend en paramètre le nom de l'activité appelante, s'assurant ainsi que ce nom
soit affiché courement dans le spinner et que les autres propositions redirigent l'utilisateur vers l'activité associée.
Cette classe masque la proposition map si le booléen isMap de la classe Factory est mis à faux. 

