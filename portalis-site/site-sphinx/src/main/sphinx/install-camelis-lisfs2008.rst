----------------------------------------------------
Compte-rendu d'installation de camelis sur Fedora 17
----------------------------------------------------

.. sectionauthor:: Benjamin Sigonneau <sigonnea@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/install-camelis-lisfs2008.pdf>`


L'installation de lisfs2008 a été faite le 31 octobre 2012.

Avant propos
------------

Notre serveur d'exploitation est ``lisfs2008.irisa.fr``. Il tourne sous le système
*Fedora 17*. La procédure d'installation ddécrite ici permet d'installes sous *Fedora 17* l'environnement
minimum nécéssaire à l'exploitation du portail *Portalis*

Installation de Camelis (bibliothèque)
--------------------------------------

export CODE=~/code

Le compilateur Ocaml et ses amis
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

*Ocaml* est déjà installé sur la machine, en version 3.12.1, avec les
outils compagnons usuels :

.. code-block:: console

 $ yum list installed 'ocaml*' -q|tail -n +2|awk '{print $1}'
 ocaml.x86_64
 ocaml-camlp4.x86_64
 ocaml-camlp4-devel.x86_64
 ocaml-camlp5.x86_64
 ocaml-docs.x86_64
 ocaml-emacs.x86_64
 ocaml-findlib.x86_64
 ocaml-findlib-devel.x86_64
 ocaml-ocamldoc.x86_64
 ocaml-runtime.x86_64
 ocaml-x11.x86_64


Dépendances dans les dépôts Fedora
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

 $ sudo yum install ocaml-xml-light-devel

Autre dépendance : ocaml-taglib
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Ici nous installons la bibliothèque *ocaml-taglib* et sa dépendance *Linux* *taglig*.

.. code-block:: console

 $ sudo yum install taglib-devel
 $ cd $CODE
 $ wget http://downloads.sourceforge.net/project/savonet/ocaml-taglib/0.2.0/ocaml-taglib-0.2.0.tar.gz
 $ tar xf ocaml-taglib-0.2.0.tar.gz
 $ cd ocaml-taglib-0.2.0
 $ ./configure
 $ make
 $ sudo make install
 $ cd $(ocamlfind query taglib)
 $ sudo ocamlopt -ccopt -L. -shared -linkall -o taglib.cmxs taglib.cmxa


Installation de *Camelis* lui même
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

 $ cd $CODE
 $ wget https://gforge.inria.fr/frs/download.php/31201/camelis-1.5.tar.gz
 $ tar xf camelis-1.5.tar.gz
 $ cd $CODE/camelis-1.5/logfun
 $ make
 $ sudo make install
 $ cd $CODE/camelis-1.5/
 $ make
 $ sudo make install


Installation de Camelis (interface)
-----------------------------------

Dépendances dans les dépôts Fedora
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: console

 $ sudo yum install ocaml-lablgtk-devel

Et hop !
~~~~~~~~

.. code-block:: console

 $ cd $CODE/camelis-1.5/interface
 $ make
 $ ./glis.exe
