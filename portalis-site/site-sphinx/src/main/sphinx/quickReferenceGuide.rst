.. -*- mode: rst -*-

==========================================
Camelis / Glis - petit manuel de reférence
==========================================

.. sectionauthor:: Yves Bekkers <bekkers@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/quickReferenceGuide.pdf>`


Qu'est-ce que c'est que Camelis
===============================

Camelis est une implémentation d'un système d'information logique. Les informations (appelées *objets* dans Camelis) sont organisées par indexation
automatique à partir de propriétés logiques de ces informations ou objets.

À quoi ça sert
--------------

Le système permet une recherche de l'information soit

   - par interrogation à l'aide de formules logiques soit
   - par une navigation non-hiérarchique.

Comment ça marche
-----------------

Camelis est basé sur l'*analyse formelle de concepts* (voir la définition sur Wikipédia très bien faite
`Formal concept analysis <http://en.wikipedia.org/wiki/Formal_concept_analysis>`_).
L'analyse formelle de concepts permet d'organiser les objets en une matrice qui spécifie un
ensemble d'objets en regard d'un ensemble de propriétés appelées *attributs*. Chaque case de la matrice possède la valeur *vraie* ou *faux*
selon que l'objet possède ou non la propriété.

   - un groupe *naturel* d'objets est l'ensemble de tous les objets qui partagent en commun un sous ensemble d'attributs
   - un groupe *naturel* de propriétés est l'ensemble de tous les attributs partagés par un des objets du goupe naturel d'objets.

Les groupes naturels de propriétés correspondent un pour un avec les groupes naturels d'objet. Un *concept* est une paire contenant un goupe naturel de propriétés
et son groupe naturel d'objets associé.

Les concepts forment un treillis dans lequels chaque objet et chaque propriété peut apparaître plusieurs fois. (voir
`l'exemple de Wikipedia <http://en.wikipedia.org/wiki/Formal_concept_analysis>`_ très bien fait).

Composants de base de Camelis
=============================

Eléments de base
----------------

Les éléments de base sont :

Objet
     Une idée, une notion n'importe quoi identifié par un nom.

Propiété ou attribut
     Un attribut porte un nom et éventuellement une valeur. Syntaxiquement le nom de l'attribut est un mot commençant par une minuscule (exemple ``couleur``).
     Les valeurs sont typées, voir ci-dessous.

Terme
     Un terme est une propriété qui peut être mise dans une relation de taxinomie avec d'autres termes.
     Les relations de taxinomique définissent des termes plus généraux que d'autres termes.
     Ces derniers sont alors dits plus spécifiques que les premiers.
     Par exemple on peut dire que ``France`` est plus général que ``Bretagne`` ou
     inversement que ``Bretagne`` est plus spécifique que ``France``.
     Syntaxiquement un terme est un mot commençant par une majuscule ou c'est une phrase encadrée par
     des simples quotes (exemples ``Gros`` ou ``'petit animal'``).
     Un terme peut apparaître soit comme un attribut simple non valué comme dans ``Gros``, soit comme valeur d'un attribut
     comme dans ``taille Gros`` (voir çi-dessous).

Valeur d'attribut
     Les valeurs d'attributs sont typées. On trouve principalement {*entier*, *chaîne*, *date*, *time*, *Terme*}. Chaque type possède ses propres opérateurs.
     **Important :** Pour un attribut donné un objet peut être associé à plusieurs valeurs.

Valeurs d'attributs
-------------------

Entiers
```````
Il s'agit toujours d'intervalles d'entiers. Un intervalle peut être définit de deux manières :

  - un entier préfixé par les symboles ``=, >=, <=``, comme dans ``age = 22``
  - un intervalle préfixé par le mot clé ``in``, comme dans ``age in [18, 80]``.

Remarquez que ``age = 22`` définit la même chose que ``age in [22, 22]``. Les entiers peuvent être approximés en utilisant les signes ``-``.
Par exemple ``2--`` définit l'intervalle ``[200, 299]``. On peut aussi utiliser les lettres  {``k, M, G, ...``} pour représenter *millier*, *million*, *milliard*,
eg. ``3-M`` signifie *trente millions*. Voici quelques exemples :

::

   distance = 250
   poids >= 80
   annee in [1900, 1999]
   prix >= 29-k
   value in [,]

Dans ces exemples l'identificateur de gauche est l'attribut, la partie droite telle que '``= 250``' est la valeur.

**Remarque importante :** Notez bien qu'un signe ou un mot clé tel que ``=``, ``>=`` ou ``in``, fait **partie intégrante** de la valeur !

Chaînes
```````
Une chaîne est une valeur entre double quotes telle que ``"ceci est une chaîne"``. Dans une déclaration de valeur ou dans une
expression une chaîne est introduite par
l'un des mots clés {``is, contains, beginswith, endswith, match``}. Remarquez que chacun des mots clés peut être
abrévié par sa première lettre {``i, c, b, e, m``}.
Dans la comparaison par l'opérateur ``match`` la chaîne doit être une expression régulière *à la Unix*.
Exemple ``"^[0-9]{4}.*"`` représente toutes les chaînes commençant par
quatre chiffres. Voici des exemples d'association *attribut/valeur* :

::

   name is "Bekkers"
   titre c "logic"
   texte beginswith "Once upon a time"
   lattitude match "[0-9]2:[0-9]2:[0-9]2"
   entierSigne match "[+-]?[0-9]+"

Dans le dernier exemple l'attribut est ``entierSigne`` et la valeur est ``match "[+-]?[0-9]+"``.
Remarquez bien que le mot clé ``match`` fait partie de la valeur.

Dates
`````
Il s'agit toujours d'intervalles de dates (voir ci-dessous). Syntaxiquement une date complète comprend trois champs, comme dans ``date 19 sep 1932``. On trouve
successivement le mot clé ``date`` suivit de ``=`` ou de ``in`` puis le jour, le mois et l'année.
Le jour est donné sur un ou deux chiffres sans 0 devant. Le mois est donné par les trois premières lettre du mois en anglais.
L'année donnée par un entier sans 0 devant. Cependant une date peut être donnée
avec trois niveaux de résolution :

  - *jour* exemple ``date = 19 sep 1932``
  - *mois* exemple ``date = jul 1945``
  - *année* exemple ``date = 1945``

Des dates relatives peuvent être exprimées par des expressions telles que ``date =`` suivi de {``next day, last year, 2 months ago``}
comme dans ``date = last day``.
La syntaxe d'un intervalle est la même que celle des entiers excepté que le préfixe est ``date in``
(au lieu d'être simplement ``in``), exemple ``date in [1 jan 2000, 31 dec 2000]``.
Pour les intervalles ouverts on trouve aussi les comparateurs comme ``>``, ``>=`` ou ``<=`` comme dans ``date >= 14 jul 1445``
ou dans ``date <= yesterday``.
Voici des exemples d'association *attribut/valeur* :

::

    hier date = last day               (signifie littéralement "hier")
    armistice date = 14 jul 1445       (racourci pour : armistice date in [14 jul 1445, 14 jul 1445 ])
    demain date >= tomorrow            (signifie littéralement "à partir de demain ...")
    desDates date in [2000, dec 2005]
    depuisAvantHier date >= 2 days ago (signifie littéralement "depuis deux jours ...")

Dans chaque association on trouve le nom de l'attribut suivit de sa valeur. Ainsi dans
le dernier exemple l'attribut est ``depuisAvantHier`` et la valeur est ``date >= 2 days ago``.
Remarquez bien que l'expression ``date >=`` fait partie de la valeur.

Times
`````
Il s'agit là aussi toujours d'intervalles, mais pour le cas, il s'agit d'intervalles de temps dans lequel un temps précis est donné syntaxiquement
sur trois champs entiers sans 0 devant, les champs sont séparés par le signe ``:``. On trouve successivement le mot clé ``time``,
le symbole ``=``, l'heure sur 24 heures, les minutes puis les secondes. Voici un exemple ``time = 1:35:43``.
Comme pour une date un temps peut être donné avec trois niveaux de résolution :

  - *heure* exemple ``time = 13``
  - *minute* exemple ``time = 13:30``
  - *seconde* exemple ``time = 13:30:45``

Des temps relatifs peuvent être exprimés par des expressions telles que ``time =`` ou ``time <=`` suivit de {``now, next hour, last minute, 2 hours ago``}.
La syntaxe d'un intervalle est la même que celle des entiers et des dates excepté que le préfixe est ``time in``. Voici des exemples d'association *attribut/valeur* :

::

    heureDeCreation time = 13:07:59
    depart time = 1 hour ago
    attr1 time in [12, 13:05]
    attr2 time in [1 hour ago, now]
    attr3 time <= 13:59``

Dans chaque association on trouve le nom de l'attribut suivit de sa valeur. Ainsi dans
le dernier exemple l'attribut est ``attr3`` et la valeur est ``time <= 13:59``.
Remarquez bien que l'expression ``time <=`` fait partie de la valeur.

Atributs valué par un attribut lui même valué à son tour (etc)
``````````````````````````````````````````````````````````````
Les espaces dans les noms d'attributs n'étant pas autorisés, dans des définitions telles que
``carateristique couleur is "rouge"`` et ``caracteristique temperature is "chaude"``
les espaces prennent une signification importante. Ils définissent
un attribut ``caractéristique`` prennant comme valeur un attribut ``couleur`` ou ``temperature``, ces deux
attibuts étant eux même
valués respectivement par les chaînes ``rouge`` et ``chaud``. Remarquer que l'interface Glis n'est pas toujours très
cohérente dans l'affichage de cette vision arborescente.

**Note** : Donner un attribut comme valeur d'un attribut ne semble pas d'une grande utilité et GLis n'étant pas
toujours cohérent dans son mode d'affichage de tels attributs valués par des attributs, il est peut-être recommandé de
l'éviter.


Termes et taxinomies
--------------------
Une valeur peut être un terme comme dans ``lieu France``. Ici l'attribut est ``lieu`` et la valeur est le terme ``France``.
L'intérêt de mettre des termes comme valeur d'attribut est que l'utilisateur peut former des taxinomies sur les termes
(et pas sur les attributs).
Par exemple l'utilisateur peut définir la taxinomie de termes suivante :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/taxinomieTerme.jpg
      :width: 50%
      :alt: Taxinomie sur les termes dans Camelis

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/taxinomieTerme.jpg
      :alt: Taxinomie sur les termes dans Camelis


Ainsi si un objet *O* se voit attribubé l'attribut ``lieu`` valué ``'Pays malouin'`` et que l'utilisateur effectue
une recherche pour les objets ayant l'attribut ``lieu`` valué ``France``
l'objet *O* lui sera présenté.

**Tricks:**

Prenez l'exemple d'un attibut ``couleur``, il y a deux manières de lui associer une valeur, par une *chaîne* ou par un *terme*.
La différence est importante. Ainsi si vous déclarez
l'attribut ``couleur`` en lui donnant des valeurs chaînes comme suit :

::

  couleur is "rouge"
  couleur is "vert"
  couleur is "jaune"

Votre recherche ne pourra se faire qu'en utilisant les opérateurs de *chaîne*, par exemple ``... is "rouge"`` ou
``... contains "ou"``. Si vous donnez des valeurs de type *terme* comme çi dessous :

::

  couleur Rouge
  couleur Vert
  couleur Jaune

Et voici une suggestion de taxinomie sur les couleurs de base :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/taxinomieCouleur.jpg
      :width: 100%
      :alt: Taxinomie sur les termes dans Camelis

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/taxinomieCouleur.jpg
      :alt: Taxinomie sur les termes dans Camelis


Vous pouvez ainsi chercher les objets ayant un attribut ``couleur`` valué ``chaud``.


Taxinomie des valeurs de chaînes, d'entiers, de dates et de temps
-----------------------------------------------------------------

Valeurs chaînes
```````````````
Camelis calcule une taxinomie automatique entre les valeurs de chaînes. Voici un exemple de taxinomie sur les
valeurs de chaînes :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/taxinomieChaine.jpg
      :width: 40%
      :alt: Taxinomie sur les chaînes dans Camelis

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/taxinomieChaine.jpg
      :alt: Taxinomie sur les chaînes dans Camelis

**Note sur les valeurs multiples :** Il n'y aucun inconvénient à ce qu'un objet soit associé à plusieurs valeurs pour un attribut donné. Par exemple une image
donnée peut appartenir à plusieurs catégorie :

  categorie is "paysage" and categorie is "mer"``

Valeurs entières
````````````````
L'intérêt des entiers comme des intervalles est qu'ils introduisent une taxinomie automatique. Voici un exemple de taxinomie sur les
intervalles d'entiers de Camelis :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/taxinomieEntiers.jpg
      :width: 40%
      :alt: Taxinomie sur les intervalles d'entiers dans Camelis

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/taxinomieEntiers.jpg
      :alt: Taxinomie sur les intervalles d'entiers dans Camelis

Utiliser *Camelis* à travers l'interface de l'application standalone *Glis*
===========================================================================

Vous trouverez la dernière version de *Camelis* ainsi qu'une interface d'utilisation appelée *Glis* sur la forge anonyme Inria ``svn://scm.gforge.inria.fr/svn/camelis``.
Voici à quoi ressemble cette interface d'utilisation :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/glis1.jpg
      :width: 100%
      :alt: Diagramme de classes de l'interface SOAPCamelis

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/glis1.jpg
      :alt: Diagramme de classes de l'interface SOAPCamelis

Utilisez la version Linux. Ne cherchez pas à mettre un racourci de cette application Glis dans un menu sous un icone (ceci est une bug connu, ça ne marche pas).

Dans cette interface il y a trois fenêtres principales :

  - La fenêtre du haut est la fenêtre de saisie de formules logiques pour une navigation par formules logiques.
  - La fenêtre du bas à gauche est celle de visualisation des attributs, on l'appelle *fenêtre de navigation* car c'est par elle
    que vous pourrez naviguer intuitivement par des clics sur les attibuts
  - La fenêtre du bas à droite est celle de visualisation des objets, on l'appelle *fenêtre d'extension*. Cette fenêtre possède deux onglets
    qui correspondent à deux présentations différentes de l'extention, une représentation textuelle des objets par leur nom et une représentation
    par une image si l'objet est associé à une image. Tous les objets n'ont pas d'images mais, en général
    l'utilisateur doit s'arranger pour qu'ils aient tous un nom, même si cela n'est pas obligatoire.

Corrélation de l'état des trois fenètres
----------------------------------------

Il est important de remarquer que l'état des trois fenêtres est corrélé en permanence. Les deux fenêtres du bas présentent un *concept formel* avec à droite
le *groupe naturel* des objets du concept (*l'extension du concept*) et à gauche on trouve l'ensemble des propriètes possédées par
tous ces objets. Les propriètés de gauche qui affichent la même cardinalité que celle de l'extention sont celles
du groupe naturel de propriètés qui est associées au concept. On appelle ce groupe naturel de propriétés l'*intension du concept*.
Remarquez que Glis affiche les propriétés de l'intension en couleur orange, ce qui donne une plus grande lisibilité.
La fenêtre du haut contient la formule logique qui même à ce concept. Cette formule a soit été écrite par vous ou bien calculée et normalisée par Camelis lors de
votre navigation par des clics.

Créer un objet
--------------

Par un clic droit dans la fenêtre de gauche un menu contextuel vous est proposé. Choisissez ``Add``.
Une fenetre de dialigue telle que ci-dessous s'ouvre alors :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/addObject.jpg
      :width: 100%
      :alt: Dialogue pour créer un objet dans Glis

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/addObject.jpg
      :alt: Dialogue pour créer un objet dans Glis

Vous pouvez choisir aussi bien une image dans un fichier ou une simple idée abstraite que vous nommez.
Vous pouvez ainsi entrer successivement des objets.

Créer un attribut
-----------------

Par un clic droit dans la fenêtre des attributs
un menu contextuel s'ouvre qui vous propose d'ajouter un attribut comme ci-dessous :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/apresQuelquesDates.jpg
      :width: 100%
      :alt: Dialogue apresQuelquesDates.jpg

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/apresQuelquesDates.jpg
      :alt: Dialogue apresQuelquesDates.jpg

Pour l'instant vous avez d'un coté des objets de l'autre des propriétès mais aucune propriétés
n'est associées à un objet et réciproquement bien entendu.
*Il n'y encore aucun concept formel de défini*.

Associer attributs et objets
----------------------------

Pour associer attributs et objets vous pouvez procéder de plusieurs manières :

   - par drag d'un objet (ou plusieurs) et drop de l'ensemble sur un attribut
   - par le menu contextuel de la page de droite "``copy and paste to``". Vous selectionnez un ensemble d'objets puis vous ouvrez
     le menu contextuel "``copy and paste to``" qui vous permet de saisir par une formule logique les propriétés à ajouter.

Navigation
----------

Vous pouvez naviguer (i.e. sélectionner des objets) en tappant directement des formules logiques dans la fenêtre du haut.
Par exemple si vous naviguez dans des images par des lieux, attribut ``lieu`` et des types, attribut ``type`` vous pourriez écrire la requète suivante :


::

  lieu Japon and (type Paysage or type Plante) and not type Portrait

L'autre méthode, plus intuive, consiste à construire votre requète par des clics sélectifs dans l'interface *Glis*.
Un clic sur le bouton "``Home``" ramène la requète "``all``" qui selectionne tous les objets. Vous disposez alors de nombreuses
méthode pour faire évoluer votre requète, par exmple :

   - chaque double clic sur un attribut réduit votre espace courramment visible en ajoutant à la requète courrante par un connecteur logique ``and``
     l'attribut doublement cliqué.
   - Un clic sur un objet construit la requète qui connecte toutes les propiètés de l'objet par un connecteur logique ``and``.
   - Vous pouvez selectionner un ensemble de propriètés puis un clic sur le bouton "``zoom``", ajoute alors à votre requète par un connecteur un ``and`` la
     disjonction des attributs sélectionnés.

Intension
---------

Pour un sous ensemble donné d'objets courramment visible à droite, on peut demander à Camelis de calculer l'*intension* de ce sous ensemble.
C'est à dire le plus grand ensemble de propriétés communes à ces objets. Voici un exemple :

   1. On crée un premier objet ``pierre`` avec les propriétés ``note = 12 and sexe is "homme"``, C'est à dire avec deux propriétés valuées.
   2. On crée un second objet ``marie`` avec la propriété ``sexe is "femme"``
   3. On crée un troisième objet ``rennes`` avec la propriété ``nature is "ville"``
   4. On crée un quatième objet ``jules`` avec la propriété ``sexe is "homme"``

Ensuite dans la fenêtre d'extension on sélection les deux objets ``pierre`` et ``marie``. Puis par le menu contextuel de la fenêtre
d'extension (clic droit dans cette fenêtre) on choisit le menu ``Intent``. On obtient dans la fenêtre de
la formule logique, la formule la plus précise, normalisée au sens de Camelis, qui selectionne ces deux objets.
Ici en l'occurence on obtient ``sexe *``. Ce qui signifie que ces deux objets ont un sexe mais avec chacun sa propre valeur.
Mais en plus il s'est passé des choses extrêmement intérressantes dans les autres fenêtres.
Tout d'abord ``rennes`` a disparu de l'extension.
Cet objet ne fait pas parti de l'extension définie par l'intention des objets ``pierre`` et ``marie``. Par contre
l'objet ``jules`` est resté. On découvre ainsi que lui aussi fait partie du groupe naturel d'objets definit par la propriété ``sexe *``.
En langage naturel on interpréte ce groupe par "*les objets qui ont un sexe*" ! Vous venez ainsi de naviguer intelligemment
par la fenêtre de droite ...

L'exemple de Wikipedia
----------------------

Considérez l'exemple de taxonomie des nombres sur le site `Formal concept analysis <http://en.wikipedia.org/wiki/Formal_concept_analysis>`_.
L'ensemble des objets est ``O = {1,2,3,4,5,6,7,8,9,10}``, celui des attributs ``A = {composite, even, odd, prime, square}``.
Voici le treillis correspondant à cet ensemble de concepts formels :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/Concept_lattice.jpg
      :width: 70%
      :alt: Concept_lattice.jpg

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/Concept_lattice.jpg
      :alt: Concept_lattice.jpg



Voici cette taxonomie des nombres décrite dans le langage de Camelis (entension ``.ctx``) :

.. literalinclude:: textes/example-numbers.ctx
   :language: text


Voici une vue dans Camelis/glis de cet ensemble de concepts formels :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/numbers.jpg
      :width: 100%
      :alt: numbers.jpg

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/numbers.jpg
      :alt: numbers.jpg

Avec Camelis/glis on peut parcourrir aisément cet ensemble. Par exemple dans la vue attribut double cliquer
sur l'attribut ``composite`` puis sur l'attribut ``square`` vous obtenez l'ensemble ``{4,9}`` et la
formule ``composite and square`` dans la fenêtre du haut, formule logique qui mène à cet ensemble.
Dans la fenêtre des attributs ceux qui sont communs au deux éléments (cardinalité 2) sont mis en évidence en couleur
orangée. Voici le résultat dans glis :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/composite-square.jpg
      :width: 100%
      :alt: composite-square.jpg

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/composite-square.jpg
      :alt: composite-square.jpg

Vous pouvez aussi très facilement calculer le plus petit concept contenant ``{3}``. Revenez à la vue d'ensemble
en cliquant sur le bouton **Home**. Puis selectionnez dans la vue des objets l'objet ``3``, puis demandez son intension
par un clic droit dans la fenêtre d'extension.
Vous obtenez l'ensemble d'objets ``{3,5,7}`` qui comme ``3`` sont impairs et premiers. Toutes ces informations sont résumées
dans Glis et vous les avez obtenues par un simple clic. Voici la vue obtenue :

.. only:: rst2pdf

   .. figure:: images/quickReferenceGuide/concept3.jpg
      :width: 100%
      :alt: concept3.jpg

.. only:: not(rst2pdf)

   .. figure:: images/quickReferenceGuide/concept3.jpg
      :alt: concept3.jpg


