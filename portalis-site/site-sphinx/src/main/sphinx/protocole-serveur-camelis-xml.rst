.. -*- mode: rst -*-

==============================================
Protocole d'interrogation d'un serveur LIS xml
==============================================

.. sectionauthor:: Benjamin Sigonneau <sigonnea@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/protocole-serveur-camelis-xml.pdf>`

Dans la suite, on suppose un serveur LIS xml lancé sur la machine
``server`` sur le port ``1234``. Nous décrivons le protocole
d'interrogation de ce serveur, ainsi que la forme des réponses.



Liste des commandes implémentées
--------------------------------

ping
  interrogation :

    http://server:1234/ping/

  réponse :

  .. code-block:: xml

    <pingResponse creator="toto@dtc.com"
                  activationDate="2012-10-23T15:20:36+02:00"
                  lastUpdate="none"
                  serviceId="testcontext"
                  nbObject="0"
                  pid="29715"/>


setRole
  interrogation :

    http://server:1234/setRole?adminKey=xxx&userKey=zzz&role=some_role

  réponse :

  .. code-block:: xml

    <setRoleResponse status="ok">
        <user key="987fed65cb" role="admin"/>
    </setRoleResponse>


delKey
  interrogation :

    http://server:1234/delKey?adminKey=xxx&userKey=zzz

  réponse :

  .. code-block:: xml

    <delKeyResponse status="ok"/>


resetCreator
  interrogation :

    http://server:1234/resetCreator?email=xxx&newKey=zzz

  réponse :

  .. code-block:: xml

    <resetCreatorResponse status="ok"/>


pingUsers
  interrogation :

    http://server:1234/pingUsers?userKey=xxx

  réponse :

  .. code-block:: xml

    <pingUsersResponse status="ok" creator="toto@irisa.fr">
        <user key="987fed65cb" role="reader" expires="2012-10-24T01:44:06+02:00"/>
        <user key="abc123ef45" role="admin" expires="2012-10-24T01:44:06+02:00"/>
    </pingUsersResponse>


importCtx
  interrogation :

    http://server:1234/importCtx?userKey=xxx

    Le fichier ctx à importer est passé en tant que paramètre POST
    ctxfile, p. ex. avec la commande ``curl -F ctxfile=@planets.ctx
    http://server:1234/importCtx/``.

  réponse :

  .. code-block:: xml

    <importCtxResponse status="ok"
                       contextName="blablabla" />



intent
  interrogation :

    http://server:1234/intent?userKey=abc123ef45&oid=1&oid=2&...

  réponse :

  .. code-block:: xml

    <intentResponse status="ok">
        <intent>
            <feature name="Distance"/>
            <feature name="satellite"/>
            <feature name="Size"/>
        </intent>
    </intentResponse>


intentX
  interrogation :

    http://server:1234/intent?userKey=abc123ef45&extent=true&oid=1&oid=2&...

  réponse :

  .. code-block:: xml

    <intentResponse status="ok">
        <intent>
            <feature name="Distance"/>
            <feature name="satellite"/>
            <feature name="Size"/>
        </intent>
        <extent>
            <object oid="1" name="earth"/>
            <object oid="2" name="jupiter"/>
            <object oid="5" name="neptune"/>
            <object oid="8" name="uranus"/>
        </extent>
    </intentResponse>


extent
  interrogation :

    http://server:1234/extent?userKey=xxx&query=yyy

  réponse :

  .. code-block:: xml

    <extentResponse status="ok">
        <extent>
            <object oid="1" name="earth"/>
            <object oid="3" name="mars"/>
        </extent>
    </extentResponse>



getTreeFromReq
  interrogation :

    http://server:1234/getTreeFromReq?wq=all&feature=all&userKey=abc123ef45

  réponse :

  .. code-block:: xml

    <getTreeFromReqResponse status="ok">
        <increments>
            <increment name="Distance" card="9"/>
            <increment name="Size" card="9"/>
            <increment name="satellite" card="7"/>
            <increment name="Basket" card="2"/>
        </increments>
    </getTreeFromReqResponse>


getTreeFromReqX
  interrogation :

    http://server:1234/getTreeFromReq?wq=all&feature=all&userKey=abc123ef45&extent=true

    On remarque qu'il s'agit d'une requête getTreeFromReq à laquelle on
    passe l'argument optionnel ``extent``.

  réponse :

  .. code-block:: xml

    <getTreeFromReqResponse status="ok">
        <increments>
            <increment name="Distance" card="9"/>
            <increment name="Size" card="9"/>
            <increment name="satellite" card="7"/>
            <increment name="Basket" card="2"/>
        </increments>
        <extent>
            <object oid="1" name="earth"/>
            <object oid="2" name="jupiter"/>
            <object oid="3" name="mars"/>
            <object oid="4" name="mercury"/>
            <object oid="5" name="neptune"/>
            <object oid="6" name="pluto"/>
            <object oid="7" name="saturn"/>
            <object oid="8" name="uranus"/>
            <object oid="9" name="venus"/>
        </extent>
    </getTreeFromReqResponse>


resetcamelis
  interrogation :

     http://server:1234/resetCamelis?userKey=abc123ef45

  réponse :

  .. code-block:: xml

     <resetCamelisResponse status="ok"/>


importLis
  interrogation :

    http://server:1234/importLis?userKey=xxx&lisfile=foo.lis

  réponse :

  .. code-block:: xml

    <importLisResponse status="ok" contextName="foo"/>


exportLis
  interrogation :

    http://server:1234/exportLis?userKey=xxx

  réponse : le fichier lis est codé en base 64 dans la réponse XML

  .. code-block:: xml

    <exportLISResponse status="ok"
                       data="une grosse chaîne en base 64"/>


exportCtx
  interrogation : (le paramètre ``query`` est optionnel)

    http://server:1234/exportCtx?userKey=xxx

    http://server:1234/exportCtx?userKey=xxx&query=foo

  réponse : le fichier ctx est codé en base 64 dans la réponse XML

  .. code-block:: xml

    <exportCtxResponse status="ok"
                       data="YXhpb20gc21hbGwsIFNpemUKYXhpb20gbmVhciwgRGlzdGFuY2UKYXhpb20gbWVkaXVtLCBTaXplCmF4aW9tIGZhciwgRGlzdGFuY2UKYXhpb20gYmlnLCBTaXplCm1rICJqdXBpdGVyIiBiaWcsc2F0ZWxsaXRlLGZhcgptayAic2F0dXJuIiBiaWcsc2F0ZWxsaXRlLGZhcgo="/>



delObjects
  interrogation :

    http://server:1234/delObjects?userKey=xxx&oid=1&oid=2...

  réponse :

  .. code-block:: xml

    <delObjectsResponse status="ok">
        <extent>
            <object oid="2" name="jupiter"/>
            <object oid="3" name="mars"/>
            <object oid="4" name="mercury"/>
            <object oid="5" name="neptune"/>
            <object oid="7" name="saturn"/>
            <object oid="8" name="uranus"/>
            <object oid="9" name="venus"/>
        </extent>
    </delObjectsResponse>

delFeature
  interrogation :

    http://server:1234/delFeature?userKey=xxx&query=foo&feature=bar&feature=baz

  réponse :

  .. code-block:: xml

    <delFeatureResponse status="ok">
        <increments>
            <increment name="Size" card="9"/>
            <increment name="satellite" card="7"/>
            <increment name="far" card="5"/>
            <increment name="near" card="4"/>
            <increment name="Basket" card="2"/>
        </increments>
    </delFeatureResponse>

createObject
  interrogation :

    http://server:1234/createObject?userKey=xxx&name=foo&feature=f1&feature=f2

    http://server:1234/createObject?userKey=xxx&name=foo&feature=f1&feature=f2&picture=foo.png

  réponse :

  .. code-block:: xml

    <createObjectResponse status="ok">
        <extent>
            <object oid="1" name="earth"/>
            <object oid="3" name="mars"/>
            <object oid="4" name="mercury"/>
            <object oid="9" name="venus"/>
            <object oid="10" name="foo"/>
        </extent>
    </createObjectResponse>


addAxiom
  interrogation :

    http://server:1234/addAxiom?userKey=xxx&premise=foo&concl=bar

  réponse :

  .. code-block:: xml

    <addAxiomResponse status="ok"/>


addAxiomX
  interrogation :

    http://server:1234/addAxiom?userKey=xxx&premise=foo&concl=bar&extent=true

  réponse :

  .. code-block:: xml

    <addAxiomResponse status="ok">
        <extent>
            <object oid="1" name="earth"/>
            <object oid="2" name="jupiter"/>
            <object oid="3" name="mars"/>
        </extent>
    </addAxiomResponse>


setFeaturesFromOids
  interrogation :

    http://server:1234/setFeaturesFromOids?userKey=xxx&query=yyy&oid=1&oid=2feature=f1&feature=f2

  réponse :

  .. code-block:: xml

    <setFeaturesFromOidsResponse status="ok"/>


setFeaturesFromOidsX:
  interrogation :

    http://server:1234/setFeaturesFromOids?userKey=xxx&query=yyy&oid=1&oid=2feature=f1&feature=f2&tree=true

  réponse :

  .. code-block:: xml

    <setFeaturesFromOidsResponse status="ok">
        <increments>
            <increment name="Distance" card="10"/>
            <increment name="Size" card="10"/>
            <increment name="satellite" card="8"/>
            <increment name="Basket" card="2"/>
        </increments>
    </setFeaturesFromOidsResponse>


setFeaturesFromReq
  interrogation :

    http://server:1234/setFeaturesFromReq?userKey=xxx&query=wq&reqForOids=foo&feature=bar

  réponse :

  .. code-block:: xml

    <setFeaturesFromReqResponse status="ok"/>


setFeaturesFromReqX
  interrogation :

    http://server:1234/setFeaturesFromReq?userKey=xxx&query=wq&reqForOids=foo&feature=bar&tree=true

  réponse :

  .. code-block:: xml

    <setFeaturesFromReqResponse status="ok">
        <increments>
            <increment name="Distance" card="9"/>
            <increment name="Size" card="9"/>
            <increment name="satellite" card="7"/>
            <increment name="bar" card="2"/>
        </increments>
    </setFeaturesFromReqResponse>


importFile
  interrogation :

      http://server:1234/importFile&userKey=xxx&file=foo&recursive=true&parts=true

  réponse :

  .. code-block:: xml

    <importFileResponse status="ok"/>


delFeatureFromReq
  interrogation :

    http://server:1234/delFeatureFromReq?userKey=xxx&query=wq&reqForOids=foo&feature=f1&feature=f2

  réponse :

  .. code-block:: xml

    <delFeatureFromReqResponse status="ok"/>


delFeatureFromOid
  interrogation :

    http://server:1234/delFeatureFromOid?userKey=xxx&query=wq&oid=o1&oid=o2&feature=f1&feature=f2

  réponse :

  .. code-block:: xml

    <delFeatureFromOidResponse status="ok"/>


Liste des commandes à implémenter
---------------------------------

Niveau Reader :

- getTreeFromZoom, getTreeFromZoomX
- getFeaturesFromOid
- getFeaturesFromReq

Niveau Collaborator :

- addFeatures, addFeaturesX
