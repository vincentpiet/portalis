.. -*- mode: rst -*-

==========================
Androlis : reprise de code
==========================

:Authors:  Vincent Piet <vincent.piet@irisa.fr>
:Date: 2013-09-06



.. sidebar:: Contents

    .. contents::

.. sectnum::


Présentation
============

Portalis
--------

**Portalis** est une application WEB répartie qui permet à des utilisateurs distants de se partager,
de manière collaboratrive, des serveurs d'information logique LIS. Chaque serveur d'information met en oeuvre un **service**.
Un service est un ensemble d'objets et d'attributs d'un domaine d'intérêt particulier. Portalis permet de naviger intelligemment
au sein des services. Une étape de navigation sélectionne un ensemble d'objets ainsi que les attributs qui les caractérisent. Il s'agit d'un `concept formel`_

Concrêtement, **Portalis** est composé d'un serveur d'administration, appelé **serveur Portalis**, et d'un ensemble de serveurs d'information,
à raison de un serveur d'information par service considéré.
Pour l'instant, les serveurs d'information accessibles sont des serveurs de type **Camelis_**. Ces derniers sont implémentés en `Objective Caml`_. 
Voici le schéma de cette application :

.. image:: images/schemaGeneralPortalis.jpg
   :width: 16 cm

Androlis
--------

**Androlis** est un client Androïd pour l'application répartie **Portalis** ainsi que ses serveurs d'information.

Les clients Androlis communiquent avec le serveur Portalis ainsi qu'avec les serveurs Camelis, à travers **internet** via le protocole **HTTP**.
Les réponses des serveurs sont retournées en XML.


Maven
=====
Maven est un outil de gestion de librairie. Il permet entre autres la récupération automatique de librairies.
Cette opération s'effectue au moyen d'un fichier pom.xml dans lequel sont spécifiés ces dites librairies.

Conversion d'un projet Android sous éclipse
-------------------------------------------
Afin de "maveniser" un projet Android sous éclipse, il est nécessaire d'avoir au préalable installé le plug-in maven m2e 
ainsi que le plug-in m2e-android. 

pom.xml
```````
Il faudra ensuite créer un fichier pom.xml. Voici un exemple de pom minimal :

::

    <project>
      <modelVersion>4.0.0</modelVersion>
      <groupId>com.mycompany.app</groupId>
      <artifactId>my-app</artifactId>
      <version>1</version>
    </project>

modelVersion : version du pom.xml utilisée
groupeID : entité à l'origine du projet
artifactId : nom des ressources générées par maven
version : version des ressources générées

plus d'informations sur le pom.xml :
 
http://maven.apache.org/guides/mini/guide-naming-conventions.html
http://java.developpez.com/faq/maven/?page=terminologie
http://stackoverflow.com/questions/2487485/what-is-maven-artifact


Afin d'ajouter des librairies au projet grâce à maven, il faut les déclarer dans le fichier pom.xml. 
Afin d'obtenir les informations nécessaires, on peut par exemple consulter le site :

http://mvnrepository.com/

Conversion
``````````
Pour convertir le projet, il ne reste plus qu'à cliquer droit sur le projet, configure,
convert to maven project...

Problème rencontré
``````````````````
L'ajout de librairies peut ne pas fonctionner : les librairies ajoutés ne sont pas retrouvées dans l'application
compilée. Une solution à ce problème se trouve dans la partie concernant les librairies tierces.


Librairies Tierces
==================
Une librairie tierce (Third-party library) est une librairie développée par une entité autre que sun. 
Elles proposent des services et évitent donc de les ré-implémenter.

Le cas d'Android
----------------
A la compilation, en utilisant maven, ces librairies ne peuvent pas être retrouvées causant une erreur de type classNotFound.
Une solution est de les ajouter, "à la main", dans le dossier libs du projet Android.

Cet ajout à la main peut être automatisé grâce à maven, et notamment grâce au plugin : "maven-dependency-plugin". En
utilisant la dépendance maven : "copy-dependencies", il est possible de copier les dépendances du projet à l'endroit souhaité
(dans notre cas, le fichier libs).
A noter qu'eclipse peut signaler une erreur dans le cas ou on utilise plusieurs plugins. Il proposera alors une correction valide,
utilisant "lifecycle-mapping" pour ordonnancer l'exécution des plugins.


plus d'information sur comment utiliser maven-dependency-plugin ici:
http://maven.apache.org/plugins/maven-dependency-plugin/examples/copying-project-dependencies.html

EGit
====
EGit est un plugin eclipse permettant une utilisation simple de projets Git. 

Importer un projet
------------------
Une fois votre projet git disponible sur un serveur hôte, vous pouvez l'importer facilement dans eclipse 
en utilisant la fonctionnalité "clone a git" de EGit.

Une fenêtre s'ouvre contenant entre autres le champ URI. Collez l'adresse web de votre projet, les deux champs suivant 
devraient alors se remplir automatiquement. Ajoutez enfin votre nom d'utilisateur et votre mot de passe pour pouvoir
mettre à jour votre projet en local ou sur le serveur simplement.

Sur la page suivante choisissez la branche que vous voulez (master si vous êtes seul).

Enfin sur la dernière page choisissez l'endroit ou vous souhaitez enregistrer le projet, 
vérifiez que la checkbox "import all existing projects after clone finishes" soit cochez puis cliquez sur finish.

Utilisation des différentes fonctionnalités
-------------------------------------------
Les principales fonctionnalités sont présentent dans le menu clic droit sur le projet, Team. La fonctionnalités
commit donne accès à un commit and push permettant d'automatiser la procédure.

Les principales fonctionnalités sont définies à cette adresse:
http://eclipsesource.com/blogs/tutorials/egit-tutorial/


Working Set
===========
L'importation d'un nombre important de projets dans eclipse pour encombrer votre espace de travail au point de ne plus vous
y retrouver dans vos projets. Afin d'y remédier, eclipse propose une notion de Working Set, permettant de séparer vos projets 
entre différents espaces de travail.

Création d'un Working Set
-------------------------
Dans la vue Package Explorer cliquez sur la flèche et sélectionnez "Select Working Set". 
Dans la fenêtre qui viens de s'ouvrir cliquez sur New. Sélectionnez alors "Ressources" afin de pouvoir
ajouter tous types de fichier dans votre Working Set. Entrez alors le nom que vous souhaitez associer à votre Working Set,
sélectionner les projets que vous souhaiter lier à cette vue, cliquez sur finish, puis double cliquez sur le Working Set
que vous venez de créer. Votre package explorer vous montre maintenant les projets de votre Working Set

Navigation
----------
Pour naviguer entre vos différents Working Set, cliquez sur la flèche de votre package explorer puis sélectionner
le Working Set sur lequel vous souhaiter travailler. Pour voir l'intégralité de vos projets choisissez le Working Set
Windows Working Set.


Utilisation de javascript au sein d'une application
===================================================
Nous avons choisi d'utiliser javascript dans notre application afin d'afficher des diagrammes représentant les données traitées 
et la carte Google maps. Ces choix ont été faits suite à des difficultés, et sont détaillés dans le document de description de code.

Affichage d'une page
--------------------
Voici les étapes que nous avons suivi pour l'intégration de javascript dans notre application :
  
   - Ajout d'un layout web view dans le fichier xml de l'activité ciblée.
   - Ajout de la page HTML contenant le code javascript dans le répertoire "assets/www"
   - Puis appel de 3 méthodes dans le fichier java de l'activité :
   
      - Ajout de la fonctionnalité de zoom au web view :
        webView.getSettings().setBuiltInZoomControls(true);
       
      - Autorisation d'utilisation de javascript pour le web view :
        webView.getSettings().setJavaScriptEnabled(true);
       
      - Liaison de notre page javascript avec le web view :
        webView.loadUrl("file:///android_asset/www/pieChart.html");
       
       
Il faut maintenant faire communiquer android et javascript afin d'échanger des informations sur les données à afficher.

Communication
-------------
Le communication entre nos pages web et notre application peut prendre trois formes :
   
   - passage de données de l'application à la page web
   - appel de fonctions java depuis javascript
   - appel de fonctions javascript depuis java
   
Afin de traiter les deux premiers cas nous avons utilisé un procédé similaire mettant en oeuvre un objet faisant l'interface entre javascript et java.

Utilisation d'un objet java pour la communication java - javascript
```````````````````````````````````````````````````````````````````
Nous avons défini dans le package com.androlis.javascriptCommunication deux classes servant d'interface entre java et javascript.

Ces classes permettent de récupérer des valeurs dans notre mémoire Memory tels que 
les propriétés disponibles à la requête courante, les coordonnées gps ainsi que les adresses de fichiers KMLs. 
Ces valeurs sont ensuite retournés au format JSon, permettant ainsi l'appel de ces fonction depuis notre code javascript.

Dans le cas de la classe destinée aux diagrammes, on a ajouté des fonctions retournant
des textes à afficher (ces textes étant récupérés depuis les fichiers values, il permettent l'affichage
de messages dans la langue du système android.
Cette classe permet également de mettre à jour un attribut permettant de connaître la propriété cliqué
par l'utilisateur, et d'effectuer une requête prenant en compte cette propriété afin de mettre a jour les données affichées
(afficher les fils de la propriété sélectionnée).

Appel de fonction javascript
````````````````````````````
Afin d'utiliser les fichiers KML et permettre aux utilisateurs de les afficher ou les cacher, la page HTML
contenant la carte contient deux fonctions javascript, l'une permettant d'afficher un fichier KML, 
l'autre permettant de le faire disparaître.

Pour se faire lors de l'appel de la fonction d'initialisation de la carte en javascript, on récupère l'ensemble des adresses des fichiers KMLS.
On créé alors à partir de ces fichiers des Layers, objets permettant l'affichage de ces KMLS. Ces Layers sont placés dans un tableau en fonction 
de l'ordre d'arrivé de l'adresse des fichiers KMLS lors de leur récupération.

On défini alors deux fonctions permettant de cacher ou d'afficher les KMLS,
prenant en paramètre un entier qui est l'indice du Layer à afficher ou à cacher.
L'affichage d'un KML se fait en lui donnant la map sur laquelle il doit s'afficher, pour le cacher, plutôt que de lui passer une carte, on lui passe null.

Ces fonctions peuvent par la suite être appelées en javascript par appel de loadUrl sur l'objet webview :
webView.loadUrl("javascript:displayKML("+(pos)+")");


Annexe
======
Les informations contenues dans cette partie concernent des choix de conception qui ont été abandonnées.

Log4j
-----
Log4j est une librairie permettant entre autres de définir le format, le degré d'importance 
ou encore un endroit ou l'on souhaite rediriger les logs. Les logs sont les messages émis à la compilation
ou à l'exécution d'un programme. Ils peuvent donc être des messages d'erreur, d'avertissement, ou encore de simples informations.

Log4j et Android
````````````````
Afin de fonctionner correctement, Log4j a besoin d'un fichier de propriétés (.properties) au format xml.
Cependant, Android utilise déjà le format xml. Il est donc nécessaire de les déclarer différemment.

Un autre problème entre Log4j et android est lié aux java beans. Plus d'explications sur ce lien :
http://mindpipe.blogspot.de/2011/11/android-log4j-exception-properties.html 

Une solution est l'utilisation de la librairie : android-logging-log4j. 
Cette librairie définie le concept de LogConfigurator en java, permettant ainsi d'écrire un fichier de propriétés en java.

Pour pouvoir utiliser les logs, il faut donc créer un fichier java de configuration que l'on placera dans le package défini
lors de la création de l'application. Il faut également appeler cette configuration dans la première activité appelée par l'application.

Il faut également donner la permission de lire et d'écrire un fichier externe si l'on souhaite rediriger les logs dans un fichier.
Pour se faire il faut les ajouter au fichier AndroidManifest.xml :

<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />

<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />

Plus d'information sur l'utilisation de Log4j pour Android sont donnés à l'adresse :
http://brucebilyeu.com/blog/content.php?/archives/8-Logging-in-Android.html

Utilisation de log4j
````````````````````
L'utilisation des logs se fait au moyen d'un objet de type Logger :
private final Logger logger = Logger.getLogger(Login.class);
   
On peut alors l'utiliser pour émettre un message auquel on spécifiera un degré d'importance.

Les différentes utilisations de Log4j sont décrites à l'adresse :

http://beuss.developpez.com/tutoriels/java/jakarta/log4j/

Avertissement dans la console
:::::::::::::::::::::::::::::
L'utilisation de la librairie m2e-android peut provoquer un certain nombre d'avertissement dans la console
de type :
Dx warning: Ignoring InnerClasses attribute for an anonymous inner class

Ces avertissements sont bénins, il n'est cependant pas possible des les supprimer, plus d'informations sont données 
à cette adresse :
http://code.google.com/p/android-logging-log4j/issues/detail?id=1

Abandon de log4j
````````````````
Au cours de ce projet Log4J a été abandonné. La raison principale est qu'il n'était pas à priori possible de récupère
les logs provenant de Portalis et Camelis, rendant ainsi difficile l'identification de la cause d'un problème.
Suite à la constatation de ce problème, un nouveau système de logs a été mis en place dans Portalis permettant une meilleur
flexibilité au niveau des logs, ainsi que l'affichage dans le LogCat d'Android des logs de Portalis et Camelis.


Carte et android-maps-extensions
--------------------------------
Android maps extensions est une librairie pour Google maps, permettant entre autres d'agréger les différents marqueurs
présents sur une carte afin de gagner en lisibilité. Cette librairie est trouvable à l'adresse :

https://code.google.com/p/android-maps-extensions/

Contenu
```````
Le téléchargement de l'archive du projet met à disposition de l'utilisateur la librairie, une démo de l'utilisation de cette librairie
et une démo de fonctionnalités de Google play services. Elle contient également un projet lib-installer dont le but est de placer
dans le fichier lib de l'application deux librairies utilisées.

Problème rencontré
``````````````````
Cette librairie est donc un multi projet maven android. Son utilisation sous l'IDE android studio se fait facilement, 
en effet, elle a été programmé grâce à cet IDE.
En revanche, l'importation peut poser problème sous Eclipse, les fichiers .projet n'étant pas à jour, les projets maven android
peuvent s'importer comme des projets maven java. Afin de solutionner le problème il faut aller modifier la nature du projet du fichier .projet
de chacun des projets android et y ajouter :
<nature>com.android.ide.eclipse.adt.AndroidNature</nature>
<nature>org.eclipse.m2e.core.maven2Nature</nature>

Utilisation
```````````
Une fois le Multi projet maven importé, il faut utiliser maven/update projet, afin d'installer les librairies mentionnées précédemment.
Il faut ensuite ajouter android-maps-extensions comme librairie à notre projet utilisateur :
Clic droit sur le projet/properties/android puis dans l'encadré Library Add et ajouter la librairie.

Si elle n’apparaît pas dans les choix de librairies, il faut suivre la même procédure en partant de la librairie,  puis cliquer sur is Library, plutôt que Add. Elle devrait alors apparaître correctement.

Il faut enfin ajouter un certain nombre de permissions ainsi que clé API délivrée par Google, propre à chaque application, dans l'application
destinée à utiliser la librairie. La clef API nécessite de connaître sa clef SHA 1, présente dans Eclipse sous Window/Preferences/Android/Build. 
Ces ajouts sont requis par Google pour l'utilisation de Google maps.

Plus d'informations sur les permissions sont données ici:
https://developers.google.com/maps/documentation/android/start

Les clefs API étant délivrées ici:

https://code.google.com/apis/console/

Abandon de android-maps-extensions
``````````````````````````````````
L'extension android-maps-extensions à été abandonné suite à la nécessité d'afficher des fichiers KMLs sur la carte. En effet, la carte
Google maps android native ne propose pas l'importation de fichier KMLs. Pour les exploiter avec cette carte, il faut donc les parser
afin d'en extraire les informations et recréer à la main ce qui était attendu.

A cause de ce problème trop contraignant, nous avons décidé d'abandonner la carte Google maps native, et par conséquent cette extension, 
et d'utiliser la carte javascript qui propose l'importation de fichiers KMLs facilement. Une librairie de clustering (markerclustererplus)
a donc été utilisée, proposant le même service qu'android-maps-extensions. Son utilisation étant simple nous considérons qu'une partie 
dédié à cette librairie est inutile.

Concernant l'utilisation de la carte javascript, celle ci nécessite les permissions et la clé api présenté dans la rubrique Utilisation ci-dessus.