.. -*- mode: rst -*-

=====================
Développer avec Maven
=====================

.. sectionauthor:: Yves Bekkers <bekkers@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/maven.pdf>`

.. |chantier| image:: images/chantier.gif
   :align: middle
   :width: 100 px


Qu'est-ce que Apache Maven
==========================

.. _`Apache Maven`: http://fr.wikipedia.org/wiki/Apache_Maven

`Apache Maven`_ est un outil logiciel libre pour la gestion et l'automatisation de production des projets logiciels
*Java* en général et *Java EE* en particulier. L'objectif recherché est comparable au système ``Make`` sous *Unix* ou bien encore
``Ant`` pour *Java*, tout en offrant beaucoup plus de services. Comme ces deux derniers système, il permet de
produire un logiciel à partir de ses sources, en optimisant les tâches réalisées à cette fin et en garantissant le bon ordre de fabrication.
Comme eux, il permet de gérer le cycle de vie du logiciel en proposant les tâches telles que :

- compiler
- tester
- mettre sous forme de packages distribuables
- installer
- déployer
- générer des rapports
- générer un site de documentation

Mais *Apache Maven*, généralement appelé *Maven*, pour faire court, va beaucoup plus loin.
**Il impose des méthodes de développement** basée sur une **structuration stricte et standard des projets**.
Chaque type de projet possède des conventions de structuration.

Maven, la base
--------------

Où la structuration déculpe les forces
''''''''''''''''''''''''''''''''''''''

Le développeur est fortement encouragé à suivre les conventions de structuration,
car on lui fournit des outils automatiques qui sont capables, connaissant ces conventions,
d'effectuer seuls les tâches du cycle de vie d'un projet. Voici quelques exemples :

Compilation *Java*
   mettre tous les sources *Java* dans le répertoire ``src/main/java`` du projet, alors :
   
   sans aucune autre précision, la commande ``mvn compile`` lancée dans le répertoire racine du projet,
   compile tous les sources *Java* et place par défaut les objets dans le répertoire ``target/classes`` du même projet !

Construction d'une application *WEB* à base de *Servlet*, de pages *JSP* et de pages *HTML* statiques
   Procéder comme suit :
   
   1. Mettre tous les sources *Java* (servlets y compris) dans le répertoire ``src/main/java`` du projet
   #. Mettre toutes les ressources de l'application *WEB* dans le répertoire ``src/main/webapp`` du projet
   #. La commande ``mvn package`` lancée dans le répertoire racine du projet aura deux effets :

      - Un nouveau site web correspondant à votre projet sera déposé
        dans le répertoire ``target/nom-du-projet-version-du-projet`` du projet
      - Un fichier *war* déployable de nom ``nom-du-projet-version-du-projet.war`` sera
        déposé dans le répertoire ``target`` du projet considéré.

En résumé, un projet *maven* est simplement un répertoire organisé selon les conventions *Maven* dans lequel on trouve un fichier
XML appelé ``pom.xml`` qui est le fichier de configuration du projet. On l'appelle aussi le *fichier pom* !

L'interêt de ces conventions est que, non seulement elles permettent de construire des outils automatiques de développement,
mais elles permettent aussi aux développeurs de s'y retrouver plus facilement lorsqu'ils passent d'un projet à un autre.
**Les choses sont toujours à la même place** d'un projet à l'autre ...

A la base *Maven* est donc juste un ensemble de conventions d'organisation, plus des fichiers pom. Puis pour chaque tâche ou groupe de tâches de développement, le programmeur peut
alors trouver un *plugin* capable d'effecter la tâche qu'il désire.

Gestion des librairies et de leur version
'''''''''''''''''''''''''''''''''''''''''

Associé aux conventions d'organisation et aux plugins, Maven défini un mode de nommage universel des libraires et les rends accessible grâce à la notion de
**dépot de librairies**. Les dépots de librairies sont organisés en une hiérarchie de dépots, avec un (ou quelques) dépots centraux connus implicitement de maven,
des dépots locaux à des groupes d'utilisateurs que l'utilisateur peut déclarer au sein de ses projet Maven et enfin un dépot privé à chaque utilisateur.
Le chargement des librairies sur la machine de l'utilisateur est alors totalement transparent. Maven fouille les dépots centraux ainsi que les dépots
locaux pour charger sur le dépot privé de l'utilisateur les libraires qu'il utilise.

La gestion des dépendances au sein de Maven est simplifiée par les notions d’héritage et de transitivité.
La déclaration de ces dépendances est alors limitée.

Un projet = un arbre de modules
'''''''''''''''''''''''''''''''


Un projet *Maven* peut être divisé en sous projets appelés *modules* en Maven. Les modules d'un projet
peuvent être de deux sortes :

1. Les modules d'implémentation qui produisent un artifact (une librairie ``*.jar``, un exécutable ``*.exe``,
   un serveur ``*.war``, une doc, ...).
   L'artifact implémente un ensemble de fonctionnalités liées sémantiquement.
2. Les modules de regroupement logique de sous modules. On les appelle les modules pom.

Ainsi un projet Maven est organisé logiquement en un arbre dont les noeuds avec des descendants sont
des modules pom et dont les feuilles sont des module d'implémentation.

Configuration de Maven
----------------------

Il y a deux niveaux de fichiers de configuration en Maven :

1. Configuration globale à l'utilisateur : fichier ``settings.xml``
2. Configuration d'un module au sein d'un projet : fichier ``pom.xml`` (fichier pom)

Configuration globale à l'utilisateur
'''''''''''''''''''''''''''''''''''''

Créer un répertoire ``.m2`` dans votre ``homedir``. C'est dans ce répertoire que Maven va gérer votre dépot privé.

Dans ce répertoire l'utilisateur doit déposer un fichier ``settings.xml`` qui contient toutes les informations de
gestion des dépots préconisés par cet utilisateur.
Voici à quoi ressemble le fichier de configuration pour notre projet *Portalis* :

.. literalinclude:: xml/example.settings.xml
   :language: xml

Ce fichier déclare que le projet *Portalis* utilise un dépot local offert par l'*Inria*. Il donne aussi les mots de passe qui permettent d'accéder ce dépot
(qui ont étés floutés à dessein dans ce document ...).

Fichiers pom
''''''''''''

.. todo::

   |chantier| introcuire les fichiers pom..


Utilisation des variables dans les fichiers pom
'''''''''''''''''''''''''''''''''''''''''''''''

Au sein des fichiers pom on peut utiliser un grand nombre de variables d'environnement prédéfines.
L'utilisateur peut aussi en définir. On les trois catégories suivantes :

``project.``\*
    Maven Project Object Model (POM). Utiliser le préfixe ``projet.``\* pour référencer des valeurs dans un POM Maven. 
``settings.``\*
    Maven Settings. Utiliser le préfixe ``settings.``\* pour référencer des valeurs de vos paramètres Maven dans ``~/.m2/settings.xml``. 
``env.``\*
    Variables d'environnement système telle que ``PATH`` and ``M2_HOME``, elles peuvent être référencées à l'aide du préfixe
    ``env.``\*. 
``java.``\*
    Utiliser le préfixe ``java.``\* pour référencer les variables d'environnement définies par *Java*. 

Variables d'environnement système
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: textes/var-sys.txt
   :language: bash

Variables du projet Maven courant
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: textes/var-project.txt
   :language: bash

Variables d'environnement java
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. literalinclude:: textes/var-java.txt
   :language: bash

Mais encore
~~~~~~~~~~~

Voir aussi `Maven Properties Guide`_ ainsi que `Maven's Resource Filtering`_

.. _Maven Properties Guide: http://docs.codehaus.org/display/MAVENUSER/MavenPropertiesGuide

.. _Maven's Resource Filtering: http://www.sonatype.com/books/maven-book/reference/resource-filtering.html

Les cycles de vie en Maven
--------------------------

*Maven* possède trois familles de cycles de vie non correlées :

1. Le *Nettoyage* ou cyle ``clean``
#. La *construction d'applications* ou cycle ``default`` dont on configure les tâches dans l'élément ``<build/>`` des fichiers pom.
#. La *préparation de la documentation* ou cycle ``site`` dont on configure les tâches dans l'élément ``<report/>`` des fichiers pom.

Chaque cycle de vie définit une séquence de *tâches* qui lui est propre.

Voici par exemple le cycle de vie des principales tâches (elles ne sont pas toutes là ... pour les voir toutes, se référer à ``life cycle reference``)
appartenant à ``default`` :

1. ``validate`` - validate the project is correct and all necessary information is available
#. ``compile`` - compile the source code of the project
#. ``test`` - test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
#. ``package`` - take the compiled code and package it in its distributable format, such as a JAR.
#. ``integration-test`` - process and deploy the package if necessary into an environment where integration tests can be run
#. ``verify`` - run any checks to verify the package is valid and meets quality criteria
#. ``install`` - install the package into the local repository, for use as a dependency in other projects locally
#. ``deploy`` - done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.

Plugin
''''''

Chaque tâche est effectuée grâce à un *plugin Maven*, tous les plugins utilisés sont déclarés au sein des fichiers pom.
Pour chaque plugin, on doit trouver un minimum d'informations requis : ``groupId, artifactId, version``. Un plugin possède des ``goals`` ou *tâche*.

La notion de plugin est transversale à celle de cycle de vie. De sorte qu'un plugin peut offrir des tâches pour un ou plusieurs des cycles de vie.

Groupes de plugins
''''''''''''''''''

pour la recherche de plugins, ces derniers peuvent être réunis en *groupes de plugins*. Un ``pluginGroup`` possède un ``groupId``
par lequel on a accès à une liste de *plugins*:

Le fichier ``settings.xml`` contient une liste d'éléments ``pluginGroup``. Chaque liste est recherché quand un plugin est utilisé
et le groupId n'est pas fournie dans la ligne de commande. le fichier ``settings.xml`` contient toujours pas défaut les groupes

- ``org.apache.maven.plugins``
- ``org.codehaus.mojo``

Génération de rapports
----------------------

Certains plugins Maven proposent des rapports en plus de goals ``default``.
Certains plugins ne proposent d'ailleurs que des rapports.
A la génération du site, Maven exécutera tous les rapports configurés pour les calculer.

Pour ajouter un rapport dans le site Maven, il faut rajouter dans le fichier ``pom.xml``
du projet ``site`` une déclaration telle que la suivante :

.. literalinclude:: xml/maven.report.declare.xml
   :language: xml

Les plugins les plus courants proposant des rapports sont les suivants :

=======================  ======================================================================
Plugin                   utilisation
=======================  ======================================================================
project-info-reports     génère les rapports standards Maven
surefire-report          génère le rapport des résultats des tests unitaires
changelog                génère la liste des changements récents du gestionnaire de source
changes                  génère le rapport du système de ticketing du projet
checkstyle               génère le rapport de checkstyle
javadoc                  génère la JavaDoc du projet
pmd                      génère le rapport PMD
=======================  ======================================================================




Mavennisation du projet Portalis
================================

Conventions de nommage des module du projet *Portalis* au sein de *Maven*
-------------------------------------------------------------------------

Les conventions de nommage du projet Portalis suivent les conventions de nommage préconisées par *Maven* et par
la communauté des développeurs *Java*. On utilise les 3 niveaux utilisés dans Maven :

- **groupId** : Le préfixe du ``groupId`` de tous les modules du projet *Portalis* est ``fr.irisa.lis.portalis``, ce nom suit la régle de nom de *paquet*
  (*librairie Java*) utilisée par la communauté des programmeurs Java.
  Cette régle consiste à utiliser comme nom de paquet un nom de domaine que l'on controle. Ce qui permet d'identifer tout paquet de manière unique sur le WEB.
  Tous les paquets *Java* (librairies *Java*) du projet *Portalis* possédent ``fr.irisa.lis.portalis`` comme préfixe de nom de paquet.
- **artifactId** : Dans le jargon de Maven un *artifact* est un module. Dans le projet Portalis, les identifants de module utilisent une notation pointée.
  On trouve par exemple les identifiants suivants :
  * ``portalis-root`` l'artifact racine du projet *Portalis*
  * ``portalis-site`` Module de construction du site web d'information du projet *Portalis*.
  * ``portalis-admin-core`` librairie de base pour la construction de modules d'administration.
  * ``portalis-camelis-server`` Serveurs Camelis, programmés en Ocaml
  * ...
  Les modules suffixés par ``-root`` sont des moudules de type ``pom`` qui ne contiennent pas de code mais regroupent
  un ensemble de sous-modules reliès logiquement
- **version** : *Portalis* utilise une identification de version à trois niveaux (0.0.4, 1.0.1, ...)
- **snapshot** : les identifications de snapshots sont suffixées par ``-SNAPSHOT`` suivi d'une signature comprenant la date de la snapshot.
  
Structure du projet Portalis
----------------------------

La structure logique des modules et sous modules du projet *Portalis* ne correspond pas
exactement à la structure physique du projet en répertoire dans le système de gestion de fichiers.
Chaque module est un sous répertoire.
Les module noeuds de l'arbre logique se trouvent dans le même répertoire que leur sous modules.
    
Voici la structure logique du projet Portalis :
    
.. literalinclude:: textes/maven-logical-structure.txt
   :language: text
    
Dans cet arbre logique :
    
- chaque noeud ayant des descendants est un projet ``pom`` sans code (norme Maven)
- chaque feuille est un ensemble de code. Le code OCaml est dans la feuille ``portalis-camelis-server``

On peut construire le projet individuellement par les feuilles ou par branche, voir pour tout le projet en se plaçant respectivement
aux noeuds ou à la racine.

Voici la structure physique du projet Portalis :

.. literalinclude:: textes/maven-physical-structure.txt
   :language: text

Tous les modules d'un même sous groupe y compris leur module ``pom`` racine du sous groupe ont été mis à plat
dans un même sous répertoire.    
Le sous répertoire ``cargo`` de ``portalis`` contient les deux utilitaires pour les tests d'intégration des divers serveurs.

Propriétés du projet Portalis
-----------------------------

Voici les propriétés définies dans le projet Portalis :

Propriétés spécifiques à l'environement de développement d'un utilisateur
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

Elles sont définies dans le fichier ``settings.xml`` de l'utilisateur, voici un exemple de déclarations.
    
.. literalinclude:: xml/example.settings.xml 
   :language: xml

Propriétés globales au projet *Portalis*
''''''''''''''''''''''''''''''''''''''''

Elles sont définies dans le fichier pom du module ``portalis-root``, voici leurs déclarations
    
.. literalinclude:: xml/portalis-root-properties.xml
   :language: xml

Propriétés locales à un module de test
''''''''''''''''''''''''''''''''''''''

Elles sont définies dans le fichier pom du module de test. Voici un exemple de définitions :
    
.. literalinclude:: xml/portalis-test-properties.xml
   :language: xml

Propriétés locales à un module de code
''''''''''''''''''''''''''''''''''''''

Elles sont définies dans le fichier pom du module. Voici un exemple :
    
.. literalinclude:: xml/portalis-main-properties.xml
   :language: xml

Fichiers de propriétes
''''''''''''''''''''''

cargo.properties
~~~~~~~~~~~~~~~~

Défini dans le module ``portalis/portalis-cargo/cargo-launcher`` dans son répertoire ``src/test/resources``.
Il est copié par *mvn* à la phase ``package`` de ce module dans les répertoires suivants :

- ``portalis/portalis-server/server-integration-test/src/test/resources``

documentation.properties
~~~~~~~~~~~~~~~~~~~~~~~~

Défini dans le module ``portalis/portalis-site/site-documentor`` dans son répertoire ``src/test/resources``.
Il est copié par *mvn* à la phase ``validate`` de ce module dans les répertoires suivants :

- ``portalis/portalis-server/server-integration-test/src/test/resources``

portalis.properties
~~~~~~~~~~~~~~~~~~~

Défini dans le module ``portalis/portalis-admin/admin-core`` dans son répertoire ``src/test/resources``.
Il est copié par *mvn* à la phase ``validate`` de ce module dans les répertoires suivants :

- ``portalis/portalis-server/server-webapp/src/main/resources``
- ``portalis/portalis-server/server-integration-test/src/test/resources``
- ``portalis/portalis-camelis/camelis-integration-test/src/test/resources``

Et voici les classes qui effectuent le chargement de ces fichiers de properties :

Dans ce tableau la variable ``${prefix}`` représente le préfixe ``fr.irisa.lis.portalis``.

+------------------------------+--------------------------------------------------------------------+--------------------------------------+------------------------+
| Fichier de Properties        | Classe                                                             | Module                               | Répertoire             |
+==============================+====================================================================+======================================+========================+
| ``portalis.properties``      | ``${prefix}.admin.core.server.service.Proprietes``                 | ``portalis-admin-core``              | ``src/test/resources`` |
+------------------------------+--------------------------------------------------------------------+--------------------------------------+------------------------+
| ``ioConstants.properties``   | ``${prefix}.admin.core.server.service.ProprietesBase``             | ``portalis-admin-core``              | ``src/main/resources`` |
| ``errorMess.properties``     |                                                                    |                                      |                        |
+------------------------------+--------------------------------------------------------------------+--------------------------------------+------------------------+
| ``documentation.properties`` | ``${prefix}.site.DocumentationProprietes``                         | ``portalis-site-documentator``       | ``src/test/resources`` |
+------------------------------+--------------------------------------------------------------------+--------------------------------------+------------------------+
| ``cargo.properties``         | ``${prefix}.admin.core.integrationTest.IntegrationTestProprietes`` | ``portalis-server-integration-test`` | ``src/test/resources`` |
+------------------------------+--------------------------------------------------------------------+--------------------------------------+------------------------+


Mavennisation du module ``portalis-camelis-server`` (écrit en OCaml)
--------------------------------------------------------------------

1. **phase "compile" de Maven** : Exécutée par un makefile appelé depuis le plugin Maven ``ant-run``, créé un exécutable ``*.exe``.
2. **phase "package" de Maven** : Dépose l'exécutable version-né et un zip version-né des sources **OCaml** dans la partie
   ``target`` du projet ``portalis-camelis-server``
3. **phase "install" de Maven** : Dépose l'exécutable version-né et un zip version-né des sources dans le
   dépot Maven local de l'utilisateur (dépot dans son espace privé)
4. **phase "deploy" de Maven** :  Dépose l'exécutable version-né et un zip version-né des sources dans le dépot de
   l'inria, visible pour tous sur le web.
   
Mavennisation du module ``portalis-site`` (écrit en Sphinx + ReStructuredText)
------------------------------------------------------------------------------

.. todo::

   |chantier| Décrire la Mavennisation du module ``portalis-site``.

