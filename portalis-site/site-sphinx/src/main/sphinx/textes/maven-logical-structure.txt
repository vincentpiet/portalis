Modules                                     Type   Répertoires

portalis-root                               pom    portalis/portalis-root
   portalis-admin-root                      pom    portalis/portalis-admin/admin-root
       portalis-admin-database              jar    portalis/portalis-admin/admin-database
       portalis-admin-database-impl-xml     jar    portalis/portalis-admin/admin-database-impl-xml
   portalis-launcher-root                   pom    portalis/portalis-launcher/launcher-root
       portalis-launcher-core               jar    portalis/portalis-launcher/launcher-core
       portalis-launcher-webapp             war    portalis/portalis-launcher/launcher-webapp
       portalis-launcher-integration-test   test   portalis/portalis-launcher/launcher-integration-test
   portalis-camelis-root                    pom    portalis/portalis-camelis/camelis-root
       portalis-camelis-server              exe    portalis/portalis-camelis/camelis-server
       portalis-camelis-integration-test    test   portalis/portalis-camelis/camelis-integration-test
   portalis-site-root                       pom    portalis/portalis-site/site-root
       portalis-site-sphinx                 pom    portalis/portalis-site/site-sphink
       portalis-site-webpapp                war    portalis/portalis-site/site-webapp
       portalis-site-documentator           jar    portalis/portalis-site/site-documentator

portalis-cargo-root                         pom    portalis/portalis-cargo/cargo-root
    portalis-cargo-webapp                   war    portalis/portalis-cargo/cargo-webapp
    portalis-cargo-launcher                 jar    portalis/portalis-cargo/cargo-launcher
   