${project.groupId}
${project.artifactId}
${project.version} attention ${pom.version} ou ${version} son "déprécated"
${project.name} ou ${pom.name}
${project.description}

${project.basedir} ou ${basedir}
${project.baseUri} = "file://"+${basedir}

${project.build.directory} = ${basedir}/target
${project.build.sourceDirectory} = ${basedir}/src/main/java
${project.build.scriptSourceDirectory} = ${basedir}/src/main/scripts
${project.build.testSourceDirectory} = ${basedir}/src/test/java
${project.build.outputDirectory} = ${basedir}/target/classes
${project.build.testOutputDirectory} = ${basedir}/target/test-classes

${project.parent.\*} idem as ${project.\*}
${project.parent.version} ou ${parent.version}

${project.distributionManagementArtifactRepository.url}
