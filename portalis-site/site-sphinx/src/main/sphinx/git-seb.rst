.. -*- mode: rst -*-

===========================
Gestion de version avec git
===========================

.. sectionauthor:: Benjamin Sigonneau <sigonnea@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/git-seb.pdf>`


Configuration de Git
--------------------

Avant de commencer, on va configurer le nom, l'adresse de courriel et
les fins de ligne :

  git config --global user.name "Your Name"
  git config --global user.email "your_email@whatever.com"
  git config --global core.autocrlf input
  git config --global core.safecrlf true

(Pour le courriel, je pense que c'est optionnel sur les machines de
l'Irisa, mais ça ne coûte rien)


Initialisation d'un dépôt et premières notions sous Git
-------------------------------------------------------

On initialise le dépôt Git ::

     cd /path/to/project-src
     git init


On va ignorer gentiment tout les artefacts de compilation en créant un
fichier ``.gitignore`` qui contient la liste des fichiers à ignorer : ::

  cat > .gitignore << EOF
  *.cm[i,o,a,x]
  *.cmx[a,s]
  *.[a,o]
  _build/*
  EOF

Enfin, on va versionner ce nouveau fichier : ::

  git add .gitignore
  git commit

L'éditeur par défaut se lance pour rentrer le message de commit. L'usage
veut que la première ligne soit un résumé du commit, que la seconde soit
vierge et que les lignes suivantes décrivent plus en détail le commit.


On peut vérifier que le commit s'est bien déroulé en listant
l'historique des changements : ``git log``.

Chaque changement (commit) est identifié par son hash SHA1. Pour voir le
contenu d'un commit : ``git show commit_id``. Comme c'est un peu long, on
peut abréger le hash et ne garder que les n premiers caractères non
ambigus.


Pour voir l'état des fichiers du projet courant : ``git status``. Les
fichiers qui ne sont pas gérés par Git ont le statut *Untracked*.



Commandes courantes
-------------------

On remarque que l'ajout de code au dépôt se fait en deux temps (``git
add`` puis ``git commit``). Ceci est dû au fonctionnement de Git, qui
possède une zone tampon (*staging area*, ou *index*) destinée à préparer
le futur commit : ::

  +---------+              +---------+                 +------------+
  | code    |   git add    | staging |   git commit    | historique |
  | courant |  --------->  | area    |  ------------>  | du dépôt   |
  +---------+              +---------+                 +------------+

La plupart des commandes de Git agissent sur la staging area. Partant de
cela, les commandes courantes sont :

+----------------------------------------------------------------------+
|Préparation du prochain commit                                        |
+------------------------+---------------------------------------------+
| ``git add foo.ml``     | ajoute (les modifications apportées à)      |
|                        | foo.ml dans la staging area                 |
+------------------------+---------------------------------------------+
| ``git rm foo.ml``      | supprime foo.ml du code courant (si ce n'est|
|                        | pas déjà fait) et de la staging area        |
+------------------------+---------------------------------------------+
|``git mv foo.ml bar.ml``|renomme foo.ml en bar.ml dans le code courant|
|                        |(si ce n'est pas déjà fait), supprime foo.ml |
|                        |de la staging area et y ajoute bar.ml        |
+------------------------+---------------------------------------------+
| Enregistrement du commit                                             |
+------------------------+---------------------------------------------+
| ``git commit``         | met le commit préparé dans la staging area  |
|                        | dans l'historique du dépôt, et vide la      |
|                        | staging area                                |
+------------------------+---------------------------------------------+
| Analyse de la staging area                                           |
+------------------------+---------------------------------------------+
| ``git status``         |montre le statut des fichiers du code courant|
|                        |dans la staging area (untracked, modified,   |
|                        |renamed, etc.)                               |
+------------------------+---------------------------------------------+
| ``git diff``           | montre la différence entre le code courant  |
|                        | et la staging area                          |
+------------------------+---------------------------------------------+
| Analyse de l'historique du dépôt                                     |
+------------------------+---------------------------------------------+
| ``git log``            | liste les commits enregistrés               |
+------------------------+---------------------------------------------+
| ``git show commit_id`` | montre le détail d'un commit ; si commit_id |
|                        | est omis, on montre le dernier commit       |
|                        | enregistré                                  |
+------------------------+---------------------------------------------+


Cela suffit largement pour une utilisation locale, avec un cycle de
développement qui ressemblera à : ::

  edit some files
  git add <files>
  git commit

Par rapport à RCS, il n'y a pas besoin de poser de verrous et de les
libérer.


Gestion des branches
--------------------

La branche par defaut s'appelle ``master``.

Commandes de base
~~~~~~~~~~~~~~~~~

- Lister les branches existantes : ``git branch``

- Créer une branche : ``git branch foo``

- Changer de branche : ``git checkout foo``

- Fusionner la branche *foo* dans *master* : ``git checkout master && git merge foo``

- Supprimer une branche : ``git branch -d foo``


Résolutions des conflits lors d'un merge
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Lors d'un conflit de merge, Git nous prévient que des fichiers posent
problèmes (``git status`` nous les montre comme étant *unmerged*).

Git annote les fichiers conflictuels de la façon suivante : ::

  <<<<<<< HEAD:hello.ml
  let msg = "hello world" in
  =======
  let msg = "goodbye" in
  >>>>>>> foo:hello.ml

Il faut alors éditer le(s) fichier(s) pour résoudre le conflit
manuellement, puis lancer ``git add`` sur ce(s) fichier(s) pour marquer
le conflit comme résolu.

Il est possible d'utiliser un outil graphique pour résoudre les conflits
en utilisant ``git mergetool``.

Pour en savoir plus, cf.
http://git-scm.com/book/fr/ch3-2.html#Conflits-de-fusion


Annulations
-----------


Modification du dernier commit
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

La commande ``git commit --amend`` permet de modifier le dernier commit.
C'est très pratique quand on est un peu trop rapide. Mise en situation :
::

  git add foo.ml bar.ml
  git commit

Oups ! juste après le commit, je m'aperçois d'une typo dans le message
de description. Pas de problème : ::

  git commit --amend

et je peux éditer le message en question. Mais, oups encore ! le
makefile a changé et j'ai oublié de le committer en même temps ! Là
encore c'est facile : ::

  git add makefile
  git commit --amend

Attention cependant, cela ne doit être utilisé que si le dernier commit
n'a pas été publié.

Cas général
~~~~~~~~~~~

On utilise, suivant le cas, ``git checkout``, ``git reset`` ou ``git
revert``. Voir
http://www.alexgirard.com/git-book/4_r%25C3%25A9paration_avec_git_%25E2%2580%2594_reset%252C_checkout_et_revert.html



Les petits plus
---------------


La cachette : git stash
~~~~~~~~~~~~~~~~~~~~~~~

Le scénario d'utilisation classique est le suivant : au cours d'une
grosse modification, on tombe sur un bug trivial à corriger mais on ne
veut pas mélanger les torchons et les serviettes dans un seul gros
commit.

On sauvegarde les changements en cours dans le ``stash``, et on remet à
zéro le répertoire de travail et l'index afin de correspondre avec le
sommet de la branche courante : ::

    git stash "work in progress for feature conquer-the-world"

On effectue les corrections comme d'habitude : on édite, on teste et on
committe : ::

    git add buggy.ml
    git commit -m "smallfix"

Ensuite, on revient tranquillement à notre gros travail : ::

    git stash pop

On peut même avoir plusieurs cachettes, cf. ``git stash list`` et ``git
stash apply``.


Recherche de régression : git bisect
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On s'assure que le dépôt est propre, puis on lance la recherche : ::

    git stash
    git bisect start

Cela crée automatiquement une branche pour la recherche. On borne la
recherche : on indique que l'erreur est présente dans la révision
courante, puis on indique une révision ne présentant pas l'erreur : ::

    git bisect bad
    git bisect good <id_commit_correct>

Ensuite, on se laisse guider. Git se place sur un commit dans
l'intervalle de recherche, on teste puis on donne avec verdict
(good/bad) : ::

    git bisect good (ou bad, donc)

Git restreint alors l'intervalle de recherche, se place sur un autre
commit et c'est reparti. Au bout d'un moment, on tombe sur le commit
responsable de l'introduction de l'erreur. Git signale que la recherche
est terminée et donne l'id du commit. La recherche est finie, on revient
dans la branche de développement : ::

    git bisect reset

Reste alors à corriger l'erreur.


Ajout partiel interactif : git add -p
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Quelquefois, après une session de code, on se rend compte qu'on a fait
du bon travail mais qu'il correspond en fait à plusieurs commits et non
un seul. Quand ça concerne des fichiers différents, on peut jouer avec
la staging area pour faire plusieurs commits séparés, ce qui est sympa.

Mais quand ça se passe au sein d'un même fichier, ça coince. Sauf si on
connaît l'ajout partiel interactif : ::

    git add -p mixed_works.ml

pour ne rentrer que certains hunks dans le prochain commit. C'est beau.


Nettoyage de l'historique : git rebase -i
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

On peut modifier l'historique d'un dépôt, cf. http://git-scm.com/book/en/Git-Tools-Rewriting-History

Un cas particulier intéressant est la fusion de plusieurs commits en un
seul. Par exemple parce qu'on a fait plein de petits commits en
travaillant à l'ajour d'une grosse fonctionnalité, et qu'une fois cela
fini on veut avoir un historique plus lisible. Dans ce cas, le
*interactive rebase* fait des merveilles, cf. le mode d'emploi ici :
http://gitready.com/advanced/2009/02/10/squashing-commits-with-rebase.html


Cueillette de cerises : git cherry-pick
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Il s'agit en fait de ne récupérer que certains commits d'une autre
branche ou d'un autre dépôt. L'exemple classique est le rétroportage des
corrections de bugs depuis la branche de développement vers la branche
stable.

Pour un exemple, cf. http://wiki.koha-community.org/wiki/Using_Git_Cherry_Pick



Travail collaboratif
--------------------

Les outils de gestion de version centralisés (CVS, Subversion...)
imposent un modèle de collaboration centré sur un dépôt unique, auquel
ont accès les développeurs (en général en lecture/écriture). ::


  .                +---------------+
                   | dépôt central |
                   +---------------+
                     ^     ^     ^
                     |     |     |
         +-----------+     |     +-----------+
         |                 |                 |
         v                 v                 v
  +-------------+   +-------------+   +-------------+
  | développeur |   | développeur |   | développeur |
  +-------------+   +-------------+   +-------------+


Au contraire, une gestion de version décentralisée (avec Git, p. ex.)
permet d'utiliser plusieurs modèles de collaboration différents.

Quel que soit le modèle choisi, chaque développeur travaille sur un
dépôt privé. Par la suite :

- si tous les développeurs publient leur travail sur un dépôt commun, on
  reproduit le modèle centralisé ;
- chaque développeur peut publier son travail sur un dépôt public
  personnel. On est alors dans un modèle décentralisé dans lequel les
  autres développeurs et le responsable de produit peuvent récupérer et
  intégrer ce travail à leurs propres dépôts ;
- en fait, on peut imaginer plein de *workflows* différents.


Pour en savoir plus (avec de beaux schémas) :
http://git-scm.com/book/fr/Git-distribu%C3%A9-D%C3%A9veloppements-distribu%C3%A9s


Dans le cas de Sewelis, on imagine bien que le dépôt public de Sébastien
Ferré soit le dépôt de référence. Les questions suivantes se posent :

- qui doit avoir accès à ce dépôt en lecture ? en écriture ?
- comment publier ce dépôt ? (protocole d'accès pour les autres
  développeurs)
- comment récupérer le travail des collaborateurs ? (patches + mail,
  pull request)
- comment mettre à jour le dépôt public de référence ?


Concernant la première question, je pars du principe que tout le monde
doit avoir un accès en lecture, et seul Sébastien Ferré a un accès en
écriture. C'est donc lui qui contrôle le dépôt public de référence (il
devient integration manager et release manager).

Les autres questions sont un poil plus complexes, mais on va y arriver
quand même.


Comment publier le dépôt ? (protocole d'accès)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les quatre protocoles suivants sont disponibles :

- local
- ssh
- démon Git
- http

http://git-scm.com/book/fr/Git-sur-le-serveur-Protocoles

Le protocole pour pousser peut être différent de celui utilisé pour
tirer.

Proposition : ssh pour pousser, http pour tirer.

étudier l'hébergement par l'Irisa, mais aussi par un fournisseur externe
(forge INRIA, GitHub, BitBucket...)


Comment récupérer le travail des collaborateurs ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Proposition : pull request

voir partie travailler avec des dépôts distants.

http://git-scm.com/book/fr/Les-branches-avec-Git-Les-branches-distantes

http://git-scm.com/book/fr/Git-distribu%C3%A9-Contribution-%C3%A0-un-projet

http://git-scm.com/book/fr/Git-distribu%C3%A9-Maintenance-d%27un-projet


Comment mettre à jour le dépôt public de référence ?
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

git pull



Travailler avec des dépôts distants
-----------------------------------

- clone/push/pull


http://git-scm.com/book/fr/Les-bases-de-Git-Travailler-avec-des-d%C3%A9p%C3%B4ts-distants


À lire aussi : http://dmathieu.com/2010/10/24/merge-and-rebase.html



Mélanger les dépôts
-------------------

- git submodule
- subtree merging

see http://stackoverflow.com/questions/1416585/can-you-share-a-file-and-its-history-between-two-git-repositories

see also (permanently merging repos and their histories): http://git.661346.n2.nabble.com/Merging-repositories-and-their-histories-td731202.html

external tool git-stitch-repo is also listed at http://stackoverflow.com/questions/277029/combining-multiple-git-repositories

last but not least: http://gbayer.com/development/moving-files-from-one-git-repository-to-another-preserving-history/


See also from question 14 Git 2011 survey: https://git.wiki.kernel.org/index.php/GitSurvey2011



Interfaces graphiques
---------------------

Git est fourni avec une interface graphique qu'on peut appeler avec
``git gui``. Une autre interface couramment installée est ``gitk``.

C'est pratique pour visualiser un gros historique, mais en dehors de ça
je n'en m'en sers pas vraiment.



Documents externes
------------------

* les pages de man sont assez fournies en général

* le manuel presque officiel : Pro Git de Scott Chacon
  - en ligne en français ici : http://git-scm.com/book/fr
  - téléchargeable sous différents formats

