.. -*- mode: rst -*-

==============================
Androlis : description de code
==============================

:Authors:  Vincent Piet <vincent.piet@irisa.fr>
:Date: 2013-09-06
  
Organisation d'un projet Android
================================

Les projets android combinent deux langages : java et xml. Le langage xml est utilisé pour tout ce qui est 
description d'interface statique, description de données, description d'informations relatives à l'affichage...
Java apporte le dynamisme (grâce par exemple aux listeners) et toute la partie calculatoire.

Nous avons également utilisé le langage javascript afin d'utiliser des librairies permettant d'afficher facilement des graphiques.

L'ensemble formé par une interface et les calculs sous-jacents s’appelle une activité.

XML
---

Les fichiers xml sont :

   - pom.xml
   - AndroidManifest.xml
   - des fichiers contenu dans le répertoire des ressources : res


Le contenu du fichier pom.xml est détaillé dans le document de reprise de code.

Android manifest
````````````````
Le fichier AndroidManifest.xml contient des informations sur : 
 
   - la compatibilité de l'application avec les différentes versions d'android
   - des permissions nécessaires, par exemple pour que l'application soit connectée 
   - une liste des activités


Nous avons apporté quelques modifications dans la description de l'application :
l'ajout de la règle android:theme="@style/CustomTheme" qui nous permet d'appliquer un thème personnel,  
ainsi que l'ajout de la règle android:windowSoftInputMode="stateHidden" qui nous permet de cacher le clavier par défaut 
lors de l'ouverture d'activités contenant des textes éditables. Ce comportement nous semble préférable, il permet à l'utilisateur
de voir l'ensemble des possibilités qui lui sont offertes, alors que dans le cas inverse elles sont cachés par le clavier.

Contenu du répertoire res
`````````````````````````
Le répertoire res contient les répertoires:
 
   - drawable
   - layout
   - menu
   - values
   
   
ainsi qu'un certain nombre de répertoires préfixés par ces noms. 
Ces répertoires préfixés contiennent des ressources adaptés aux différentes tailles d'écran, ou langues du système. 
Le fonctionnement correspondant à la taille des écrans est décrit à l'adresse :
http://developer.android.com/guide/practices/screens_support.html.


Drawable
    Le répertoire drawable contient essentiellement les images utilisées dans l'application. 
    Il contient également des fichiers xml de design décrivant l'aspect  de composant graphique (par exemple la ligne 
    servant de séparateur dans l'activité request).


Layout
    Le répertoire layout contient des fichiers préfixés par activity.
    Ces fichiers contiennent la description de l'interface graphique statique des activités. 
    Pour chacun des fichiers, activity est suivi par le nom de l'activité qu'il décrit.

    Il contient également les fichiers décrivant des composants tel que l'entête de l'application, préfixé par header,
    ou décrivant les différentes représentation d'objets contenu dans nos listes (objets simples, nom de rubrique, objets checkable).


Menu
    Le répertoire menu contient des fichiers relatifs aux menus contextuels. Il décrit les différentes options du menu
    proposées à l'utilisateur.


Values
    Le répertoire values contient les fichiers color, dimens, strings, styles, et themes 
    permettant d'utiliser pour des noms symboliques, à la place de valeurs.

    L'intérêt de ce mécanisme est de permettre par la suite d'utiliser un autre fichier, par exemple values-fr/string.xml,
    redéfinissant ces valeurs dans des cas particulier, ici lorsque l'OS Android est en français. Les valeurs contenus dans ce fichier 
    remplacent alors celle du fichier par défaut.


Java
----

Les fichiers java sont contenus dans le répertoire src et regroupés par packages. 

com.androlis
````````````
Le package com.androlis contient les fichiers java de chacune des activités. 
Ils permettent de lier une interface à une activité, d'apporter du dynamisme en modifiant des données 
dans des conteneurs ou en déclarant des listeners, d'appeler l'activité suivante avec éventuellement des données.

com.androlis.memorizator
````````````````````````
Ce package contient trois classes java: une classe Memory, une classe ButtonState et une classe TextViewState. 
La classe Memory permet d’accéder et de mémoriser l'état de variables statiquement, et fourni ainsi un mécanisme de conservation
de données. En effet, si android défini le notion de contexte, il n'est en revanche pas possible d'y stocker durablement une information.
Il n'est possible de passer une variable d'une activité à une autre qu'en la passant explicitement au moment de changer d'activité.
L’intérêt de cette classe est donc de pouvoir accéder à ces variables peut importe l'activité courante sans la contrainte
la passer explicitement durant chaque changement d'activités.

Les deux autres classes permettent de conserver l'état de composants, la classe TextViewState permettant de 
retrouver les différents états d'un texte, de sa création à son état courant, en se basant sur un comportement semblable
à ce que l'on peut trouver dans des éditeurs de textes.

com.androlis.list
`````````````````
Le package com.android.list contient entre autres une ré-implémentation du composant list d'android. 
C'est en effet le seul moyen d'obtenir une liste filtrable contenant des noms génériques, ou noms de rubrique 
pour classifier les objets, ou des checkboxs.


La classe List_MyCustomAdapter.java
    La fonction de filtrage nécessite deux conteneurs, un pour l'ensemble des données servant de référentiel 
    (dans notre classe c'est la liste _originalData), et un pour les données affichés (dans notre classe c'est la liste _filteredData). 
    La liste des données filtrées se mettra à jour en fonction de la recherche de l'utilisateur.

    La fonction getView est appelé implicitement dès qu'un objet apparaît à l'écran 
    (suite au lancement de l'activité contenant la liste ou à un scroll sur la liste), 
    afin d'afficher chacun des objets avec la vue qui lui correspond.

    La classe getFilter décrit le fonctionnement du filtre, assure l'opération de publication du résultat 
    (dans notre cas l'affectation des listes calculées aux listes filtrées et une notification appelant explicitement getView), 
    et contient une description du filtre utilisé.


com.androlis.javascriptCommunication
````````````````````````````````````
Le package com.android.javascriptCommunication contient deux classes dont le rôle est de fournir une interface de communication
entre le code javascript et le code java contenu dans les activités

com.androlis.networkCommunication
`````````````````````````````````
Le package com.android.networkCommunication contient les classes réalisant des taches asynchrones effectuant 
des requêtes sur le serveur lisfs2008.

javascript
----------

Les pages web contenant le javascript sont contenus dans le répertoire asset/www. Elles permettent d'afficher trois diagrammes :
un camembert, un nuage de mot et une timeline. Javascript à également été utilisé pour implémenter la carte, bénéficiant la encore de librairies.

Les diagrammes
``````````````
Nos diagrammes sont créés grâce à des librairies utilisant l'api Google chart. 
Ce choix c'est fait sur des critères esthétiques, et de simplicité.
La manière dont nous les avons utilisées est décrite dans le document de reprise de code.

Les apis des différents diagrammes sont disponibles ici :
https://developers.google.com/chart/


Choix de javascript
    Nous avons dans un premier temps cherché à implémenter ces diagrammes directement sous android. Cependant, l'aspect final d'une application n'étant
    calculé que tardivement, il n'est pas possible de connaître les différentes mesures d'un layout au lancement de l'application, rendant ainsi difficile
    la conception d'un nuage de mots (car on ne connais pas la taille occupée par un mot, le layout étant plus grand que le mot lui même et ce,
    proportionnellement à la taille de l'écran). Cette difficultés est donc à l'origine du choix de javascript pour l'affichage des diagrammes.


La carte
````````
Nous avons choisi d'utiliser comme carte Google map car elle est facile d'utilisation et son api est bien définie.


Choix de javascript
    Dans un premier temps nous avons choisi d'utiliser la map Android. Native, son chargement est rapide et elle est simple d'utilisation.
    Cependant, avec la volonté d'afficher des fichiers Kml (calque qui viennent s'ajouter au dessus de la carte), il nous est apparu que les
    différentes apis de Google maps n'étaient pas égales entres elles. Alors que le chargement d'un fichier kml nécessite deux lignes en 
    javascript, en java l'api ne propose pas d'utiliser directement de fichiers kml, il faut donc les charger, les parser, puis traiter les données
    point par point.

    Nous avons donc préféré abandonner la carte native ainsi que le travail fourni (intégration de la librairie "android-maps-extensions" et son ajout dans
    une architecture maven cohérente), au profit de la simplicité d'intégration du kml en javascript (des librairies de clustering existant également en javascript).