.. -*- mode: rst -*-

===========================================
Portail Portalis - Spécifications générales
===========================================

.. sectionauthor:: Yves Bekkers <bekkers@irisa.fr>

.. only:: html

   :download:`[version PDF] <_build/pdf/generales-spec.pdf>`

.. admonition:: Portalis est en cours de développement,
  voici sa TODO list :

  * **Implémenter** un serveur de fichiers. Sur ce serveur les applications pourront gérer les images et les fichiers de texte
    qu'elles référencient.
  * **Implémenter** un serveur de lancement d'application LIS-XML. Ceci permettra aux utilisateur de mettre en place des serveurs
    d'application indépendant des serveurs Portalis.
  * **Problème** : les commandes d'utilisation de serveurs d'information logique qui possèdent deux formes, la forme directe où le serveur est
    accédé directement et où il n'a donc pas besoin d'être nommé et la forme indirecte qui passe par un serveur Portalis où
    le serveur d'information doit être connu (soit explicitement dans la commande, soit par défaut au sein du serveur). **D'où la question** :
    Comment avertir les utilisateurs que ces deux formes existent et qu'elle ne s'adressent pas au même serveur ?
  * **Problème** : Il n'y a pas grand sens pour un client d'émettre des commandes ``autorize`` et ``unautorize`` en directe aux serveurs d'information logique.
    Seuls les serveurs Portalis on besoin de les utiliser pour enregistrer les logins auprès des serveurs d'information ...
    Cependant, on ne peut interdire à un client d'en émettre s'il le désire car, vue du point de vue des serveurs d'information logique
    on ne peut pas faire de différence entre les sortes d'applelant. **D'où la question** : Doit-on cacher les commandes directes ?
  * **Rédiger** : La documentation de réutilisation de code, la liste de librairies tières utilisée.



.. _Camelis:        http://www.irisa.fr/LIS/ferre/camelis/
.. _`Objective Caml`: http://caml.inria.fr/ocaml/
.. _GWT:            https://developers.google.com/web-toolkit/

Présentation
============

Portalis est une application WEB collaborative qui permet à des utilisateurs distants de se partager
des serveurs d'information logique LIS. Pour l'instant seuls des serveurs Camelis_ sont implémentés.
Camelis est écrit en `Objective Caml`_, les clients
peuvent être écrits en n'importe quel language. Ils communiquent avec
le serveur et les clients via un protocole XML propre à Portalis.

Portalis comporte un client par défaut écrit en Java à l'aide de GWT_, une boite à outils pour le développement
de clients WEB pour les navigateurs HTML.

Portalis est construit à partir de modules réutilisables, de sorte qu'il est possible à des utilisateurs de
construire leurs propres clients.

Voici le schéma de cette application :

.. image:: images/schemaGeneralPortalis.jpg
   :width: 16 cm

Les Composants de Portalis
--------------------------

Portalis comporte des serveurs d'administration Portalis, des serveurs d'information logique qui sont de deux types, serveurs
Camelis et serveurs Sewelis, ainsi que des clients.

Serveur d'admistration Portalis
```````````````````````````````

Le serveur *Portalis*. Il s'agit d'un serveur XML construit en technologie J2EE.

On soumet au serveur Portalis des requêtes HTTP. Portalis répond en XML.


Portalis offre deux types de commandes :

  . Les *commandes d'administration* qui permettent de gérer les serveurs LIS-XML et leur utilisateurs, par exemple déclarer,
  enlever, démarrer et arréter des serveurs ou ajouter/enlever des utilisateurs et des droits aux utilisateurs.

  . Les *commandes d'utilisation* des serveurs LIS-XML qui sont de deux types, Camelis-XML et Sewelis-XML.
  Elles ne fond que transiter au sein du serveur d'administration vers les serveurs d'information.

Serveurs d'information Lis-XML
``````````````````````````````

Les serveurs LIS-XML sont des exécutables Linux. On en trouve deux types Camelis-XML et Sewelis-XML.

Serveurs d'information Camelis-XML
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Il s'agit d'un assemblage entre :

1. Une application ``Camelis 1.4`` : écrite en Ocaml par Sébastien Ferré.
2. Une extension ``Camelis Extension`` écrite par Yves Bekkers, qui transforme Camelis en un ensemble de fonctions
   propres à la mise en oeuvre des requètes XML. C'est cette partie qui définit l'interface abstraite d'utilisation
   des serveurs Camelis sur le WEB.
3. Un serveur ``Xml`` Il s'agit du programme principal écrit en Ocaml, il implémente un serveur HTTP à raison de
   une requète HTTP/XML par fonction Ocaml impémenté par l'extension ci-dessus. De plus il étend Camelis 1.4 afin
   de permettre un usage multi-utilisateurs.

Serveurs d'information Sewelis-XML
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. |chantier| image:: images/chantier.gif
   :align: middle
   :width: 100 px

.. todo::

   |chantier| À faire, réservé pour une version future du document

Avertissement pour la suite
---------------------------

A propos de la syntaxe utilisée ci-dessous
``````````````````````````````````````````

   Dans ce qui suit, pour la spécification des paramêtres des commandes, nous utilisons le langage des expressions
   régulières. On trouvera les opérateurs suivants :

   +-----------+--------------------------+
   | Opérateur | Description              |
   +===========+==========================+
   | ``|``     | Choix entre deux options |
   +-----------+--------------------------+
   | ``*``     | Zéro ou plusieurs        |
   +-----------+--------------------------+
   | ``+``     | Un ou plusieurs          |
   +-----------+--------------------------+
   | ``?``     | Zéro ou un               |
   +-----------+--------------------------+


Synopsis des commandes
======================

Les commandes de l'application Portalis sont de quatre types:

- Administration générale
- Administration des utilisateurs et de leur droits
- Administration des applications et des services
- utilisation des serveursde d'information logique


Administration générale
-----------------------

Commandes d'administration générale
```````````````````````````````````

==================  ============================  ====================================  ======================================================================  =================
Nom                 Résultat                      Paramêtres                            Description                                                             Droits
==================  ============================  ====================================  ======================================================================  =================
getLog              String                                                              Renvoie le contenu du fichier de log.                                   None
clearLog                                          userKey                               Efface les fichiers de log.                                             Admin
clear                                                                                   stopService + clear_log                                                 Admin
==================  ============================  ====================================  ======================================================================  =================



Administration des utilisateurs
-------------------------------

Objets de représentation des utilisateurs
`````````````````````````````````````````

Un utilisateur, classe ``UserCore``, possède des attributs ``pseudo`` et ``email`` qui servent, l'un comme l'autre d'identifiant.
Un utilisateur possède aussi un mot de passe ``md5Password`` utilisé pour se connecter
aux services offerts par Portalis. Enfin un utilisateur possède des droits qui peuvent être :

- soit ``GeneralAdmin`` et avoir tous les droits dans Portalis,
- soit limités, à certains services du portail.

Lorsqu'un utilisateur est connecté à Portalis il possède une clè `userKey`` qui est proche à sa connection et événtuellement un service courant
``currentServiceKey``. Un utilisateur peut avoir plusieurs connections en parallèle, pourvu que ces connections soient faite à partire de clients différents.

Voici le shéma UML des classes définitant les utilisateurs :

.. image:: images/generales-spec/userData.jpg
   :width: 16 cm

.. _`liste des droits`:

Les droits d'un utilisateur sont définis dans l'ordre décroissant comme suit :

============================  =====================================================================================================================
Type                          Droits
============================  =====================================================================================================================
``GeneralAdmin``              Possède tous les droits. En particulier il est le seul a possèder le droit de créer des utilisateurs
                              ayant les droits ``Admin``. Il peut aussi gérer n'importe quel service, les creer et les détruire.
``Admin``                     Peut gérer seulement les services dont il est ``Admin`` et les utilisateurs de ces services.
``Editeur``                   Peut gérer (i.e. charger/sauvergarder) le contexte des services dont il est ``Admin`` mais aussi
                              ajouter/enlever des objets au contexte d'un service ainsi que ajouter/enlever des propriétés au contexte.
                              Ne peut pas gerer les utilisateurs.
``Collaborateur``             Peut ajouter des objets au contexte d'un service ainsi que ajouter des propriétés au contexte. Mais ne peut rien
                              enlever.
``Lecteur``                   Peut seulement naviguer dans un contexte.
``None``                      Ne possède aucun droit.
============================  =====================================================================================================================

.. todo::

   |chantier| Réflechir aux problèmes
   suivants :

     * Comment introduire des utilisateurs ``anonymes`` dans les services, avec des droits potentiellement spécifiques chaque service. Ce
       sont les ``Admin`` du service qui en fixent la limite.
     * Les clés d'utilisateur qui sont données en paramètre des commandes sont dynamiques. Elles ont la durée de vie d'un session d'un utilisateur. Dans
       un serveur Lis-XML, ces clès sont conservées pour la durée d'un session d'utilisateur. La question est comment éviter que au bout d'un certain
       plus aucun utilisateur n'ai le droit de faire certaine modifications dans le contexte ? Par exemple, si tous les ``Admin`` ont disparus, on ne peut
       plus :

         - Ajouter un nouvel utilisateur.
         - Recharger un contexte ...


Commandes d'administration des utilisateurs
```````````````````````````````````````````

==================  ============================  ====================================  ======================================================================  =================
Nom                 Résultat                      Paramêtres                            Description                                                             Droits
==================  ============================  ====================================  ======================================================================  =================
login               userKey                       (pseudo | email) PassWord sessionId   Ouvrir une session pour un utilisateur. Le même utilisateur peut avoir  None
                                                                                        plusieurs sessions ouvertes en parallèle. Une par client distant.
logout                                            userKey                               Fermer la session.                                                      None
==================  ============================  ====================================  ======================================================================  =================

.. todo::

   |chantier| Insérer ici la description des commandes d'administration des utilisateurs restantes

Administration des services
---------------------------

On distingue les services activables dits ``ServiceCore`` et les services actifs dits ``ServiceActif``. Chaque service actif possède sa
propre URL. Un même service activables peut avoir courramment 0 ou plusieurs services actifs correspondant.

Commandes de gestion des modèles de services
````````````````````````````````````````````

.. todo::

   |chantier| Insérer ici la description des commandes d'administration des services activables

.. _`lancement du service`:

Activation d'un service Camelis-xml selon un modèle
```````````````````````````````````````````````````

On lance un serveur Camelis-xml par une commande :

::

 soapServeur
    -port portnum   :width: 16 cm
         Numéro de port sur lequel on doit lancer le serveur Lis
    -creator email
         email du createur
    -key adminKey
         clé de l'administrateur propriétaire du service
    -id serviceId
        identification du service (une chaîne telle que "http://localhost:8060::planet:planet")
    [-log fileName]
        nom du ficher de log (valeur par défaut : "/srv/logs/portalis.log")
    [-userAware {true|false}]
        Vérification des droits d'accès ou non (par défaut false), si false il n'y a plus de
        vérification des droits d'accès (utile en période de test).


Administration par Portalis
^^^^^^^^^^^^^^^^^^^^^^^^^^^

=======================  ============================  ====================================  ======================================================================  =================
Nom                      Résultat                      Paramêtres                            Description                                                             Droits
=======================  ============================  ====================================  ======================================================================  =================
ping                     ServiceActif[]                serviceId                             Renvoie le(s) service(s) actif(sà pour un serviceId donné.              None
ping                     ServiceActif                  port                                  Renvoie le service actif sur le port donné.                             None
ping                     ServiceActif[]                                                      Renvoie tous les services actifs actuellement.                          None
getPortsActifs           PortsActifsReponse                                                  Renvoie la liste des ports acifs occupés par un serveur XML Camelis
getService               EtatService                   serviceId                             Renvoie la description du service (actif ou non actif) par son Id.      None
getService               EtatService[]                                                       Rends les descriptions de tous les services.                            None
setServiceActif                                        userKey serviceKey                    Définit le service courant pour cet utilisateur                         Reader
startService             ServiceActif                  userKey serviceId                     Démarre un service                                                      Admin
updateServiceActif                                     userKey ServiceKey nbObjects date     Met à jour un service actif                                             Admin
stopServiceActif                                       userKey serviceKey*                   Arrête le(les) services actifs considérés                               Admin
stopServiceActif                                       userKey serviceId                     Arrête tous les services actifs de serviceId considéré                  Admin
=======================  ============================  ====================================  ======================================================================  =================

Il peut arriver que ces commandes fassent appel à des commandes de gestion internes aux serveurs LIS-XML. Ces commandes des gestion internes aux serveur LIS-XML
Sont décrites ci-dessous.

Administration par les serveurs LIS-XML
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

=====================  =========================  ====================================  ================================================================  ===============
Nom                    Résultat                   Paramêtres                            Description                                                       Droits
=====================  =========================  ====================================  ================================================================  ===============
autorize                                          adminKey serviceId userKey Droits     Donne les droits "Droits" pour l'utilisateur de session "key"     Admin
                                                                                        pour tous les services actifs d'identification serviceId.
unAutorize                                        adminKey serviceId userKey            Enleve les droits à l'utilisateur de session "key" pour tous      Admin
                                                                                        les services actifs d'identification serviceId.
ping                   PingReponse                                                      Renvoie les informations sur le service couramment actif          None
ping                   PingReponse                url                                   Renvoie les informations sur le service actif sur l'url donnée    None
pingUsers              UserListReponse            adminKey                              Renvoie la liste des utilisateurs connus avec                     Admin
                                                                                        leur droit sur le présent service
=====================  =========================  ====================================  ================================================================  ===============

Utilisation des serveurs Camelis-XML
------------------------------------

Représentation des informations de Camelis
``````````````````````````````````````````

Les commandes des serveurs Camelis rendent en résultat des informations qui sont soit des chaînes soit des objets
construits dont voici le diagramme UML :

.. image:: images/generales-spec/CamelisData.jpg
   :width: 16 cm

Objets de réponses aux commandes d'utilisation de Camelis
`````````````````````````````````````````````````````````
.. image:: images/generales-spec/reponse.jpg
   :width: 16 cm

Utilisation de Camelis
``````````````````````
Toutes les commandes possèdent un paramêtre ``userKey`` obligatoire tel que ci-dessus. Elles possèdent aussi un paramètre non obligatoire
``serviceKey`` qui définit quel service actif est concerné. Par défaut il s'agit du service courant défini par l'utilisateur lui même grâce à la commande
d'administration ``setService``.

=========================================================  ========================================================  =======================================  ==========================================================  ===================================
Nom                                                        Résultat                                                  Paramêtres                               Description                                                 Droits
=========================================================  ========================================================  =======================================  ==========================================================  ===================================
resetCamelis                                                                                                         userKey serviceKey?                      Efface le contexte courant de Camelis.                      Admin
importCtx                                                  ExtentReponse                                        userKey serviceKey? option* url          Ajouter un fichier ``.ctx`` au contexte courant.            Admin
importFile                                                 ExtentReponse                                        userKey serviceKey? option* url          Ajouter des informations au contexte par lecture            Admin
                                                                                                                     suffixes*                                de fichiers. les répertoires peuvent être
                                                                                                                                                              traversés si nécessaire. Dans ce cas, les fichiers
                                                                                                                                                              à lire sont choisis par leur suffixes.
importLis                                                  ExtentReponse                                        userKey serviceKey? url                  Chargement d'un contexte à partir d'un fichier              Admin
                                                                                                                                                              ``.lis``.
exportCtx                                                  ExtentReponse                                        userKey serviceKey? option* url          Sauver le contexte courant sous forme de fichier            Editeur
                                                                                                                                                              ``.ctx``.
exportLis                                                  ExtentReponse                                        userKey serviceKey? url                  Sauver le contexte courant en binaire sous forme            Editeur
                                                                                                                                                              de fichier binaire ``.lis``.
delObjects                                                 ExtentReponse                                        userKey serviceKey? oid?                 Effacer des objets par leurs identifications.               Editeur
delFeature                                                 PropertyTreeReponse                                       userKey serviceKey? requete              Effacer toutes les features considérée                      Editeur
                                                                                                                     featurePart+
delFeatureFromOid                                          PropertyTreeReponse                                       userKey serviceKey? requete oid+         Effacer les features considérées des objets                 Editeur
                                                                                                                     featurePart+                             considérés.
delFeatureFromReq                                          PropertyTreeReponse                                       userKey serviceKey? requete              Effacer les features considérées des objets                 Editeur
                                                                                                                     requeteForOids featurePart+              selectionés par la requète ``requeteForOids``.              Editeur
createObject                                               ExtentReponse                                        userKey serviceKey? pre feature+         Créer un objet avec des features.                           Collaborateur
addFeatures                                                PropertyTreeReponse                                       userKey serviceKey? requete feature+     Creer une liste de features. Rendre le nouveau              Collaborateur
                                                                                                                                                              ``propertyTree`` associé à la requête.
addFeaturesX                                               XPropertyTreeReponse                                      userKey serviceKey? requete feature+     Creer une liste de features. Rendre le nouveau              Collaborateur
                                                                                                                                                              ``propertyTree`` associé à la requête.
setFeaturesFromOids                                        PropertyTreeReponse                                       userKey serviceKey? requete oid+         Ajouter une liste de features à une liste                   Collaborateur
                                                                                                                     feature+                                 d'objets. Rendre l'arbre associé à la requête.
setFeaturesFromOidsX                                       XPropertyTreeReponse                                      userKey serviceKey? requete oid+         Ajouter une liste de features à une liste                   Collaborateur
                                                                                                                     feature+                                 d'objets. Rendre l'arbre associé à la requête.
setFeaturesFromReq                                         PropertyTreeReponse                                       userKey serviceKey? requete reqForOids   Ajouter une liste de features à une liste                   Collaborateur
                                                                                                                     feature+                                 d'objets résultants d'une requête. Rendre
                                                                                                                                                              l'arbre associé à la requête.
setFeaturesFromReqX                                        XPropertyTreeReponse                                      userKey serviceKey? requete reqForOids   Ajouter une liste de features à une liste                   Collaborateur
                                                                                                                     feature+                                 d'objets résultants d'une requête. Rendre
                                                                                                                                                              l'arbre associé à la requête.
addAxiome                                                  PropertyTreeReponse                                       userKey serviceKey? requete axiome+      Ajouter une liste d'axiomes.                                Collaborateur
addAxiomeX                                                 XPropertyTreeReponse                                      userKey serviceKey? requete axiome+      Ajouter une liste d'axiomes.                                Collaborateur
intent                                                     ZoomReponse                                               userKey serviceKey? oids                 Calculer l'intention d'un ensemble d'objets.                Lecteur
intentX                                                    XZoomReponse                                              userKey serviceKey? oids                 Calculer l'intention d'un ensemble d'objets.                Lecteur
getTreeFromReq                                             PropertyTreeReponse                                       userKey serviceKey? requete feature      Rend l'arbre des Features pour une requète                  Lecteur
                                                                                                                                                              ``Feature``. Le point de vue utilisé est celui
                                                                                                                                                              donné par la requête ``req``.
getTreeFromReqX                                            XPropertyTreeReponse                                      userKey serviceKey? requete feature      Rend l'arbre des Features pour une requète                  Lecteur
                                                                                                                                                              ``Feature``. Le point de vue utilisé est celui
                                                                                                                                                              donné par la requête ``req``.
getTreeFromZoom                                            ZoomReponse                                               userKey serviceKey? requete incr         Rend l'arbre des Features et la nouvelle requète            Lecteur
                                                                                                                                                              normalisée à partir d'une requète ``requete`` et
                                                                                                                                                              d'un incrément ``incr``.
getTreeFromZoomX                                           XZoomReponse                                              userKey serviceKey? requete incr         Rend l'arbre des Features et la nouvelle                    Lecteur
                                                                                                                                                              requète normalisée à partir d'une requète
                                                                                                                                                              ``requete`` et d'un incrément ``incr``.
getExtent		                                           ExtentReponse                                        userKey serviceKey? requete              Calculer l'extension d'une requète.                         Lecteur
getFeaturesFromOids                                        PropertySetReponse                                        userKey serviceKey? oids featurePart+    Donner les valeurs d'un ensemble de propriétés              Lecteur
                                                                                                                                                              d'un ensemble d'objets.
getFeaturesFromReq                                         PropertySetReponse                                        userKey serviceKey? reqForOids           Donner les valeurs d'un ensemble de propriétés              Lecteur
                                                                                                                     featurePart+                             d'un ensemble d'objets résultant d'une requête.
=========================================================  ========================================================  =======================================  ==========================================================  ===================================

Détail des commandes
====================

Administration générale
-----------------------

.. todo::

   |chantier| Insérer ici le détail des commandes d'aministration génér les de l'application Portalis.


Administration des utilisateurs
-------------------------------

login()
```````

Un même utilisateur ne peut de loger qu'une seule fois à partir d'une machine cliente donné. Mais il peut se loger
plusieurs fois s'il utilise plusieurs machines clientes. Un login rend une clè pour une machine donnée.
Comme identification l'utilisateur fourni soit son pseudo, soit son email.
Voici un exemple de requête de login :

::

 http://localhost:8080/portalis/login.jsp?userId=yves&password=ppp

La réponse en cas de succès est une clé d'utilisateur :

.. code-block:: xml

  <string>b147e2d2b6b8b95398839d9c816a6067730323eba1531592911d42c1eb10d398</string>

En cas de peudo inconnu la réponse est du type :

.. code-block:: xml

  <error>fr.irisa.lis.camelis.data.shared.admin.CamelisException : Pseudo d'utilisateur Inconnu toto</error>

Voici d'autres messages d'erreur possibles pour le login

::

  <error>fr.irisa.lis.camelis.data.shared.admin.CamelisException : Incorrect Password for yves</error>
  <error>fr.irisa.lis.camelis.data.shared.admin.CamelisException : User allready loggedIn : yves</error>

.. error::
   La requête ``login`` n'analyse pas le paramêtre ``sessionId``.

.. error::
   Un utilisateur peut se loger 2 fois à partir d'une même machine cliente, une fois avec son pseudo, l'autre avec son email ...
   attributs

logout()
````````

Voici un exemple de requête ``logout`` :

::

 http://lisfs2008.irisa.fr:8080/portalis/logout.jsp?userKey=kkk

En cas de succès la réponse est la suivante :

.. code-block:: xml

  <reponse state="Ok">
     <logout>
        You succesfully log out
     </logout>
  </reponse>

Administration des services
---------------------------

Administration par Portalis
```````````````````````````

setService()
^^^^^^^^^^^^

Après appel d'une requête ``login`` on obtient une clè d'utilisateur. Il est alors possible de positionnner le servive courant de l'utilisateur
grâce à la requête ``setService``

::

 http://localhost:8080/portalis/login.jsp?userId=yves&password=ppp
 http://localhost:8080/portalis/setService.jsp?userKey=kkk&serviceKey=http://localhost:8060::planet:planet

Si le service dont la clè est donnée est bien actif on obtient la réponse :

.. code-block:: xml

 <reponse state="Ok"/>

Voici les réponses en cas d'erreur :

Service non actif
"""""""""""""""""

.. code-block:: xml

  <reponse state="fr.irisa.lis.camelis.data.shared.admin.CamelisException : Service non actif = http://localhost:8060::planet:planet"/>



Administration par les serveurs LIS-XML
```````````````````````````````````````

ping()
^^^^^^

Voici le schéma UML d'une réponse à la commande ``ping()`` :

.. image:: images/generales-spec/pingReponse.jpg
   :width: 4 cm

Voici un exemple de réponse XML obtenue :

.. code-block:: xml

 <reponse
     adminKey="5R45TFDDR5"
     creator="bekker@irisa.fr"
     activationDate="10 dec 2011 11:31:44"
     lastUpdate="11 jan 2012 08:45:06"
     fullName="planet::planetPhoto"
     nbObject="123"
     pid="23432"/>

pingUsers()
^^^^^^^^^^^

Voici le schéma UML d'une réponse à la commande ``userList()`` :

.. image:: images/generales-spec/userListReponse.jpg
   :width: 4 cm

Voici un exemple de réponse XML obtenue :

.. code-block:: xml

 <users state="ok" creator="bekker@irisa.fr">
    <user key="EFTRZEF1ZZ" right="Admin"/>
    <user key="djkfh67bjk" right="Lecteur"/>
 </users>

Pour les droits autorisés voir le tableau `liste des droits`_. Le createur est l'email  qui a été donné en paramètre au `lancement du service`_.

Voici trois exemples de réponse en cas d'erreur :

Clé de l'administrateur absente
"""""""""""""""""""""""""""""""

.. code-block:: xml

  <users state="need a user key" />

Utilisateur inconnu dans ce serveur d'information logique
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

.. code-block:: xml

  <users state="user unknown, userKey is jdujr5FggTYG" />

Manque de droit de l'utilisateur
""""""""""""""""""""""""""""""""

.. code-block:: xml

  <users state="user not allowed, userKey is 123FggTYG" />


.. todo::

   |chantier| Insérer ici le détail des commandes d'aministration internes aux serveurs LIS-XML.


Utilisation de Camelis
----------------------


getTreeFromZoom()
`````````````````

Après positionnement du service ``http://localhost:8060::planet:planet`` on peut lancer une requête ``getTreeFromZoom`` comme suit :

::

 http://localhost:8080/portalis/getTreeFromZoom.jsp?userKey=kkk&requete=all&feature=Near

La réponse est un arbre de ``properties`` comme suit :

.. code-block:: xml

 <zoomReponse newReq="Near"
   lastUpdate="1348047928894">
   <propertyTree feature="all" supp="4">
      <oids>10 5 4 2</oids>
      <propertyTree feature="Distance" supp="4">
         <oids>10 5 4 2</oids>
         <propertyTree feature="Near" supp="4">
            <oids>10 5 4 2</oids>
         </propertyTree>
      </propertyTree>
      <propertyTree feature="satellite" supp="2">
         <oids>4 2</oids>
      </propertyTree>
      <propertyTree feature="Size" supp="4">
         <oids>10 5 4 2</oids>
         <propertyTree feature="Small" supp="4">
            <oids>10 5 4 2</oids>
         </propertyTree>
      </propertyTree>
      <propertyTree feature="caracteristique ?" supp="3">
         <oids>5 4 2</oids>
         <propertyTree feature="caracteristique couleur ?" supp="3">
            <oids>5 4 2</oids>
            <propertyTree
    feature="caracteristique couleur is &amp;quote;bleu&amp;quote;"
               supp="2">
               <oids>5 2</oids>
            </propertyTree>
            <propertyTree
    feature="caracteristique couleur is &amp;quote;rouge&amp;quote;"
               supp="1">
               <oids>5</oids>
            </propertyTree>
            <propertyTree
    feature="caracteristique couleur is &amp;quote;blanche&amp;quote;"
               supp="1">
               <oids>4</oids>
            </propertyTree>
         </propertyTree>
         <propertyTree feature="caracteristique vent ?" supp="1">
            <oids>4</oids>
            <propertyTree feature="caracteristique vent = 250"
               supp="1">
               <oids>4</oids>
            </propertyTree>
         </propertyTree>
         <propertyTree feature="caracteristique matiere ?" supp="1">
            <oids>5</oids>
            <propertyTree
     feature="caracteristique matiere is &amp;quote;h2o&amp;quote;"
               supp="1">
               <oids>5</oids>
            </propertyTree>
         </propertyTree>
      </propertyTree>
   </propertyTree>
 </zoomReponse>

En cas de réponse vide on reçoit une réponse telle que :

.. code-block:: xml

 <zoomReponse newReq="Truc" lastUpdate="1348047928894">
    <propertyTree feature="all" supp="0">
       <oids/>
    </propertyTree>
 </zoomReponse>

.. todo::

   |chantier| Insérer ici le détail des autres commandes de Camelis

Exemple d'appel en Java
=======================

Voici une classe Java qui effectue la séquence d'appel à Portalis suivante :

``login, getServices, setService, zoom, logout``

.. code-block:: java

 public class TestCamelis {

   static final String SERVEUR = "http://lisfs2008.irisa.fr:8080/portalis/";

   static final String URL_LOGIN = SERVEUR +"login.jsp?userId=yves&password=9195c7b9bb56d375948ba058cddea1982b49864e12c700de58d69c5c72dbd075";
   static final String URL_LOGOUT = SERVEUR +"logout.jsp?userKey=";
   static final String URL_GET_SERVICES = SERVEUR +"getServices.jsp?userKey=";
   static final String URL_GET_ZOOM = SERVEUR +"getTreeFromZoom.jsp?requete=all&feature=Near&userKey=";
   static final String URL_SET_SERVICE = SERVEUR +"setService.jsp?serviceKey=";

   /**
    * @param args
    */
   public static void main(String[] args) {
      Document document=null;
      URL url=null;
      try {
         url = new URL(URL_LOGIN);
         System.out.println("\n==> "+url);
         document=parse(url);
         System.out.println(document.asXML());
         Element userKey = (Element) document.selectSingleNode("/userkey");
         String key = userKey.getText();
         System.out.println(key);

         url = new URL(URL_GET_SERVICES+key);
         System.out.println("\n==> "+url);
         document=parse(url);
         System.out.println(document.asXML());
         Element planet = (Element) document.selectSingleNode("/services/service[@fullName='planet:planet']");
         String planetKey = planet.attributeValue("key");
         System.out.println(planetKey);

         url = new URL(URL_SET_SERVICE+planetKey+"&userKey="+key);
         System.out.println("\n==> "+url);
         document=parse(url);
         System.out.println(document.asXML());

         url = new URL(URL_GET_ZOOM+key);
         System.out.println("\n==> "+url);
         document=parse(url);
         System.out.println(document.asXML());

         url = new URL(URL_LOGOUT+key);
         System.out.println("\n==> "+url);
         document=parse(url);
         System.out.println(document.asXML());
      } catch (MalformedURLException e1) {
         // TODO Auto-generated catch block
         e1.printStackTrace();
      } catch (DocumentException e) {
         // TODO Auto-generated catch block
         System.out.println("Erreur parse");
         e.printStackTrace();
      }

   }

   public static Document parse(URL url) throws DocumentException {
      SAXReader reader = new SAXReader();
      Document document = reader.read(url);
      return document;
   }

 }

::

  ==> http://lisfs2008.irisa.fr:8080/portalis/login.jsp?userId=yves&password=9195c7b9bb56d375948ba058cddea1982b49864e12c700de58d69c5c72dbd075

.. code-block:: xml

   <userkey>74ffb82eb42f998a1f02dcebbb7589f7a22e36adf87b8915bd3b6df3f8ae424e</userkey>

::

  ==> http://lisfs2008.irisa.fr:8080/portalis/getServices.jsp?userKey=74ffb82eb42f998a1f02dcebbb7589f7a22e36adf87b8915bd3b6df3f8ae424e

.. code-block:: xml

    <services>
      <service fullName="illustres:bd" src="catalogue_BD.csv" pid="1250" port="8060"
      url="http://lisfs2008.irisa.fr:8060" key="http://lisfs2008.irisa.fr:8060::illustres:bd"
      nbObject="975" creationDate="19 Sep 2012 14:14:16" lastUpdate="19 Sep 2012 13:23:06"/>
      <service fullName="illustres:comics" src="catalogue_Comics.csv" pid="1259" port="8061"
      url="http://lisfs2008.irisa.fr:8061" key="http://lisfs2008.irisa.fr:8061::illustres:comics"
      nbObject="628" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:21:12"/>
      <service fullName="cartolis:cartolis" src="cartolis1.ctx" pid="1265" port="8062"
      url="http://lisfs2008.irisa.fr:8062" key="http://lisfs2008.irisa.fr:8062::cartolis:cartolis"
      nbObject="500" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:13:16"/>
      <service fullName="photo:mesPhotos" src="mesPhotos.photo.xml.ctx" pid="1272" port="8063"
      url="http://lisfs2008.irisa.fr:8063" key="http://lisfs2008.irisa.fr:8063::photo:mesPhotos"
      nbObject="345" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:25:52"/>
      <service fullName="photo:photoShort" src="photoShort.photo.xml.ctx" pid="1281" port="8064"
      url="http://lisfs2008.irisa.fr:8064" key="http://lisfs2008.irisa.fr:8064::photo:photoShort"
      nbObject="13" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:11:07"/>
      <service fullName="photo:vietnam" src="vietnam.photo.xml.ctx" pid="1287" port="8065"
      url="http://lisfs2008.irisa.fr:8065" key="http://lisfs2008.irisa.fr:8065::photo:vietnam"
      nbObject="30" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:12:58"/>
      <service fullName="planet:planet" pid="1299" port="8066"
      url="http://lisfs2008.irisa.fr:8066" key="http://lisfs2008.irisa.fr:8066::planet:planet"
      nbObject="10" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:14:13"/>
      <service fullName="planet:planetPhoto" pid="1306" port="8067"
      url="http://lisfs2008.irisa.fr:8067" key="http://lisfs2008.irisa.fr:8067::planet:planetPhoto"
      nbObject="10" creationDate="19 Sep 2012 14:14:17" lastUpdate="19 Sep 2012 13:16:03"/>
    </services>

..

  ==> http://lisfs2008.irisa.fr:8080/portalis/setService.jsp?serviceKey=http://lisfs2008.irisa.fr:8066::planet:planet&userKey=kkk

.. code-block:: xml

    <reponse state="Ok"/>

..

  ==> http://lisfs2008.irisa.fr:8080/portalis/getTreeFromZoom.jsp?requete=all&feature=Near&userKey=kkk

.. code-block:: xml

   <zoomReponse newReq="Near" lastUpdate="1348053253138">
      <propertyTree feature="all" supp="4">
        <oids>10 5 4 2</oids>
        <propertyTree feature="Distance" supp="4">
          <oids>10 5 4 2</oids>
          <propertyTree feature="Near" supp="4">
            <oids>10 5 4 2</oids>
          </propertyTree>
        </propertyTree>
        <propertyTree feature="satellite" supp="2">
          <oids>4 2</oids>
        </propertyTree>
        <propertyTree feature="Size" supp="4">
          <oids>10 5 4 2</oids>
          <propertyTree feature="Small" supp="4">
            <oids>10 5 4 2</oids>
          </propertyTree>
        </propertyTree>
        <propertyTree feature="caracteristique ?" supp="3">
          <oids>5 4 2</oids>
          <propertyTree feature="caracteristique couleur ?" supp="3">
            <oids>5 4 2</oids>
            <propertyTree feature="caracteristique couleur is &amp;amp;quote;bleu&amp;amp;quote;" supp="2">
                <oids>5 2</oids>
            </propertyTree>
            <propertyTree feature="caracteristique couleur is &amp;amp;quote;rouge&amp;amp;quote;" supp="1">
                <oids>5</oids>
            </propertyTree>
            <propertyTree feature="caracteristique couleur is &amp;amp;quote;blanche&amp;amp;quote;" supp="1">
              <oids>4</oids>
            </propertyTree>
          </propertyTree>
        <propertyTree feature="caracteristique vent ?" supp="1">
          <oids>4</oids>
          <propertyTree feature="caracteristique vent = 250" supp="1">
            <oids>4</oids>
          </propertyTree>
        </propertyTree>
        <propertyTree feature="caracteristique matiere ?" supp="1">
          <oids>5</oids>
          <propertyTree feature="caracteristique matiere is &amp;amp;quote;h2o&amp;amp;quote;" supp="1">
            <oids>5</oids>
          </propertyTree>
        </propertyTree>
      </propertyTree>
    </propertyTree>
  </zoomReponse>

..

  ==> http://lisfs2008.irisa.fr:8080/portalis/logout.jsp?userKey=kkk

.. code-block:: xml

  <reponse state="Ok">
    <logout>You succesfully log out</logout>
  </reponse>

