package fr.irisa.lis.portalis.site;

import java.util.logging.Logger;
import fr.irisa.lis.portalis.shared.PortalisException;
import fr.irisa.lis.portalis.shared.ProprietesBase;


/**
 * Classe permettant la lecture des proprietés de l'application en test
 */
@SuppressWarnings("serial")
public class DocumentationProprietes extends ProprietesBase {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(DocumentationProprietes.class.getName());


	public static final DocumentationProprietes documentation = new DocumentationProprietes(
			"documentation.properties");

	protected DocumentationProprietes(String fileName) {
		super(fileName);
	}

	protected DocumentationProprietes() {
		super();
	}

	public static boolean test() throws PortalisException {
		if (documentation.getProperty("camelis-id")==null) {
			throw new PortalisException("Fichier 'documentation.properties' inaccessible - il doit être dans le classpath");
		}
		return true;
	}
}