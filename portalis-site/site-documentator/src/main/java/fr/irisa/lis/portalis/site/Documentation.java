package fr.irisa.lis.portalis.site;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import java.util.logging.Logger;
import fr.irisa.lis.portalis.shared.PortalisException;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public class Documentation {private static final Logger LOGGER = Logger.getLogger(Documentation.class.getName());

	private String srcDocPath;
	private String targetDocPath;
	private final String titre;
	private final String docId;

	private final String targetFileName;
	private final String targetXmlFilesPath;
	
	private final Map<String, Integer> num = new HashMap<String, Integer>();
	private final Map<String, StringBuffer> buffers = new HashMap<String, StringBuffer>();
	private final Map<String, List<Integer>> cmds = new HashMap<String, List<Integer>>();

	public Documentation(String titre, String docId) throws PortalisException {
		this.titre = titre;
		this.docId = docId;

		if (targetDocPath == null)
			targetDocPath = DocumentationProprietes.documentation.getProperty(
					"sphinx.target.path", true);
		if (srcDocPath == null)
			srcDocPath = DocumentationProprietes.documentation.getProperty(
					"sphinx.src.path", true);
		
		File targetDir = new File(targetDocPath);
		File dirdir = targetDir.getParentFile();
		if (!dirdir.exists()) {
			dirdir.mkdirs();
		}

		targetFileName = targetDocPath + "/" + docId + ".rst";
		targetXmlFilesPath = targetDocPath + "/" + docId + "/";

		LOGGER.fine("new Documentation(" + titre + ", " + docId
				+ ")\ntargetFileName = " + targetFileName
				+ ", targetXmlFilesPath = " + targetXmlFilesPath
				+ ", srcDocPath = " + srcDocPath);

		checkDirs();

	}

	private void checkDirs() throws PortalisException {
		LOGGER.fine("checkDirs("+targetDocPath+")");

		File targetDir = new File(targetDocPath);
		if (!targetDir.exists() ) {
			targetDir.mkdirs();
		}
	}

	public StringBuffer init() {

		LOGGER.fine("init() docId=" + docId + " targetFileName=" + targetFileName);
		int l = titre.length();
		StringBuffer buf = new StringBuffer(".. -*- mode: rst -*-\n\n")
				.append(new String(new char[l]).replace("\0", "="))
				.append("\n")
				.append(titre)
				.append("\n")
				.append(new String(new char[l]).replace("\0", "="))
				.append("\n\n")
				.append(".. sectionauthor:: Calculé automatiquement à partir d'un programme de test\n\n")
				.append(".. only:: html\n\n")
				.append("   :download:`[version PDF] <../_build/pdf/" + docId
						+ ".pdf>`\n\n");

		return buf;
	}

	private StringBuffer startCmd(String cmd) throws PortalisDocumentException {
		LOGGER.fine("startCmd(" + cmd + ")");
		StringBuffer buf = new StringBuffer(cmd + "\n").append(
				new String(new char[cmd.length()]).replace("\0", "=")).append(
				"\n\n");

		File cmdFile = new File(srcDocPath, cmd + ".rst");
		try {
			if (!cmdFile.exists()) {
				throw new PortalisException("'" + cmdFile.getAbsolutePath()
						+ "' n'existe pas");
			}

			BufferedReader in = new BufferedReader(new FileReader(cmdFile));
			while (in.ready()) {
				buf.append(in.readLine()).append("\n");
			}
			in.close();
		} catch (Exception e) {
			throw new PortalisDocumentException(
					"Impossible de générer le document, raison : ", e);
		}

		return buf;
	}

	public void pushDoc(String cmd, String url, VoidReponse reponse,
			String format, Object... params) throws PortalisDocumentException {


		pushDoc(cmd, String.format(format, params), url, reponse);
	}

	public void pushDoc(String cmd, String texte, String url,
			VoidReponse reponse) throws PortalisDocumentException {
		Integer i = num.get(cmd);
		if (i==null) {
			i = 0;
		} else {
			i++;
		}
		num.put(cmd, i);
		LOGGER.fine("pushDoc(" + cmd + i + "," + url + "+Util.stack2string(" + reponse + ")");

		if (!cmds.containsKey(cmd)) {
			cmds.put(cmd, new ArrayList<Integer>());
		}
		cmds.get(cmd).add(i);

		String idCmd = cmd + "-" + i;
		StringBuffer localBuf = new StringBuffer("\nExample" + i + "\n")
				.append("----------\n\n")
				.append(texte + "\n")
				.append("\nRequète\n")
				.append("'''''''\n\n")
				.append(".. code-block:: text\n\n")
				.append("   " + url + "\n\n")
				.append("Réponse\n")
				.append("'''''''\n\n")
				.append(".. literalinclude:: " + docId + "/" + idCmd + ".xml\n")
				.append("   :language: xml\n\n");

		buffers.put(idCmd, localBuf);

		File targetDir = new File(targetXmlFilesPath);
		try {
			if (!targetDir.exists())
				if (!targetDir.mkdir())
					throw new PortalisException(
							"Impossible de créer le répertoire "
									+ targetDir.getAbsolutePath());

			if (!targetDir.isDirectory())
				throw new PortalisException("Le répertoire '"
						+ targetDir.getAbsolutePath()
						+ "' n'est pas un répertoire");

			File targetFile = new File(targetDir, idCmd + ".xml");
			LOGGER.fine("pushDoc() writting on file '" + targetFile.getPath()
					+ "'");

			BufferedWriter out = new BufferedWriter(new FileWriter(targetFile));
			if (reponse != null)
				out.write(reponse.toString());
			else
				out.write("<null-element/>\n");
			out.close();
		} catch (Exception e) {
			throw new PortalisDocumentException(
					"Impossible de générer le document, raison : ", e);
		}
	}

	public void generate() throws PortalisDocumentException {
		LOGGER.fine("generate() targetFileName=" + targetFileName);
		try {
			BufferedWriter out = new BufferedWriter(new FileWriter(
					targetFileName));
			out.write(init().toString());

			// insertion de la présentation à partir du fichier "overview.rst"
			// s'il y en a un
			File overviewFile = new File(srcDocPath, "overview.rst");
			if (overviewFile.exists()) {
				try {
					BufferedReader in = new BufferedReader(new FileReader(
							overviewFile));
					StringBuffer buf = new StringBuffer();
					while (in.ready()) {
						buf.append(in.readLine()).append("\n");
					}
					in.close();
					out.write(buf.toString());
				} catch (Exception e) {
					out.close();
					throw new PortalisDocumentException(
							"Impossible de générer le document, raison : ", e);
				}
			}

			// sortie de la table des matières
			out.write("\n\nListe des commandes");
			out.write("\n===================\n");
			for (String cmd : asSortedList(cmds.keySet())) {
				String line = String.format("\n- `%s`_", cmd);
				out.write(line);
			}
			out.write("\n");

			/* Now get a sorted list of the *keys* in the map. */
			for (String cmd : asSortedList(cmds.keySet())) {
				out.write(startCmd(cmd).toString());

				for (Integer i : asSortedList(cmds.get(cmd))) {
					out.write(buffers.get(cmd + "-" + i).toString());
				}
			}

			out.close();
		} catch (Exception e) {
			throw new PortalisDocumentException(
					"Impossible de générer le document, raison : ", e);
		}

	}

	private static <T extends Comparable<? super T>> List<T> asSortedList(
			Collection<T> c) {
		List<T> list = new ArrayList<T>(c);
		java.util.Collections.sort(list);
		return list;
	}

}
