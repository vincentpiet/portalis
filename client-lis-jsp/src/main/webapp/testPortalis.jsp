<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="java.util.Set"%>
<%@ page import="java.util.Map"%>

<%@ page import="fr.irisa.lis.portalis.clientLisJsp.ClientLisJsp"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Constants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ServiceCore"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Application"%>
<%@ page import="fr.irisa.lis.portalis.shared.http.admin.AdminHttp"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveUser"%>
ActiveUser
<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.CamelisService"%>

<%@ page import="fr.irisa.lis.portalis.system.linux.LinuxCmd"%>

<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "testPortalis.jsp";
	static final private LoggerProvider log = LoggerProvider
			.getDefault("fr.irisa.lis.portalis.admin." + jspName);%>

<%
	response.setContentType("text/html; charset=UTF-8");
	response.setHeader("Expires", "0");

	LOGGER.fine("\n================================================= "
			+ jspName
			+ " =================================================");
%>
<html>
<body>
	<jsp:include page="header.html" />

	<h1>Test de Portalis admin</h1>
	<%
		String mess = null;
			String email = request.getParameter("email");
			String serverName = request.getServerName();
			int serverPort = request.getServerPort();

			LoginReponse reponse = null;
			LOGGER.fine("sessionId = " + session.getId());

			// on charge la sesssion admin s'il y en a une
			Session adminSession = (Session) session
			.getAttribute(ClientLisJsp.ADMIN_SESSION);
			String clearLogs = request.getParameter("clearLogs");
			if (clearLogs != null) {
		if (adminSession == null) {
			throw new PortalisException(
					"ya gros problème ... clearLogs sans session admin courante ...");
		} else {
			AdminHttp.clearLog(adminSession);
		}
			}

			SiteReponse siteReponse = null;
			try {

		if (adminSession == null) { // adminSession == null
			LOGGER.fine("première connection de " + email);
			// première connection de l'utilisateur
			String host = request.getServerName();
			int port = request.getServerPort();
			reponse = AdminHttp.login(email,
					request.getParameter(XmlIdentifier.PASSWORD()));
			if (reponse.isOk()) {
				adminSession = reponse.getSession();
				session.setAttribute(ClientLisJsp.ADMIN_SESSION,
						adminSession);
			} else {
				throw new PortalisException(
						reponse.getMessagesAsString());
			}
		} else {
			if (adminSession.getActiveUser().getUserCore().getEmail()
					.equals(email)) { //
				// utilisateur déjà connecté
				LOGGER.fine("reconnection de " + email);
				reponse = new LoginReponse(adminSession);
			} else {
				throw new PortalisException(
						String.format(
								"ya un problème : l'utilisateur courrant '%s' est différent du parametre email : '%s'",
								adminSession.getActiveUser()
										.getUserCore().getEmail(),
								email));
			}
		}
		siteReponse = AdminHttp.getSite(adminSession);
		if (siteReponse == null || !siteReponse.isOk()) {
			throw new PortalisException(
					siteReponse.getMessagesAsString());
		}
			} catch (Exception e) {
		mess = "Erreur "
				+ jspName
				+ (e.getMessage() == null ? e.getClass().getName() : e
						.getMessage());
		LOGGER.severe(mess+Util.stack2string( e));
			}
			LOGGER.fine("reponse = " + reponse);
			LOGGER.fine("siteReponse = " + siteReponse);
			String key = adminSession.getPortalisSessionId();

			Map<String, Application> applications = siteReponse.getSite()
			.getApplications();
			Set<String> appliIds = applications.keySet();

			PingReponse pingReponse = AdminHttp
			.getActiveLisServicesOnPortalis(adminSession);
			Set<CamelisService> activeServices = pingReponse.getLesServices();

			if (mess != null) {
	%>
	<h2>Erreur dans le chargement de testPortalis.jsp</h2>
	<pre><%=mess%></pre>
	<%
		} else {
	%>
	<h2>Tester des commandes d'administration</h2>
	<ul>
		<li><a
			href="/portalis/startCamelis.jsp?creator=<%=email%>&amp;key=<%=key%>&amp;serviceName=monAppli:leService">startCamelis.jsp?creator=<%=email%>&amp;key=<%=key%>&amp;serviceName=monAppli:leService
		</a></li>
		<li><a href="/portalis/stopCamelis.jsp?port=8060">stopCamelis.jsp?port=8060</a></li>
		<li><a
			href="/portalis/startCamelis.jsp?creator=<%=email%>&amp;key=<%=key%>&amp;port=8090&amp;serviceName=planets:planets">startCamelis.jsp?creator=<%=email%>&amp;key=<%=key%>&amp;serviceName=planets:planets&amp;port=8090
		</a></li>
		<li><a href="/portalis/stopCamelis.jsp?port=8090">stopCamelis.jsp?port=8090</a></li>
		<li><a href="/portalis/stopCamelis.jsp">stopCamelis.jsp</a></li>
		<li><a href="/portalis/ping.jsp?port=8090">ping.jsp?port=8090</a></li>
		<li><a href="/portalis/ping.jsp">ping.jsp</a></li>
		<li><a href="/portalis/getActifsPorts.jsp">getActifsPorts.jsp</a></li>
		<li><a href="/portalis/getCamelisProcessId.jsp">getCamelisProcessId.jsp</a></li>
		<li><a href="/portalis/lsof.jsp">lsof.jsp</a></li>
	</ul>

	<h2>Gérer les services</h2>

	<table border="1">
		<tr>
			<th>Lancer un service</th>
			<th>Aller à un service</th>
		</tr>
		<tr>
			<td>
				<%
					if (siteReponse.isOk()) {
							Set<ServiceCore> services = siteReponse.getSite()
									.getLesServices();
				%> <%
 	if (services.size() > 0) {
 %>
				<form action="testCamelis.jsp">
					<table>
						<%
							int i = 0;
										for (ServiceCore service : services) {
											String fullName = service.getFullName();
											out.println("<tr><td>" + fullName + "</td>");
											out.println("<td>");
											out.println(String
													.format("<input type=\"radio\" name=\"%s\" value=\"%s\" %s/>",
															XmlIdentifier.SERVICE_NAME(),
															fullName,
															i == 0 ? "checked=\"checked\" "
																	: ""));
											out.println("</td></tr>");
											i++;
										}
						%>
						<tr>
							<td colspan="2"><div align="center">
									<input type="submit" value="Démarrer Camelis" />
								</div></td>
						</tr>
					</table>
				</form> <%
 	} else {
 %> No service are proposed <%
 	}
 %>
			</td>
			<td>
				<%
					if (activeServices.size() > 0) {
				%>
				<form action="testCamelis.jsp">
					<table>
						<%
							int i = 0;
										for (CamelisService service : activeServices) {
											String serviceName = service.getId();
											out.println("<tr><td>" + serviceName + "</td>");
											out.println("<td>");
											out.println(String
													.format("<input type=\"radio\" name=\"%s\" value=\"%s\" %s/>",
															XmlIdentifier.SERVICE_NAME(),
															serviceName,
															i == 0 ? "checked=\"checked\" "
																	: ""));
											out.println("</td></tr>");
											i++;
										}
						%>
						<tr>
							<td colspan="2"><div align="center">
									<input type="submit" value="Tester ce service" />
								</div></td>
						</tr>
					</table>
				</form> <%
 	} else {
 %> No active service at the moment <%
 	}
 %>
			</td>
		</tr>
	</table>

	<h2>File Upload</h2>
	<form action="uploadFile.jsp" method="post"
		enctype="multipart/form-data">
		<table border="1">
			<tr>
				<td>Select an active Camelis service</td>
				<td>
					<div align="center">
						<select name="<%=XmlIdentifier.SERVICE_NAME()%>">
							<%
								for (ServiceCore service : services) {
											String fullName = service.getFullName();
							%>
							<option value="<%=fullName%>"><%=fullName%></option>
							<%
								}
							%>
						</select>
					</div>Erreur Login null
				</td>
			</tr>
			<tr>
				<td>Select a file to upload</td>
				<td><input type="file" name="file" size="50" /></td>
			</tr>
			<tr>
				<td colspan="2">
					<div align="center">
						<input type="submit" value="Upload File" />
					</div>
				</td>
			</tr>
		</table>
	</form>
	<%
		} else {
	%>
	<p>No Camelis services available</p>
	<%
		}
		}
	%>
	<h2>Autres actions</h2>
	<table>
		<tr>
			<td>
				<form action="sortieTestPortalis.jsp">
					<input type="hidden" value="<%=email%>" name="email" /> <input
						type="submit" value="Logout" />
				</form>
			</td>
			<td>
				<form
					action="http://<%=serverName%> %>:<%=serverPort%>/portalis-site">
					<input type="submit" value="Voir le site de documentation" />
				</form>
			</td>
			<td>
				<form action="testPortalis.jsp">
					<input type="hidden" value="clearLogs" name="clearLogs" /> <input
						type="hidden" value="<%=email%>" name="email" /> <input
						type="submit" value="Clear log files" />
				</form>
			</td>
			<td>
				<form action="portalis/showLog.jsp">
					<input type="submit" value="Show log file" />
				</form>
			</td>

		</tr>
	</table>
</body>
</html>
