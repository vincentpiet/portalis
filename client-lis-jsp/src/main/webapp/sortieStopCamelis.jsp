<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/xml; charset=UTF-8"
	pageEncoding="UTF-8"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Constants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>

<%@ page import="fr.irisa.lis.portalis.system.linux.LinuxCmd"%>
<%@ page import="fr.irisa.lis.portalis.shared.http.admin.AdminHttp"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse"%>

<%@ page import="java.util.logging.Logger"%>

<jsp:include page="header.html" />

<%!private static final String jspName = "stopCamelis.jsp";
	static final private LoggerProvider log = LoggerProvider
			.getDefault("fr.irisa.lis.portalis.admin." + jspName);%>
<%
	response.setContentType(Constants.CONTENT_TYPE_XML);
	response.setHeader("Expires", "0");

	LOGGER.fine("\n================================================= "
	+ jspName
	+ " =================================================");

	PingReponse reponse = null;
	try {
		Session camelisSession = (Session) session
		.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		LOGGER.fine("sessionId = " + session.getId());
		if (camelisSession == null) {
	String mess = String.format(
			"Erreur %s : pas de session en cours", jspName);
	LOGGER.fine(mess);
	reponse = new PingReponse(Constants.ERROR, mess);
		} else {
	Session portalisSession = Session
			.createPortalisSession(camelisSession
					.getActiveUser());

	String portNum = request.getParameter(XmlIdentifier.PORT());
	String hostName = request
			.getParameter(XmlIdentifier.HOST());
	if (hostName == null) {
		hostName = camelisSession.getHost();
	}

	if (portNum != null) {
		int port = Integer.parseInt(portNum);
		reponse = AdminHttp.stopCamelis(portalisSession, port);
	} else {
		LinuxCmd.killAllActiveLisProcess();
		reponse = AdminHttp
				.getActiveLisServicesOnPortalis(portalisSession);
	}

		}
		RequestDispatcher dispatcher = request
		.getRequestDispatcher("testPortalis.jsp");
		dispatcher.forward(request, response);

	} catch (Exception e) {
		String state = Constants.ERROR;
		String mess = (e.getMessage() == null ? String.format(
		"Erreur %s : %s", jspName, e.getClass().getName())
		: String.format("Erreur %s : %s", jspName,
				e.getMessage()));
		LOGGER.severe(mess+Util.stack2string( e));
		reponse = new PingReponse(state, mess);
	}
	LOGGER.fine("reponse = " + reponse);
%>
<%=reponse%>
