<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@ page import="java.util.Set"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Constants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ServiceCore"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Application"%>

<%@ page import="fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.CamelisService"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse"%>

<%@ page import="fr.irisa.lis.portalis.clientLisJsp.ClientLisJsp"%>


<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "demarrerUnCamelis.jsp";
	static final private LoggerProvider log = LoggerProvider
			.getDefault("fr.irisa.lis.portalis.clientJsp." + jspName);%>
<%
	response.setContentType("text/html; charset=UTF-8");
	response.setHeader("Expires", "0");
%>
<html>
<head>
<meta charset="UTF-8" />
<title>XML LIS server -- test</title>
</head>
<body>
	<jsp:include page="header.html" />

<%
	String mess = null;
	String serviceName = request.getParameter(XmlIdentifier
			.SERVICE_NAME());
	StartReponse reponse = null;
	Session adminSession = (Session) session
			.getAttribute(ClientLisJsp.ADMIN_SESSION);
	Session camelisSession = null;
	String host = request.getServerName();
	String camelisPort = null;
	CamelisService camelisService = null;
	LOGGER.fine("\n================================================= "
			+ jspName
			+ " =================================================");

	try {
		if (adminSession == null) {
			String message = String.format(
					"Erreur %s : pas de session admin en cours",
					jspName);
			throw new PortalisException(message);
		}
		LOGGER.fine("adminSession = " + adminSession);

		if (serviceName == null) {
			mess = "Erreur " + jspName + " pas de nom de service";
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		LOGGER.fine("serviceName = " + serviceName);

		reponse = AdminHttp.startCamelis(adminSession, serviceName,
				null, null);
		if (!reponse.isOk()) {
			throw new PortalisException(reponse.getMessagesAsString());
		}
		camelisService = reponse.getCamelisService();
		int portNum = camelisService.getPort();
		camelisPort = camelisPort + "";

		if (camelisService.getHost() != host) {
			throw new PortalisException(
					String.format(
							"Démarrage Camelis pas normal : hôte Camelis = %s différent de l'hôte Portalis = %s",
							camelisService.getHost(), host));
		}

		// On cree une session Camelis
		camelisSession = Session.createCamelisSession(
				adminSession.getActiveUser(), camelisService);
		session.setAttribute(XmlIdentifier.CAMELIS_SESSION,
				camelisSession);

	} catch (Exception e) {
		mess = "Erreur " + jspName + e.getMessage();
		LOGGER.severe(mess+Util.stack2string( e));
%>
	<h1>Erreur dans le démarrage de Camelis</h1>
	<pre><%=mess%></pre>
	<pre>Util.stack2string(e)</pre>
	
	<a href="testPortalis.jsp">Retour à testPortalis</a>
</body>
</html>
<%  
	}
	
%>
	<h1>Démarrage de Camelis réussit</h1>
	<h3>StartReponse</h3>
	
    <pre><%= reponse.toString().replaceAll("<", "&gt;").replaceAll("<", "&lt;")%></pre>
    
	<h3>camelisSession</h3>
	
    <pre><%= camelisSession.toString().replaceAll("<", "&gt;").replaceAll("<", "&lt;")%></pre>
    
    	<a href="importerCtx.jsp">Importer un contexte</a>
 
 </body>
</html>
    
    