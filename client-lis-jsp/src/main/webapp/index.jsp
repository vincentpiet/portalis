<%@ page import="fr.irisa.lis.portalis.shared.admin.data.PortalisService"%>

<%!

	public void jspInit() {
		String hostName = getInitParameter("portalis.host");
		String port = getInitParameter("portalis.port");
		int hostPort = Integer.parseInt(port);
		PortalisService.setPORTALIS_SERVICE_ID(hostName, hostPort);
	}
%>

<html>
<body>
<jsp:include page="header.html" />
	<h1>Serveur de lancement des serveurs Camelis</h1>
	<h2>Test des commandes</h2>
	
	

<form action="testPortalis.jsp">
	<table>
		<tr>
			<th>email :</th>
			<td><input type="text" name="email"></td>
		</tr>
		<tr>
			<th>Password :</th>
			<td><input type="password" name="password" /></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="Log in" /></td>
		</tr>
    </table>

    <p>Hint: you can log in as user test@example.org (empty password).</p>
</form>
</body>
</html>
