<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>


<%@ page import="java.util.Set"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.PortalisException"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Util"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.XmlIdentifier"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.Constants"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Session"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ServiceCore"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.Application"%>

<%@ page import="fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp"%>
<%@ page import="fr.irisa.lis.portalis.shared.http.admin.AdminHttp"%>

<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse"%>
<%@ page import="fr.irisa.lis.portalis.shared.admin.data.ActiveLisService"%>
<%@ page
	import="fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse"%>

<%@ page import="fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse"%>

<%@ page import="fr.irisa.lis.portalis.clientLisJsp.ClientLisJsp"%>


<%@ page import="java.util.logging.Logger"%>

<%!private static final String jspName = "importerCtx.jsp";
	static final private LoggerProvider log = LoggerProvider
			.getDefault("fr.irisa.lis.portalis.admin." + jspName);%>
<%
	response.setContentType("text/html; charset=UTF-8");
	response.setHeader("Expires", "0");
%>
<html>
<head>
<meta charset="UTF-8" />
<title>XML LIS server -- test</title>
</head>

<body>
	<jsp:include page="header.html" />
	<%
		LOGGER.fine("\n================================================= "
				+ jspName
				+ " =================================================");
	Session camelisSession = (Session) session
			.getAttribute(XmlIdentifier.CAMELIS_SESSION_ID);
		String serviceId = null;
		
		String ctxFile = null;
		
		ImportCtxReponse reponse = null;

		try {
			if (camelisSession == null) {
				String message = String.format(
						"Erreur %s : pas de session camelis en cours",
						jspName);
				throw new PortalisException(message);
			}
			LOGGER.fine("camelisSession = " + camelisSession);
			
			ActiveLisService camelisService = (ActiveLisService) camelisSession.getActiveService();

			serviceId = camelisService.getId();
			
			String fullName = camelisService.getFullName();
			
			String[] tab = fullName.split(":");
			
			ctxFile = tab[0] + "/" + tab[1] + "/" +  tab[1] + ".ctx";

			LOGGER.fine("ctxFile = " + ctxFile);
			reponse = CamelisHttp.importCtx(camelisSession);
			
		} catch (Exception e) {
			String mess = "Erreur " + jspName + e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
	%>
		<h1>Erreur dans l'import d'un ctx</h1>
		<pre><%=mess%></pre>
		<pre>Util.stack2string(e)</pre>
		
		<a href="testPortalis.jsp">Retour à testPortalis</a>
</body>
</html>
	<%  
		}
		
	%>
	
	<h1>ImportCtx réussit pour le service <%=serviceId %></h1>
	
	<p>Fichier chargé : <%= ctxFile%></p>
	
	<h3>ImportCtxReponse</h3>
	<pre><%=reponse.toString().replaceAll("<", "&gt;").replaceAll("<", "&lt;") %></pre>
		<a href="testCamelis.jsp">Tester ce service</a>

</body>
</html>

