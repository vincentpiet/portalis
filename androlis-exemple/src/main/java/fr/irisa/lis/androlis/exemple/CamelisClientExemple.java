package fr.irisa.lis.androlis.exemple;

import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

public class CamelisClientExemple {private static final Logger LOGGER = Logger.getLogger(CamelisClientExemple.class.getName());

	final static String BEKKERS_EMAIL = "bekkers@irisa.fr";
	final static String BEKKERS_PASSWORD = "yves";
	final static String SERVICE_PLANET_PLANET = "planets:planets";

	final static String PIET_EMAIL = "vincent.piet@irisa.fr";
	final static String PIET_PASSWORD = "vincent";

	private static void initPortalisWithArgs(String[] args) {
		System.out.println(args.length);
		String host = "localhost";
		int portNum = 8080;
		String portalisAppli = "portalis";
		if (args.length == 3) {
			host = args[0];
			try {
				portNum = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				stopError();
			}
			portalisAppli = args[2];
		} else if (args.length != 0) {
			stopError();
		}
		PortalisService.getInstance().init(host, portNum, portalisAppli);
	}

	private static void stopError() {
		System.out.println("usage is 'androlis [<hostName> <port number> <portalis application name>]'\n"
				+"   default <hostName> = 'localhost'\n"
				+"   default <port number> = 8080\n"
				+"   default <portalis application name> = portalis");
		System.exit(-1);
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {

		initPortalisWithArgs(args);

		try {

			System.out.println("\n====================================== AdminHttp.login ====================================");
			LoginReponse bekkersLoginReponse = AdminHttp.login(BEKKERS_EMAIL,
					BEKKERS_PASSWORD);
			checkReponse(bekkersLoginReponse);

			Session bekkersLoginSession = null;
			bekkersLoginSession = bekkersLoginReponse.getSession();

			System.out.println("\n====================================== AdminHttp.startCamelis ====================================");
			StartReponse startPlanetPlanetReponse = AdminHttp.startCamelis(
					bekkersLoginSession, SERVICE_PLANET_PLANET, null, null);
			checkReponse(startPlanetPlanetReponse);

			Session bekkersCamelisSession = Session.createCamelisSession(
					bekkersLoginSession,
					startPlanetPlanetReponse.getActiveLisService());

			System.out.println("\n====================================== AdminHttp.login ====================================");
			LoginReponse pietLoginReponse = AdminHttp.login(PIET_EMAIL,
					PIET_PASSWORD);
			checkReponse(pietLoginReponse);


			System.out.println("\n====================================== CamelisHttp.importCtx ====================================");
			Session pietLoginSession = pietLoginReponse.getSession();
			Session pietCamelisSession = Session.createCamelisSession(
					pietLoginSession,
					startPlanetPlanetReponse.getActiveLisService());
			System.out.println(pietCamelisSession);


			ImportCtxReponse importCtxReponse = CamelisHttp
					.importCtx(pietCamelisSession);
			checkReponse(importCtxReponse);

			System.out.println("\n====================================== 100 fois \"CamelisHttp.ping\" suivi de \"Http.zoom\" ====================================");
			System.out.print("Iterating 100 X 3 commands : ");
			System.out.flush();
			for (int i = 0; i < 100; i++) {
				if (i%5==0) {
					System.out.print(".");
					System.out.flush();
				}
				PingReponse pingReponse = CamelisHttp.ping(pietCamelisSession);

				ExtentReponse extentReponse = CamelisHttp
						.extent(pietCamelisSession);

				ZoomReponse zoomreponse = CamelisHttp.zoom(pietCamelisSession,
						"all", "satelite", true);

			}

			System.out.println("\n\n====================================== AdminHttp.stopCamelis ====================================");
			PingReponse pingReponse = AdminHttp.stopCamelis(
					bekkersLoginSession, bekkersCamelisSession.getPort());
			checkReponse(pingReponse);

			VoidReponse bekkersLogoutReponse = AdminHttp
					.logout(bekkersLoginSession);
			checkReponse(bekkersLogoutReponse);

			System.out.println("\n====================================== AdminHttp.logout 2 ====================================");
			VoidReponse pietLogoutReponse = AdminHttp.logout(pietLoginSession);
			checkReponse(pietLogoutReponse);

			System.out.println("Succes");

		} catch (PortalisException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(1);
		}
	}

	private static void checkReponse(VoidReponse reponse) {
		System.out.println("\n"+reponse);
		if (!reponse.getStatus().equals(XmlIdentifier.OK)) {
			String mess = reponse.getMessagesAsString();
			LOGGER.severe(mess);
			System.exit(1);
		}
	}

}
