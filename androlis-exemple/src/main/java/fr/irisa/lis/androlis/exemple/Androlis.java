package fr.irisa.lis.androlis.exemple;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;
import fr.irisa.lis.portalis.shared.admin.http.AdminHttp;
import fr.irisa.lis.portalis.shared.camelis.http.CamelisHttp;

public class Androlis {

	final static String sessionId = "ABCED1234F";
	final static String email = "bekkers@irisa.fr";
	final static String password = "yves";
	final static String PLANETS_PLANETS_FULL_NAME = "planets:planets";
	final static String PATRIMOINE_BRETAGNE_FULL_NAME = "patrimoine:patrimoineBretagne";
	final static String FORMAL_CONCEPT_NUMBER_FULL_NAME = "formalConcepts:number";
	

	static final String PROCESS_PATTERN = "^[\\w\\-]*\\s([\\d]+)\\s.*$";
	static final String PORT_PATTERN = "^.*\\sTCP\\s\\*:([\\d]+)\\s.*$";

	/**
	 * @param args
	 */

		public static void main(String[] args) {

		initPortalisWithArgs(args);

		try {
			ClientConstants.setPrintHttpRequest(true);

			System.out.println("\n====================================== login ====================================");
			LoginReponse loginReponse = AdminHttp.login(email, password);
			checkResult(loginReponse);
			Session loginSession = loginReponse.getSession();

			System.out.println("\n====================================== getSite ====================================");
			SiteReponse siteReponse = AdminHttp.getSite(loginSession);
			checkResult(siteReponse);

			System.out.println("\n====================================== getActiveLisServicesOnPortalis ====================================");
			PingReponse pingReponse = AdminHttp.getActiveLisServicesOnPortalis(loginSession);
			checkResult(pingReponse);

			System.out.println("\n====================================== StartCamelis formalConcepts:number ====================================");
			StartReponse formalConceptStartReponse = AdminHttp.startCamelis(loginSession,
					FORMAL_CONCEPT_NUMBER_FULL_NAME, null, null);
			checkResult(formalConceptStartReponse);

			System.out.println("\n====================================== createCamelisSession formalConcepts:number ====================================");
			Session formalConceptSession = Session.createCamelisSession( 
					loginSession,
					formalConceptStartReponse.getActiveLisService());
			System.out.println(formalConceptSession);
			
			System.out.println("\n====================================== importCtx  formalConcepts/number/number.ctx ====================================");
			String ctxNumber = "formalConcepts/number/number.ctx";
			ImportCtxReponse importNumberReponse = CamelisHttp.importCtx(
					formalConceptSession, ctxNumber);
			checkResult(importNumberReponse);
			

//			System.out.println("\n====================================== createCamelisSession patrimoineBretagne ====================================");
//			Session camelisSession0 = Session.createCamelisSession( 
//					loginSession,
//					formalConceptStartReponse.getActiveLisService());
//			System.out.println(camelisSession0);
			
//			System.out.println("\n====================================== importCtx  patrimoine/patrimoineBretagne/patrimoineBretagne.ctx ====================================");
//			String ctxFile0 = "patrimoine/patrimoineBretagne/patrimoineBretagne.ctx";
//			ImportCtxReponse importReponse0 = CamelisHttp.importCtx(
//					camelisSession0, ctxFile0);
//			checkResult(importReponse0);
			

//			System.out
//					.println("\n====================================== loadContext patrimoineBretagne ====================================");
//			LoadContextReponse loadContextReponse = CamelisHttp
//					.loadContext(camelisSession0);
//			checkResult(loadContextReponse);			
			
//			System.out.println("\n====================================== ping ====================================");
//			PingReponse pingReponse0 = CamelisHttp.ping(camelisSession0);
//			checkResult(pingReponse0);
			
			System.out.println("\n====================================== StartCamelis planets/planets ====================================");
			StartReponse startReponse = AdminHttp.startCamelis(loginSession,
					PLANETS_PLANETS_FULL_NAME, null, null);
			checkResult(startReponse);
			int port1 = startReponse.getActiveLisService().getPort();

			System.out.println("\n====================================== createCamelisSession 1 ====================================");
			Session camelisSession1 = Session.createCamelisSession( 
					loginSession,
					startReponse.getActiveLisService());
			System.out.println(camelisSession1);
	
			System.out
					.println("\n====================================== loadContext planets/planets ====================================");
			LoadContextReponse loadContextReponse1 = CamelisHttp
					.loadContext(camelisSession1);
			checkResult(loadContextReponse1);

//			System.out.println("\n====================================== importCtx planets/planets/planets.ctx ====================================");
//			String ctxFile = "planets/planets/planets.ctx";
//			ImportCtxReponse importReponse = CamelisHttp.importCtx(
//					camelisSession1, ctxFile);
//			checkResult(importReponse);
			
			System.out.println("\n====================================== extent(\"Medium\") ====================================");
			String lisQuery = "Medium";
			ExtentReponse ExtentReponse = CamelisHttp.extent(camelisSession1, lisQuery);
			checkResult(ExtentReponse);

			System.out.println("\n====================================== ping ====================================");
			PingReponse pingReponse1 = CamelisHttp.ping(camelisSession1);
			checkResult(pingReponse1);
			
			
			System.out.println("\n====================================== StartCamelis 1 ====================================");
			StartReponse startReponse2 = AdminHttp.startCamelis(loginSession,
					PLANETS_PLANETS_FULL_NAME+"999", null, null);
			checkResult(startReponse2);


			System.out.println("\n====================================== stopCamelis 1 ====================================");
			PingReponse pingReponse2 = AdminHttp.stopCamelis(loginSession, port1);
			checkResult(pingReponse2);
			int port2 = startReponse2.getActiveLisService().getPort();

			System.out.println("\n====================================== stopCamelis 2 ====================================");
			PingReponse pingReponse3 = AdminHttp.stopCamelis(loginSession, port2);
			checkResult(pingReponse3);

			System.out.println("\n====================================== logout ====================================");
			VoidReponse logoutReponse = AdminHttp.logout(loginSession);
			checkResult(logoutReponse);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void checkResult(VoidReponse loginReponse) {
		System.out.println(loginReponse);
		if (!loginReponse.isOk())
			stop(loginReponse.getMessagesAsString());
	}

	private static void stop(String messagesAsString) {
		System.out.println("Arr�t en erreur : "+messagesAsString);
		System.exit(0);
	}

	private static void initPortalisWithArgs(String[] args) {
		System.out.println(args.length);
		String host = "localhost";
		int portNum = 8080;
		String portalisAppli = "portalis";
		if (args.length == 3) {
			host = args[0];
			try {
				portNum = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				stopError();
			}
			portalisAppli = args[2];
		} else if (args.length != 0) {
			stopError();
		}
		PortalisService.getInstance().init(host, portNum, portalisAppli);
	}

	private static void stopError() {
		System.out.println("usage is 'androlis [<hostName> <port number> <portalis application name>]'\n"
				+"   default <hostName> = 'localhost'\n"
				+"   default <port number> = 8080\n"
				+"   default <portalis application name> = portalis");
		System.exit(-1);
	}


}
