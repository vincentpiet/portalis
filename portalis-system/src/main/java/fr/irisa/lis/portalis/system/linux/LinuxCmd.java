package fr.irisa.lis.portalis.system.linux;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.regex.Matcher;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public class LinuxCmd {private static final Logger LOGGER = Logger.getLogger(LinuxCmd.class.getName());

	public static String CAMELIS_APPLICATION_NAME;


	private static final String bin_lsof = "/usr/bin/lsof";
	private static final String sbin_lsof = "/usr/sbin/lsof";
	private static String LSOF_CMD = null;

	static {
		File lsof1 = new File(bin_lsof);
		File lsof2 = new File(sbin_lsof);
		if (lsof1.exists()) {
			LSOF_CMD = bin_lsof;
		} else if (lsof2.exists()) {
			LSOF_CMD = sbin_lsof;
		}
	}
	
	public static void init(String camelisApplicationName) {
		CAMELIS_APPLICATION_NAME = camelisApplicationName;
	}

	/**
	 * 
	 * @return l'information de version de Maven
	 */
	public static String getPortalisVersion() {
		return LinuxCmd.class.getPackage().getImplementationVersion();
	}

	public static Set<Integer> lsof(int port) throws PortalisException {
		LOGGER.fine("\nPortalisCtx LinuxCmd.lsof(" + port + ")");
		String cmd = LSOF_CMD + " -i tcp:" + port + " -Fp";
		int count = 0;
		Set<Integer> pidList = new HashSet<Integer>();
		String reponse = LinuxInteractor.executeCommand(cmd, true, false);
		LOGGER.info("==== Linux ==== >> lsof(" + port + ")" + "\ncmd = " + cmd
				+ "\nreponse="
				+ (reponse == null ? "null" : "'" + reponse + "'"));
		if (reponse != null && reponse.length() > 0) {
			String[] lines = reponse.split("\n");
			int javaPid = getMyJavaPid();
			for (String line : lines) {
				LOGGER.fine("lsof(" + port + ")\ninputLine" + count + " : "
						+ line);
				try {
					// On enlève le 'p' de 'p3456'
					int pid = Integer.parseInt(line.substring(1));
					if (pid != javaPid) {
						pidList.add(pid);
						count += 1;
					}
				} catch (NumberFormatException e) {
					String mess = "Impossible de lire le numéro de processus : "
							+ line.substring(1);
					LOGGER.severe(mess+Util.stack2string( e));
					throw new PortalisException(mess);
				}
			}
		}
		return pidList;
	}

	static final String PROCESS_PATTERN = "^[\\w\\-]*\\s([\\d]+)\\s.*$";
	static final String PORT_PATTERN = "^.*\\sTCP\\s\\*:([\\d]+)\\s.*$";

	

	public static int getMyJavaPid() throws PortalisException {
		String pid = ManagementFactory.getRuntimeMXBean().getName()
				.replaceFirst("@.*$", "");
		int n = 0;
		try {
			n = Integer.parseInt(pid);
		} catch (Exception e) {
			String mess = "Erreur interne : impossible de calculer le N° de processus Java, la chaîne d'entrée est '"
					+ pid + "'";
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		LOGGER.info("==== Linux ==== >> getMyJavaPid() = " + n);
		return n;
	}

	public static int getCamelisProcessId(int port) throws PortalisException {
		LOGGER.info("==== Linux ==== >> getCamelisPid(" + port + ")");
		Set<PortActif> actifs = getActifsCamelisPortsOnPortalis();
		for (PortActif actif : actifs) {
			if (actif.getPort() == port) {
				int pid = actif.getPid();
				if (checkPid(port, pid))
					return actif.getPid();
				else
					throw new PortalisException(
							"Erreur système : command 'lsof' et 'ps' pas d'accord pour dire que le processus "
									+ pid + " est actif sur le port " + port);
			}
		}
		// process not yet visible
		return 0;
	}

	public static VoidReponse clearLog(File logFile) {
		VoidReponse voidReponse = new VoidReponse();
		final String logFileName = logFile.getName();
		final File logDir = logFile.getParentFile();

		Calendar calendar = new GregorianCalendar();
		StringBuffer buff = new StringBuffer();
		buff.append(calendar.get(Calendar.DAY_OF_MONTH) + "-");
		buff.append(calendar.get(Calendar.MONTH) + "-");
		buff.append(calendar.get(Calendar.YEAR) + "_");
		buff.append(calendar.get(Calendar.HOUR) + ":");
		buff.append(calendar.get(Calendar.MINUTE) + ":");
		buff.append(calendar.get(Calendar.SECOND) + ":");
		buff.append(calendar.get(Calendar.MILLISECOND) + "_");

		String prefixe = buff.toString();
		for (File file : logDir.listFiles()) {
			if (file.getName().startsWith(logFileName)) {
				String cp_cmd = String.format("cp %s %s/%s%s", file.getPath(),
						logDir.getPath(), prefixe, file.getName());
				try {
					LinuxInteractor.executeCommand(cp_cmd, true);
				} catch (PortalisException e) {
					String mess = String.format(
							"Impossible d'exécuter la commande '%s' : %s",
							cp_cmd, e.getMessage());
					LOGGER.severe(mess+Util.stack2string( e));
					return new VoidReponse(XmlIdentifier.ERROR, mess);
				}
				if (file.getName().length() == logFileName.length()) {
					BufferedWriter out;
					try {
						out = new BufferedWriter(new FileWriter(file));
						out.write("");
						out.close();
					} catch (IOException e) {
						String mess = String.format(
								"Impossible d'exécuter la commande '%s' : %s",
								cp_cmd, e.getMessage());
						LOGGER.severe(mess+Util.stack2string( e));
						return new VoidReponse(XmlIdentifier.ERROR, mess);
					}
				} else {
					file.delete();
				}
			}
		}
		return voidReponse;
	}

	public static Set<PortActif> getActifsCamelisPortsOnPortalis()
			throws PortalisException {
		final String cmd = "ps -ef | grep "
				+ CAMELIS_APPLICATION_NAME;
		LOGGER.info("==== Linux ==== >> getActifsCamelisPorts()");
		HashSet<PortActif> result = new HashSet<PortActif>();
		String reponse = LinuxInteractor.executeCommand(cmd, true);

		String[] lignes = reponse.split("\n");
		for (String inputLine : lignes) {
			int i = inputLine.indexOf("-port");
			if (i != -1) {
				String portNum = inputLine.substring(i + 6, i + 10);

				Matcher m = ClientConstants.PID_PATTERN.matcher(inputLine);
				m.find();
				String processNum = m.group(1);
				PortActif actif = new PortActif(
						PortalisService.getInstance().getHost(),
						new Integer(portNum), new Integer(processNum));
				LOGGER.fine(actif.toString());
				result.add(actif);
			}
		}
		return result;
	}

	private static boolean checkPid(int port, int pid) throws PortalisException {

		Set<Integer> pidList = LinuxCmd.lsof(port);

		boolean ok = false;
		for (int i : pidList) {
			if (i == pid) {
				ok = true;
				break;
			}
		}
		LOGGER.fine(String.format("checkPid(%d+Util.stack2string( %d)) result = %b, Java pid = ",
				port, pid, ok, LinuxCmd.getMyJavaPid()));
		return ok;
	}

	public static void killCamelisProcessByPid(int pid)
			throws PortalisException {
		LOGGER.info("==== Linux ==== >> killCamelisProcessByPid(" + pid + ")");
		try {
			final String cmd = "kill -9 " + pid;
			LinuxInteractor.executeCommand(cmd, true);
			LOGGER.fine("killProcess(" + pid + ")");
		} catch (Exception e) {
			String mess = e.getClass().getName()
					+ " : impossible de tuer le processus " + pid + ", "
					+ e.getMessage();
			LinuxInteractor.throwPortalisException(mess);
		}
	}

}
