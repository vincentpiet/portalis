package fr.irisa.lis.portalis.system.linux;

import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;




public class PortService {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(PortService.class.getName());

	private final static int PORT_MIN = Integer
			.parseInt(AdminProprietes.portalis.getProperty("admin.portBorneInf"));
	private final static int PORT_MAX = Integer
			.parseInt(AdminProprietes.portalis.getProperty("admin.portBorneSup"));
	private static int[] PORTS_EXCLUS;

	static {
		String[] portsExclusStrings = AdminProprietes.portalis.getProperty(
				"admin.portsExclus").split(";");
		PORTS_EXCLUS = new int[portsExclusStrings.length];
		for (int i = 0; i < portsExclusStrings.length; i++) {
			PORTS_EXCLUS[i] = Integer.parseInt(portsExclusStrings[i]);
		}
	}

	/**
	 * Checks to see if a specific port is available.
	 * 
	 * @param port
	 *            the port to check for availability
	 * @throws CamelisException
	 */
	private static boolean available(int port) {

		ServerSocket ss = null;
		DatagramSocket ds = null;
		try {
			ss = new ServerSocket(port);
			ss.setReuseAddress(true);
			ds = new DatagramSocket(port);
			ds.setReuseAddress(true);
			return true;
		} catch (IOException e) {
		} finally {
			if (ds != null) {
				ds.close();
			}

			if (ss != null) {
				try {
					ss.close();
				} catch (IOException e) {
					/* should not be thrown */
				}
			}
		}

		return false;

	}

	/**
	 * rend vrai si le numéro de port est interdit pour un serveur Camelis
	 * 
	 * @param le
	 *            numero de port
	 * @return vrai si le numéro de port est interdit pour un serveur Camelis
	 */
	private static boolean isReserved(int port) {
		boolean reserved = false;
		for (int portExclu : PORTS_EXCLUS) {
			if (port == portExclu) {
				reserved = true;
				break;
			}
		}
		return reserved;
	}

	public static boolean isAutorisedPort(int port) {
		return PORT_MIN <= port & port <= PORT_MAX & !isReserved(port);
	}

	public static Integer[] choosePorts(int max) throws PortalisException {
		ArrayList<Integer> lpliberes = new ArrayList<Integer>();

		int nb = 0;
		for (int port = PORT_MIN; port < PORT_MAX + 1; port++) {
			if (nb >= max) {
				break;
			}

			if (!isReserved(port) && available(port)) {
				nb++;
				lpliberes.add(port);
			}
		}

		Integer[] maListe = new Integer[lpliberes.size()];
		lpliberes.toArray(maListe);
		return maListe;
	}
	
	public static Integer choosePort() throws PortalisException {
		Integer[] ports = PortService.choosePorts(1);
		if (ports.length == 0) {
			LinuxInteractor
					.throwPortalisException("Impossible de trouver un port");
		}
		return ports[0];
		
	}

}
