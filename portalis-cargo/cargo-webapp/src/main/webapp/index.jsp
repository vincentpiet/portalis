<html>
<body>
<h2>Test webapp</h2>
<h3>URL, URI and Co ...</h3>
<table id="uri">
<tr><th>Nom</th><th>Valeur</th></tr>
<tr><td>ContextPath</td><td><%= request.getContextPath()%></td></tr>
<tr><td>URI</td><td><%= request.getRequestURI() %></td></tr>
<tr><td>URL</td><td><%= request.getRequestURL().toString() %></td></tr>
<tr><td>ServletPath</td><td><%= request.getServletPath() %></td></tr>
<tr><td>PathTranslated</td><td><%= request.getPathTranslated() %></td></tr>
<tr><td>PathInfo</td><td><%= request.getPathInfo() %></td></tr>
</table>
<h3>Headers</h3>
<table id="headers">
<tr><th>Nom</th><th>Valeur</th></tr>
<% 
java.util.Enumeration<String> headerNames = request.getHeaderNames() ;
String name = headerNames.nextElement();
while (name!=null) {
%>
<tr><td><%=name %></td><td><%=request.getHeader(name)%></td></tr>
<%
name = headerNames.nextElement();
}
%>
</table>
</body>
</html>
