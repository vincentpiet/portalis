package fr.irisa.lis.cargo;

@SuppressWarnings("serial")
public class CargoLauncherException extends Exception implements
		java.io.Serializable {

	public CargoLauncherException(String mess) {
		super(mess);
	}

	public CargoLauncherException() {
	}

	public CargoLauncherException(String mess, Throwable e) {
		super(mess, e);
	}
}
