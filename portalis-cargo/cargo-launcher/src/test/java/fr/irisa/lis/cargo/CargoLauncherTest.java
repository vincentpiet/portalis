package fr.irisa.lis.cargo;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Set;

import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.irisa.lis.cargo.CargoLauncher;
import fr.irisa.lis.cargo.CargoLauncherException;
import fr.irisa.lis.cargo.XML;

public class CargoLauncherTest {


	private static final int TOMCAT_PORT = 8080;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Set<Integer> pids = CargoLauncher.lsof(TOMCAT_PORT);
		if (pids.size() > 0) {
			throw new CargoLauncherException("Désolé le port " + TOMCAT_PORT
					+ " est déjà occupé, pids = " + pidsToString(pids));
		}
	}

	private static String pidsToString(Set<Integer> pids) {
		StringBuffer buff = new StringBuffer("[");
		int i = 0;
		for (Integer pid : pids) {
			buff.append(i > 0 ? ", " : "").append(pid);
			i++;
		}
		try {
			buff.append("] javaPid = ").append(CargoLauncher.getJavaPid());
		} catch (CargoLauncherException e) {
			buff.append("] javaPid error");
		}
		return buff.toString();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void TestGetPidNoPids() {
		try {
			assertTrue("un processus est actif sur le port " + TOMCAT_PORT,
					CargoLauncher.lsof(TOMCAT_PORT).size() == 0);
		} catch (Exception e) {
			fail("erreur testStart : " + e.getMessage() + "\nStack :\n"
					+ UtilCargo.stack2string(e));
		}

	}

	@Test
	public void testStart() {
		try {
			startWebappli();
			assertTrue("aucun processus actif sur le port " + TOMCAT_PORT,
					CargoLauncher.lsof(TOMCAT_PORT).size() > 0);
		} catch (Exception e) {
			fail("erreur testStart : " + e.getMessage() + "\nStack :\n"
					+ UtilCargo.stack2string(e));
		} finally {
			CargoLauncher.stop();
		}

	}

	@Test
	public void testTomcatIsActif() {
		try {
			startWebappli();
			assertTrue("Tomcat n'a pas été démarré",
					CargoLauncher.tomcatIsActif());
		} catch (Exception e) {
			fail("erreur testTomcatIsActif : " + e.getMessage() + "\nStack :\n"
					+ UtilCargo.stack2string(e));
		} finally {
			CargoLauncher.stop();
		}

	}

	private static void startWebappli() throws FileNotFoundException,
			IOException, MalformedURLException, CargoLauncherException {
		CargoLauncher.start();
		String url = UtilCargo.buildURL("index.jsp");
		Element root = null;
		try {
			root = XML.parse(new URL(url)).getRootElement();
			XML.checkRootName(root, "html");
			String xpathExpr = "body/table[@id='uri']";
			Element table = (Element) root.selectSingleNode(xpathExpr);
			assertTrue("Elément <table id='uri'> absent", table != null);
			Set<Integer> pids = CargoLauncher.lsof(TOMCAT_PORT);
			assertTrue("Tomcat n'a pas été lancé sur le port " + TOMCAT_PORT
					+ ", pids = " + pidsToString(pids), pids.size() == 1);
		} catch (DocumentException e) {
			fail("erreur : " + e.getMessage() + "\nStack :\n"
					+ UtilCargo.stack2string(e));
		} 
	}

	@Test
	public void testStop() {
		boolean stopped = false;
		try {
			startWebappli();
			CargoLauncher.stop();
			stopped = true;
			Thread.sleep(1000);
			assertTrue("un processus est actif sur le port " + TOMCAT_PORT,
					CargoLauncher.lsof(TOMCAT_PORT).size() == 0);
			String url = UtilCargo.buildURL("index.jsp");
			XML.parse(new URL(url)).getRootElement();
		} catch (DocumentException e) {
			// ok
		} catch (Exception e) {
			fail("erreur : " + e.getMessage() + "\nStack :\n"
					+ UtilCargo.stack2string(e));
		} finally {
			if (!stopped)
				CargoLauncher.stop();
		}
	}

}
