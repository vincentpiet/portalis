package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;

@SuppressWarnings("serial")
public class XAddAxiomReponse extends AddAxiomReponse {

	private LisExtent extent = new LisExtent();

	
	public XAddAxiomReponse() {
		super();
	}

	public XAddAxiomReponse(String status, String message) {
		super(status, message);
	}
	
	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

    @Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof XAddAxiomReponse))
			return false;

		final XAddAxiomReponse that = (XAddAxiomReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.extent, that.extent).isEquals();
}

    @Override
    public int hashCode() {
		return new HashCodeBuilder(17, 31)
		.appendSuper(super.hashCode()).append(extent)
		.toHashCode();
	}


	
	public LisExtent getExtent() {
		return extent;
	}

	public void setExtent(LisExtent extent) {
		this.extent = extent;
	}


}
