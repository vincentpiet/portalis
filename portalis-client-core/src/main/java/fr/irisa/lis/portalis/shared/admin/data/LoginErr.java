package fr.irisa.lis.portalis.shared.admin.data;

public enum LoginErr {
	ok, incorrectPassword, alreadylogged, unknown, unexpected;
}

