package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;

@SuppressWarnings("serial")
public class PingReponse extends LisReponse implements CamelisReponseObject {private static final Logger LOGGER = Logger.getLogger(PingReponse.class.getName());
	
	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected List<ActiveLisService> lesServices = new ArrayList<ActiveLisService>();

	public PingReponse() {
		super();
		LOGGER.fine("PingReponse() this="+this);
	}

	public PingReponse(ActiveLisService ActiveLisService){
		super();
		this.lesServices.add(ActiveLisService);
		LOGGER.fine("PingReponse() this="+this);
	}

	public PingReponse(List<ActiveLisService> lesServices2){
		super();
		this.lesServices = lesServices2;
		LOGGER.fine("PingReponse() this="+this);
	}

	public PingReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public PingReponse(String status, String message) {
		super(status, message);
	}

	public PingReponse(String mess, Throwable e) {
		super(mess, e);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PingReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PingReponse that = (PingReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.lesServices, that.lesServices)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(lesServices).
				toHashCode();
	}

	public void addActiveLisService(ActiveLisService ActiveLisService) {
		this.lesServices.add(ActiveLisService);
	}

	public List<ActiveLisService> getLesServices() {
		return lesServices;
	}

	public void setLesServices(List<ActiveLisService> lesServices) {
		this.lesServices = lesServices;
	}

	public ActiveLisService getFirstActiveLisService() {
		if (lesServices.size()>0) {
			return lesServices.get(0);
		} else {
			return null;
		}
	}


}
