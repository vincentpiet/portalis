package fr.irisa.lis.portalis.shared.admin.data;

import org.w3c.dom.Element;

public class AdminDataHtmlWriter extends AdminXMLWriter implements
AdminDataWriterInterface<Element> {

	@Override
	public Element visit(boolean bool) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(String s) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Integer[] intTable) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(RightProperty userRights) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(PortActif portActif) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Session session) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveUser loggedInUser) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveUser[] loggedInUser) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(UserCore user) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(UserCore[] users) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ServiceCore serviceCore) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveLisService lisService) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Application application) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(Site portalisSite) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(LoginBean loginBean) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(PortalisService portalisService) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(LoginResult loginResult) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveSite activeSite) {
		Element bodyElem = createRootElement("body");
		
		return bodyElem;
	}
	
	protected Element htmlRoot(Element content) {
		Element rootElem = AdminXMLWriter.createRootElement("html");
		addChild(rootElem, content);
		return rootElem;
	}


}
