package fr.irisa.lis.portalis.shared.admin.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class Site implements AdminDataObject {

	private Map<String, Application> applications = new HashMap<String, Application>();

	public Site() {
	}

	public Site(Map<String, Application> applications) {
		this.applications = applications;
	}

	public void setApplications(Map<String, Application> applications) {
		this.applications = applications;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.SiteInterface#getApplications()
	 */
	public Map<String, Application> getApplications() {
		return applications;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.SiteInterface#getLesServices()
	 */
	public Set<ServiceCore> getLesServices() {
		Set<ServiceCore> result = new HashSet<ServiceCore>();
		for (Application appli : applications.values()) {
			for (ServiceCore service : appli.getServices()) {
				result.add(service);
			}
		}
		return result;
	}

	public void addApplication(Application appli) {
		this.applications.put(appli.getId(), appli);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.SiteInterface#getApplication(java.lang.String)
	 */
	public Application getApplication(String id) {
		return this.applications.get(id);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.SiteInterface#getServiceCore(java.lang.String)
	 */
	public ServiceCoreInterface getServiceCore(String fullName) throws PortalisException {
		String appliId = ServiceCore.extractAppliName(fullName);
		String serviceName = ServiceCore.extractServiceName(fullName);
		Application appli = getApplication(appliId);
		if (appli == null) {
			throw new PortalisException("Application inconnue : " + fullName);
		}
		ServiceCoreInterface service = appli.getService(serviceName);
		if (service == null) {
			throw new PortalisException(
					"Service inconnu pour cet application : " + fullName);
		}
		return service;
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof Site))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final Site that = (Site) aThat;
		return new EqualsBuilder().
		// if deriving: appendSuper(super.equals(obj)).
				append(this.applications, that.applications).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// if deriving: appendSuper(super.hashCode()).
				append(applications).toHashCode();
	}
}
