package fr.irisa.lis.portalis.shared.camelis.reponse;



public interface CamelisReponseWriterInterface<T> {

	public T visit(ZoomReponse zoomReponse);
	public T visit(XZoomReponse zoomReponse);
	public T visit(ExtentReponse reponse);
	public T visit(PropertyTreeReponse reponse);
	public T visit(XPropertyTreeReponse reponse);
	public T visit(PingUsersReponse pingUsersReponse);
	public T visit(RightPropertyReponse rightPropReponse);
	public T visit(CamelisUserReponse setRoleReponse);
	public T visit(ImportCtxReponse importCtxReponse);
	public T visit(ImportLisReponse importLisReponse);
	public T visit(IntentReponse intentReponse);
	public T visit(XIntentReponse reponse);
	public T visit(PingReponse pingReponse);
	public T visit(StartReponse startReponse);
	public T visit(DelKeyReponse delKeyReponse);
	public T visit(ResetCreatorReponse resetCreatorReponse);
	public T visit(DelObjectsReponse delObjectReponse);
	public T visit(AddAxiomReponse addAxiomReponse);
	public T visit(XAddAxiomReponse reponse);
	public T visit(ResetCamelisReponse reponse);
	public T visit(DelFeatureReponse reponse);
	public T visit(GetValuedFeaturesReponse reponse);
	public T visit(LoadContextReponse reponse);

}
