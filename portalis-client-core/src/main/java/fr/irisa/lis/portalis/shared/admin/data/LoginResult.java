package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;



@SuppressWarnings("serial")
public class LoginResult implements AdminDataObject {
	private UserCore userCore;
	private LoginErr loginErr;
	
	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	public LoginResult() {}
	
	public LoginResult(UserCore userCore, LoginErr loginErr) {
		this.loginErr = loginErr;
		this.userCore = userCore;
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LoginResult) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LoginResult that = (LoginResult)aThat;
	        return new EqualsBuilder().
	                // if deriving: appendSuper(super.equals(obj)).
	                append(this.loginErr, that.loginErr).
	                append(this.userCore, that.userCore).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
                append(loginErr).
                append(userCore).
           toHashCode();
    }

	public UserCore getUserCore() {
		return userCore;
	}
	
	public void setUserCore(UserCore userCore) {
		this.userCore = userCore;
	}
	
	public LoginErr getLoginErr() {
		return loginErr;
	}
	
	public void setLoginErr(LoginErr loginErr) {
		this.loginErr = loginErr;
	}


}
