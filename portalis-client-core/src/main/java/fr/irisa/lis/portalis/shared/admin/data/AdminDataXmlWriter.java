package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public class AdminDataXmlWriter extends AdminXMLWriter implements
		AdminDataWriterInterface<Element> {
	
	private static final Logger LOGGER = Logger.getLogger(AdminDataXmlWriter.class.getName());

	static private AdminDataXmlWriter instance;

	public static AdminDataXmlWriter getInstance() {
		if (instance == null) {
			instance = new AdminDataXmlWriter();
		}
		return instance;
	}

	protected static Element buildRootElement(String name, VoidReponse reponse) {
		Element rootElem = createRootElement(name);
		;
		if (reponse.isOk()) {
			addAttribute(rootElem, XmlIdentifier.STATUS(), XmlIdentifier.OK);
		} else {
			List<String> messages = reponse.getMessages();
			errors(rootElem, messages);
			return rootElem;
		}
		return rootElem;
	}

	protected static Element error(Element reponseElem, String message) {
		reponseElem.setAttribute(XmlIdentifier.STATUS(), XmlIdentifier.ERROR);
		addTextChild(reponseElem, XmlIdentifier.MESSAGE(), message);
		LOGGER.warning("visitor error = " + message);
		return reponseElem;
	}

	protected static Element errors(Element reponseElem, List<String> messages) {
		reponseElem.setAttribute(XmlIdentifier.STATUS(), XmlIdentifier.ERROR);
		addAttribute(reponseElem, XmlIdentifier.STATUS(), XmlIdentifier.ERROR);
		for (String mess : messages) {
			addTextChild(reponseElem, XmlIdentifier.MESSAGE(),
					mess == null ? "null" : mess);
		}
		LOGGER.warning("visitor errors = "
				+ Arrays.toString(messages.toArray(new String[messages.size()])));
		return reponseElem;
	}

	@Override
	public Element visit(LoginBean loginBean) {
		Element elem = createRootElement(XmlIdentifier.USER());
		addAttribute(elem, XmlIdentifier.EMAIL(), loginBean.getId());
		addAttribute(elem, XmlIdentifier.PASSWORD(),
				Util.sha(loginBean.getPassword()));
		return elem;
	}

	@Override
	public Element visit(Application application) {
		Element rootElement = createRootElement(XmlIdentifier.APPLICATION());
		return setApplication(rootElement, application);
	}

	public Element setApplication(Element rootElement, Application application) {

		// set attribute to status element
		addAttribute(rootElement, XmlIdentifier.APPLICATION_ID(),
				application.getId());

		for (ServiceCore service : application.getServices()) {
			addChild(rootElement, visit(service));
		}
		return rootElement;
	}

	@Override
	public Element visit(Site site) {
		Element siteElem = createRootElement(XmlIdentifier.SITE());

		for (Application appli : site.getApplications().values()) {
			addChild(siteElem, visit(appli));
		}
		return siteElem;
	}

	@Override
	public Element visit(ActiveSite activeSite) {
		Element siteElem = createRootElement(XmlIdentifier.ACTIVE_SITE());

		for (Application appli : activeSite.getApplications().values()) {
			addChild(siteElem, visit(appli));
		}

		for (ActiveLisService service : activeSite.getActiveLisServices()) {
			addChild(siteElem, visit(service));
		}
		return siteElem;
	}

	@Override
	public Element visit(boolean bool) {
		Element elem = createRootElement(XmlIdentifier.BOOLEAN());

		elem.setTextContent(Boolean.toString(bool));
		return elem;
	}

	@Override
	public Element visit(String s) {
		String ss = s;
		if (ss == null) {
			ss = "";
		}
		Element elem = createRootElement(XmlIdentifier.STRING());

		elem.setTextContent(ss);
		return elem;
	}

	@Override
	public Element visit(Integer[] intTable) {
		Element elem = createRootElement(XmlIdentifier.INTEGER_LIST());
		for (Integer integer : intTable) {
			addTextChild(elem, XmlIdentifier.INTEGER(), integer + "");
		}
		return null;
	}

	@Override
	public Element visit(RightProperty userRights) {
		Element right = createRootElement(XmlIdentifier.ROLE());
		addAttribute(right, XmlIdentifier.EMAIL(), userRights.getUserEmail());
		addAttribute(right, XmlIdentifier.SERVICE_NAME(),
				userRights.getServiceName());
		addAttribute(right, XmlIdentifier.VALUE(), userRights.getRight()
				.toString());
		return right;
	}

	private void setUserData(UserData user, Element elem) {
		addAttribute(elem, XmlIdentifier.EMAIL(), user.getEmail());
		addAttribute(elem, XmlIdentifier.PSEUDO(), user.getPseudo());
	}

	@Override
	public Element visit(Session session) {
		Element reponseElem = createRootElement(XmlIdentifier.SESSION());
		ActiveService activeService = session.getActiveService();
		if (activeService instanceof ActiveLisService) {
			ActiveLisService service = (ActiveLisService) activeService;
			addChild(reponseElem, visit(service));
		} else {
			PortalisService service = (PortalisService) activeService;
			addChild(reponseElem, visit(service));
		}

		for (String cookie : session.getCookies()) {
			addTextChild(reponseElem, XmlIdentifier.COOKIE(), cookie);
		}

		addChild(reponseElem, visit(session.getActiveUser()));
		addAttribute(reponseElem, XmlIdentifier.ID(),
				session.getPortalisSessionId());
		return reponseElem;
	}

	@Override
	public Element visit(ActiveLisService lisService) {
		Element reponseElem = createRootElement(XmlIdentifier.CAMELIS());
		setActiveService(reponseElem, lisService);
		addAttribute(reponseElem, XmlIdentifier.ACTIVE_SERVICE_ID(),
				lisService.getActiveId());
		addAttribute(reponseElem, XmlIdentifier.HOST(),
				lisService.getHost());
		addAttribute(reponseElem, XmlIdentifier.PORT(),
				lisService.getPort()+"");
		addAttribute(reponseElem, XmlIdentifier.PID(), lisService.getPid() + "");
		addAttribute(reponseElem, XmlIdentifier.NB_OBJECT(),
				lisService.getNbObject() + "");
		addAttribute(reponseElem, XmlIdentifier.CONTEXT_LOADED(),
				lisService.isContextLoaded() + "");
		addAttribute(reponseElem, XmlIdentifier.LAST_UPDATE(),
				Util.writeDate(lisService.getLastUpdate()));
		return reponseElem;
	}

	@Override
	public Element visit(PortActif portActif) {
		Element elem = createRootElement(XmlIdentifier.PORT_ACTIF());
		addAttribute(elem, XmlIdentifier.PORT(), portActif.getPort() + "");
		addAttribute(elem, XmlIdentifier.PID(), portActif.getPid() + "");
		addAttribute(elem, XmlIdentifier.HOST(), portActif.getServer());
		return elem;
	}

	@Override
	public Element visit(ActiveUser user) {
		Element elem = createRootElement(XmlIdentifier.ACTIF_USER());
		addAttribute(elem, XmlIdentifier.ACTIF_USER_KEY(),
				user.getPortalisSessionId());
		addChild(elem, visit(user.getUserCore()));
		return elem;
	}

	@Override
	public Element visit(PortalisService portalisService) {
		Element reponseElem = createRootElement(XmlIdentifier
				.PORTALIS_SERVICE());
		addAttribute(reponseElem, XmlIdentifier.SERVICE_ID(),
				portalisService.getActiveId());
		setActiveService(reponseElem, portalisService);
		return reponseElem;
	}

	protected Element setActiveService(Element reponseElem,
			ActiveService lisService) {
		addAttribute(reponseElem, XmlIdentifier.CREATOR(),
				lisService.getCreator());
		addAttribute(reponseElem, XmlIdentifier.PORT(), lisService.getPort()
				+ "");
		addAttribute(reponseElem, XmlIdentifier.HOST(), lisService.getHost());

		addAttribute(reponseElem, XmlIdentifier.ACTIVATION_DATE(),
				Util.writeDate(lisService.getActivationDate()));
		return reponseElem;
	}

	@Override
	public Element visit(UserCore user) {
		Element elem = AdminXMLWriter.createRootElement(XmlIdentifier
				.USER_CORE());
		addAttribute(elem, XmlIdentifier.PASSWORD(), user.getPassword());
		addAttribute(elem, XmlIdentifier.IS_GENERAL_ADMIN(),
				Boolean.toString(user.isGeneralAdmin()));
		setUserData(user, elem);
		if (user.getRights().size() > 0) {
			Element rightListElems = createAddChild(elem, XmlIdentifier.ROLES());
			for (RightProperty prop : user.getRights()) {
				addChild(rightListElems, visit(prop));
			}
		}

		return elem;
	}

	@Override
	public Element visit(UserCore[] users) {
		Element elem = AdminXMLWriter.createRootElement(XmlIdentifier
				.USER_LIST());
		for (UserCore user : users) {
			addChild(elem, visit(user));
		}
		return elem;
	}

	@Override
	public Element visit(ActiveUser[] loggedInUsers) {
		Element elem = createRootElement(XmlIdentifier.ACTIF_USER_LIST());
		for (ActiveUser user : loggedInUsers) {
			addChild(elem, visit(user));
		}
		return elem;
	}

	@Override
	public Element visit(ServiceCore service) {
		Element elem = createRootElement(XmlIdentifier.SERVICE_CORE());
		addAttribute(elem, XmlIdentifier.SERVICE_ID(), service.getServiceName());
		addAttribute(elem, XmlIdentifier.APPLICATION_ID(),
				service.getAppliName());

		return elem;
	}

	@Override
	public Element visit(LoginResult loginResult) {
		Element reponseElem = createRootElement(XmlIdentifier.LOGIN_RESULT());
		addAttribute(reponseElem, XmlIdentifier.STATUS(), loginResult
				.getLoginErr().toString());
		addChild(reponseElem, visit(loginResult.getUserCore()));

		return reponseElem;
	}

}