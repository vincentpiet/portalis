package fr.irisa.lis.portalis.shared.admin.data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class UserCore extends UserData {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public static final UserCore ANONYMOUS_USER = new UserCore(ClientConstants.ANONYMOUS_EMAIL, 
			ClientConstants.ANONYMOUS_PSEUDO, ClientConstants.ANONYMOUS_PASSWORD, false);

	public static final UserCore ANONYMOUS_ADMIN = new UserCore(ClientConstants.ANONYMOUS_ADMIN_EMAIL, 
			ClientConstants.ANONYMOUS_ADMIN_PSEUDO, ClientConstants.ANONYMOUS_ADMIN_PASSWORD, true);

	private String md5Password;

	public UserCore() {
		super();
	}

	public UserCore(String email, String pseudo, String password,
			boolean isGeneralAdmin) {
		super(email, pseudo, isGeneralAdmin);
		this.md5Password = password;
	}
		
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof UserCore) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final UserCore that = (UserCore)aThat;
	        return new EqualsBuilder().
	                appendSuper(super.equals(aThat)).
	                append(this.md5Password, that.md5Password).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            	appendSuper(super.hashCode()).
                append(md5Password).
            toHashCode();
    }



	public void setPassword(String password) throws PortalisException {
		this.md5Password = password;
	}

	@NotNull
	@Size(min = 8, max = 50)
	public String getPassword() {
		return md5Password;
	}

}