package fr.irisa.lis.portalis.shared.admin.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;

public class HttpPortalisUrlConnection extends AbstractHttpUrlConnection {private static final Logger LOGGER = Logger.getLogger(HttpPortalisUrlConnection.class.getName());

	public Element doFirstConnection(Session sess, String cmd)
			throws PortalisException {
		return doFirstConnection(sess, cmd, new String[0][2]);
	}

	public Element doFirstConnection(Session sess, String cmd,
			String[][] httpReqArgs) throws PortalisException {
		String portalisCmd = PortalisService.getInstance().getAppliName()
				+ (cmd.startsWith("/") ? cmd : "/" + cmd);
		URL url = buildUrl(PortalisService.getInstance().getHost(), PortalisService.getInstance().getPort(), portalisCmd,
				httpReqArgs);
		return connect(sess, url, true);
	}

	public Element doConnection(Session sess, String cmd)
			throws PortalisException {
		return doConnection(sess, cmd, new String[0][2]);
	}

	public Element doConnection(Session sess, String cmd, String[][] httpReqArgs)
			throws PortalisException {
		String portalisCmd = PortalisService.getInstance().getAppliName()
				+ (cmd.startsWith("/") ? cmd : "/" + cmd);
		URL url = buildUrl(PortalisService.getInstance().getHost(), PortalisService.getInstance().getPort(), portalisCmd,
				httpReqArgs);
		Element result = connect(sess, url, false);
		return result;
	}

	/**
	 * Returns the output from the given URL.
	 * 
	 * I tried to hide some of the ugliness of the exception-handling in this
	 * method, and just return a high level Exception from here.
	 * 
	 * @param desiredUrl
	 * @return
	 * @throws PortalisException
	 */
	public static Element connect(Session sess, URL url, boolean first)
			throws PortalisException {
		return connect(sess, url, first, "ISO-8859-1");
	}

	public static Element connect(Session sess, URL url, boolean first,
			String charset) throws PortalisException {
		if (ClientConstants.isPrintHttpRequest()) {
			System.out.println("\n"+url+"\n");
		}
		LOGGER.info("\nPortalisCtx @portalisConnect "+url.toString()+" "+Boolean.toString(first));
		BufferedReader reader = null;
		StringBuilder stringBuilder;

		try {
			// create the HttpURLConnection
			HttpURLConnection connection = (HttpURLConnection) url
					.openConnection();
			// just want to do an HTTP GET here
			connection.setRequestMethod("GET");

			// uncomment this if you want to write output to this url
			// connection.setDoOutput(true);

			// give it 15 seconds to respond
			connection.setReadTimeout(15 * 1000);

			if (!first) {
				// Use the same cookies as the ones found in the first one.
				List<String> cookies = sess.getCookies();
				if (cookies.size()<1) {
					String mess = "this session is not a recorded Portalis session "+sess.getPortalisSessionId();
					LOGGER.severe(mess);
					throw new PortalisException(mess);
				}
				for (String cookie : cookies) {
					LOGGER.fine(String.format("portalisConnect(%b) setting Cookie : %s",
							first, cookie));
					connection.addRequestProperty("Cookie",
							cookie.split(";", 2)[0]);
				}
			}

			connection.connect();
			
			int responseCode = connection.getResponseCode();
			if (responseCode!=200) {
				String mess = String.format("portalisConnect failure during http request, error code is : %d, error message is : %s, url is : \n%s",
						responseCode,
						connection.getResponseMessage(),
						url.toString());
				LOGGER.severe(mess);
				throw new PortalisException(mess);
			}

			if (first) {
				// Gather all cookies on the first request.
				Map<String, List<String>> headers = connection.getHeaderFields();
				List<String> cookies = headers.get(
						"Set-Cookie");
				if (cookies != null) {
					sess.setCookies(cookies);
					for (String cookie : cookies) {
						LOGGER.fine(String.format(
								"portalisConnect(%b) saving Cookie : %s", first,
								cookie));
					}
				} else {
					String errMess = String.format("Internal error : no 'Set-Cookie' cookies returned on a login request, cookies are :\n%s",
							cookiesToString(headers));
					LOGGER.severe(errMess);
					throw new PortalisException(errMess);
				}
			}

			// read the output from the server
			reader = new BufferedReader(new InputStreamReader(
					connection.getInputStream(), charset));
			stringBuilder = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null) {
				stringBuilder.append(line + "\n");
			}

			String result = stringBuilder.toString();
			
			Element resultElem = XmlUtil.parseXMLString(result);
			String ok = resultElem.getAttribute(XmlIdentifier.STATUS());
			LOGGER.info("\nPortalisCtx @fin portalisConnect "+resultElem.getNodeName()+
					(ok.equals(XmlIdentifier.ERROR) ? resultElem.getAttribute(XmlIdentifier.MESSAGE()) : "") );
			return resultElem;
		} catch (Exception e) {
			String errMess = String
					.format("connection failure during http request %s\n%s",
							url.toString(), e.getMessage());
			LOGGER.severe(errMess+Util.stack2string( e));
			throw new PortalisException(errMess);
		} finally {
			// close the reader; this can throw an exception too, so
			// wrap it in another try/catch block.
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException ioe) {
					ioe.printStackTrace();
				}
			}
		}
	}

	private static String cookiesToString(Map<String, List<String>> headers) {
		StringBuffer buff = new StringBuffer();
		for (String key : headers.keySet()) {
			List<String> cookies = headers.get(key);
			buff.append("\n   . ").append(key).append(" : ").
			append(cookies == null ? "null" :
				Arrays.toString(cookies.toArray(new String[cookies.size()])));
		}
		return buff.append("\n").toString();
	}

}
