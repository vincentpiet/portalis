package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class ActiveSite extends Site {

	private List<ActiveLisService> activeServices = new ArrayList<ActiveLisService>();

	public ActiveSite() {
		super();
	}

	public ActiveSite(Map<String, Application> applications) {
		super(applications);
	}

	public ActiveSite(Map<String, Application> applications, List<ActiveLisService> activeServices) {
		super(applications);
		this.activeServices = activeServices;
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ActiveSite))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ActiveSite that = (ActiveSite) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.activeServices, that.activeServices).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				. // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(activeServices)
				.toHashCode();
	}

	public List<ActiveLisService> getActiveLisServices() {
		return activeServices;
	}

	public void setActivelisServices(List<ActiveLisService> activeServices) {
		this.activeServices = activeServices;
	}
	
	public void addActiveLisService(ActiveLisService activeService) {
		this.activeServices.add(activeService);
	}
	
	public ActiveLisService getActiveLisService(String id) {
		ActiveLisService result = null;
		for (ActiveLisService activeService : this.activeServices) {
			if (activeService.getActiveId().equals(id)) {
				return activeService;
			}
		}
		return result;
	}

}
