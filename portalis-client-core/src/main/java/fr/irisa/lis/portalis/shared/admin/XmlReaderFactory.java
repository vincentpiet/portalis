package fr.irisa.lis.portalis.shared.admin;

import fr.irisa.lis.portalis.shared.admin.data.AdminDataReaderInterface;
import fr.irisa.lis.portalis.shared.admin.data.AdminReaderInterface;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseReaderInterface;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataReaderInterface;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseReaderInterface;



public interface XmlReaderFactory<T> {
	public AdminReaderInterface<T> getXmlReader();
	public AdminDataReaderInterface<T> getPortalisDataXmlReader();
	public AdminReponseReaderInterface<T> getAdminReponseXmlReader();
	public CamelisDataReaderInterface<T> getCamelisDataXmlReader();
	public CamelisReponseReaderInterface<T> getCamelisReponseXmlReader();
}
