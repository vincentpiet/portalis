package fr.irisa.lis.portalis.shared.camelis.http;

import java.net.URL;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.http.AbstractHttpUrlConnection;

/**
 * 
 * A complete Java class that shows how to open a URL, then read data (text)
 * from that URL, HttpURLConnection class (in combination with an
 * InputStreamReader and BufferedReader).
 * 
 * @author alvin alexander, devdaily.com.
 * 
 */
public class HttpStatelessUrlConnection extends AbstractHttpUrlConnection {private static final Logger LOGGER = Logger.getLogger(HttpStatelessUrlConnection.class.getName());

	public HttpStatelessUrlConnection() {
	}

	public Element doConnection(Session sess, String command)
			throws PortalisException {
		return doConnection(sess, command, new String[0][2]);
	}

	public Element doConnection(Session session, String command,
			String[][] httpReqArgs) throws PortalisException {
		if (session == null) {
			String errorMess = String.format("Erreur interne : la session est nulle, http commande = %s", command);
			LOGGER.severe(errorMess);
			throw new PortalisException(errorMess);
		} else {
			return doConnection(session.getHost(), session.getPort(), command, httpReqArgs);
		}
	}

	public Element doConnection(String hostName, int port, String command) throws PortalisException {
		URL url = buildUrl(hostName, port, command, new String[0][2]);
		return parseXMLFromUri(url.toString());
	}

	public Element doConnection(String hostName, int port, String command,
			String[][] httpReqArgs) throws PortalisException {
		URL url = buildUrl(hostName, port, command, httpReqArgs);
		return parseXMLFromUri(url.toString());
	}

	/** 
	 * Attention : cette lecture est faite sans session client.
	 * Toutes les lectures sont indépendantes les unes des autres
	 * @param uri
	 * @return
	 * @throws PortalisException
	 */
	public static Element parseXMLFromUri(String uri) throws PortalisException {
		if (ClientConstants.isPrintHttpRequest()) {
			System.out.println("\n"+uri+"\n");
		}

		LOGGER.info("\nPortalisCtx   @parseXMLFromUri : "+uri);
		Document doc = null;
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(uri);
		} catch (Exception e) {
			StringBuffer errMess = new StringBuffer("Error during http request").append(Util.stack2string( e));
			Throwable ee = e.getCause();
			while (ee!=null) {
				errMess.append(Util.stack2string(ee));
				ee = ee.getCause();
			}
			
			LOGGER.warning(errMess.toString());
			throw new PortalisException(errMess.toString());
		}
		Element elemResult = doc.getDocumentElement();
		String ok = elemResult.getAttribute(XmlIdentifier.STATUS());
		LOGGER.info("\nPortalisCtx   @fin parseXMLFromUri "+elemResult.getNodeName()+
				(ok.equals(XmlIdentifier.ERROR) ? elemResult.getAttribute(XmlIdentifier.MESSAGE()) : "") );
		return elemResult;
	}

}