package fr.irisa.lis.portalis.shared.admin;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.MessageDigest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class Util {

	private static final String SHA_CODE = "sha-256";private static final Logger LOGGER = Logger.getLogger(Util.class.getName());
	
	static final String simpleFormat = "dd MMM yyyy 'at' HH:mm:ss";
	static final SimpleDateFormat prettyFormatter = new SimpleDateFormat(simpleFormat);


	private static String PARSE_NO_MILLIS = AdminProprietes.portalis
			.getProperty("admin.parseNoMillis");
	private static String NULL_DATE = AdminProprietes.ioConstants
			.getProperty("NULL_DATE");

	private static String fmtNoMillis = "yyyy-MM-dd'T'HH:mm:ssZ";
	private static DateTimeFormatter formaterNoMillis = DateTimeFormat
			.forPattern(fmtNoMillis);

	private static boolean IS_PARSE_NO_MILLIS = (PARSE_NO_MILLIS == null
			|| PARSE_NO_MILLIS.equals("false") ? false : true);

	public static Date parseNoMillis(String text) {
		return formaterNoMillis.parseDateTime(text).toDate();
	}

	static public String writeDate(Date date) {
		if (date == null)
			return XmlIdentifier.NONE();
		else
			return format(date);
	}

	static public String prettyDate(Date date) {
		if (date == null)
			return XmlIdentifier.NONE();
		else
			return prettyFormatter.format(date);
	}

	static public Date readDate(String s) throws PortalisException {
		if (s == null) {
			throw new PortalisException("impossible de convertir la date");
		} else if (s.equals(XmlIdentifier.NONE())) {
			return null;
		} else {
			return Util.parse(s);
		}
	}
	
	/**
	 * String representation of an Exception's stack
	 * 
	 * @param e
	 *            the Exception
	 * @return the string representation of the exception stack
	 */
	public static String stack2string(Throwable e) {
		try {
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			return "\n------ begin stack -----\r\n" + sw.toString() + "\n------ end stack -----\r\n";
		} catch (Exception e2) {
			return "Bad stack2string";
		}
	}


	/**
	 * ISO8601/RFC333true9 date formatter
	 * 
	 * @param d
	 *            the date to be formatted
	 * @return the formatted string representing the date
	 */
	private static String format(Date d) {
		DateTime dt = new DateTime(d);
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		return fmt.print(dt);
	}

	/**
	 * ISO8601/RFC3339 date parser
	 * 
	 * @param text
	 *            the text to parse
	 * @return the date represented by the string
	 */
	private static Date parse(String text) {
		if (text.equals(NULL_DATE))
			return null;
		DateTimeFormatter fmt = ISODateTimeFormat.dateTime();
		Date result = null;
		try {
			result = fmt.parseDateTime(text).toDate();
		} catch (IllegalArgumentException e) {
			if (IS_PARSE_NO_MILLIS) {
				LOGGER.fine("parseNoMillis(" + text + ")");
				result = parseNoMillis(text);
			} else {
				throw e;
			}

		}
		return result;
	}

	public static int evaluateExpr(String s) throws PortalisException {
		try {
			DocumentBuilderFactory docFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			// root elements
			Document doc = docBuilder.newDocument();
			Element root = doc.createElement("company");
			doc.appendChild(root);
			XPath xpath = XPathFactory.newInstance().newXPath();
			// XPath Query for showing all nodes value
			XPathExpression expr = xpath.compile(s);
			Double d = (Double) expr.evaluate(root, XPathConstants.NUMBER);
			return d.intValue();
		} catch (Exception e) {
			String message = "Impossible d'évaluer l'expression : " + s;
				LOGGER.severe(message);
			throw new PortalisException(message);
		}
	}

	private static String convertToHex(byte[] data) {
		StringBuffer buf = new StringBuffer();
		for (int i = 0; i < data.length; i++) {
			int halfbyte = (data[i] >>> 4) & 0x0F;
			int two_halfs = 0;
			do {
				if ((0 <= halfbyte) && (halfbyte <= 9))
					buf.append((char) ('0' + halfbyte));
				else
					buf.append((char) ('a' + (halfbyte - 10)));
				halfbyte = data[i] & 0x0F;
			} while (two_halfs++ < 1);
		}
		return buf.toString();
	}

	public static String sha(String text) {
		MessageDigest md = null;
		byte[] sha1hash = null;
		try {
			md = MessageDigest.getInstance(SHA_CODE);
			sha1hash = new byte[40];
			md.update(text.getBytes("iso-8859-1"), 0, text.length());
		} catch (Exception e) {
			return "Erreur de codage : " + text;
		}
		sha1hash = md.digest();
		return convertToHex(sha1hash);
	}

}
