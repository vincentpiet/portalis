package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;

public class AdminDataXmlReader extends AdminXmlReader implements
		AdminDataReaderInterface<Element> {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(AdminDataXmlReader.class.getName());

	static private AdminDataXmlReader instance;

	public static AdminDataXmlReader getInstance() {
		if (instance == null) {
			instance = new AdminDataXmlReader();
		}
		return instance;
	}

	public AdminDataXmlReader() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getPortalisSite(org.dom4j.Element)
	 */
	@Override
	public Site getSite(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.SITE());
		Site site = new Site();
		List<Element> appliElems = getElements(root,
				XmlIdentifier.APPLICATION());
		for (Element applicationElem : appliElems) {
			site.addApplication(getApplication(applicationElem));
		}
		return site;
	}

	@Override
	public ActiveSite getActiveSite(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.ACTIVE_SITE());
		ActiveSite site = new ActiveSite();
		List<Element> appliElems = getElements(root,
				XmlIdentifier.APPLICATION());
		for (Element applicationElem : appliElems) {
			site.addApplication(getApplication(applicationElem));
		}

		List<Element> serviceElems = getElements(root, XmlIdentifier.CAMELIS());
		for (Element serviceElem : serviceElems) {
			site.addActiveLisService(getActiveLisService(serviceElem));
		}
		return site;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getApplication(org.dom4j.Element)
	 */
	@Override
	public Application getApplication(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.APPLICATION());
		Application result = new Application();
		return setApplication(root, result);
	}

	private Application setApplication(Element root, Application result)
			throws PortalisException, RequestException {
		result.setId(getStringAttribute(root, XmlIdentifier.APPLICATION_ID()));
		List<ServiceCore> services = new ArrayList<ServiceCore>();
		List<Element> serviceElems = getElements(root,
				XmlIdentifier.SERVICE_CORE());
		for (Element serviceElem : serviceElems) {
			services.add(getServiceCore(serviceElem));
		}
		result.setServices(services);
		return result;
	}

	@Override
	public ActiveLisService getActiveLisService(Element root)
			throws PortalisException {
		ActiveLisService result = new ActiveLisService();
		result.setActivationDate(getDateAttribute(root,
				XmlIdentifier.ACTIVATION_DATE()));
		result.setCreator(getStringAttribute(root, XmlIdentifier.CREATOR()));
		result.setId(getStringAttribute(root, XmlIdentifier.SERVICE_ID()));
		String host = root.getAttribute(XmlIdentifier.HOST());
		if (host != null && host.length() > 0) {
			result.setHost(host);
			result.setPort(getIntAttribute(root, XmlIdentifier.PORT()));
		}
		result.setPid(getIntAttribute(root, XmlIdentifier.PID()));
		result.setLastUpdate(getLastUdate(root));
		result.setNbObject(getIntAttribute(root, XmlIdentifier.NB_OBJECT()));
		result.setContextLoaded(getBooleanAttribute(root,
				XmlIdentifier.CONTEXT_LOADED()));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getPortActif(org.dom4j.Element)
	 */
	@Override
	public PortActif getPortActif(Element root) throws RequestException,
			PortalisException {
		checkElement(root, XmlIdentifier.PORT_ACTIF());
		int port = getIntAttribute(root, XmlIdentifier.PORT());
		int pid = getIntAttribute(root, XmlIdentifier.PID());
		String server = getStringAttribute(root, XmlIdentifier.HOST());
		return new PortActif(server, port, pid);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getServiceCore(org.dom4j.Element)
	 */
	@Override
	public ServiceCore getServiceCore(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.SERVICE_CORE());
		ServiceCore reponse;
		String serviceId = getStringAttribute(root, XmlIdentifier.SERVICE_ID());
		String appliId = getStringAttribute(root,
				XmlIdentifier.APPLICATION_ID());
		reponse = new ServiceCore(appliId, serviceId);
		return reponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getLastUdate(org.dom4j.Element)
	 */
	@Override
	public Date getLastUdate(Element root) throws PortalisException {
		return getDateAttribute(root, XmlIdentifier.LAST_UPDATE());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#getSession
	 * (org.dom4j.Element)
	 */
	@Override
	public Session getSession(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.SESSION());
		Session session = new Session();

		// TODO la lecture d'une session doit se faire différemment en
		// fonction du type de session active
		NodeList list = root.getElementsByTagName(XmlIdentifier
				.PORTALIS_SERVICE());

		ActiveService activeService = null;
		if (list.getLength() > 0) {
			activeService = PortalisService.getInstance();
		} else {
			activeService = getActiveLisService(getElement(root,
					XmlIdentifier.CAMELIS()));
		}

		session.setActiveService(activeService);
		session.setActiveUser(getActiveUser(getElement(root,
				XmlIdentifier.ACTIF_USER())));

		NodeList cookiesElems = root.getElementsByTagName(XmlIdentifier
				.COOKIE());
		List<String> cookies = new ArrayList<String>();
		for (int i = 0; i < cookiesElems.getLength(); i++) {
			Element cookieElem = (Element) cookiesElems.item(i);
			cookies.add(cookieElem.getTextContent());
		}
		session.setCookies(cookies);

		return session;
	}

	private ActiveUser getActiveUser(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.ACTIF_USER());
		ActiveUser activeUser = new ActiveUser();
		activeUser.setUserCore(getUserCore(getElement(root,
				XmlIdentifier.USER_CORE())));
		activeUser.setPortalisSessionId(getStringAttribute(root,
				XmlIdentifier.ACTIF_USER_KEY()));
		return activeUser;
	}

	private Set<RightProperty> getRightProperties(Element root, String userEmail)
			throws PortalisException, RequestException {
		checkElement(root, XmlIdentifier.ROLES());
		Set<RightProperty> setOfRoles = new HashSet<RightProperty>();
		List<Element> roles = getElements(root, XmlIdentifier.ROLE());
		for (Element roleElem : roles) {
			setOfRoles.add(getRightPropertyForUser(roleElem, userEmail));
		}
		return setOfRoles;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getRightPropertyForUser(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public RightProperty getRightPropertyForUser(Element root, String userEmail)
			throws PortalisException, RequestException {
		checkElement(root, XmlIdentifier.ROLE());
		RightProperty prop = new RightProperty();
		prop.setUserEmail(userEmail);
		prop.setServiceName(getStringAttribute(root,
				XmlIdentifier.SERVICE_NAME()));
		prop.setRight(getRightValueAttribute(root, XmlIdentifier.VALUE()));
		return prop;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PoratlisDataDom4jReader#
	 * getRightProperty(org.dom4j.Element)
	 */
	@Override
	public RightProperty getRightProperty(Element root)
			throws PortalisException, RequestException {
		checkElement(root, XmlIdentifier.ROLE());
		String userEmail = getStringAttribute(root, XmlIdentifier.EMAIL());
		return getRightPropertyForUser(root, userEmail);
	}

	private void setUserData(UserData user, Element root)
			throws PortalisException, RequestException {
		user.setEmail(getStringAttribute(root, XmlIdentifier.EMAIL()));
		user.setPseudo(getStringAttribute(root, XmlIdentifier.PSEUDO()));
		user.setIsGeneralAdmin(getBooleanAttribute(root,
				XmlIdentifier.IS_GENERAL_ADMIN()));
		Element rolesElem = getElement(root, XmlIdentifier.ROLES(), true);
		if (rolesElem != null)
			user.setRights(getRightProperties(rolesElem, user.getEmail()));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * fr.irisa.lis.portalis.client.dom4j.reader.PortalisDataDom4jReader#getUserCore
	 * (org.dom4j.Element)
	 */
	@Override
	public UserCore getUserCore(Element root) throws PortalisException,
			RequestException {
		checkElement(root, XmlIdentifier.USER_CORE());
		UserCore user = new UserCore();
		setUserData(user, root);

		String password = getStringAttribute(root, XmlIdentifier.PASSWORD());
		user.setPassword(password);

		return user;
	}

}