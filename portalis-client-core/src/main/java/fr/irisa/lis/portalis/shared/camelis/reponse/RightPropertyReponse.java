package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.Date;
import java.util.List;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.RightProperty;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;

@SuppressWarnings("serial")
public class RightPropertyReponse extends LisReponse implements CamelisReponseObject   {

	private RightProperty rightProperty;
	private Date expire;

	public RightPropertyReponse() {
		super();
	}

	public RightPropertyReponse(String userKey, String serviceKey, RightValue right, Date expire) {
		super();
		rightProperty = new RightProperty(userKey, serviceKey, right);
		this.expire = expire;
	}

	public RightPropertyReponse(String status, List<String> messages) {
		super(status, messages);
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public Date getExpire() {
		return expire;
	}

	public void setExpire(Date expire) {
		this.expire = expire;
	}

	public RightProperty getRightProperty() {
		return rightProperty;
	}

	public void setRightProperty(RightProperty rightProperty) {
		this.rightProperty = rightProperty;
	}


}
