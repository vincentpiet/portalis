package fr.irisa.lis.portalis.shared.camelis.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class LisObject extends CamelisDataObject {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private int oid;
	private String ref;

	public LisObject() {}

	public LisObject(int oid, String ref) {
		this.oid = oid;
		this.ref = ref;
	}

	public int getOid() {
		return oid;
	}

	public String getRef() {
		return ref;
	}

	public void setRef(String ref) {
		this.ref = ref;
	}

	public void setOid(int oid) {
		this.oid = oid;
	}


	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisObject) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LisObject that = (LisObject)aThat;
			return new EqualsBuilder()
			//.appendSuper(super.equals(aThat))
			.append(this.ref, that.ref)
			.append(this.oid, that.oid)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(ref).
				append(oid).
				toHashCode();
	}

}
