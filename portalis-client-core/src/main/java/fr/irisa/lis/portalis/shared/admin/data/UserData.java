package fr.irisa.lis.portalis.shared.admin.data;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.RightValue;


@SuppressWarnings("serial")
public abstract class UserData implements AdminDataObject {


	protected String email;
	protected String pseudo;
	protected boolean isGeneralAdmin;
	protected Set<RightProperty> rights = new HashSet<RightProperty>();

	protected UserData() {
		super();
	}

	protected UserData(String email, String pseudo,
			boolean isGeneralAdmin) {
		super();
		this.email = email;
		this.pseudo = pseudo;
		this.isGeneralAdmin = isGeneralAdmin;
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof UserData) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final UserData that = (UserData)aThat;
	        return new EqualsBuilder().
	                // appendSuper(super.equals(aThat)).
	                append(this.email, that.email).
	                append(this.pseudo, that.pseudo).
	                append(this.isGeneralAdmin, that.isGeneralAdmin).
	                append(this.rights, that.rights).
	                isEquals();
		}


	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            	// appendSuper(super.hashCode()).
                append(email).
                append(pseudo).
                append(isGeneralAdmin).
                append(rights).
            toHashCode();
    }
	@NotNull
	@Size(min = 2, max = 50)
	public String getPseudo() {
		return this.pseudo;
	}

	@NotNull
	@Size(min = 3)
	@Pattern(regexp = "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$", message = "Ceci n'est pas une adresse email")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPseudo(String pseudo) {
		this.pseudo = pseudo;
	}

	public boolean isGeneralAdmin() {
		return isGeneralAdmin;
	}

	public void setIsGeneralAdmin(boolean isGeneralAdmin) {
		this.isGeneralAdmin = isGeneralAdmin;
		if (isGeneralAdmin) {
			rights.clear();
		}
	}

	public void addRole(String serviceFullName, RightValue value) {
		rights.add(new RightProperty(email, serviceFullName, value));
	}

	public RightValue getRight(String fullName) {
		if (isGeneralAdmin) {
			return RightValue.GENERALADMIN;
		}
		RightValue role = null;
		 
		for (RightProperty roleProp : rights) {
			if (roleProp.getServiceName().equals(fullName)) {
				role = roleProp.getRight();
			}
		}

		return role == null ? RightValue.NONE : role;
	}

	public void setRights(Set<RightProperty> rights) {
		if (!isGeneralAdmin())
			this.rights = rights;
	}
	
	public Set<RightProperty> getRights() {
		return this.rights;
	}
	
}