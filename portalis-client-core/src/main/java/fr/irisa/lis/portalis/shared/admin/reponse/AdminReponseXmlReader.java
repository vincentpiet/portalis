package fr.irisa.lis.portalis.shared.admin.reponse;

import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlReader;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.Session;

public class AdminReponseXmlReader extends AdminDataXmlReader implements AdminReponseReaderInterface<Element> {private static final Logger LOGGER = Logger.getLogger(AdminReponseXmlReader.class.getName());
	
	static private AdminReponseXmlReader instance;
	
	public static AdminReponseXmlReader getInstance() {
		if (instance == null) {
			instance = new AdminReponseXmlReader();
		}
		return instance;
	}
	

	private AdminReponseXmlReader() {
		super();
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.admin.dom4j.reader.AdminReponseXmlReaderInterface#getSiteReponse(org.dom4j.Element)
	 */
	@Override
	public ActiveSiteReponse getActiveSiteReponse(Element root)
			throws PortalisException, RequestException {
		LOGGER.fine("getSiteReponse(" + root.getNodeName() + ")");
		ActiveSiteReponse reponse = new ActiveSiteReponse();
		if (!checkReponseElement(root, XmlIdentifier.ACTIVE_SITE_REPONSE(), reponse)) {
			return reponse;
		} else {
			Element siteElem = getElement(root, XmlIdentifier.ACTIVE_SITE());
			reponse.setSite(getActiveSite(siteElem));
		}
		return reponse;
	}


	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.admin.dom4j.reader.AdminReponseXmlReaderInterface#getSiteReponse(org.dom4j.Element)
	 */
	@Override
	public SiteReponse getSiteReponse(Element root)
			throws PortalisException, RequestException {
		LOGGER.fine("getSiteReponse(" + root.getNodeName() + ")");
		SiteReponse reponse = new SiteReponse();
		if (!checkReponseElement(root, XmlIdentifier.SITE_REPONSE(), reponse)) {
			return reponse;
		} else {
			Element siteElem = getElement(root, XmlIdentifier.SITE());
			reponse.setSite(getSite(siteElem));
		}
		return reponse;
	}


	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.admin.dom4j.reader.AdminReponseXmlReaderInterface#getPortsActifsReponse(org.dom4j.Element)
	 */
	@Override
	public PortsActifsReponse getPortsActifsReponse(Element root)
			throws PortalisException, RequestException {
		LOGGER.fine("getActivesPort(" + root.getNodeName() + ")");
		PortsActifsReponse reponse = new PortsActifsReponse();
		if (!checkReponseElement(root, XmlIdentifier.PORT_ACTIF_REPONSE(), reponse)) {
			return reponse;
		} else {
			HashSet<PortActif> portsActifs = new HashSet<PortActif>();
			List<Element> portActifElems = getElements(root,XmlIdentifier
					.PORT_ACTIF());
			for (Element elem : portActifElems) {
				portsActifs.add(getPortActif(elem));
			}
			reponse.setListPortsActifs(portsActifs);
			return reponse;
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.admin.dom4j.reader.AdminReponseXmlReaderInterface#getPidReponse(org.dom4j.Element)
	 */
	@Override
	public PidReponse getPidReponse(Element root)
			throws PortalisException, RequestException {
		LOGGER.fine("PidReponse(" + root.getNodeName() + ")");
		PidReponse reponse = new PidReponse();
		if (!checkReponseElement(root, XmlIdentifier.PID_REPONSE(), reponse)) {
			return reponse;
		} else {
			List<String> pidList = getStringElements(root, XmlIdentifier.PID());
			for (String pid : pidList) {
				int i = 0;
				try {
					i = Integer.parseInt(pid);
				} catch (NumberFormatException e) {
					String mess = "Contenu d'élement <" + XmlIdentifier.PID()
							+ "> non numérique = " + pid;
					LOGGER.severe(mess);
					throw new PortalisException(mess);
				}
				reponse.addPid(i);
			}
			reponse.setJavaPid(getIntAttribute(root, XmlIdentifier.JAVA_PID()));
			return reponse;
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.admin.dom4j.reader.AdminReponseXmlReaderInterface#getLoginReponse(org.dom4j.Element)
	 */
	@Override
	public LoginReponse getLoginReponse(Element root) 
			throws RequestException, PortalisException {
		LOGGER.fine("getLoginReponse\n" + XmlUtil.prettyXmlString(root) + ")");
		LoginReponse reponse = new LoginReponse();
		if (!checkReponseElement(root, XmlIdentifier.LOGIN_REPONSE(), reponse)) {
			return reponse;
		} else {
			Session session = getSession(getElement(root, XmlIdentifier.SESSION()));
			reponse.setSession(session);
			return reponse;
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.admin.dom4j.reader.AdminReponseXmlReaderInterface#getVersionReponse(org.dom4j.Element)
	 */
	@Override
	public VersionReponse getVersionReponse(Element root) 
			throws RequestException, PortalisException {
		VersionReponse reponse = new VersionReponse();
		if (!checkReponseElement(root, XmlIdentifier.VERSION_REPONSE(), reponse)) {
			return reponse;
		} else {
			reponse.setVersion(getStringAttribute(root, XmlIdentifier.VERSION()));
			return reponse;
		}
	}
	
	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PortalisReponseXmlReaderInterface#getVoidReponse(org.dom4j.Element)
	 */
	@Override
	public VoidReponse getVoidReponse(Element root)
			throws PortalisException, RequestException {
		LOGGER.fine("getVoidReponse(" + root.getNodeName() + ")");
		VoidReponse reponse = new VoidReponse();
		checkReponseElement(root, XmlIdentifier.VOID_REPONSE(), reponse);
		return reponse;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.PortalisReponseXmlReaderInterface#getLogReponse(org.dom4j.Element)
	 */
	@Override
	public LogReponse getLogReponse(Element root)
			throws PortalisException, RequestException {
		LOGGER.fine("getVoidReponse(" + root.getNodeName() + ")");
		LogReponse reponse = new LogReponse();
		checkReponseElement(root, XmlIdentifier.LOG_REPONSE(), reponse);
		List<Element> linesElems = getElements(root, XmlIdentifier.LINE());
		for (Element line : linesElems) {
			reponse.addLine(getTextValue(line));
		}
		return reponse;
	}


}
