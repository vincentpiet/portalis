package fr.irisa.lis.portalis.shared.camelis.reponse;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;

@SuppressWarnings("serial")
public class XPropertyTreeReponse extends PropertyTreeReponse {

	private LisExtent extent;
	
	public XPropertyTreeReponse() {
		super();
	}

	public void setExtent(LisExtent extent) {
		this.extent = extent;
	}

	public LisExtent getExtent() {
		return extent;
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

}
