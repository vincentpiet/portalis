package fr.irisa.lis.portalis.shared.admin;

@SuppressWarnings("serial")
public class PortalisError extends Error {

	public PortalisError(String mess) {
		super(mess);
	}
}
