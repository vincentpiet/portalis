package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.List;

import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;

@SuppressWarnings("serial")
public class SetRoleReponse extends CamelisUserReponse {

	private String elemName = CamelisXmlName.SET_ROLE_REPONSE;


	public SetRoleReponse() {
		super();
	}

	public SetRoleReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public SetRoleReponse(String status, String message) {
		super(status, message);
	}

	public SetRoleReponse(CamelisUser user2) {
		setUser(user2);
	}

	@Override
	public String getElemName() {
		return elemName ;
	}
}
