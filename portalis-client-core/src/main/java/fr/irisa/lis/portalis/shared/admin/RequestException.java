package fr.irisa.lis.portalis.shared.admin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@SuppressWarnings("serial")
public class RequestException extends Exception implements
		java.io.Serializable {
	
	private List<String> messages = new ArrayList<String>();

	public RequestException(String mess) {
		super(mess);
	}

	public RequestException(List<String> messages) {
		super("Erreurs détéctées");
		this.messages = messages;
	}

	public RequestException() {
	}

	public RequestException(String mess, Throwable e) {
		super(mess, e);
	}

	public RequestException(Exception e) {
		super(e);
	}

	public List<String> getMessages() {
		return messages;
	}

	public void setMessages(List<String> messages) {
		this.messages = messages;
	}
	
	@Override
	public String getMessage() {
		String[] tab = new String[messages.size()];
		return super.getMessage()+" : "+Arrays.toString(messages.toArray(tab));
	}
}
