package fr.irisa.lis.portalis.shared.camelis;

@SuppressWarnings("serial")
public class CamelisException extends Exception implements
		java.io.Serializable {

	public CamelisException(String mess) {
		super(mess);
	}

	public CamelisException() {
	}

	public CamelisException(String mess, Throwable e) {
		super(mess, e);
	}
}
