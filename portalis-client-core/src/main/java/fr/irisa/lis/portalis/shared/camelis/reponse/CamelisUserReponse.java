package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;

@SuppressWarnings("serial")
public abstract class CamelisUserReponse extends LisReponse implements Serializable, CamelisReponseObject {
	

	private CamelisUser user;

	public CamelisUserReponse() {
		super();
	}

	public CamelisUserReponse(String creator,
			Date dateCreation, Date lastUpdate, int port, int pid, int nbObject){
		super();
		setStatus(XmlIdentifier.OK);
	}

	public CamelisUserReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public CamelisUserReponse(String status, String message) {
		super(status, message);
	}

	public CamelisUserReponse(CamelisUser user2) {
		this.user = user2;
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof CamelisUserReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final CamelisUserReponse that = (CamelisUserReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.user, that.user)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(user).
				toHashCode();
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	public CamelisUser getUser() {
		return user;
	}

	public void setUser(CamelisUser user) {
		this.user = user;
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public abstract String getElemName();
}
