package fr.irisa.lis.portalis.shared.camelis.data;


public interface CamelisDataWriterInterface<T>  {

	public T visit(Property property);
	public T visit(LisIntent intent);
	public T visit(LisIncrement incr);
	public T visit(LisIncrementSet incrs);
	public T visit(PropertyTree tree);
	public T visit(Axiome axiome);
	public T visit(LisObject lisObject);
	public T visit(LisExtent lisExtent);
	public T visit(CamelisUser camelisUser);
}
