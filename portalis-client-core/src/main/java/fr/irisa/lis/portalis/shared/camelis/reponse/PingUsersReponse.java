package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.RightProperty;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;

@SuppressWarnings("serial")
public class PingUsersReponse extends LisReponse implements CamelisReponseObject  {
	
	private RightProperty[] usersRightProp;
	private String creator;
	
	public PingUsersReponse() {
		super();
	};

	public PingUsersReponse(RightProperty[] users, String creator) {
		super();
		this.usersRightProp= users;
		this.creator = creator;
	};

	public PingUsersReponse(String status, List<String> messages) {
		super(status, messages);
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PingUsersReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PingUsersReponse that = (PingUsersReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.usersRightProp, that.usersRightProp)
			.append(this.creator, that.creator)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(usersRightProp).
				append(creator).
				toHashCode();
	}


	public void setUsers(RightProperty[] users) {
		this.usersRightProp = users;
	}


	public RightProperty[] getUsers() {
		return usersRightProp;
	}


	public void setCreator(String creator) {
		this.creator = creator;
	}


	public String getCreator() {
		return creator;
	}
	
}
