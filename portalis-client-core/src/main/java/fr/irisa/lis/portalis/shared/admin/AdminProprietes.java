package fr.irisa.lis.portalis.shared.admin;





/**
 * Classe permettant la lecture des proprietés de l'application
 */
@SuppressWarnings("serial")
public class AdminProprietes extends ProprietesBase {


	public static final AdminProprietes portalis = new AdminProprietes(
			"portalis.properties");
	public static final AdminProprietes ioConstants = new AdminProprietes(
			"ioConstants.properties");
	public static final AdminProprietes errorMess = new AdminProprietes(
			"errorMess.properties");
	

	
	public AdminProprietes(String fileName) {
		super(fileName);
	}

	public static boolean test() throws PortalisException {
		String errorMessage = "";
		if (portalis.getProperty("server.launcher.xml.content.type")==null) {
			errorMessage += " portalis.properties";
		}
		if (errorMess.getProperty("ok")==null) {
			errorMessage += " errorMess.properties";
		}
		if (ioConstants.getProperty("STATUS")==null) {
			errorMessage += " ioConstants.properties";
		}
		if (errorMessage.length()>0) {
			throw new PortalisException("Fichier(s)"+errorMessage+" inaccessible(s) - ils doivent être dans le classpath");
		}
		return true;
	}

}