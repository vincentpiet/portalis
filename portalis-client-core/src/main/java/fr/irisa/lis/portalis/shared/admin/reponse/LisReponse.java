package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;



@SuppressWarnings("serial")
public class LisReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected Date lastUpdate;
	
	
	public LisReponse() {
		super();
	}
	
	public LisReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public LisReponse(String status, String message) {
		super(status, message);
	}
	
	public LisReponse(String mess, Throwable e) {
		super(mess, e);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LisReponse that = (LisReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.lastUpdate, that.lastUpdate)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(lastUpdate).
				toHashCode();
	}



	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

}