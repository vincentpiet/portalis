package fr.irisa.lis.portalis.shared.admin;

import java.util.regex.Pattern;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.data.AdminDataReaderInterface;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataWriterInterface;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseReaderInterface;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataReaderInterface;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseReaderInterface;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseWriterInterface;

public class ClientConstants {

	
	public static boolean isPrintHttpRequest() {
		return printHttpRequest;
	}

	public static void setPrintHttpRequest(boolean printHttpRequest1) {
		printHttpRequest = printHttpRequest1;
	}

	/**
	 * 
	 */
	private static boolean printHttpRequest = false;

	public final static String CONTENT_TYPE_XML = AdminProprietes.portalis
			.getProperty("server.launcher.xml.content.type");
	public final static String CONTENT_TYPE_HTML = AdminProprietes.portalis
			.getProperty("server.launcher.html.content.type");

	public static final Pattern PID_PATTERN = Pattern.compile("([0-9]{3,5})");
	

	public static final String ANONYMOUS_EMAIL = 
			AdminProprietes.portalis.getProperty("anonymous.email");
	public static final String ANONYMOUS_PSEUDO = 
			AdminProprietes.portalis.getProperty("anonymous.pseudo");
	public static final String ANONYMOUS_PASSWORD = 
			AdminProprietes.portalis.getProperty("anonymous.password");
	public static final String ANONYMOUS_ADMIN_EMAIL = 
			AdminProprietes.portalis.getProperty("anonymousAdmin.email");
	public static final String ANONYMOUS_ADMIN_PSEUDO = 
			AdminProprietes.portalis.getProperty("anonymousAdmin.pseudo");
	public static final String ANONYMOUS_ADMIN_PASSWORD = 
			AdminProprietes.portalis.getProperty("anonymousAdmin.password");
	public static final String NO_SERVICE_NAME = "noServiceName";

	private static XmlReaderFactory<Element> xmlReaderFactory;
	private static XmlWriterFactory<Element> xmlWriterFactory;
	
	public static final CamelisReponseReaderInterface<Element> CAMELIS_REPONSE_XML_READER = 
			getXmlReaderFactory().getCamelisReponseXmlReader();
	public static final CamelisDataReaderInterface<Element> CAMELIS_DATA_XML_READER = 
			getXmlReaderFactory().getCamelisDataXmlReader();
	public static final AdminReponseReaderInterface<Element> ADMIN_REPONSE_XML_READER = 
			getXmlReaderFactory().getAdminReponseXmlReader();
	public static final AdminDataReaderInterface<Element> ADMIN_DATA_XML_READER = 
			getXmlReaderFactory().getPortalisDataXmlReader();
	
	
	public static final CamelisReponseWriterInterface<Element> CAMELIS_REPONSE_XML_WRITER = 
			getXmlWriterFactory().getCamelisReponseVisitor();
	public static final CamelisDataWriterInterface<Element> CAMELIS_DATA_XML_WRITER = 
			getXmlWriterFactory().getCamelisDataVisitor();
	public static final AdminReponseWriterInterface<Element> ADMIN_REPONSE_XML_WRITER = 
			getXmlWriterFactory().getAdminReponseVisitor();
	public static final AdminDataWriterInterface<Element> PORTALIS_DATA_XML_WRITER = 
			getXmlWriterFactory().getPortalisDataVisitor();


	
	public static XmlWriterFactory<Element> getXmlWriterFactory() {
		if (xmlWriterFactory==null)
			xmlWriterFactory = new DomWriterFactory();
		return xmlWriterFactory;
	}
	
	public static XmlReaderFactory<Element> getXmlReaderFactory() {
		if (xmlReaderFactory==null)
			xmlReaderFactory = new GWTReaderFactory();
		return xmlReaderFactory;
	}

}
