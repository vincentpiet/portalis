package fr.irisa.lis.portalis.shared.admin.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CoreServiceNameValidator implements PortalisValidator<String> {

	private Pattern pattern;
	private Matcher matcher;
	private static CoreServiceNameValidator instance;

	private static final String coreServiceName_PATTERN = "^[\\.\\-_A-Za-z0-9-]+:[\\.\\-_A-Za-z0-9-]+$";

	/**
	 * Singleton class use getInstance() to get the singleton instance of this class
	 */
	private CoreServiceNameValidator() {
		pattern = Pattern.compile(coreServiceName_PATTERN);
	}
	
	/**
	 * 
	 * @return the singleton instance of this class validator
	 */
	public static CoreServiceNameValidator getInstance() {
		if (instance==null)
			instance = new CoreServiceNameValidator();
		return instance;
	}

	/**
	 * Validate ServiceName with ServiceName_PATTERN regular expression
	 * 
	 * @param ServiceName
	 *            ServiceName for validation
	 * @return true valid ServiceName, false invalid ServiceName
	 */
	public boolean validate(final String serviceName) {

		matcher = pattern.matcher(serviceName);
		return matcher.matches();

	}
}
