package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;


@SuppressWarnings("serial")
public class PidReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(PidReponse.class.getName());

	private Set<Integer> pids = new HashSet<Integer>();
	
	private int javaPid;

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof PidReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final PidReponse that = (PidReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.pids, that.pids)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(pids).
				toHashCode();
	}



	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

    public PidReponse() {
    	super();
    }

	public PidReponse(Set<Integer> pidList) {
    	super();
		this.pids = pidList;
	}

	public PidReponse(String status, String message) {
		super(status, message);
	}

	public PidReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public PidReponse(String message) {
		super(XmlIdentifier.ERROR, message);
	}

	public PidReponse(List<String> messages) {
		super(XmlIdentifier.ERROR, messages);
	}

	public PidReponse(String mess, Throwable e) {
		super(mess, e);
	}

	public void setPids(Set<Integer> pids2) {
		this.pids = pids2;
	}

	public void addPid(int pid) {
		this.pids.add(new Integer(pid));
	}

	public Set<Integer> getPids() {
		return pids;
	}

	public String getPidsAsString() {
		if (pids.size()==0)
			return "[]";
		StringBuffer buf = new StringBuffer();
		String first = "[";
		for (Integer pid : pids) {
			buf.append(first).append(pid.toString());
			first = ",";
		}
		return buf.append("]").toString();
	}

	public int getJavaPid() {
		return javaPid;
	}

	public void setJavaPid(int javaPid) {
		this.javaPid = javaPid;
	}
}
