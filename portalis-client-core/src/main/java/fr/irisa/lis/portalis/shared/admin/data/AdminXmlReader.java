package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;


public class AdminXmlReader implements AdminReaderInterface<Element> {private static final Logger LOGGER = Logger.getLogger(AdminXmlReader.class.getName());
		
	static private AdminXmlReader instance;
	
	public static AdminXmlReader getInstance() {
		if (instance == null) {
			instance = new AdminDataXmlReader();
		}
		return instance;
	}
	
	protected AdminXmlReader() {}
	

	private void isValidName(String name) throws PortalisException {
		if (name == null) {
			String mess = "isValidName() nom d'attribut ou nom d'élément null";
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		} else if (name.length() == 0) {
			String mess = "isValidName() nom d'attribut ou nom d'élément vide";
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
	}

	protected String getTextValue(Element elem)
			throws PortalisException {
		if (elem == null ) {
			String mess = "erreur interne getTextValue(null)";
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		Node textNode = elem.getFirstChild();
		if (textNode == null) {
			String mess = String.format("manque le fils text() ou cdata() au sein de l'élément <%s>", elem.getNodeName());
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		} else if (textNode.getNodeType()==Element.TEXT_NODE || textNode.getNodeType()==Element.CDATA_SECTION_NODE) {
			return textNode.getNodeValue();
		} else {
			String mess = String.format("L'élément <%s> ne contient pas du text, il contient %s", elem.getNodeName(), XmlUtil.prettyXmlString(textNode));
			LOGGER.severe(mess);
			throw new PortalisException(mess);
			
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getMessageElements(org.dom4j.Element)
	 */
	@Override
	public List<String> getMessageElements(Element root)
			throws PortalisException {
		String name = XmlIdentifier.MESSAGE();
		isValidName(name);
		LOGGER.fine("getMessageElements(\n" + XmlUtil.prettyXmlString(root) + "\n)");
		List<String> reponse = new ArrayList<String>();
		NodeList elems = root.getElementsByTagName(name);
		if (elems == null || elems.getLength() == 0) {
			String mess = String.format("manque les éléments fils <%s> au sein de l'élément <%s>", 
					name, XmlUtil.prettyXmlString(root));
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		} else {
			for (int i = 0; i<elems.getLength(); i++) {
				Element elem = (Element) elems.item(i);
				reponse.add(getTextValue(elem));
			}
		}
		return reponse;
	}

	@Override
	public Element getElement(Element root, String name)
			throws PortalisException {
		return getElement(root, name, false);
	}

		/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getElement(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public Element getElement(Element root, String name, boolean isOptional)
			throws PortalisException {
		isValidName(name);
		// on prend le premier élément de nom 'name' fils de <root>
		NodeList nodeList = root.getElementsByTagName(name);
		if (nodeList==null || nodeList.getLength()== 0) {
			if (isOptional)
				 return null;
			String errMess = String.format("Manque l'élément fils <%s> au sein de l'élément <%s>", name, root.getNodeName());
			LOGGER.severe(errMess);
			throw new PortalisException(errMess);
		} else if (nodeList.getLength()!=1) {
			String errMess = String.format("%d éléments fils <%s> (au lieu d'un seul) au sein de l'élément\n%s", 
					nodeList.getLength(), name, XmlUtil.prettyXmlString(root));
			LOGGER.severe(errMess);
			throw new PortalisException(errMess);
		}
		Element s = (Element) nodeList.item(0);

		return s;
	}

	public List<Element> getElements(Element root, String name)
			throws PortalisException {
		isValidName(name);
		// on prend le premier élément de nom 'name' fils de <root>
		NodeList nodeList = root.getElementsByTagName(name);
		List<Element> result = new ArrayList<Element>(); 
		if (nodeList!=null) {
			for (int i = 0; i<nodeList.getLength(); i++)  {
				result.add((Element) nodeList.item(i)); 
			}
		}
		return result;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getRightValueAttribute(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public RightValue getRightValueAttribute(Element root, String name)
			throws PortalisException {
		RightValue reponse = null;
		String role = getAttribute(root, name);
		if (role == null) {
			String mess = String.format("manque l'attribut '%s' au sein de l'élément %s", 
					name, XmlUtil.prettyXmlString(root));
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		try {
			reponse = RightValue.valueOf(role.toUpperCase());
		} catch (IllegalArgumentException e) {
			throw new PortalisException("Role incorrect : " + role);
		}
		return reponse;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getBooleanAttribute(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public boolean getBooleanAttribute(Element root, String attName) throws PortalisException {
		String boolValue = getStringAttribute(root, attName);
		return Boolean.parseBoolean(boolValue);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getStringElement(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public String getStringElement(Element root, String name)
			throws PortalisException {
		return getTextValue(getElement(root, name, false));
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getStringElements(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public List<String> getStringElements(Element root, String name)
			throws PortalisException {
		isValidName(name);
		List<Element> listElem = getElements(root, name);;
		List<String> reponse = new ArrayList<String>();
	
		for (Element elem : listElem) {
			reponse.add(getTextValue(elem));
		}
	
		return reponse;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getStringAttribute(org.dom4j.Element, java.lang.String, java.lang.String)
	 */
	@Override
	public String getStringAttribute(Element root, String name,
			String defaultValue) throws PortalisException {
		isValidName(name);
		String s = getAttribute(root, name);
		if (s == null) {
			s = defaultValue;
		}

		return s;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getStringAttribute(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public String getStringAttribute(Element root, String name)
			throws PortalisException {
		isValidName(name);
		String s = getAttribute(root, name);
		if (s == null) {
			String mess = String.format("manque l'attribut '%s' pour l'élément %s", 
					name, XmlUtil.prettyXmlString(root));
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}

		return s;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getDateElement(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public Date getDateElement(Element root, String name)
			throws PortalisException {
		isValidName(name);
		String value = getStringElement(root, name);
		return Util.readDate(value);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getDateAttribute(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public Date getDateAttribute(Element root, String name)
			throws PortalisException {
		isValidName(name);
		String value = getStringAttribute(root, name);
		return Util.readDate(value);
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getIntElement(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public int getIntElement(Element elem, String name)
			throws PortalisException {
		isValidName(name);
		String val = getStringElement(elem, name);
		int i = 0;
		try {
			i = Integer.parseInt(val);
		} catch (NumberFormatException e) {
			String mess = String.format("Contenu non numérique = '%s', pour l'élement %s", 
					val, XmlUtil.prettyXmlString(elem));
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		return i;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getIntElements(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public List<Integer> getIntElements(Element root, String name)
			throws PortalisException {
		isValidName(name);
		List<Element> listElem = getElements(root, name);
		List<Integer> reponse = new ArrayList<Integer>();
	
		for (Element elem : listElem) {
			String val = getTextValue(elem);
			int i = 0;
			try {
				i = Integer.parseInt(val);
			} catch (NumberFormatException e) {
				String mess = "Contenu d'élement <" + name
						+ "> non numérique = " + val;
				LOGGER.severe(mess);
				throw new PortalisException(mess);
			}
			reponse.add(new Integer(i));
		}
	
		return reponse;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#getIntAttribute(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public int getIntAttribute(Element elem, String attName)
			throws PortalisException {
		String val = getStringAttribute(elem, attName);
	
		int i = 0;
		try {
			i = Integer.parseInt(val);
		} catch (NumberFormatException e) {
			String mess = "Valeur d'attribut " + attName + " non numérique = "
					+ val;
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		return i;
	}


	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#checkElement(org.dom4j.Element, java.lang.String)
	 */
	@Override
	public void checkElement(Element root, String name)
			throws PortalisException, RequestException {
		isValidName(name);
		if (root == null) {
			String mess = "Erreur interne checkElement(null, "+name+")";
			throw new PortalisException(mess);			
		} else if (!root.getNodeName().equals(name)) {
			String mess = "Le nom de l'élément réponse <" + root.getNodeName()
					+ "> incorrect, il devrait être '" + name + "'";
			LOGGER.severe(mess);
			List<String> messages = new ArrayList<String>();
			messages.add(mess);
			throw new RequestException(messages);
		}
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.client.dom4j.reader.XmlReaderInterface#checkReponseElement(org.dom4j.Element, java.lang.String, fr.irisa.lis.portalis.client.dom4j.reponse.VoidReponse)
	 */
	@Override
	public boolean checkReponseElement(Element root, String name, VoidReponse reponse)
			throws RequestException, PortalisException {
		LOGGER.fine("checkReponseElement(\n" + XmlUtil.prettyXmlString(root) + ")");
		checkElement(root, name);
		String state = getAttribute(root, XmlIdentifier.STATUS());
		if (state != null) {
			reponse.setStatus(state);
			if (state.length() == 0) {
				String mess = "checkReponseElement() attribut "
						+ XmlIdentifier.STATUS() + "=''";
				LOGGER.warning(mess);
				throw new PortalisException(mess);
			} else if (state.equals(XmlIdentifier.OK)) {
				return true;
			} else if (state.equals(XmlIdentifier.ERROR)) {
				String mess = getAttribute(root, XmlIdentifier.MESSAGE());
				if (mess != null) {
					// le message est un attribut
					reponse.addMessage(state, mess);
				} else {
					// les messages sont des éléments
					reponse.setMessages(XmlIdentifier.ERROR, getMessageElements(root));
				}
				LOGGER.fine("fin checkReponseElement(\n" + XmlUtil.prettyXmlString(root) + ") erreurs détectées, " 
						+ "type = "+reponse.getClass().getCanonicalName()
						+ " reponse = "+reponse.toString());
				return false;
			} else {
				String mess = String.format("incorrect %s = %s, élément en cours de lecture = %s",XmlIdentifier.STATUS(), state, XmlUtil.prettyXmlString(root));
				LOGGER.severe(mess);
				throw new PortalisException(mess);
			}
		}
		// pas d'attribut status est equivalent à status OK
		reponse.setStatus(XmlIdentifier.OK);
		return true;
	}

	protected String getAttribute(Element root, String name) {
		String result = root.getAttribute(name);
		return result.length()==0 ? null : result;
	}



}