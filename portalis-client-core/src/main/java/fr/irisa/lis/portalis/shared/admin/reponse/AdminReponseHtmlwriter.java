package fr.irisa.lis.portalis.shared.admin.reponse;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import fr.irisa.lis.portalis.shared.admin.data.AdminDataHtmlWriter;

public class AdminReponseHtmlwriter extends AdminDataHtmlWriter implements
		AdminReponseWriterInterface<Element> {

	static private AdminReponseHtmlwriter instance;
	
	public static AdminReponseHtmlwriter getInstance() {
		if (instance == null) {
			instance = new AdminReponseHtmlwriter();
		}
		return instance;
	}	

	@Override
	public Element visit(LisReponse lisReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}


	@Override
	public Element visit(LogReponse logReponse) {
		Element bodyElem = createRootElement("body");
		addTextChild(bodyElem, "h1", "Fichier de log");
		Document doc = bodyElem.getOwnerDocument();
		for (String line : logReponse.getLines()) {
			Text childNode = doc.createTextNode(line);
			bodyElem.appendChild(childNode);
			createAddChild(bodyElem, "br");
		}
		return htmlRoot(bodyElem);
	}


	@Override
	public Element visit(VersionReponse implementationVersionReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}


	@Override
	public Element visit(VoidReponse voidReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}
	

	@Override
	public Element visit(SiteReponse siteReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}


	@Override
	public Element visit(PidReponse pidReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}


	@Override
	public Element visit(LoginReponse loginReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}


	@Override
	public Element visit(PortsActifsReponse portsActifsReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

	@Override
	public Element visit(ActiveSiteReponse siteReponse) {
		// TODO fill method stub, currently raises an error
		throw new Error("Empty method stub");
	}

}
