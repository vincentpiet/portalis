package fr.irisa.lis.portalis.shared.admin.data;

import org.joda.time.IllegalFieldValueException;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.validation.CoreServiceNameValidator;

@SuppressWarnings("serial")
public class ServiceCore implements AdminDataObject, ServiceCoreInterface {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected String appliName;
	protected String serviceName;

	public ServiceCore() {
	}

	public ServiceCore(String idAppli, String idService) {
		this.appliName = idAppli;
		this.serviceName = idService;
	}

	public ServiceCore(String fullName) {
		this(extractAppliName(fullName), extractServiceName(fullName));
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ServiceCore))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ServiceCore that = (ServiceCore) aThat;
		return new EqualsBuilder()
				.
				// if deriving: appendSuper(super.equals(obj)).
				append(this.appliName, that.appliName)
				.append(this.serviceName, that.serviceName)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// if deriving: appendSuper(super.hashCode()).
				append(appliName).append(serviceName).toHashCode();
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface#getFullName()
	 */
	@Override
	public String getFullName() {
		if (appliName == null || serviceName == null) {
			return null;
		} else {
			return appliName + ":" + serviceName;
		}
	}

	public void setFullName(String fullName) {
		if (!CoreServiceNameValidator.getInstance().validate(fullName)) {
			throw new IllegalFieldValueException(XmlIdentifier.SERVICE_NAME(),
					fullName);
		}
		String[] names = fullName.split(":");
		this.appliName = names[0];
		this.serviceName = names[1];
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface#getAppliName()
	 */
	@Override
	public String getAppliName() {
		return appliName;
	}

	/* (non-Javadoc)
	 * @see fr.irisa.lis.portalis.shared.admin.data.ServiceCoreInterface#getServiceName()
	 */
	@Override
	public String getServiceName() {
		return serviceName;
	}

	public static String extractAppliName(String fullName) {
		return fullName.split(":")[0];
	}

	public static String extractServiceName(String fullName) {
		return fullName.split(":")[1];
	}

}