package fr.irisa.lis.portalis.shared.admin;

public enum RightValue {
	NONE, READER, COLLABORATOR, EDITOR, ADMIN, GENERALADMIN;

	public static RightValue max(RightValue v1, RightValue v2) {
		if (v1.ordinal() > v2.ordinal())
			return v1;
		else
			return v2;
	}

	public static boolean greaterEq(RightValue v1, RightValue v2) {
		return v1.ordinal() >= v2.ordinal();
	}
	
	public String toString() {
		return name().toLowerCase();
	}
	
}
