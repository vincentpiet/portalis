package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;

@SuppressWarnings("serial")
public class XZoomReponse extends ZoomReponse {

	private LisExtent extent;

	public XZoomReponse() {
		super();
	}

	public void setExtent(LisExtent extent) {
		this.extent = extent;
	}

	public LisExtent getExtent() {
		return extent;
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof XZoomReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final XZoomReponse that = (XZoomReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.extent, that.extent).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(extent).toHashCode();
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

}
