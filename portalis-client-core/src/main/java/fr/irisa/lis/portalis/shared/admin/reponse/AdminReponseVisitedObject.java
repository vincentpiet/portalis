package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;



public interface AdminReponseVisitedObject extends Serializable {
	public <T> T accept(AdminReponseWriterInterface<T> visitor);
}
