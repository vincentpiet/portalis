package fr.irisa.lis.portalis.shared.admin;


import fr.irisa.lis.portalis.shared.admin.data.AdminDataWriterInterface;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataWriterInterface;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisReponseWriterInterface;

public interface XmlWriterFactory<T> {
	public AdminDataWriterInterface<T> getPortalisDataVisitor();
	public AdminReponseWriterInterface<T> getAdminReponseVisitor();
	public CamelisDataWriterInterface<T> getCamelisDataVisitor();
	public CamelisReponseWriterInterface<T> getCamelisReponseVisitor();
}
