package fr.irisa.lis.portalis.shared.camelis.data;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.AdminXMLWriter;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

public class CamelisDataXmlWriter extends AdminDataXmlWriter
		implements CamelisDataWriterInterface<Element> {

	protected static int XML_OIDS_MAX;
	protected static int XML_MAX_CHILDREN;

	static {
		String max = AdminProprietes.portalis.getProperty("admin.OidsMax");
		XML_OIDS_MAX = Integer.parseInt(max);

		max = AdminProprietes.portalis.getProperty("admin.PropTreeChildrenMax");
		XML_MAX_CHILDREN = Integer.parseInt(max);
	}

	static private CamelisDataXmlWriter instance;
	
	public static CamelisDataXmlWriter getInstance() {
		if (instance == null) {
			instance = new CamelisDataXmlWriter();
		}
		return instance;
	}
	
	
	public Element visit(Property property) {
		// TODO check if Property should be visited or not
		Element elem = AdminXMLWriter.createRootElement(CamelisXmlName.PROPERTY);
		AdminXMLWriter.addAttribute(elem, CamelisXmlName.PROPERTY_NAME,
				property.getName());
		return elem;
	}


	public Element visit(LisIntent intent) {
		Element root = AdminXMLWriter.createRootElement(intent.getXmlName());
		for (Property p : intent.getProperties()) {
			AdminXMLWriter.addChild(root, visit(p));
		}
		return root;
	}

	
	public Element visit(LisIncrement incr) {
		Element elem = AdminXMLWriter.createRootElement(incr.getXmlName());
		AdminXMLWriter.addAttribute(elem, CamelisXmlName.PROPERTY_NAME,
				incr.getName());
		AdminXMLWriter.addAttribute(elem, CamelisXmlName.PROPERTY_CARD,
				Integer.toString(incr.getCard()));
		return elem;
	}
		

	public Element visit(LisIncrementSet incrs) {
		Element elem = AdminXMLWriter.createRootElement(incrs.getXmlName());
		for (LisIncrement i : incrs.getIncrements()) {
			AdminXMLWriter.addChild(elem, visit(i));
		}
		return elem;
	}
	

	public Element visit(Axiome axiome) {
		Element result = AdminXMLWriter.createRootElement(CamelisXmlName.AXIOME);
		AdminXMLWriter.addAttribute(result, CamelisXmlName.Terme1,
				axiome.getTerme1());
		AdminXMLWriter.addAttribute(result, CamelisXmlName.Terme2,
				axiome.getTerme2());
		return result;
	}

	
	public Element visit(LisObject lisObject) {
		Element object = AdminXMLWriter.createRootElement(CamelisXmlName.OBJECT);
		AdminXMLWriter.addAttribute(object,
								  CamelisXmlName.OID,
								  Integer.toString(lisObject.getOid()));
		AdminXMLWriter.addAttribute(object, CamelisXmlName.REF,
				lisObject.getRef());
		return object;
	}
	
	
	public Element visit(LisExtent lisExtent) {
		Element root = AdminXMLWriter.createRootElement(CamelisXmlName.EXTENT);
		for (LisObject obj : lisExtent.getLisObjects()) {
			AdminXMLWriter.addChild(root, visit(obj));
		}
		return root;
	}

	
	public Element visit(PropertyTree tree) {
		Element propertiesElement = AdminXMLWriter.createRootElement(CamelisXmlName.PROPERTY_TREE);
		AdminXMLWriter.addAttribute(propertiesElement, CamelisXmlName.FEATURE,
				tree.getFeature());
		AdminXMLWriter.addAttribute(propertiesElement, CamelisXmlName.SUPP, tree.getSupp() + "");

		StringBuffer buff = new StringBuffer();
		if (XML_OIDS_MAX == 0) {
			buff.append(tree.getOids());
		} else {
			String[] tab = tree.getOids().split(" ");
			for (int i = 0; i < XML_OIDS_MAX || i < tab.length; i++) {
				buff.append(" " + tab[i]);
			}
		}
		AdminXMLWriter.addTextChild(propertiesElement, CamelisXmlName.OIDS,
				buff.toString());

		// modification ici
		int length = 0;
		if (tree.getChildren() != null)
			length = tree.getChildren().length;
		if (length == 0) {
			return propertiesElement;
		}
		int max = XML_MAX_CHILDREN == 0 ? length : XML_MAX_CHILDREN;
		for (int i = 0; i < max; i++) {
			AdminXMLWriter.addChild(propertiesElement, visit(tree.getChildren()[i]));
		}
		if (max < length) {
			AdminXMLWriter.addAttribute(propertiesElement, CamelisXmlName.SUPP, length + "");
		}
		return propertiesElement;
	}


	public Element visit(CamelisUser camelisUser) {
		Element reponse = AdminXMLWriter.createRootElement(CamelisXmlName.USER);
		AdminXMLWriter.addAttribute(reponse, CamelisXmlName.KEY, camelisUser.getKey());
		AdminXMLWriter.addAttribute(reponse, CamelisXmlName.ROLE,
						camelisUser.getRole().toString());
		AdminXMLWriter.addAttribute(reponse, CamelisXmlName.EXPIRES,
						Util.writeDate(camelisUser.getExpires()));
		return reponse;
	}

}
