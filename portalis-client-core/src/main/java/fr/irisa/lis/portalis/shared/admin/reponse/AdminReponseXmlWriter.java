package fr.irisa.lis.portalis.shared.admin.reponse;

import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.AdminXMLWriter;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;

public class AdminReponseXmlWriter extends AdminDataXmlWriter implements
		AdminReponseWriterInterface<Element> {private static final Logger LOGGER = Logger.getLogger(AdminReponseXmlWriter.class.getName());

	static private AdminReponseXmlWriter instance;
	
	public static AdminReponseXmlWriter getInstance() {
		if (instance == null) {
			instance = new AdminReponseXmlWriter();
		}
		return instance;
	}
	

	@Override
	public Element visit(PidReponse pidReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.PID_REPONSE(), pidReponse);
		if (pidReponse.isOk()) {
			for (Integer i : pidReponse.getPids()) {
				AdminXMLWriter.addTextChild(reponseElem, XmlIdentifier.PID(),
						i.toString());
			}
			AdminXMLWriter.addAttribute(reponseElem, XmlIdentifier.JAVA_PID(), pidReponse.getJavaPid()+"");
		}
		return reponseElem;
	}

	@Override
	public Element visit(LoginReponse loginReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.LOGIN_REPONSE(), loginReponse);
		if (loginReponse.isOk()) {
			LOGGER.fine("visit(loginReponse)");
				try {
					AdminXMLWriter.addChild(reponseElem, new AdminDataXmlWriter().visit(loginReponse.getSession()));
				} catch (PortalisException e) {
					return error(reponseElem, e.getMessage());
				}
		}
		return reponseElem;
	}


	@Override
	public Element visit(PortsActifsReponse portsActifsReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.PORT_ACTIF_REPONSE(), portsActifsReponse);
		if (portsActifsReponse.isOk()) {
			for (PortActif portActif : portsActifsReponse.getListPortsActifs()) {
				AdminXMLWriter.addChild(reponseElem, new AdminDataXmlWriter().visit(portActif));
			}
		}
		return reponseElem;
	}


	@Override
	public Element visit(SiteReponse siteReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.SITE_REPONSE(), siteReponse);
		if (siteReponse.isOk()) {
			AdminXMLWriter.addChild(reponseElem, new AdminDataXmlWriter().visit(siteReponse.getSite()));
		}
		return reponseElem;
	}

	@Override
	public Element visit(ActiveSiteReponse siteReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.ACTIVE_SITE_REPONSE(), siteReponse);
		if (siteReponse.isOk()) {
			AdminXMLWriter.addChild(reponseElem, new AdminDataXmlWriter().visit(siteReponse.getSite()));
		}
		return reponseElem;
	}

	@Override
	public Element visit(LisReponse lisReponse) {
		Element reponseElem = buildRootElement(XmlIdentifier.LIS_REPONSE(),
				lisReponse);
		if (lisReponse.isOk()) {
			AdminXMLWriter.addAttribute(reponseElem, XmlIdentifier.LAST_UPDATE(),
					Util.writeDate(lisReponse.getLastUpdate()));
		}
		return reponseElem;
	}

	@Override
	public Element visit(VersionReponse implementationVersionReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.VERSION_REPONSE(), implementationVersionReponse);
		if (implementationVersionReponse.isOk()) {
			AdminXMLWriter.addAttribute(reponseElem,XmlIdentifier.VERSION(),
					implementationVersionReponse.getVersion());
		}
		return reponseElem;
	}

	@Override
	public Element visit(VoidReponse voidReponse) {
		return buildRootElement(
				XmlIdentifier.VOID_REPONSE(), voidReponse);
	}

	@Override
	public Element visit(LogReponse logReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.LOG_REPONSE(), logReponse);
		if (logReponse.isOk()) {
			for (String line : logReponse.getLines()) {
				AdminXMLWriter.addTextChild(reponseElem, XmlIdentifier.LINE(), line);
			}
		}
		return reponseElem;
	}


}