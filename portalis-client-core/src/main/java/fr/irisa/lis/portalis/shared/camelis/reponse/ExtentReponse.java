package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;

@SuppressWarnings("serial")
public class ExtentReponse extends LisReponse implements CamelisReponseObject {

	private LisExtent extent = new LisExtent();

	public ExtentReponse() {
		super();
	}

	public ExtentReponse(String status, String message) {
		super(status, message);
	}

	public void setExtent(LisExtent extent) {
		this.extent = extent;
	}

	public void setExtent(LisObject[] objs) {
		this.extent = new LisExtent(objs);
	}

	public LisExtent getExtent() {
		return extent;
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ExtentReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ExtentReponse that = (ExtentReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.extent, that.extent).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(extent).toHashCode();
	}

}
