package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.Session;


@SuppressWarnings("serial")
public class LoginReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(LoginReponse.class.getName());
	
	private Session session;
	
	public LoginReponse() {}

	public LoginReponse(String status, String mess) {
		super(status, mess);
	}

	public LoginReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public LoginReponse(Session session2) {
		this.session = session2;
	}

	public LoginReponse(String mess, Throwable e) {
		super(mess, e);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LoginReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LoginReponse that = (LoginReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.session, that.session)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(session).
				toHashCode();
	}
	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}


	public Session getSession() throws PortalisException {
		if (session == null) {
			String mess = "session non initiatlisée";
			throw new PortalisException(mess);
		}
		return session;
	}

	public void setSession(Session session) {
		this.session = session;
	}

}
