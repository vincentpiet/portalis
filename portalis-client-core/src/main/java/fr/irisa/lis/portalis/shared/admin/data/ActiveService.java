package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.validation.ActiveServiceIdValidator;

@SuppressWarnings("serial")
public abstract class ActiveService implements AdminDataObject {private static final Logger LOGGER = Logger.getLogger(ActiveService.class.getName());

	protected int port;
	protected String host;
	protected String creator;
	protected Date activationDate;
	
	static final private ActiveServiceIdValidator activeServiceIdValidator =  ActiveServiceIdValidator.getInstance();

	public ActiveService() {
		super();
	}

	public ActiveService(String creator, String host1, int port,
			Date dateCreation) {
		this.creator = creator;
		this.host = host1;
		this.port = port;
		this.activationDate = dateCreation;
	}

	@Override
	public abstract String toString();

	@Override
	public abstract <T> T accept(AdminDataWriterInterface<T> visitor);

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ActiveService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ActiveService that = (ActiveService) aThat;
		return new EqualsBuilder()
				.
				// if deriving: appendSuper(super.equals(obj)).
				append(this.port, that.port).append(this.host, that.host)
				.append(this.activationDate, that.activationDate)
				.append(this.creator, that.creator).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31)
				. // two randomly chosen prime numbers
				// if deriving: appendSuper(super.hashCode()).
				append(port).append(host).append(activationDate)
				.append(creator).toHashCode();
	}

	public void setActivationDate(Date dateCreation) {
		this.activationDate = dateCreation;
	}

	public Date getActivationDate() {
		return activationDate;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String hostName) {
		this.host = hostName;
	}

	public String getCreator() {
		return creator;
	}

	public void setCreator(String creator) {
		this.creator = creator;
	}

	public String getActiveId() {
		String result = host + ":" + port + "::" + getFullName();
		return result;
	}

	public abstract String getFullName();

	public String getActivePartOfId() {
		return this.host + "::" + this.port;
	}

	public static String extractFullName(String actifId) throws PortalisException {
		if (!activeServiceIdValidator.validate(actifId)) {
			String mess = "activeId incorrecte : "+actifId;
			LOGGER.severe(mess);
			throw new PortalisException(mess);
		}
		String[] parts = actifId.split("::");
		return parts[1];
	}

	public static String extractAppliName(String activeServiceId) throws PortalisException {
		String fullName = extractFullName(activeServiceId);
		return fullName.split(":")[0];
	}

	public static String extractServiceName(String activeServiceId) throws PortalisException {
		String fullName = extractFullName(activeServiceId);
		return fullName.split(":")[1];
	}

	public static int extractPort(String actifId) {
		String[] parts = actifId.split("::");
		int port = 0;
		try {
			port = Integer.parseInt(parts[0].split(":")[1]);
		} catch (NumberFormatException e) {
			String mess = "Identification de service incorrecte '" + actifId
					+ "' le port doit être un entier";
			LOGGER.severe(mess);
		}
		return port;
	}

	public static String extractHost(String actifId) throws PortalisException {
		String[] parts = actifId.split("::");
		return parts[0].split(":")[0];
	}

}