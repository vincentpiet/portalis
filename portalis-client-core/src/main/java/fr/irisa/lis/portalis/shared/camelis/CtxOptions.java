package fr.irisa.lis.portalis.shared.camelis;



public enum CtxOptions {
	sources, axioms, extrinsic, features, wells, rules, actions;
	
	public static String[] split(String string) throws CamelisException {
		String[] options = string.split("::");
		
		try {
		for (int i=0; i<options.length; i++) {
			CtxOptions.valueOf(options[i]);
		}
		} catch (Exception e) {
			String mess = "Liste d'options incorrecte : "+string;
			throw new CamelisException(mess);
		}
		return options;
	}
}
