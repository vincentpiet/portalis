package fr.irisa.lis.portalis.shared.camelis.http;

import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.reponse.AddAxiomReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.CamelisUserReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelFeatureReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.DelObjectsReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.GetValuedFeaturesReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportCtxReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ImportLisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.LoadContextReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.RegisterKeyReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCamelisReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ResetCreatorReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.SetRoleReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

/**
 * Commandes camelis pour programmer les clients.
 * 
 * @author bekkers
 * 
 */

public class CamelisHttp {private static final Logger LOGGER = Logger.getLogger(CamelisHttp.class.getName());

	/*
	 * ------------------------------ ping
	 * 
	 * PingReponse ping (String hostName , (String port | int port))
	 */

	public static PingReponse ping(String host, int port) {
		PingReponse reponse = null;
		try {
			String cmd = "ping";

			Element elem = new HttpStatelessUrlConnection().doConnection(host,
					port, cmd);

			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER.getPingReponse(elem);
			if (reponse.getStatus().equals(XmlIdentifier.OK)) {
				ActiveLisService camelisService = reponse.getFirstActiveLisService();
				if (camelisService == null)
					new PingReponse(XmlIdentifier.ERROR,
							"Internal error : unable to retreive camelisService on port "
									+ port);
				else {
					camelisService.setHost(host);
					camelisService.setPort(port);
				}
			}
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.warning(mess+Util.stack2string( e));
			return new PingReponse(XmlIdentifier.ERROR, mess);
		}
		return reponse;
	}

	public static PingReponse ping(Session sess) {
		try {
			Session.isNotPortalis(sess);
			String host = sess.getHost();
			int port = sess.getPort();
			return ping(host, port);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.warning(mess+Util.stack2string( e));
			return new PingReponse(XmlIdentifier.ERROR, mess);
		}

	}

	/*
	 * ------------------------------ setRole
	 * 
	 * SetRoleReponse setRole(Session sess , String adminKey, String userKey,
	 * (String | RightValue) role )
	 */

	public static SetRoleReponse setRole(Session sess, String userKey,
			String role) {
		SetRoleReponse reponse = new SetRoleReponse();
		try {
			Session.isNotPortalis(sess);
			reponse = new SetRoleReponse();
			roleAction(reponse, sess, userKey, role, "setRole");
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.warning(mess+Util.stack2string( e));
			return new SetRoleReponse(XmlIdentifier.ERROR, mess);
		}
		return reponse;
	}

	public static SetRoleReponse setRole(Session sess, String userKey,
			RightValue role) throws PortalisException {
		SetRoleReponse reponse = new SetRoleReponse();
		try {
			Session.isNotPortalis(sess);
			roleAction(reponse, sess, userKey, role, "setRole");
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.warning(mess+Util.stack2string( e));
			return new SetRoleReponse(XmlIdentifier.ERROR, mess);
		}
		return reponse;
	}

    
   	public static LoadContextReponse loadContext(Session sess) {
		LoadContextReponse reponse = new LoadContextReponse();
		sess.roleCheck(reponse, RightValue.ADMIN);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, sess.getPortalisSessionId() }
			};

		String cmd = "load";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER.getLoadContextReponse(elem);
		} catch (Exception e) {
			reponse = new LoadContextReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static ImportCtxReponse importCtx(Session sess, String ctxFile) {
		ImportCtxReponse reponse = new ImportCtxReponse();
		try {
			Session.isNotPortalis(sess);
			sess.roleCheck(reponse, RightValue.ADMIN);
			if (!reponse.isOk()) {
				return reponse;
			}

			String[][] httpReqArgs = {
					{ CamelisXmlName.CTX_FILE, ctxFile },
					{ CamelisXmlName.ACTIF_USER_KEY,
							sess.getPortalisSessionId() } };
			String cmd = "importCtx";
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getImportCtxReponse(elem);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ImportCtxReponse(XmlIdentifier.ERROR, mess);
		}

		return reponse;
	}

	public static ImportCtxReponse importCtx(Session sess) {
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ImportCtxReponse(XmlIdentifier.ERROR, mess);
		}
		LOGGER.fine("importCtx() session=\n" + sess.toString());

		String serviceName = sess.getActiveService().getFullName();
		String[] ids = serviceName.split(":");
		String ctxFile = new StringBuffer(ids[0]).append("/").append(ids[1])
				.append("/").append(ids[1]).append(".ctx").toString();
		return importCtx(sess, ctxFile);
	}

	public static ImportLisReponse importLis(Session sess, String lisFile) {
		ImportLisReponse reponse = new ImportLisReponse();
		try {
			sess.roleCheck(reponse, RightValue.ADMIN);
			if (!reponse.isOk()) {
				return reponse;
			}

			String[][] httpReqArgs = {
					{ CamelisXmlName.LIS_FILE, lisFile },
					{ CamelisXmlName.ACTIF_USER_KEY,
							sess.getPortalisSessionId() } };
			String cmd = "importLis";
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getImportLisReponse(elem);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ImportLisReponse(XmlIdentifier.ERROR, mess);
		}
		return reponse;
	}

	public static ImportLisReponse importLis(Session sess) {
		// derive .lis filename from service name
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));

			return new ImportLisReponse(XmlIdentifier.ERROR, mess);
		}
		String serviceName = sess.getActiveService().getFullName();
		String[] ids = serviceName.split(":");
		String lisFile = new StringBuffer(ids[0]).append("/").append(ids[1])
				.append("/").append(ids[1]).append(".lis").toString();
		return importLis(sess, lisFile);
	}

	/*
	 * ------------------------------ registerKey
	 * 
	 * registerKey setRole(Session sess , String adminKey, String userKey,
	 * (String | RightValue) role, optional int timeout )
	 */

	public static RegisterKeyReponse registerKey(Session sess, String userKey,
			String role, int timeout) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));

			return new RegisterKeyReponse(XmlIdentifier.ERROR, mess);
		}
		roleAction(reponse, sess, userKey, role, timeout, "registerKey");
		return reponse;
	}

	public static RegisterKeyReponse registerKey(Session sess, String userKey,
			RightValue role, int timeout) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new RegisterKeyReponse(XmlIdentifier.ERROR, mess);
		}
		roleAction(reponse, sess, userKey, role, timeout, "registerKey");
		return reponse;
	}

	public static RegisterKeyReponse registerKey(Session sess, String userKey,
			String role) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new RegisterKeyReponse(XmlIdentifier.ERROR, mess);
		}
		roleAction(reponse, sess, userKey, role, "registerKey");
		return reponse;
	}

	public static RegisterKeyReponse registerKey(Session sess, String userKey,
			RightValue role) throws PortalisException {
		RegisterKeyReponse reponse = new RegisterKeyReponse();
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new RegisterKeyReponse(XmlIdentifier.ERROR, mess);
		}
		roleAction(reponse, sess, userKey, role, "registerKey");
		return reponse;
	}

	// méthodes communes à setRole et registerKey

	private static void roleAction(CamelisUserReponse reponse, Session sess,
			String userKey, String roleParam, String cmd)
			throws PortalisException {
		RightValue role = null;
		try {
			role = RightValue.valueOf(roleParam);
		} catch (Exception e) {
			role = RightValue.NONE;
			String mess = String.format(
					"parameter %s is incorrect, its value is %s",
					CamelisXmlName.ROLE, roleParam);
			LOGGER.warning(mess+Util.stack2string( e));
			reponse.addMessage(CamelisXmlName.ERROR, mess);
		}
		if (reponse.isOk())
			roleAction(reponse, sess, userKey, role, cmd);
	}

	private static <T extends CamelisUserReponse> T roleAction(T reponse,
			Session sess, String userKey, RightValue role, String cmd)
			throws PortalisException {
		LOGGER.fine("roleAction(reponse =" + (reponse == null ? "null" : reponse)
				+ ", session =" + (sess == null ? "null" : sess) + ")");
		sess.roleCheck(reponse, RightValue.ADMIN);
		if (!reponse.isOk()) {
			return reponse;
		}
		String adminKey = sess.getPortalisSessionId();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_ADMIN_KEY, adminKey },
				{ CamelisXmlName.ROLE, role == null ? null : role.toString() },
				{ CamelisXmlName.ACTIF_USER_KEY, userKey } };

		roleAction(sess, reponse, cmd, httpReqArgs);
		return reponse;
	}

	private static void roleAction(CamelisUserReponse reponse, Session sess,
			String userKey, String roleParam, int timeout, String cmd)
			throws PortalisException {
		RightValue role = null;
		try {
			role = RightValue.valueOf(roleParam);
		} catch (Exception e) {
			role = RightValue.NONE;
			String mess = String.format(
					"parameter %s is incorrect, its value is %s",
					CamelisXmlName.ROLE, roleParam);
			LOGGER.severe(mess+Util.stack2string( e));
			reponse.addMessage(CamelisXmlName.ERROR, mess);
		}
		if (reponse.isOk())
			roleAction(reponse, sess, userKey, role, timeout, cmd);
	}

	private static <T extends CamelisUserReponse> T roleAction(T reponse,
			Session sess, String userKey, RightValue role, int timeout,
			String cmd) throws PortalisException {
		LOGGER.fine("roleAction(reponse =" + (reponse == null ? "null" : reponse)
				+ ", session =" + (sess == null ? "null" : sess) + ")");
		sess.roleCheck(reponse, RightValue.ADMIN);
		if (!reponse.isOk()) {
			return reponse;
		}
		String adminKey = sess.getPortalisSessionId();

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_ADMIN_KEY, adminKey },
				{ CamelisXmlName.ROLE, role == null ? null : role.toString() },
				{ CamelisXmlName.ACTIF_USER_KEY, userKey },
				{ CamelisXmlName.TIMEOUT, String.valueOf(timeout) } };

		roleAction(sess, reponse, cmd, httpReqArgs);
		return reponse;
	}

	private static void roleAction(Session sess, CamelisUserReponse reponse,
			String cmd, String[][] httpReqArgs) {
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			ClientConstants.CAMELIS_REPONSE_XML_READER.setCamelisUserReponse(reponse,
					elem);
		} catch (Exception e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			reponse.addMessage(CamelisXmlName.ERROR, e.getMessage());
		}
	}

	/*
	 * ------------------------------------------ Extent
	 */

	public static ExtentReponse extent(Session sess, String query) {
		ExtentReponse reponse = new ExtentReponse();
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ExtentReponse(XmlIdentifier.ERROR, mess);
		}
		sess.roleCheck(reponse, RightValue.READER);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, sess.getPortalisSessionId() },
				{ CamelisXmlName.QUERY, query }, };
		String cmd = "extent";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getExtentReponse(elem);
		} catch (Exception e) {
			reponse = new ExtentReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static ExtentReponse extent(Session sess) {
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ExtentReponse(XmlIdentifier.ERROR, mess);
		}
		return extent(sess, "all");
	}

	public static IntentReponse intent(Session sess, String[] oids,
			boolean showExtent) {
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new IntentReponse(XmlIdentifier.ERROR, mess);
		}
		IntentReponse reponse = new IntentReponse();
		sess.roleCheck(reponse, RightValue.READER);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = new String[2 + oids.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = sess.getPortalisSessionId();
		httpReqArgs[1][0] = CamelisXmlName.EXTENT;
		httpReqArgs[1][1] = Boolean.valueOf(showExtent).toString();
		for (int i = 0; i < oids.length; i++) {
			httpReqArgs[i + 2][0] = CamelisXmlName.OID;
			httpReqArgs[i + 2][1] = oids[i];
		}

		String cmd = "intent";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			if (showExtent) {
				reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
						.getXIntentReponse(elem);
			} else {
				reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
						.getIntentReponse(elem);
			}
		} catch (Exception e) {
			reponse = new IntentReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static IntentReponse intent(Session sess, String[] oids) {
		return intent(sess, oids, false);
	}

	public static ZoomReponse zoom(Session sess, String wq, String feat,
			boolean showExtent) {
		ZoomReponse reponse = new ZoomReponse();
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ZoomReponse(XmlIdentifier.ERROR, mess);
		}
		sess.roleCheck(reponse, RightValue.READER);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = {
				{ CamelisXmlName.ACTIF_USER_KEY, sess.getPortalisSessionId() },
				{ CamelisXmlName.WQ, wq },
				{ CamelisXmlName.FEATURE, feat },
				{ CamelisXmlName.EXTENT, Boolean.valueOf(showExtent).toString() } };
		String cmd = "zoom";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			if (showExtent) {
				reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
						.getXZoomReponse(elem);
			} else {
				reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
						.getZoomReponse(elem);
			}
		} catch (Exception e) {
			reponse = new ZoomReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static ZoomReponse zoom(Session sess, String wq, String feat) {
		// do not show extent by default
		return zoom(sess, wq, feat, false);
	}

	public static ResetCreatorReponse resetCreator(Session sess,
			String creatorEmail) {
		try {
			Session.isNotPortalis(sess);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new ResetCreatorReponse(XmlIdentifier.ERROR, mess);
		}
		ResetCreatorReponse reponse = new ResetCreatorReponse();
		String[][] httpReqArgs = { { CamelisXmlName.EMAIL, creatorEmail },
				{ CamelisXmlName.NEW_KEY, sess.getPortalisSessionId() } };

		String cmd = "resetCreator";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(
					sess.getHost(), sess.getPort(), cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getResetCreatorReponse(elem);
		} catch (Exception e) {
			reponse = new ResetCreatorReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static DelKeyReponse delKey(Session sess, String userKey) {
		DelKeyReponse reponse = new DelKeyReponse();
		try {
			Session.isNotPortalis(sess);
			String[][] httpReqArgs = {
					{ CamelisXmlName.ACTIF_ADMIN_KEY,
							sess.getActiveUser().getPortalisSessionId() },
					{ CamelisXmlName.ACTIF_USER_KEY, userKey } };

			String cmd = "delKey";
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelKeyReponse(elem);
		} catch (Exception e) {
			String mess = e.getMessage();
			LOGGER.severe(mess);
			return new DelKeyReponse(XmlIdentifier.ERROR, mess);
		}

		return reponse;
	}

	public static DelObjectsReponse delObjects(Session sess, int[] oids) {
		DelObjectsReponse reponse = new DelObjectsReponse();
		sess.roleCheck(reponse, RightValue.COLLABORATOR);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = new String[1 + oids.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = sess.getPortalisSessionId();
		for (int i = 0; i < oids.length; i++) {
			httpReqArgs[i + 1][0] = CamelisXmlName.OID;
			httpReqArgs[i + 1][1] = Integer.toString(oids[i]);
		}

		String cmd = "delObjects";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getDelObjectsReponse(elem);
		} catch (Exception e) {
			reponse = new DelObjectsReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static DelFeatureReponse delFeature(Session sess, String query, String[] features) {
		DelFeatureReponse reponse = new DelFeatureReponse();
		sess.roleCheck(reponse, RightValue.COLLABORATOR);
		if (!reponse.isOk()) {
			return reponse;
		}
		
		String[][] httpReqArgs = new String[2 + features.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = sess.getPortalisSessionId();
		httpReqArgs[1][0] = CamelisXmlName.QUERY;
		httpReqArgs[1][1] = query;
		for (int i = 0; i < features.length; i++) {
			httpReqArgs[i + 2][0] = CamelisXmlName.FEATURE;
			httpReqArgs[i + 2][1] = features[i];
		}
		
		String cmd = "delFeature";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess, cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER.getDelFeatureReponse(elem);
		} catch (Exception e) {
			reponse = new DelFeatureReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}
	
	public static AddAxiomReponse addAxiom(Session sess, String premise, String conclusion) {
		return addAxiom(sess, premise, conclusion, false);
	}
	
	
	public static AddAxiomReponse addAxiom(Session sess, String premise, String conclusion, boolean showExtent) {
		AddAxiomReponse reponse = new AddAxiomReponse();
		sess.roleCheck(reponse, RightValue.EDITOR);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = {
			{ CamelisXmlName.ACTIF_USER_KEY, sess.getPortalisSessionId() },
			{ CamelisXmlName.AXIOM_PREMISE, premise },
			{ CamelisXmlName.AXIOM_CONCLUSION, conclusion },
			{ CamelisXmlName.EXTENT, Boolean.valueOf(showExtent).toString() }};

		String cmd = "addAxiom";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess, cmd, httpReqArgs);
			if (showExtent) {
				reponse = ClientConstants.CAMELIS_REPONSE_XML_READER.getXAddAxiomReponse(elem);
			} else {
				reponse = ClientConstants.CAMELIS_REPONSE_XML_READER.getAddAxiomReponse(elem);
			}
		} catch (Exception e) {
			reponse = new AddAxiomReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}

	public static ResetCamelisReponse resetCamelis(Session sess) {
		ResetCamelisReponse reponse = new ResetCamelisReponse();
		sess.roleCheck(reponse, RightValue.ADMIN);
		if (!reponse.isOk()) {
			return reponse;
		}

		String[][] httpReqArgs = { { CamelisXmlName.ACTIF_USER_KEY,
				sess.getPortalisSessionId() } };

		String cmd = "resetCamelis";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess,
					cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getResetCamelisReponse(elem);
		} catch (Exception e) {
			reponse = new ResetCamelisReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;

	}

	
    public static GetValuedFeaturesReponse getValuedFeatures(
			Session sess, int[] oids, String[] feats) {
		GetValuedFeaturesReponse reponse = new GetValuedFeaturesReponse();
		sess.roleCheck(reponse, RightValue.READER);
		if (!reponse.isOk()) {
			return reponse;
		}
		
		String[][] httpReqArgs = new String[1 + oids.length + feats.length][2];
		httpReqArgs[0][0] = CamelisXmlName.ACTIF_USER_KEY;
		httpReqArgs[0][1] = sess.getPortalisSessionId();
		for (int i = 0; i < oids.length; i++) {
			httpReqArgs[i + 1][0] = CamelisXmlName.OID;
			httpReqArgs[i + 1][1] = Integer.toString(oids[i]);
		}
		for (int i = 0; i < feats.length; i++) {
			httpReqArgs[i + 1 + oids.length][0] = "featname";
			httpReqArgs[i + 1 + oids.length][1] = feats[i];
		}
		
		String cmd = "getValuedFeatures";
		try {
			Element elem = new HttpStatelessUrlConnection().doConnection(sess, cmd, httpReqArgs);
			reponse = ClientConstants.CAMELIS_REPONSE_XML_READER.getGetValuedFeaturesReponse(elem);
		} catch (Exception e) {
			reponse = new GetValuedFeaturesReponse(XmlIdentifier.ERROR, e.getMessage());
		}

		return reponse;
	}
    
	
}
