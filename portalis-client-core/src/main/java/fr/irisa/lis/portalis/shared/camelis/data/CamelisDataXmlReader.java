package fr.irisa.lis.portalis.shared.camelis.data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlReader;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

public class CamelisDataXmlReader extends AdminDataXmlReader implements CamelisDataReaderInterface<Element> {private static final Logger LOGGER = Logger.getLogger(CamelisDataXmlReader.class.getName());

	static private CamelisDataXmlReader instance;
	
	public static CamelisDataXmlReader getInstance() {
		if (instance == null) {
			instance = new CamelisDataXmlReader();
		}
		return instance;
	}
	

	protected CamelisDataXmlReader() {
		super();
	}


	public LisObject getLisObject(Element extentPaireElem)
			throws CamelisException, PortalisException, RequestException {
		checkElement(extentPaireElem, CamelisXmlName.OBJECT);
		LisObject lisObject = new LisObject();

		String oid = getAttribute(extentPaireElem, CamelisXmlName.OID);
		if (oid == null || oid.length() == 0) {
			throw new CamelisException("Attribut " + CamelisXmlName.OID
					+ " incorrect dans un élément " + CamelisXmlName.OBJECT);
		}
		lisObject.setOid(Integer.parseInt(oid));

		String ref = getAttribute(extentPaireElem, CamelisXmlName.REF);
		if (ref == null || ref.length() == 0) {
			throw new CamelisException("Attribut " + CamelisXmlName.REF
					+ " incorrect dans un élément " + CamelisXmlName.OBJECT);
		}
		lisObject.setRef(ref);

		return lisObject;
	}


	public LisExtent getLisExtent(Element root)
			throws CamelisException, PortalisException, RequestException {
		checkElement(root, CamelisXmlName.EXTENT);
		ArrayList<LisObject> lisObjects = new ArrayList<LisObject>();
		List<Element> lisObjectElems = getElements(root, CamelisXmlName.OBJECT);
		for (Element e : lisObjectElems) {
			lisObjects.add(getLisObject(e));
		}
		return new LisExtent(lisObjects);
	}


	public CamelisUser getCamelisUser(Element root) throws PortalisException, RequestException {
		LOGGER.fine("CamelisUser(\n"+XmlUtil.prettyXmlString(root)+")");
		checkElement(root, CamelisXmlName.USER);
		String key = getStringAttribute(root, CamelisXmlName.KEY);
		RightValue role = getRightValueAttribute(root, CamelisXmlName.ROLE);
		Date expires = getDateAttribute(root, CamelisXmlName.EXPIRES);
		return new CamelisUser(key, role, expires);
	}


	public LisIncrement getIncrement(Element root)
		throws PortalisException, RequestException
	{
		checkElement(root, CamelisXmlName.INCR);
		String name = getAttribute(root, CamelisXmlName.PROPERTY_NAME);
		String sCard = getAttribute(root, CamelisXmlName.PROPERTY_CARD);
		int card = Integer.parseInt(sCard);
		return new LisIncrement(name, card);
	}


	public Property getProperty(Element root)
			throws PortalisException, RequestException
	{
		return getProperty(root, CamelisXmlName.FEATURE);
	}

	// TODO remove after switch to LisIncrement and LisFeature
	public Property getProperty(Element root, String rootName)
			throws PortalisException, RequestException
	{
		checkElement(root, rootName);
		String name = getAttribute(root, CamelisXmlName.PROPERTY_NAME);
		Property res = new Property(name);
		return res;
	}


	public LisIntent getLisIntent(Element root)
			throws PortalisException, RequestException
	{
		LisIntent res = new LisIntent();
		checkElement(root, res.getXmlName());

		List<Element> propertyElems = getElements(root, CamelisXmlName.PROPERTY);
		for (Element e : propertyElems) {
			res.add(getProperty(e));
		}
		return res;
	}

	
	public LisIncrementSet getLisIncrementSet(Element root)
			throws PortalisException, RequestException {
		LisIncrementSet res = new LisIncrementSet();
		checkElement(root, res.getXmlName());
		List<Element> incrs = getElements(root, CamelisXmlName.INCR);
		for (Element e : incrs) {
			res.add(getIncrement(e));
		}
		return res;
	}


	public PropertyTree getPropertyTree(Element root) throws PortalisException {
		PropertyTree tree = new PropertyTree();
		tree.setFeature(getAttribute(root, CamelisXmlName.FEATURE));
		tree.setOids(getStringElement(root, CamelisXmlName.OIDS));
		List<Element> tabE = getElements(root, CamelisXmlName.PROPERTY_TREE);
		if (!tabE.isEmpty()) {
			PropertyTree[] tab = new PropertyTree[tabE.size()];
			int i = 0;
			for (Element e : tabE) {
				tab[i] = getPropertyTree(e);
				i++;
			}
			tree.setChildren(tab);
		}
		return tree;
	}

}
