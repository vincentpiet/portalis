package fr.irisa.lis.portalis.shared.camelis.reponse;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;


public interface CamelisReponseReaderInterface<T> {

	public abstract StartReponse getStartReponse(T root)
			throws PortalisException, RequestException;

	public abstract PingReponse getPingReponse(T root)
			throws PortalisException, RequestException;

	public abstract ImportCtxReponse getImportCtxReponse(T root)
			throws PortalisException, RequestException;

	public abstract ImportLisReponse getImportLisReponse(T root)
			throws PortalisException, RequestException;
	
	public abstract DelKeyReponse getDelKeyReponse(T root)
			throws PortalisException, RequestException;
	
	public abstract ResetCreatorReponse getResetCreatorReponse(T root)
			throws PortalisException, RequestException;
	
	public abstract ExtentReponse getExtentReponse(T root)
			throws CamelisException, PortalisException, RequestException;

	public abstract IntentReponse getIntentReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract XIntentReponse getXIntentReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract ZoomReponse getZoomReponse(T root)
			throws PortalisException, RequestException, CamelisException;
			
	public abstract XZoomReponse getXZoomReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract void setCamelisUserReponse(CamelisUserReponse reponse,
			T root) throws PortalisException, RequestException;

	public abstract DelObjectsReponse getDelObjectsReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract AddAxiomReponse getAddAxiomReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract ResetCamelisReponse getResetCamelisReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract XAddAxiomReponse getXAddAxiomReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract DelFeatureReponse getDelFeatureReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract GetValuedFeaturesReponse getGetValuedFeaturesReponse(T root)
			throws PortalisException, RequestException, CamelisException;

	public abstract LoadContextReponse getLoadContextReponse(T root)
			throws PortalisException, RequestException, CamelisException;

}
