package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class AddAxiomReponse extends LisReponse implements CamelisReponseObject {

	private String xmlName = CamelisXmlName.ADD_AXIOM_REPONSE;

	public AddAxiomReponse() {
		super();
	}

	public AddAxiomReponse(String status, String message) {
		super(status, message);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof AddAxiomReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

	        return new EqualsBuilder().
	                appendSuper(super.equals(aThat)).
	                isEquals();
		}

	@Override
	public int hashCode() {
	    return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
	        // if deriving: appendSuper(super.hashCode()).
        toHashCode();
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public String getXmlName() {
		return xmlName;
	}

}
