package fr.irisa.lis.portalis.shared.admin.validation;

public interface PortalisValidator<T> {
	public boolean validate(final T hex);
}
