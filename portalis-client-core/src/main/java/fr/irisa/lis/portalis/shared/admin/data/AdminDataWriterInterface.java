package fr.irisa.lis.portalis.shared.admin.data;


public interface AdminDataWriterInterface<T> {
	
	public T visit(boolean bool);
	public T visit(String s);
	public T visit(Integer[] intTable);
	public T visit(RightProperty userRights);
	public T visit(PortActif portActif);
	public T visit(Session session);
	public T visit(ActiveUser loggedInUser);
	public T visit(ActiveUser[] loggedInUser);
	public T visit(UserCore user);
	public T visit(UserCore[] users);
	public T visit(ServiceCore serviceCore);
	public T visit(ActiveLisService lisService);
	public T visit(Application application);
	public T visit(Site portalisSite);
	public T visit(LoginBean loginBean);
	public T visit(PortalisService portalisService);
	public T visit(LoginResult loginResult);
	public T visit(ActiveSite activeSite);
	
}

