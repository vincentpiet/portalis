package fr.irisa.lis.portalis.shared.admin;

public class ErrorMessages {

	public static final String TEMOIN_IS_NULL = "ne doit pas être null";
	public static final String TEMOIN_UNKNOWN_OPTION_CREATOR = "Unknown option -creator";
	public static final String ERROR_DURING_HTTP_REQUEST = "Error during http request";
	public static final String TEMOIN_CONNECTION_FAILURE = "connection failure";
	public static final String TEMOIN_PORT_INTEGER = "should be an integer";
	public static final String TEMOIN_PORT_POSITIF = "strickly positive integer";
	public static final String TEMOIN_IS_MISSING = "is missing";
	public static final String TEMOIN_IS_INCORRECT = "is incorrect";
	public static final String TEMOIN_IS_NOT_REGISTERED = "is not registered";
	public static final String TEMOIN_IS_ALREADY_REGISTERED = "is already registered";
	public static final String TEMOIN_NOT_A_REGISTERED_PORTALIS_SESSION = "is not a recorded Portalis session";
	public static final String TEMOIN_SA_VALEUR_EST = "sa valeur est";
	public static final String TEMOIN_NO_CAMELIS_SERVER_LISTENING = "ne soit pas un serveur camelis";
	public static final String TEMOIN_INCORRECT_SERVICE_NAME = "Incorrect service name";
	public static final String TEMOIN_ALREADY_IN_USE = "the required port is already in use";
	public static final String TEMOIN_CONNECTION_STILL_ALLOCATED = "connection still allocated";
	public static final String TEMOIN_PAS_DE_SESSION_EN_COURS = "pas de session en cours";
	public static final String TEMOIN_ONE_USER_ALLREADY_LOGGED = "one user is allready logged";
}
