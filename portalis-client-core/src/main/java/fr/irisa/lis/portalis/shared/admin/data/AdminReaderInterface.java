package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;
import java.util.List;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;

public interface AdminReaderInterface<T> {

	public abstract T getElement(T root, String name, boolean isOptional)
			throws PortalisException;

	public abstract T getElement(T root, String name) throws PortalisException;

	public abstract String getStringElement(T root, String name)
			throws PortalisException;

	public abstract RightValue getRightValueAttribute(T root, String name)
			throws PortalisException;

	public abstract List<String> getMessageElements(T root)
			throws PortalisException;

	public abstract String getStringAttribute(T root, String name,
			String defaultValue) throws PortalisException;

	public abstract String getStringAttribute(T root, String name)
			throws PortalisException;

	public abstract Date getDateElement(T root, String name)
			throws PortalisException;

	public abstract Date getDateAttribute(T root, String name)
			throws PortalisException;

	public abstract int getIntElement(T elem, String name)
			throws PortalisException;

	public abstract List<Integer> getIntElements(T root, String name)
			throws PortalisException;

	public abstract List<String> getStringElements(T root, String name)
			throws PortalisException;

	public abstract boolean getBooleanAttribute(T root, String attName)
			throws PortalisException;

	public abstract int getIntAttribute(T elem, String attName)
			throws PortalisException;

	public abstract void checkElement(T root, String name)
			throws PortalisException, RequestException;

	public abstract boolean checkReponseElement(T root, String name,
			VoidReponse reponse) throws RequestException, PortalisException;


}