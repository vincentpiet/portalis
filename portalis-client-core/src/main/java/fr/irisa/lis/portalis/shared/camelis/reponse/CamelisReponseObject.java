package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.io.Serializable;


public interface CamelisReponseObject extends Serializable {

	public abstract <T> T accept(CamelisReponseWriterInterface<T> visitor);
	
	public abstract String toString();

}
