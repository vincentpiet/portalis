package fr.irisa.lis.portalis.shared.admin;

import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;


@SuppressWarnings("serial")
public class ProprietesBase extends Properties {

	protected String myFile;
	private static final Logger LOGGER = Logger.getLogger(ProprietesBase.class.getName());

	public ProprietesBase() {
		super();
	}

	public ProprietesBase(Properties defaults) {
		super(defaults);
	}

	public ProprietesBase(String fileName) {
		super();
		myFile = fileName;
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		java.io.InputStream is = cl.getResourceAsStream(fileName);
		if (is != null) {
			try {
				this.load(is);
			} catch (IOException e) {
				// on ne peut pas utiliser logfile : il n'y en a pas ...
				System.out.println("impossible de charger le fichier "+fileName);
			}
		}

	}

	public String getProperty(String key) {
		String prop = super.getProperty(key);
		if (prop==null)
			LOGGER.warning("Fichier "+myFile+" : "+key+" = null");
		return prop;
	}

	public String getProperty(String key, boolean force)
			throws PortalisException {
				if (force == false) {
					return getProperty(key);
				} else {
					String prop = this.getProperty(key);
					if (prop==null) {
						String mess = "Fichier "+myFile + " : " + key + " = null, should be initialised";
						LOGGER.severe(mess);
						throw new PortalisException(mess);
					} else
					    return prop;
				}
			}

}