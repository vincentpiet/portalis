package fr.irisa.lis.portalis.shared.admin.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisError;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class PortalisService extends ActiveService {

	private String appliName;

	private static final Logger LOGGER = Logger.getLogger(PortalisService.class
			.getName());

	public void init(String hostName, int port, String appliName) {
		init(hostName, port, appliName, null);
	}

	public void init(String hostName, int port, String appliName, InputStream loggingConfFile) {
		this.host = hostName;
		this.port = port;
		this.appliName = appliName;
		initFileLoggingProperties(loggingConfFile);
	}

	/** Holder */
	private static class SingletonHolder {
		/** Instance unique non préinitialisée */
		private final static PortalisService instance = new PortalisService();
	}

	/** Point d'accès pour l'instance unique du singleton */
	public static PortalisService getInstance() {
		return SingletonHolder.instance;
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private PortalisService() {
		this.activationDate = new Date();
		this.creator = "%none%";
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof PortalisService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		return new EqualsBuilder().appendSuper(super.equals(aThat)).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).toHashCode();
	}

	@Override
	public String getFullName() {
		if (this.appliName == null || this.appliName.length() == 0) {
			String mess = "PortalisService n'a pas encore été initialisé";
			LOGGER.severe(mess);
			throw new PortalisError(mess);
		}
		return String.format("%s:_", this.appliName);
	}

	public String getAppliName() {
		return this.appliName;
	}

	private static void initFileLoggingProperties(InputStream loggingConfFile) {

		try {
			InputStream inputStream = null;
			if (loggingConfFile==null) {
				ClassLoader cl = Thread.currentThread().getContextClassLoader();
				inputStream = cl.getResourceAsStream("logging.properties");
			} else {
				inputStream = loggingConfFile;
			}
			LogManager.getLogManager().readConfiguration(inputStream);
		} catch (final IOException e) {
			Logger.getAnonymousLogger().severe(
					"Could not load default logging.properties file");
			Logger.getAnonymousLogger().severe(e.getMessage());
		}

	}

}
