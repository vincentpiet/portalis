package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.Iterator;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.camelis.CamelisException;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataXmlReader;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;
import fr.irisa.lis.portalis.shared.camelis.data.Property;

public class CamelisReponseXmlReader extends CamelisDataXmlReader implements
		CamelisReponseReaderInterface<Element> {private static final Logger LOGGER = Logger.getLogger(CamelisReponseXmlReader.class.getName());

	static private CamelisReponseXmlReader instance;

	public static CamelisReponseXmlReader getInstance() {
		if (instance == null) {
			instance = new CamelisReponseXmlReader();
		}
		return instance;
	}

	protected CamelisReponseXmlReader() {
		super();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getStartReponse(org.dom4j.Element)
	 */
	@Override
	public StartReponse getStartReponse(Element root) throws PortalisException,
			RequestException {
		LOGGER.fine("getStartReponse(<" + root.getNodeName() + "> ...)");
		StartReponse reponse = new StartReponse();
		if (!checkReponseElement(root, XmlIdentifier.START_REPONSE(), reponse)) {
			return reponse;
		} else {
			reponse.setActiveLisService(getActiveLisService(getElement(root,
					XmlIdentifier.CAMELIS())));
			return reponse;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getPingReponse(org.dom4j.Element)
	 */
	@Override
	public PingReponse getPingReponse(Element root) throws PortalisException,
			RequestException {
		LOGGER.fine("getPingReponse(\nElement = " + XmlUtil.prettyXmlString(root) + ")");
		PingReponse reponse = new PingReponse();
		if (!checkReponseElement(root, XmlIdentifier.PING_REPONSE(), reponse)) {
			LOGGER.fine("retour avec erreur de getPingReponse() reponse="
					+ reponse);
			return reponse;
		} else {
			String creator = getAttribute(root, XmlIdentifier.CREATOR());
			if (creator != null) {
				// S'il y a un créator comme attribut de root, la réponse vient de Camelis
				reponse.addActiveLisService(getActiveLisService(root));

				return reponse;
			} else {
				// S'il n'y a pas un créator comme attribut de root, la réponse vient de Portalis
				List<Element> lesElems = getElements(root,
						XmlIdentifier.CAMELIS());
				if (lesElems != null) {
					for (Element elem : lesElems) {
						reponse.addActiveLisService(getActiveLisService(elem));
					}
				}
				return reponse;
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getImportCtxReponse(org.dom4j.Element)
	 */
	@Override
	public ImportCtxReponse getImportCtxReponse(Element root)
			throws PortalisException, RequestException {
		ImportCtxReponse reponse = new ImportCtxReponse();
		if (!checkReponseElement(root, XmlIdentifier.IMPORT_CTX_REPONSE(),
				reponse)) {
			LOGGER.fine("retour avec erreur de getImportCtxReponse() reponse="
					+ reponse);
			return reponse;
		} else {
			if (getAttribute(root, CamelisXmlName.CTX_FILE) != null) {
				reponse.setCtxFile(getStringAttribute(root,
						CamelisXmlName.CTX_FILE));
			} else {
				String mess = String.format(
						"Attribute %s is missing in element %s",
						CamelisXmlName.CTX_FILE, XmlUtil.prettyXmlString(root));
				LOGGER.severe(mess);
				reponse = new ImportCtxReponse("error", mess);
			}
		}
		return reponse;
	}

	@Override
	public ImportLisReponse getImportLisReponse(Element root)
			throws PortalisException, RequestException {
		ImportLisReponse reponse = new ImportLisReponse();
		if (!checkReponseElement(root, CamelisXmlName.IMPORT_LIS_REPONSE,
				reponse)) {
			LOGGER.fine("retour avec erreur de getImportLisReponse() reponse="
					+ reponse);
			return reponse;
		} else {
			if (root.getAttribute(CamelisXmlName.LIS_FILE) != null) {
				reponse.setLisFile(getStringAttribute(root,
						CamelisXmlName.LIS_FILE));
			} else {
				String mess = String.format(
						"Attribute %s is missing in element %s",
						CamelisXmlName.LIS_FILE, XmlUtil.prettyXmlString(root));
				LOGGER.severe(mess);
				reponse = new ImportLisReponse("error", mess);
			}
		}
		return reponse;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.irisa.lis.portalis.client.camelis.dom4j.reader.
	 * CamelisReponseXmlReaderInterface#getExtentPaireReponse(org.dom4j.Element)
	 */
	@Override
	public ExtentReponse getExtentReponse(Element root)
			throws CamelisException, PortalisException, RequestException {
		ExtentReponse reponse = new ExtentReponse();

		if (!checkReponseElement(root, CamelisXmlName.EXTENT_REPONSE, reponse)) {
			LOGGER.fine("retour avec erreur de getExtent()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}

		List<Element> el = getElements(root, CamelisXmlName.EXTENT);
		if (el == null || el.size() != 1) {
			String msg = String.format("Élément %s manquant ou non unique.",
					CamelisXmlName.EXTENT);
			LOGGER.severe(msg);
			throw new PortalisException(msg);
		}

		LisExtent extent = getLisExtent(el.get(0));

		reponse.setExtent(extent);
		return reponse;
	}


	private void initIntentReponse(IntentReponse reponse, Element root)
			throws PortalisException, RequestException, CamelisException {
		Element intentElem = getElement(root, CamelisXmlName.INTENT);

		LisIntent intent = new LisIntent();
		List<Element> xmlProps = getElements(intentElem, CamelisXmlName.FEATURE);
		if (xmlProps != null) {
			Iterator<Element> iter = xmlProps.iterator();
			while (iter.hasNext()) {
				Element e = iter.next();
				Property p = getProperty(e);
				intent.add(p);
			}
		}
		reponse.setIntent(intent);	
	}
	
	public IntentReponse getIntentReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		IntentReponse reponse = new IntentReponse();

		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("retour avec erreur de getIntent()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}
		initIntentReponse(reponse, root);
		return reponse;
	}

	public XIntentReponse getXIntentReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		XIntentReponse reponse = new XIntentReponse();

		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("retour avec erreur de getIntent()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}
		initIntentReponse(reponse, root);

		// now parse extent
		List<Element> extentElem = getElements(root, CamelisXmlName.EXTENT);
		if (extentElem.size() > 1) {
			String msg = String.format("Élément %s non unique.",
					CamelisXmlName.EXTENT);
			LOGGER.severe(msg);
			throw new PortalisException(msg);
		} else if (extentElem != null && extentElem.size() == 1) {
			LisExtent extent = getLisExtent(extentElem.get(0));
			reponse.setExtent(extent);
		}
		return reponse;
	}

	private void initZoomReponse(ZoomReponse reponse, Element root)
			throws PortalisException, RequestException, CamelisException {
		LisIncrementSet incrs = new LisIncrementSet();
		Element incrsElem = getElement(root, incrs.getXmlName());

		List<Element> xmlProps = getElements(incrsElem, CamelisXmlName.INCR);
		if (xmlProps != null) {
			Iterator<Element> iter = xmlProps.iterator();
			while (iter.hasNext()) {
				Element e = iter.next();
				LisIncrement i = getIncrement(e);
				incrs.add(i);
			}
		}
		reponse.setIncrements(incrs);
	}
	
	public ZoomReponse getZoomReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		ZoomReponse reponse = new ZoomReponse();

		if (!checkReponseElement(root, CamelisXmlName.ZOOM_REPONSE, reponse)) {
			LOGGER.fine("retour avec erreur de getZoomReponse()+Util.stack2string( reponse=" + reponse);
			return reponse;
		}

		initZoomReponse(reponse, root);
		return reponse;
	}

	public XZoomReponse getXZoomReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		XZoomReponse reponse = new XZoomReponse();

		if (!checkReponseElement(root, CamelisXmlName.ZOOM_REPONSE, reponse)) {
			LOGGER.fine("retour avec erreur de getZoomReponse()+Util.stack2string( reponse=" + reponse);
			return reponse;
		};

		initZoomReponse(reponse, root);
		
		LisExtent ext = new LisExtent();
		Element extElem = getElement(root, ext.getXmlName());
		ext = getLisExtent(extElem);
		reponse.setExtent(ext);
		return reponse;
	}
	
	
	/*
	 * (non-Javadoc)
	 * 
	 * 
	 * CamelisReponseXmlReaderInterface
	 * #setCamelisUserReponse(fr.irisa.lis.portalis
	 * .client.camelis.dom4j.reponse.CamelisUserReponse, org.dom4j.Element)
	 */
	public void setCamelisUserReponse(CamelisUserReponse reponse, Element root)
			throws PortalisException, RequestException {
		if (!checkReponseElement(root, reponse.getElemName(), reponse)) {
		} else {
			Element userElem = getElement(root, CamelisXmlName.USER);
			reponse.setUser(getCamelisUser(userElem));
		}
	}


	public DelKeyReponse getDelKeyReponse(Element root)
			throws PortalisException, RequestException {
		DelKeyReponse delKeyReponse = new DelKeyReponse();
		if (!checkReponseElement(root, CamelisXmlName.DEL_KEY_REPONSE,
				delKeyReponse)) {
		}
		return delKeyReponse;
	}

	
	public ResetCreatorReponse getResetCreatorReponse(Element root)
			throws PortalisException, RequestException {
		ResetCreatorReponse resetCreatorReponse = new ResetCreatorReponse();
		if (!checkReponseElement(root, CamelisXmlName.RESET_CREATOR_REPONSE,
				resetCreatorReponse)) {
		}
		return resetCreatorReponse;
	}

	
	public DelObjectsReponse getDelObjectsReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		DelObjectsReponse reponse = new DelObjectsReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getDelObjectsReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		} else {
			LisExtent ext = new LisExtent();
			Element extElem = getElement(root, ext.getXmlName());
			ext = getLisExtent(extElem);
			reponse.setExtent(ext);
		}
		return reponse;
	}

	
	public DelFeatureReponse getDelFeatureReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		DelFeatureReponse reponse = new DelFeatureReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getDelFeatureReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		} else {

			LisIncrementSet incrs = new LisIncrementSet();
			Element incrsElem = getElement(root, incrs.getXmlName());
			incrs = getLisIncrementSet(incrsElem);
			reponse.setIncrements(incrs);
		}	
		return reponse;
	}


	public AddAxiomReponse getAddAxiomReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		AddAxiomReponse reponse = new AddAxiomReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getAddAxiomReponse returned with an error+Util.stack2string( reponse=" + reponse);
		}
		return reponse;
	
	}

	
	public XAddAxiomReponse getXAddAxiomReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		XAddAxiomReponse reponse = new XAddAxiomReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getXAddAxiomReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		}
		List<Element> el = getElements(root, CamelisXmlName.EXTENT);
		if (el == null || el.size() != 1) {
			String msg = String.format("%s element missing or not unique.",
					CamelisXmlName.EXTENT);
			LOGGER.severe(msg);
			throw new PortalisException(msg);
		}
		LisExtent extent = getLisExtent(el.get(0));
		reponse.setExtent(extent);		
		return reponse;
	}
	
	
	public ResetCamelisReponse getResetCamelisReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		ResetCamelisReponse reponse = new ResetCamelisReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getResetCamelisReponse() returned with an error+Util.stack2string( reponse=" + reponse);
		}
		return reponse;
	}

	
	public GetValuedFeaturesReponse getGetValuedFeaturesReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		GetValuedFeaturesReponse reponse = new GetValuedFeaturesReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getGetValuedFeaturesReponse() returned with an error+Util.stack2string( reponse = " + reponse);
			return reponse;
		}
		
		List<Element> objs = getElements(root, "object");
		for (Element objElem: objs) {
			int oid = Integer.parseInt(objElem.getAttribute("oid"));
			List<Element> props = getElements(objElem, "property");
			for (Element propElem: props) {
				String featname = propElem.getAttribute("name");
				String value = propElem.getAttribute("value");
				reponse.setValue(oid, featname, value);
			}
		}
		
		return reponse;
	}

	
	public LoadContextReponse getLoadContextReponse(Element root)
			throws PortalisException, RequestException, CamelisException {
		LoadContextReponse reponse = new LoadContextReponse();
		if (!checkReponseElement(root, reponse.getXmlName(), reponse)) {
			LOGGER.fine("getGetValuedFeaturesReponse() returned with an error+Util.stack2string( reponse = " + reponse);
			return reponse;
		}
		
		String name = root.getAttribute(CamelisXmlName.CONTEXT_NAME);
		reponse.setContextName(name);
		return reponse;
	}
	
}
