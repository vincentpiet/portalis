package fr.irisa.lis.portalis.shared.admin.http;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Precondition;

public class AbstractHttpUrlConnection {private static final Logger LOGGER = Logger.getLogger(AbstractHttpUrlConnection.class.getName());
	


	protected URL buildUrl(String hostName, int port, String command,
			String[][] httpReqArgs) throws PortalisException {
				URL url = null;
				Precondition errors = new Precondition();
				errors.setHttpReqArgs(httpReqArgs);
				errors.checkJavaArg(port > 0,
						"port should be a strickly positive integer its value is %d",
						port);
				errors.checkJavaArg(hostName != null, "Sorry, no server name");
				if (errors.isOk()) {
					try {
						url = new URL("http", hostName, port,
								(command.startsWith("/") ? command : "/"+command)
										+ errors.getArgsAsHttpRequestString());
					} catch (MalformedURLException e) {
						String errMess = String.format(
								"build URL failure during http request\nCause = %s",
								e.getMessage());
						LOGGER.warning(errMess);
						throw new PortalisException(errMess, e);
					} catch (UnsupportedEncodingException e) {
						String errMess = String.format(
								"build URL failure during http request\nCause = %s",
								e.getMessage());
						LOGGER.warning(errMess);
						throw new PortalisException(errMess, e);
					}
				} else {
					String errMess = String.format(
							"build URL failure during http request\nCause = %s",
							errors.getMessages());
					LOGGER.warning(errMess);
					throw new PortalisException(errMess);
				}
				return url;
			}



}