package fr.irisa.lis.portalis.shared.camelis.reponse;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.Property;

@SuppressWarnings("serial")
public class ZoomReponse extends LisReponse implements CamelisReponseObject {

	protected LisIncrementSet increments = new LisIncrementSet();
	protected Property wq;
	protected Property feature;

	public ZoomReponse() {
		super();
	}

	public ZoomReponse(String status, String message) {
		super(status, message);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof ZoomReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final ZoomReponse that = (ZoomReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.increments, that.increments)
				.append(this.wq, that.wq)
				.append(this.feature, that.feature)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode())
				.append(increments)
				.append(wq)
				.append(feature)
				.toHashCode();
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	@Override
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public LisIncrementSet getIncrements() {
		return increments;
	}

	public void setIncrements(LisIncrementSet increments) {
		this.increments = increments;
	}

	public void setIncrements(LisIncrement[] increments) {
		this.increments = new LisIncrementSet(increments);
	}

	public Property getWq() {
		return wq;
	}

	public void setWq(Property wq) {
		this.wq = wq;
	}

	public Property getFeature() {
		return feature;
	}

	public void setFeature(Property feature) {
		this.feature = feature;
	}

}
