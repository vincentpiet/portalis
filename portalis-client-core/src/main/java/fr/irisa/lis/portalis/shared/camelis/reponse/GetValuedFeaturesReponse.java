package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.HashMap;
import java.util.Set;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class GetValuedFeaturesReponse extends LisReponse implements
		CamelisReponseObject {

	private HashMap<Integer, HashMap<String, String>> values =
			new HashMap<Integer, HashMap<String, String>>();  
	
	private final String xmlName = CamelisXmlName.GET_VALUED_FEATURES_REPONSE;
	
	public GetValuedFeaturesReponse() {
		super();
	}


	public GetValuedFeaturesReponse(String status, String message) {
		super(status, message);
	}

	
	public Set<Integer> getOids() {
		return values.keySet();
	}
	
	public Set<String> getFeatnames(int oid) {
		return values.get(oid).keySet();
	}
	
	
	public String getValue(int oid, String featname) {
		return values.get(oid).get(featname); 
	}
	
	public void setValue(int oid, String featname, String val) {
		if (values.containsKey(oid)) {
			HashMap<String, String> old = values.get(oid);
			old.put(featname, val);
		} else {
			HashMap<String, String> tmp = new HashMap<String, String>();
			tmp.put(featname, val);
			values.put(oid, tmp);
		}
	}
	
	
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
	
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof GetValuedFeaturesReponse))
			return false;

		final GetValuedFeaturesReponse that = (GetValuedFeaturesReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.values, that.values).isEquals();
	}


	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(values).toHashCode();
	}


	public String getXmlName() {
		return xmlName;
	}

	

}
