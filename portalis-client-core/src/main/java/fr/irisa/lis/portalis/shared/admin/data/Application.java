package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;


@SuppressWarnings("serial")
public class Application implements AdminDataObject {

	protected String id;
	protected List<ServiceCore> services = new ArrayList<ServiceCore>();

	public Application() {}
	
	public Application(String appliName) {
		this.id = appliName;
	}

	public Application(String appliName, List<ServiceCore> services) {
		this.id = appliName;
		this.services = services;
	}

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof Application) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final Application that = (Application)aThat;
	        return new EqualsBuilder().
	                // appendSuper(super.equals(aThat)).
	                append(this.services, that.services).
	                append(this.id, that.id).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            	// appendSuper(super.hashCode()).
                append(services).
                append(id).
            toHashCode();
    }

	public void setId(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setServices(List<ServiceCore> services) {
		this.services = services;
	}

	public List<ServiceCore> getServices() {
		return services;
	}
	
	public ServiceCoreInterface getService(String name) throws PortalisException {
		ServiceCoreInterface result = null;
		for (ServiceCore service : services.toArray(new ServiceCore[services.size()])) {
			if (service.getServiceName().equals(name)) {
				result = service;
				break;
			}
		}
		return result;
	}
	

	public void add(ServiceCore service) {
		this.services.add(service);
	}

	public void removeService(ServiceCoreInterface service) {
		this.services.remove(service);
	}


}
