package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class LoadContextReponse extends LisReponse implements
		CamelisReponseObject {

	private String contextName = "";
	private final String xmlName = CamelisXmlName.LOAD_REPONSE;
	
	public LoadContextReponse() {
		super();
	}

	public LoadContextReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public LoadContextReponse(String status, String message) {
		super(status, message);
	}

	
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof LoadContextReponse))
			return false;

		final LoadContextReponse that = (LoadContextReponse) aThat;
		return new EqualsBuilder().appendSuper(super.equals(aThat))
				.append(this.contextName, that.contextName).isEquals();
	}

	
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).append(contextName).toHashCode();
	}
	
	
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER
						.visit(this));
	}

	
	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	
	public String getXmlName() {
		return xmlName;
	}
	
	public String getContextName() {
		return contextName;
	}

	public void setContextName(String newName) {
		this.contextName = newName;
	}



}
