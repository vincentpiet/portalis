package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;



@SuppressWarnings("serial")
public class VersionReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject  {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private String version;
		
	
	public VersionReponse() {
		super();
	}
	
	public VersionReponse(String version1) {
		super();
		this.version = version1;
	}
	
	public VersionReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public VersionReponse(String status, String message) {
		super(status, message);
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof VersionReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final VersionReponse that = (VersionReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.version, that.version)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(version).
				toHashCode();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

}