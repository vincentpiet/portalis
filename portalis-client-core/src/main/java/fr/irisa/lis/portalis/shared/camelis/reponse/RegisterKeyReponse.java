package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.Date;
import java.util.List;

import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;

@SuppressWarnings("serial")
public class RegisterKeyReponse extends CamelisUserReponse {

	private String elemName = CamelisXmlName.REGISTER_KEY_REPONSE;

	public RegisterKeyReponse() {
		super();
	}

	public RegisterKeyReponse(String creator,
			Date dateCreation, Date lastUpdate, int port, int pid, int nbObject){
		super();
		setStatus(XmlIdentifier.OK);
	}

	public RegisterKeyReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public RegisterKeyReponse(String status, String message) {
		super(status, message);
	}

	public RegisterKeyReponse(CamelisUser user2) {
		setUser(user2);
	}

	@Override
	public String getElemName() {
		return elemName ;
	}
}
