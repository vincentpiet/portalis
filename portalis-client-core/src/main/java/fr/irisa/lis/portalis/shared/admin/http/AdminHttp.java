package fr.irisa.lis.portalis.shared.admin.http;

import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Precondition;
import fr.irisa.lis.portalis.shared.admin.RequestException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.PortActif;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PidReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.PortsActifsReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.SiteReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VersionReponse;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.StartReponse;

public class AdminHttp {private static final Logger LOGGER = Logger.getLogger(AdminHttp.class.getName());

	public final static String WEBAPP_PATH = AdminProprietes.portalis
			.getProperty("admin.webappsDirPath");
	public static final String LOG_FILE_PATH = AdminProprietes.portalis
			.getProperty("admin.camelis.logFilePath");

	public static SiteReponse getSite(Session session) {
		if (session == null) {
			String mess = "erreur session nulle";
			LOGGER.severe(mess);
			return new SiteReponse(XmlIdentifier.ERROR, mess);
		}

		// vérification du rôle
		SiteReponse reponseErr = session.roleCheck(new SiteReponse(),
				RightValue.ADMIN);
		if (!reponseErr.isOk()) {
			return reponseErr;
		}

		Element reponseElem;
		try {
			reponseElem = new HttpPortalisUrlConnection().doConnection(session,
					"getSite.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getSiteReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			return new SiteReponse(XmlIdentifier.ERROR, e.getMessage());
		}

	}

	public static SiteReponse clearLog(Session session) {
		if (session == null) {
			String mess = "erreur session nulle";
			LOGGER.severe(mess);
			return new SiteReponse(XmlIdentifier.ERROR, mess);
		}

		// vérification du rôle
		SiteReponse reponseErr = session.roleCheck(new SiteReponse(),
				RightValue.ADMIN);
		if (!reponseErr.isOk()) {
			return reponseErr;
		}

		Element reponseElem;
		try {
			reponseElem = new HttpPortalisUrlConnection().doConnection(session,
					"clearLog.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getSiteReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			return new SiteReponse(XmlIdentifier.ERROR, e.getMessage());
		}

	}

	public static LoginReponse login(String email, String password) {

		// appelée pour un test via http ou par les clients Java
		LOGGER.info(String.format("login(%s, %s)", email, password));

		try {
			String[][] httpReqArgs = {
					{ XmlIdentifier.EMAIL(), email },
					{ XmlIdentifier.PASSWORD(), password } };
			Session session = Session.createAnonymousPortalisSession();
			Element reponseElem = new HttpPortalisUrlConnection()
					.doFirstConnection(session, "login.jsp", httpReqArgs);

			LoginReponse reponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLoginReponse(reponseElem);
			if (reponse.isOk()) {
				reponse.getSession().setCookies(session.getCookies());
				session.resetCookies();
			}

			return reponse;
		} catch (Throwable t) {
			Throwable e = t.getCause();
			String mess = t.getMessage();
			if (e == null) {
				e = t;
			} else {
				mess = mess + e.getMessage();
			}
			LOGGER.severe(t.getMessage()+Util.stack2string( e));
			return new LoginReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static LoginReponse anonymousLogin() {

		// appelée pour un test via http ou par les clients Java
		LOGGER.fine("anonymousLogin()");

		try {
			Session session = Session.createAnonymousPortalisSession();
			Element reponseElem = new HttpPortalisUrlConnection()
					.doFirstConnection(session, "anonymousLogin.jsp");

			LoginReponse reponse = ClientConstants.ADMIN_REPONSE_XML_READER
					.getLoginReponse(reponseElem);
			if (reponse.isOk()) {
				reponse.getSession().setCookies(session.getCookies());
				session.resetCookies();
			}

			return reponse;
		} catch (Throwable t) {
			Throwable e = t.getCause();
			String mess = t.getMessage();
			if (e == null) {
				e = t;
			} else {
				mess = mess + e.getMessage();
			}
			LOGGER.severe(t.getMessage()+Util.stack2string( e));
			return new LoginReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static PortsActifsReponse getActifsPorts(Session session) {
		if (session == null) {
			String mess = "erreur session nulle";
			LOGGER.severe(mess);
			return new PortsActifsReponse(XmlIdentifier.ERROR, mess);
		}

		try {

			// vérification du rôle
			PortsActifsReponse reponseErr = session.roleCheck(
					new PortsActifsReponse(), RightValue.ADMIN);
			if (!reponseErr.isOk()) {
				return reponseErr;
			}

			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					session, "getActifsPorts.jsp");

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getPortsActifsReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			return new PortsActifsReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static PingReponse getActiveLisServicesOnPortalis(Session session) {
		PingReponse pingReponse = new PingReponse();
		try {
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					session, "getActiveCamelisServices.jsp");

			pingReponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getPingReponse(reponseElem);
			return pingReponse;

		} catch (Throwable e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			return new PingReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static PidReponse getCamelisProcessId(Session session, String port) {
		if (session == null) {
			String mess = "erreur session nulle";
			LOGGER.severe(mess);
			return new PidReponse(XmlIdentifier.ERROR, mess);
		}

		if (port == null) {
			return getCamelisProcessId(session, session.getPort());
		}
		Precondition errors = new Precondition();
		int portNum = errors.checkIntArg(port,
				"Le parametre %s doit être un entier, sa valeur est %s",
				XmlIdentifier.PORT(), port);
		if (errors.isOk()) {
			return getCamelisProcessId(session, portNum);
		} else {
			return new PidReponse(XmlIdentifier.ERROR, errors.getMessages());
		}
	}

	public static PidReponse getCamelisProcessId(Session session, int port) {
		if (session == null) {
			String mess = "erreur session nulle";
			LOGGER.severe(mess);
			return new PidReponse(XmlIdentifier.ERROR, mess);
		}

		LOGGER.fine("getCamelisProcessId(" + session + "+Util.stack2string( " + port + ")");

		// vérification du rôle
		PidReponse pidReponse = session.roleCheck(new PidReponse(),
				RightValue.ADMIN);
		if (!pidReponse.isOk()) {
			return pidReponse;
		}

		try {
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					session, String.format("getCamelisProcessId.jsp?%s=%d",
							XmlIdentifier.PORT(), port));

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getPidReponse(reponseElem);
		} catch (Throwable e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			return new PidReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	/**
	 * 
	 * @return l'information de version de Maven
	 */
	public static VersionReponse getImplementationVersion(Session session) {
		try {
			Session.isPortalisSession(session);

			// vérification du rôle
			VersionReponse reponseErr = session.roleCheck(new VersionReponse(),
					RightValue.ADMIN);
			if (!reponseErr.isOk()) {
				return reponseErr;
			}

		} catch (Throwable t) {
			String mess = t.getMessage();
			LOGGER.severe(mess+Util.stack2string( t));
			return new VersionReponse(XmlIdentifier.ERROR, mess);
		}
		return new VersionReponse(AdminHttp.class.getPackage()
				.getImplementationVersion());
	}

	public static VoidReponse logout(Session sess) {
		try {
			// appelée pour un test via http ou par les clients Java
			LOGGER.fine(String.format("logout(%s)", sess));
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					sess, "logout.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getVoidReponse(reponseElem);
		} catch (Throwable t) {
			String mess = t.getMessage();
			LOGGER.severe(mess+Util.stack2string( t));
			return new VoidReponse(XmlIdentifier.ERROR, mess);
		}

	}

	public static VoidReponse propertyFilesCheck(Session loginSession) {
		try {
			LOGGER.fine(String.format("adminPropertyFilesCheckHttp(%s)", loginSession));
			Element reponseElem = new HttpPortalisUrlConnection().doConnection(
					loginSession, "propertiesFileCheck.jsp");

			return ClientConstants.ADMIN_REPONSE_XML_READER
					.getVoidReponse(reponseElem);
		} catch (Throwable t) {
			String mess = t.getMessage();
			LOGGER.severe(mess+Util.stack2string( t));
			return new VoidReponse(XmlIdentifier.ERROR, mess);
		}
	}

	public static StartReponse startCamelis(Session sess, String serviceName,
			String webApplicationDataPath, String logFile) {
		String port = null;

		StartReponse rep = startCamelis(sess, port, serviceName,
				webApplicationDataPath, logFile);
		return rep;
	}

	public static StartReponse startCamelis(Session sess, String port,
			String serviceName, String webApplicationDataPath, String logFile) {
		Precondition errors = new Precondition();
		int portNum = -1;
		if (port != null) {
			portNum = errors.checkIntArg(port,
					"Le parametre %s doit être un entier, sa valeur est %s",
					XmlIdentifier.PORT(), port);
		}
		if (!errors.isOk()) {
			LOGGER.warning(errors.getMessagesAsString());
			return new StartReponse(XmlIdentifier.ERROR, errors.getMessages());
		}
		return startCamelis(sess, portNum, serviceName, webApplicationDataPath,
				logFile);
	}

	public static StartReponse startCamelis(Session sess, int port,
			String serviceName, String webApplicationDataPath, String logFile) {
		try {
			Session.isPortalisSession(sess);
			LOGGER.fine(String.format("startCamelis(%d+Util.stack2string( %s)\n%s))", port,
					serviceName, sess));

			StartReponse startReponse = sess.roleCheck(new StartReponse(),
					RightValue.ADMIN);
			if (!startReponse.isOk()) {
				return startReponse;
			}

			ActiveUser activeUser = sess.getActiveUser();
			String[][] httpReqArgs;
			if (port <= 0)
				httpReqArgs = new String[][] {
						{ XmlIdentifier.CREATOR(),
								activeUser.getUserCore().getEmail() },
						{ XmlIdentifier.KEY(),
								activeUser.getPortalisSessionId() },
						{ XmlIdentifier.SERVICE_NAME(), serviceName },
						{
								XmlIdentifier.LOG(),
								(logFile == null || logFile.length() == 0 ? logFile = LOG_FILE_PATH
										: logFile) },
						{
								XmlIdentifier.DATADIR(),
								(webApplicationDataPath == null
										|| webApplicationDataPath.length() == 0 ? webApplicationDataPath = WEBAPP_PATH
										: webApplicationDataPath) } };
			else

				httpReqArgs = new String[][] {
						{ XmlIdentifier.CREATOR(),
								activeUser.getUserCore().getEmail() },
						{ XmlIdentifier.PORT(), port + "" },
						{ XmlIdentifier.KEY(),
								activeUser.getPortalisSessionId() },
						{ XmlIdentifier.SERVICE_NAME(), serviceName },
						{
								XmlIdentifier.LOG(),
								(logFile == null || logFile.length() == 0 ? logFile = LOG_FILE_PATH
										: logFile) },
						{
								XmlIdentifier.DATADIR(),
								(webApplicationDataPath == null
										|| webApplicationDataPath.length() == 0 ? webApplicationDataPath = WEBAPP_PATH
										: webApplicationDataPath) } };

			return startCamelis(sess, httpReqArgs);
		} catch (Throwable e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new StartReponse(XmlIdentifier.ERROR, mess);
		}
	}

	private static StartReponse startCamelis(Session sess,
			String[][] httpReqArgs) {
		StartReponse resultReponse = null;

		try {
			Session.isPortalisSession(sess);

			Element elem = new HttpPortalisUrlConnection().doConnection(sess,
					"startCamelis.jsp", httpReqArgs);

			resultReponse = ClientConstants.CAMELIS_REPONSE_XML_READER
					.getStartReponse(elem);
		} catch (Throwable e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new StartReponse(XmlIdentifier.ERROR, mess);
		}

		return resultReponse;
	}

	/*
	 * ------------------------------ stopCamelis
	 */
	/**
	 * stop all camelis sessions
	 * 
	 * @param sess
	 * @return
	 */
	public static PingReponse stopCamelis(Session sess) {
		try {
			Session.isPortalisSession(sess);
		} catch (PortalisException e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new PingReponse(XmlIdentifier.ERROR, mess);
		}
		PortsActifsReponse portsActifs = getActifsPorts(sess);
		PingReponse pingReponse = new PingReponse();
		for (PortActif portActif : portsActifs.getListPortsActifs()) {
			pingReponse = stopCamelis(sess, portActif.getPort());
		}
		return pingReponse;
	}

	public static PingReponse stopCamelis(Session sess, int port) {
		try {
			Session.isPortalisSession(sess);
		} catch (PortalisException e) {
			String mess = e.getMessage();
			LOGGER.severe(mess+Util.stack2string( e));
			return new PingReponse(XmlIdentifier.ERROR, mess);
		}

		PingReponse stopReponse = sess.roleCheck(new PingReponse(),
				RightValue.ADMIN);
		if (!stopReponse.isOk()) {
			return stopReponse;
		}

		String[][] httpReqArgs = { { XmlIdentifier.PORT(), port + "" } };
		try {
			Element elem = new HttpPortalisUrlConnection().doConnection(sess,
					"stopCamelis.jsp", httpReqArgs);
			return ClientConstants.CAMELIS_REPONSE_XML_READER.getPingReponse(elem);
		} catch (Throwable e) {
			LOGGER.severe(e.getMessage()+Util.stack2string( e));
			return new PingReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static PortsActifsReponse lsofAll(Session sess) {
		try {
			Session.isPortalisSession(sess);
			LOGGER.fine(String.format("lsofAll()\n%s)", sess));

			PortsActifsReponse lsofReponse = sess.roleCheck(new PortsActifsReponse(),
					RightValue.ADMIN);
			if (!lsofReponse.isOk()) {
				return lsofReponse;
			}
			Element elem = new HttpPortalisUrlConnection().doConnection(sess,
					"lsofAll.jsp");
			return ClientConstants.ADMIN_REPONSE_XML_READER.getPortsActifsReponse(elem);
		} catch (Exception e) {
			LOGGER.severe("lsofAll error : " + e.getMessage()+Util.stack2string( e));
			return new PortsActifsReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static PidReponse lsof(Session sess, int port) {
		try {
			Session.isPortalisSession(sess);
			LOGGER.fine(String.format("lsof(%d)\n%s)", port, sess));

			PidReponse lsofReponse = sess.roleCheck(new PidReponse(),
					RightValue.ADMIN);
			if (!lsofReponse.isOk()) {
				return lsofReponse;
			}
			String[][] httpReqArgs = { { XmlIdentifier.PORT(), port + "" } };

			Element elem = new HttpPortalisUrlConnection().doConnection(sess,
					"lsof.jsp", httpReqArgs);
			return ClientConstants.ADMIN_REPONSE_XML_READER.getPidReponse(elem);
		} catch (Exception e) {
			LOGGER.severe("lsof error : " + e.getMessage()+Util.stack2string( e));
			return new PidReponse(XmlIdentifier.ERROR, e.getMessage());
		}
	}

	public static PingReponse killAllCamelisProcess(Session sess) {
		PingReponse stopReponse = sess.roleCheck(new PingReponse(),
				RightValue.ADMIN);
		if (!stopReponse.isOk()) {
			return stopReponse;
		}

		PingReponse pingReponse = getActiveLisServicesOnPortalis(sess);
		for (ActiveLisService service : pingReponse.getLesServices()) {
			stopCamelis(sess, service.getPort());
		}
		return getActiveLisServicesOnPortalis(sess);
	}

	public static VoidReponse clearActifUsers(Session sess) throws PortalisException, RequestException {
		VoidReponse reponse = sess.roleCheck(new PingReponse(),
				RightValue.ADMIN);
		if (!reponse.isOk()) {
			return reponse;
		}

		Element elem = new HttpPortalisUrlConnection().doConnection(sess,
				"clearActifUsers.jsp");
		
		return ClientConstants.ADMIN_REPONSE_XML_READER.getVoidReponse(elem);
	}


}
