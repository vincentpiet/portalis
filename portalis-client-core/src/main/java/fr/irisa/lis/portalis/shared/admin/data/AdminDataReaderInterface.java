package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RequestException;

public interface AdminDataReaderInterface<T> {

	public abstract Site getSite(T root)
			throws RequestException, PortalisException;

	public abstract ActiveSite getActiveSite(T root)
			throws RequestException, PortalisException;

	public abstract Application getApplication(T root)
			throws RequestException, PortalisException;

	public abstract ActiveLisService getActiveLisService(T root) throws PortalisException;

	public abstract ServiceCore getServiceCore(T root) throws PortalisException,
			RequestException;

	public abstract PortActif getPortActif(T root)
			throws RequestException, PortalisException;

	public abstract Date getLastUdate(T root) throws PortalisException;

	public abstract Session getSession(T root) throws PortalisException,
			RequestException;

	public abstract UserCore getUserCore(T root)
			throws PortalisException, RequestException;

	public abstract RightProperty getRightPropertyForUser(T root,
			String userEmail) throws PortalisException, RequestException;

	public abstract RightProperty getRightProperty(T root)
			throws PortalisException, RequestException;

}