package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class VoidReponse implements Serializable, AdminReponseVisitedObject {private static final Logger LOGGER = Logger.getLogger(VoidReponse.class.getName());

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER
				.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	protected List<String> messages = new ArrayList<String>();
	protected String status = XmlIdentifier.OK;

	public boolean canEqual(Object other) {
		return (other instanceof VoidReponse);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;

		if (!(aThat instanceof VoidReponse))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;

		final VoidReponse that = (VoidReponse) aThat;
		return new EqualsBuilder()
				// if deriving: appendSuper(super.equals(obj)).
				.append(this.messages, that.messages)
				.append(this.status, that.status)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				// if deriving: appendSuper(super.hashCode()).
				append(messages).
				append(status).
				toHashCode();
	}

	public VoidReponse() {
		super();
	}

	public VoidReponse(String status, List<String> messages) {
		super();
		this.status = status;
		this.messages = messages;
	}

	public VoidReponse(String status, String mess) {
		super();
		this.status = status;
		List<String> messages = new ArrayList<String>();
		messages.add(mess);
		this.messages = messages;
	}

	public VoidReponse(String mess, Throwable t) {
		this.addMessage(XmlIdentifier.ERROR, 
				String.format("%s\n%s\n%s", mess, t.getMessage(), Util.stack2string(t)));
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public List<String> getMessages() {
		return messages;
	}

	public String getMessagesAsString() {
		return Arrays.toString(messages.toArray(new String[messages.size()]));
	}

	public void setMessages(String status, List<String> messages) {
		this.status = status;
		this.messages = messages;
	}

	public void addMessage(String status, String message) {
		this.status = status;
		this.messages.add(message);
	}

	public void setMessages(String mess) {
		this.status = XmlIdentifier.ERROR;
		this.messages.add(mess);
	}

	public boolean messagesContains(String text) {
		LOGGER.fine("messagesContains(" + text + ") messages.size()="
				+ messages.size());
		boolean result = false;
		for (String mess : this.getMessages()) {
			LOGGER.fine("coucou : " + mess);
			if (mess.contains(text)) {
				LOGGER.fine("oui oui");
				result = true;
				break;
			}
		}
		LOGGER.fine(this + "\nmessagesContains(" + text + ") " + result);
		return result;
	}

	public String messagesAsString() {
		if (messages.size() == 0) {
			return "";
		} else
			return Arrays
					.toString(messages.toArray(new String[messages.size()]));
	}

	public boolean isOk() {
		return status.equals(XmlIdentifier.OK);
	}

}