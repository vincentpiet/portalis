package fr.irisa.lis.portalis.shared.camelis.reponse;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.LisReponse;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;

@SuppressWarnings("serial")
public class DelFeatureReponse extends LisReponse implements CamelisReponseObject {

	private final String xmlName = CamelisXmlName.DEL_FEATURE_REPONSE;
	private LisIncrementSet incrs;

	public DelFeatureReponse () {
		super();
		incrs = new LisIncrementSet();
	}

	public DelFeatureReponse (String status, String message) {
		super(status, message);
		incrs = new LisIncrementSet();
	}

	public <T> T accept(CamelisReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	public String getXmlName() {
		return xmlName ;
	}

	public LisIncrementSet getIncrements() {
		return incrs;
	}

	public void setIncrements(LisIncrementSet incrs) {
		this.incrs = incrs;
	}

	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_REPONSE_XML_WRITER.visit(this));
	}

}
