package fr.irisa.lis.portalis.shared.admin;

import fr.irisa.lis.portalis.shared.admin.data.UserCore;

public interface RoleChecker {

	public boolean isAuthorized(UserCore userCore, RightValue role);
	
}
