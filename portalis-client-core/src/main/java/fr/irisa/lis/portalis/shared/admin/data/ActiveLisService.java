package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class ActiveLisService extends ActiveService implements ServiceCoreInterface {
	
	@SuppressWarnings("unused")
	private static final Logger LOGGER = Logger.getLogger(ActiveLisService.class.getName());
	
	private String fullName;
	protected int pid;
	protected Date lastUpdate;
	protected int nbObject;
	private boolean contextLoaded = false;

	public ActiveLisService() {
		super();
	}

	public ActiveLisService(String fullName, String creator, String host1, int port,
			Date dateCreation, Date lastUpdate2, int pid2, int nbObject2) {
		super(creator, host1, port, dateCreation);
		this.fullName = fullName;
		this.lastUpdate = lastUpdate2;
		this.pid = pid2;
		this.nbObject = nbObject2;
	}
	
	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	@Override
	public boolean equals(Object aThat) {
		if (this == aThat)
			return true;
	
		if (!(aThat instanceof ActiveLisService))
			return false;
		// you may prefer this style, but see discussion in Effective Java
		// if ( aThat == null || aThat.getClass() != this.getClass() ) return
		// false;
	
		final ActiveLisService that = (ActiveLisService) aThat;
	    return new EqualsBuilder().
	            appendSuper(super.equals(aThat)).
	            append(this.lastUpdate, that.lastUpdate).
	            append(this.nbObject, that.nbObject).
	            append(this.pid, that.pid).
	            append(this.contextLoaded, that.contextLoaded).
	            isEquals();
	}

	@Override
	public int hashCode() {
	    return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
	        	appendSuper(super.hashCode()).
	            append(lastUpdate).
	            append(nbObject).
	            append(pid).
	            append(contextLoaded).
	        toHashCode();
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	public int getPid() {
		return pid;
	}

	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}

	public Date getLastUpdate() {
		return lastUpdate;
	}

	public void setNbObject(int nbObject) {
		this.nbObject = nbObject;
	}

	public int getNbObject() {
		return this.nbObject;
	}

	public boolean isContextLoaded() {
		return contextLoaded;
	}

	public void setContextLoaded(boolean contextLoaded) {
		this.contextLoaded = contextLoaded;
	}

	@Override
	public String getFullName() {
		return this.fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	@Override
	public String getAppliName() throws PortalisException {
		return extractAppliName(this.fullName);
	}

	@Override
	public String getServiceName() throws PortalisException {
		return extractServiceName(this.fullName);
	}
		
	public void setId(String id) throws PortalisException {
		this.fullName = extractFullName(id);
	}


}