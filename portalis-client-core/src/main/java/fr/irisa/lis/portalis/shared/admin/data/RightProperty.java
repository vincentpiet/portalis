package fr.irisa.lis.portalis.shared.admin.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class RightProperty implements AdminDataObject {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private String serviceName;
	protected String userEmail;
	
	private RightValue right;

	public RightProperty() {
	}

	public RightProperty(String userEmail, String serviceName, RightValue right) {
		this.userEmail = userEmail;
		this.serviceName = serviceName;
		this.right = right;
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof RightProperty) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final RightProperty that = (RightProperty)aThat;
	        return new EqualsBuilder().
	                // if deriving: appendSuper(super.equals(obj)).
	                append(this.userEmail, that.userEmail).
	                append(this.serviceName, that.serviceName).
	                append(this.right, that.right).
	                isEquals();
		}

	@Override
	public int hashCode() {
	    return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
	        // if deriving: appendSuper(super.hashCode()).
	            append(userEmail).
	            append(serviceName).
	            append(right).
	        toHashCode();
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getId() {
		return serviceName+"::"+userEmail;
	}

	public RightValue getRight() {
		return right;
	}

	public void setRight(RightValue right) {
		this.right = right;
	}


}
