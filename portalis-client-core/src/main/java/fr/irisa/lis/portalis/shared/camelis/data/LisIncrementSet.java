package fr.irisa.lis.portalis.shared.camelis.data;

import java.util.ArrayList;
import java.util.Arrays;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class LisIncrementSet extends CamelisDataObject {

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

	private ArrayList<LisIncrement> incrs;
	private static String xmlName = CamelisXmlName.INCRS;
	
	public LisIncrementSet() {
		super();
		this.incrs = new ArrayList<LisIncrement>();
	}

	public LisIncrementSet(LisIncrement[] props) {
		super();
		this.setIncrements(props);
	}

	public void setIncrements(LisIncrement[] props) {
		this.incrs = new ArrayList<LisIncrement>(Arrays.asList(props));
	}


	public ArrayList<LisIncrement> getIncrements() {
		return this.incrs;
	}
	
	public LisIncrement[] toArray() {
		return incrs.toArray(new LisIncrement[incrs.size()]);
	}

	public void add(LisIncrement x) {
		this.incrs.add(x);
	}
	
	public int card() {
		return this.incrs.size();
	}
	
	public boolean contains(String name) {
		for (LisIncrement i: incrs) {
			if (i.getName().equals(name)) return true;
		}
		return false;
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisIncrementSet) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LisIncrementSet that = (LisIncrementSet)aThat;
			return new EqualsBuilder()
			//.appendSuper(super.equals(aThat))
			.append(this.incrs, that.incrs)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(incrs).
				toHashCode();
	}

	public String getXmlName() {
		return xmlName;
	}

}
