package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.List;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;

@SuppressWarnings("serial")
public class ActiveSiteReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	private ActiveSite site;


	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof ActiveSiteReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final ActiveSiteReponse that = (ActiveSiteReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.site, that.site)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(site).
				toHashCode();
	}


	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

    public ActiveSiteReponse() {
    	super();
    }

	public ActiveSiteReponse(ActiveSite site) {
    	super();
		this.site = site;
	}

	public ActiveSiteReponse(String status, String message) {
		super(status, message);
	}

	public ActiveSiteReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public ActiveSiteReponse(String message) {
		super(XmlIdentifier.ERROR, message);
	}

	public ActiveSiteReponse(List<String> messages) {
		super(XmlIdentifier.ERROR, messages);
	}

	public ActiveSite getSite() {
		return site;
	}

	public void setSite(ActiveSite site) {
		this.site = site;
	}


}
