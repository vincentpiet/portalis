package fr.irisa.lis.portalis.shared.admin.data;

import fr.irisa.lis.portalis.shared.admin.PortalisException;

public interface ServiceCoreInterface {

	public abstract String getFullName();

	public abstract String getAppliName() throws PortalisException;

	public abstract String getServiceName() throws PortalisException;

}