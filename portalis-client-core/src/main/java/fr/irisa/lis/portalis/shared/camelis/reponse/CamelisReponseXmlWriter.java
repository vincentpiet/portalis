package fr.irisa.lis.portalis.shared.camelis.reponse;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.AdminDataXmlWriter;
import fr.irisa.lis.portalis.shared.admin.data.AdminXMLWriter;
import fr.irisa.lis.portalis.shared.admin.data.RightProperty;
import fr.irisa.lis.portalis.shared.admin.reponse.AdminReponseXmlWriter;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisDataXmlWriter;
import fr.irisa.lis.portalis.shared.camelis.data.CamelisUser;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;

public class CamelisReponseXmlWriter extends CamelisDataXmlWriter implements
		CamelisReponseWriterInterface<Element> {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(CamelisReponseXmlWriter.class.getName());

	static private CamelisReponseXmlWriter instance;
	
	public static CamelisReponseXmlWriter getInstance() {
		if (instance == null) {
			instance = new CamelisReponseXmlWriter();
		}
		return instance;
	}
	
	
	public Element visit(PingReponse pingReponse) {
		Element reponseElem = buildRootElement(
				XmlIdentifier.PING_REPONSE(), pingReponse);
		if (pingReponse.isOk()) {
			List<ActiveLisService> lesServices = pingReponse.getLesServices();
			if (lesServices.size() == 1) {
				ActiveLisService activeLisService = pingReponse
						.getFirstActiveLisService();
				AdminXMLWriter.addAttribute(reponseElem, XmlIdentifier.SERVICE_ID(),
								activeLisService.getActiveId());

				setActiveLisService(reponseElem, activeLisService);
				
				return reponseElem;
			} else {
				List<Element> lesElements = visit(lesServices);
				for (Element elem : lesElements) {
					addChild(reponseElem, elem);
				}
			}
		}
		return reponseElem;
	}


	private void setActiveLisService(Element reponseElem,
			ActiveLisService activeLisService) {
		addChild(reponseElem, visit(activeLisService));		
	}


	private List<Element> visit(List<ActiveLisService> activeLisServices) {
		List<Element> reponse = new ArrayList<Element>();
		for (ActiveLisService activeLisService : activeLisServices) {
			reponse.add(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(activeLisService));
		}
		return reponse;
	}


	public Element visit(StartReponse startReponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				XmlIdentifier.START_REPONSE(), startReponse);
		if (startReponse.isOk()) {
			AdminXMLWriter.addChild(reponseElem, ClientConstants.PORTALIS_DATA_XML_WRITER.visit(startReponse.getActiveLisService()));
		}
		return reponseElem;
	}

	
	public Element visit(ExtentReponse extentReponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.EXTENT_REPONSE, extentReponse);
		if (extentReponse.isOk()) {
			AdminXMLWriter.addChild(elem, visit(extentReponse.getExtent()));
			AdminXMLWriter.addAttribute(elem, 
					AdminProprietes.ioConstants.getProperty("LAST_UPDATE"),
					Util.writeDate(extentReponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(IntentReponse intentReponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.INTENT_REPONSE, intentReponse);
		if (intentReponse.isOk()) {
			LisIntent intent = intentReponse.getIntent();
			if (intent != null) {
				AdminXMLWriter.addChild(elem, visit(intent));
			}
			AdminXMLWriter.addAttribute(elem, 
					AdminProprietes.ioConstants.getProperty("LAST_UPDATE"),
					Util.writeDate(intentReponse.getLastUpdate()));
		}
		return elem;
	}

	public Element visit(XIntentReponse reponse) {
		Element elem = visit((IntentReponse) reponse);
		if (reponse.isOk()) {
			LisExtent extent = reponse.getExtent();
			if (extent != null) {
				AdminXMLWriter.addChild(elem, visit(extent));
			}
		}
		return elem;
	}

	
	public Element visit(ZoomReponse zoomReponse) {
        // WTF DomXMLWriter replaced by AdminReponseDomXmlVisitor?
		// Element elem = DomXMLWriter.createRootElement(CamelisXmlName.ZOOM_REPONSE);
		// DomXMLWriter.addAttribute(elem,XmlIdentifier.STATUS(), XmlIdentifier.OK());
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.ZOOM_REPONSE, zoomReponse);
		if (zoomReponse.isOk()) {
			//FIXME handle newReq in camelis-server and in ZoomReponse
			//elem.addAttribute(CamelisXmlName.NEW_REQ, zoomReponse.getNewReq());
			LisIncrementSet incrs = zoomReponse.getIncrements();
			if (incrs != null) {
				Element xmlIncrs = visit(incrs);
				AdminXMLWriter.addChild(elem, xmlIncrs);
			}
			AdminXMLWriter.addAttribute(elem, 
					AdminProprietes.ioConstants.getProperty("LAST_UPDATE"),
					Util.writeDate(zoomReponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(XZoomReponse zoomReponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.ZOOM_REPONSE, zoomReponse);
		if (zoomReponse.isOk()) {
			//FIXME handle newReq, cf. supra
			//elem.addAttribute(CamelisXmlName.NEW_REQ, zoomReponse.getNewReq());
			AdminXMLWriter.addChild(elem, visit(zoomReponse.getIncrements()));
			AdminXMLWriter.addChild(elem, visit(zoomReponse.getExtent()));
			AdminXMLWriter.addAttribute(elem, 
					AdminProprietes.ioConstants.getProperty("LAST_UPDATE"),
					Util.writeDate(zoomReponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(PropertyTreeReponse reponse) {
		Element elem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.PROPERTY_TREE_REPONSE, reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addChild(elem, visit(reponse.getTree()));
			AdminXMLWriter.addAttribute(elem, 
					AdminProprietes.ioConstants.getProperty("LAST_UPDATE"),
					Util.writeDate(reponse.getLastUpdate()));
		}
		return elem;
	}

	
	public Element visit(XPropertyTreeReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.PROPERTY_TREE_REPONSE, reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addChild(reponseElem, visit(reponse.getTree()));
			AdminXMLWriter.addChild(reponseElem, visit(reponse.getExtent()));
			AdminXMLWriter.addAttribute(reponseElem, 
					AdminProprietes.ioConstants.getProperty("LAST_UPDATE"),
					Util.writeDate(reponse.getLastUpdate()));
		}
		return reponseElem;
	}

	
	public Element visit(PingUsersReponse pingUsersReponse) {

		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.PING_USERS_REPONSE, pingUsersReponse);
		if (pingUsersReponse.isOk()) {
			AdminXMLWriter.addTextChild(reponseElem, CamelisXmlName.STATUS, CamelisXmlName.OK);
			AdminXMLWriter.addAttribute(reponseElem, CamelisXmlName.CREATOR,
							pingUsersReponse.getCreator());

			Element rightListElems = AdminXMLWriter.createRootElement(CamelisXmlName.ROLES);
			for (RightProperty prop : pingUsersReponse.getUsers()) {
				AdminXMLWriter.addChild(rightListElems, new AdminDataXmlWriter().visit(prop));
				AdminXMLWriter.addChild(reponseElem, rightListElems);
			}
		}

		return reponseElem;
	}

	
	public Element visit(RightPropertyReponse rightPropReponse) {
		// TODO à implémenter
		throw new NoSuchMethodError("RightPropertyReponse not yet emplemented");
	}

	
	public Element visit(CamelisUserReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				reponse.getElemName(), reponse);
		if (reponse.isOk()) {
			CamelisUser user = reponse.getUser();
			if (user!=null)
				AdminXMLWriter.addChild(reponseElem, visit(user));

		}
		return reponseElem;
	}

	
	public Element visit(ImportCtxReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.IMPORT_CTX_REPONSE, reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addAttribute(reponseElem, CamelisXmlName.CTX_FILE, reponse.getCtxFile());
		}
		return reponseElem;
	}


	
	public Element visit(ImportLisReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.IMPORT_LIS_REPONSE, reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addAttribute(reponseElem, CamelisXmlName.LIS_FILE, reponse.getLisFile());
		}
		return reponseElem;
	}


	public Element visit(DelKeyReponse delKeyReponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.DEL_KEY_REPONSE, delKeyReponse);
		return reponseElem;
	}

	
	public Element visit(ResetCreatorReponse resetCreatorReponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				CamelisXmlName.RESET_CREATOR_REPONSE, resetCreatorReponse);
		return reponseElem;
	}


	public Element visit(DelObjectsReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addChild(reponseElem, visit(reponse.getExtent()));
		}
		return reponseElem;
	}


	
	public Element visit(DelFeatureReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addChild(reponseElem, visit(reponse.getIncrements()));
		}
		return reponseElem;		
	}


	public Element visit(AddAxiomReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		return reponseElem;
	}



	public Element visit(XAddAxiomReponse reponse) {
		Element elem = visit((AddAxiomReponse) reponse);
		if (reponse.isOk()) {
			LisExtent extent = reponse.getExtent();
			if (extent != null) {
				AdminXMLWriter.addChild(elem, visit(extent));
			}
		}
		return elem;
	}

	
	public Element visit(ResetCamelisReponse reponse) {
		Element reponseElem = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		return reponseElem;
	}


	public Element visit(GetValuedFeaturesReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			for (int oid: reponse.getOids()) {
				Element obj = AdminXMLWriter.createRootElement("object");
				AdminXMLWriter.addAttribute(obj, "oid", Integer.toString(oid));
				for (String featname: reponse.getFeatnames(oid)) {
					Element prop = AdminXMLWriter.createRootElement("property");
					AdminXMLWriter.addAttribute(prop, "name", featname);
					AdminXMLWriter.addAttribute(prop, "value", reponse.getValue(oid,  featname));
					AdminXMLWriter.addChild(obj,  prop);
				}
				AdminXMLWriter.addChild(root, obj);
			}
		}
		return root;
	}


	@Override
	public Element visit(LoadContextReponse reponse) {
		Element root = AdminReponseXmlWriter.buildRootElement(
				reponse.getXmlName(), reponse);
		if (reponse.isOk()) {
			AdminXMLWriter.addAttribute(root, CamelisXmlName.CONTEXT_NAME, reponse.getContextName());
		}
		return root;		
	}


}
