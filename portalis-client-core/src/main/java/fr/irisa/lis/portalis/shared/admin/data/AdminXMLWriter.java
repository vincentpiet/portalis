package fr.irisa.lis.portalis.shared.admin.data;

import java.util.logging.Logger;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

//import org.apache.xml.serialize.XMLSerializer;

public class AdminXMLWriter {

	static final String attributeModele = "([a-zA-z]*=\\\".*?\\\"\\s*)";private static final Logger LOGGER = Logger.getLogger(AdminXMLWriter.class.getName());

	public static String voidReponseAsString(String mess, String stack) {
		return String
				.format("<%s %s='%s'><message>%s</message><stack><![CDATA[%s]]></stack></%s>",
						XmlIdentifier.VOID_REPONSE(), XmlIdentifier.STATUS(),
						XmlIdentifier.ERROR, mess, stack,
						XmlIdentifier.VOID_REPONSE());
	}

	public static String voidReponseAsString(String mess) {
		return String.format("<%s %s='%s'><message>%s</message></%s>",
				XmlIdentifier.VOID_REPONSE(), XmlIdentifier.STATUS(),
				XmlIdentifier.ERROR, mess, XmlIdentifier.VOID_REPONSE());
	}

	public static Document buildErrorReponse(String elemName, String mess) throws PortalisException {
			Document doc = XmlUtil.createDocument();
			Element rootElement = doc.createElement(elemName);
			doc.appendChild(rootElement);
			Element elemMessage = doc.createElement(XmlIdentifier.MESSAGE());
			rootElement.appendChild(elemMessage);

			// set attribute to status element
			Attr attr = doc.createAttribute(XmlIdentifier.STATUS());
			attr.setValue(XmlIdentifier.ERROR);
			rootElement.setAttributeNode(attr);

			elemMessage.setTextContent(mess);

			LOGGER.fine("buildErrorReponse(" + elemName + "+Util.stack2string( " + mess + ") ");
			return doc;
	}

	
	public static void addChild(Element parent, Element child) {
		Node node = parent.getOwnerDocument().importNode(child, true);
		parent.appendChild(node);
	}

	public static Element createRootElement(String nodeName) {
		Document doc = XmlUtil.createDocument();
		Element child = doc.createElement(nodeName);
		doc.appendChild(child);
		return child;
	}

	public static Element createAddChild(Element parent, String nodeName) {
		Document doc = parent.getOwnerDocument();
		Element child = doc.createElement(nodeName);
		parent.appendChild(child);
		return child;
	}

	public static void addAttribute(Element parent, String attName, String attrValue) {
		Document doc = parent.getOwnerDocument();
		Attr attr = doc.createAttribute(attName);
		parent.setAttributeNode(attr);
		attr.setValue(attrValue);
	}

	public static void addTextChild(Element parent, String nodeName, String text) {
		Document doc = parent.getOwnerDocument();
		Element child = doc.createElement(nodeName);
		child.setTextContent(text);
		parent.appendChild(child);
	}


}
