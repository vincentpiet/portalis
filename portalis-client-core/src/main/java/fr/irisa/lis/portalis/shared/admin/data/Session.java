package fr.irisa.lis.portalis.shared.admin.data;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.RoleChecker;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.reponse.VoidReponse;


public class Session implements AdminDataObject {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static class AllwaysRoleChecker implements RoleChecker {
		
		@Override
		public boolean isAuthorized(UserCore user, RightValue role) {
			return true;
		}
		
	}

	public final static RoleChecker roleChecker = new AllwaysRoleChecker();private static final Logger LOGGER = Logger.getLogger(Session.class.getName());
	
	List<String> cookies = new ArrayList<String> ();

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}
	
	public String toSmallString() {
		return getPortalisSessionId()+" "+getActiveUser().getUserCore().getEmail()+" "+getActiveService().getActivePartOfId();
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
	
	private ActiveService activeService;
	private ActiveUser activeUser;
	
	
	public Session() {}
	

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof Session) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final Session that = (Session)aThat;
	        return new EqualsBuilder().
	                // if deriving: appendSuper(super.equals(obj)).
	                append(this.activeService, that.activeService).
	                append(this.activeUser, that.activeUser).
	                append(this.cookies, that.cookies).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
                append(activeService).
                append(activeUser).
                append(cookies).
            toHashCode();
    }


	public String getHost() {
		return activeService.getHost();
	}
		
	public int getPort() {
		return activeService.getPort();
	}
	
	public ActiveUser getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(ActiveUser activeUser) {
		this.activeUser = activeUser;
	}


	public <T extends VoidReponse> T roleCheck(T reponse, RightValue role) {
		if (roleChecker.isAuthorized(this.activeUser.getUserCore(), role)) {
			return reponse;
		} else {
			String mess = "no " + role.toString() + " right for " + reponse.getClass().getName() + "";
			LOGGER.warning(mess);
			reponse.addMessage(XmlIdentifier.ERROR, mess);
			return reponse;
		}
	}

	public ActiveService getActiveService() {
		return activeService;
	}

	public void setActiveService(ActiveService activeService) {
		this.activeService = activeService;
	}

	public String getPortalisSessionId() {
		return activeUser.getPortalisSessionId();
	}

	public static Session createPortalisSession(ActiveUser activeUser2) throws PortalisException {
		Session sess = new Session();
		sess.setActiveService(PortalisService.getInstance());
		sess.setActiveUser(activeUser2);
		return sess;
	}
	
	public static Session createAnonymousPortalisSession() throws PortalisException {
		ActiveUser anonymousAdmin = ActiveUser.createAnonymousActiveAdmin();
		return createPortalisSession(anonymousAdmin);
	}

	
//	public static Session createAnonymousAdminCamelisSession(CamelisService camelisService) {
//		String host = camelisService.getHost();
//		int port = camelisService.getPort();
//		LOGGER.fine(String.format("getUnidentifiedSession(" +   + ", " +   + ")", host+Util.stack2string( port)));
//		ActiveUser anonymousAdmin = ActiveUser.createAnonymousActiveAdmin();
//		return createCamelisSession(anonymousAdmin, camelisService);
//	}
 

	public static Session createAnonymousAdminCamelisSession(ActiveLisService camelisService) {
		Session sess = new Session();
		sess.setActiveService(camelisService);
		sess.setActiveUser(ActiveUser.createAnonymousActiveAdmin());
		return sess;
	}
	
	public static Session createCamelisSession(Session loginSession,
			ActiveLisService camelisService) throws PortalisException {
		Session sess = new Session();
		sess.setActiveService(camelisService);
		sess.setActiveUser(loginSession.getActiveUser());
		List<String> cookies = loginSession.getCookies();
		if (cookies.size()<1) {
			throw new PortalisException("Cette session n'est pas une session de login : "+loginSession.getPortalisSessionId());
		}
		sess.setCookies(cookies);
		return sess;
	}

	public void setCookies(List<String> cookies) {
		this.cookies = cookies;
	}

	public void resetCookies() {
		this.cookies = new ArrayList<String> ();
	}

	public List<String> getCookies() {
		return cookies;
	}

	public static void isPortalisSession(Session sess) throws PortalisException {
		if (sess== null) {
			String mess = "null session";
			LOGGER.warning(mess);
			throw new PortalisException(mess);
		}
		PortalisService portalisService = PortalisService.getInstance();
		if (sess.getPort()!=portalisService.getPort() || !sess.getHost().equals(portalisService.getHost())) {
			String mess = "Cette session n'est pas une session portalis : '" +  sess.getHost() + ":" + sess.getPort() + 
					"', elle devrait être '" + portalisService.getHost() + ":" + portalisService.getPort() + "'";
			LOGGER.warning(mess);
			throw new PortalisException(mess);
		}
	}

	public static void isNotPortalis(Session sess) throws PortalisException {
		if (sess== null) {
			String mess = "null session";
			LOGGER.warning(mess);
			throw new PortalisException(mess);
		}
		PortalisService portalisService = PortalisService.getInstance();
		if (sess.getPort()==portalisService.getPort() && sess.getHost().equals(portalisService.getHost())) {
			String mess = "Cette session est une session portalis : '" +sess.getHost()+ ":" + sess.getPort() + "'";
			LOGGER.warning(mess);
			throw new PortalisException(mess);
		}
	}
}
