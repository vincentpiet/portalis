package fr.irisa.lis.portalis.shared.admin.data;

import java.util.Date;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;

@SuppressWarnings("serial")
public class ActiveUser implements AdminDataObject {private static final Logger LOGGER = Logger.getLogger(ActiveUser.class.getName());

	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.PORTALIS_DATA_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}


	
	private UserCore userCore;
	private String portalisSessionId;
	
	
	public ActiveUser() {

	}

	// TODO introduire le HttpClient en même temps que l'id de la session
	public ActiveUser(UserCore user, String sessionId) {
		this.userCore = user;
		try {
			this.portalisSessionId = createKey(user, sessionId);
			
		} catch (PortalisException e) {
			LOGGER.severe("impossible de créer une clé pour l'utilisateur "+user.getEmail());
		}
	}
	
	public static ActiveUser createAnonymousActiveUser() {
		return new ActiveUser(UserCore.ANONYMOUS_USER, Util.sha(new Date().toString()));
	}
	
	public static ActiveUser createAnonymousActiveAdmin() {
		return new ActiveUser(UserCore.ANONYMOUS_ADMIN, Util.sha(new Date().toString()));
	}
	
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof ActiveUser) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final ActiveUser that = (ActiveUser)aThat;
	        return new EqualsBuilder().
	                // if deriving: appendSuper(super.equals(that)).
	                append(this.userCore, that.userCore).
	                append(this.portalisSessionId, that.portalisSessionId).
	                isEquals();
		}

	@Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
            // if deriving: appendSuper(super.hashCode()).
                append(userCore).
                append(portalisSessionId).
            toHashCode();
    }


	private static String createKey(UserCore user, String sessionId) throws PortalisException {
		return Util.sha(user.getEmail()+sessionId);
	}

	public UserCore getUserCore() {
		return userCore;
	}

	public void setUserCore(UserCore userCore) {
		this.userCore = userCore;
	}


	public String getPortalisSessionId() {
		return portalisSessionId;
	}

	public void setPortalisSessionId(String portalisSessionId) {
		this.portalisSessionId = portalisSessionId;
	}

	public String toShortString() {
		return new StringBuffer(portalisSessionId).append(":").append(userCore.getEmail()).toString();
	}

}
