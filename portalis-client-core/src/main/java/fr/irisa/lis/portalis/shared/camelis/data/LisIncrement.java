package fr.irisa.lis.portalis.shared.camelis.data;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.camelis.CamelisXmlName;

@SuppressWarnings("serial")
public class LisIncrement extends Property {

	private int card;
	private final String xmlName = CamelisXmlName.INCR; 
	
	
	public LisIncrement() {
		super();
	}

	public LisIncrement(String name) {
		super(name);
	}

	public LisIncrement(String name, int card) {
		super(name);
		this.card = card;
	}

	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof LisIncrement) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final LisIncrement that = (LisIncrement)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.card, that.card)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(card).
				toHashCode();
	}

	
	public int getCard() {
		return this.card;
	}

	public String getXmlName() {
		return xmlName;
	}


	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.CAMELIS_DATA_XML_WRITER.visit(this));
	}


	@Override
	public <T> T accept(CamelisDataWriterInterface<T> visitor) {
		return visitor.visit(this);
	}
	
}
