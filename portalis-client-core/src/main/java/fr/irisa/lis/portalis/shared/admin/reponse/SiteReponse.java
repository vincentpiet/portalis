package fr.irisa.lis.portalis.shared.admin.reponse;

import java.io.Serializable;
import java.util.List;
import java.util.logging.Logger;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.Site;


@SuppressWarnings("serial")
public class SiteReponse extends VoidReponse implements Serializable, AdminReponseVisitedObject {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(SiteReponse.class.getName());
	
	private Site site;


	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof SiteReponse) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final SiteReponse that = (SiteReponse)aThat;
			return new EqualsBuilder()
			.appendSuper(super.equals(aThat))
			.append(this.site, that.site)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				appendSuper(super.hashCode()).
				append(site).
				toHashCode();
	}


	@Override
	public String toString() {
		return XmlUtil.prettyXmlString(ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(this));
	}

	@Override
	public <T> T accept(AdminReponseWriterInterface<T> visitor) {
		return visitor.visit(this);
	}

    public SiteReponse() {
    	super();
    }

	public SiteReponse(Site site) {
    	super();
		this.site = site;
	}

	public SiteReponse(String status, String message) {
		super(status, message);
	}

	public SiteReponse(String status, List<String> messages) {
		super(status, messages);
	}

	public SiteReponse(String message) {
		super(XmlIdentifier.ERROR, message);
	}

	public SiteReponse(List<String> messages) {
		super(XmlIdentifier.ERROR, messages);
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}


}
