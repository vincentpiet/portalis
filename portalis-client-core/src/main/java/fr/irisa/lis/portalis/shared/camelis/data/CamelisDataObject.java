package fr.irisa.lis.portalis.shared.camelis.data;

import java.io.Serializable;


@SuppressWarnings("serial")
public abstract class CamelisDataObject implements Serializable {

	public abstract <T> T accept(CamelisDataWriterInterface<T> visitor);
	
	@Override
	public abstract String toString();

}
