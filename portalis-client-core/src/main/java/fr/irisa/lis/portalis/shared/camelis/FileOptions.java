package fr.irisa.lis.portalis.shared.camelis;



public enum FileOptions {
	recursive, parts, suffixes;
	
	public static String[] parse(String string) throws CamelisException {
		String[] options = string.split("::");
		
		try {
		for (int i=0; i<options.length; i++) {
			FileOptions.valueOf(options[i]);
		}
		} catch (Exception e) {
			String mess = "Liste d'options incorrecte : "+string;
			throw new CamelisException(mess);
		}
		return options;
	}
}
