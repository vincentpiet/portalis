package fr.irisa.lis.portalis.shared.admin.data;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.test.CoreTestConstants;


public class TestAdminData {private static final Logger LOGGER = Logger.getLogger(TestAdminData.class.getName());


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @BeforeClass : TestPortalisCore        |"
				+ "\n            ----------------------------------------------------\n");
		PortalisService.getInstance().init("localhost", 8080, "portalis");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @AfterClass : TestPortalisCore         |"
				+ "\n            ----------------------------------------------------\n");
	}

	@Test
	public void testServiceCore1() {
		try {
			LOGGER.info("--------------> @Test : testServiceCore1\n");
			ServiceCore in = new ServiceCore("appli", "service");
			LOGGER.info("\nin  = " + in);
			ServiceCore in1 = new ServiceCore("appli1", "service");
			ServiceCore in2 = new ServiceCore("appli", "service1");
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin = "
							+ in1 + "\nin1 = " + in1, !in.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin = "
							+ in2 + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin1 = "
							+ in1 + "\nin2 = " + in2, !in1.equals(in2));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			ServiceCore out = ClientConstants.ADMIN_DATA_XML_READER.getServiceCore(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("les deux objets ServiceCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testServiceCore2() {
		try {
			LOGGER.info("--------------> @Test : testServiceCore2\n");
			ServiceCore in = new ServiceCore("appli", "service");
			LOGGER.info("\nin  = " + in);
			ServiceCore in1 = new ServiceCore("appli", "service");
			ServiceCore in2 = new ServiceCore("appli1", "service");
			ServiceCore in3 = new ServiceCore("appli", "service1");
			assertTrue("les deux objets ServiceCore doivent être égaux\nin = "
					+ in + "\nin1 = " + in1, in.equals(in1));
			assertTrue("les deux objets ServiceCore doivent être égaux\nin1 = "
					+ in1 + "\nin = " + in, in1.equals(in));
			assertTrue("les deux objets ServiceCore doivent être égaux\nin1 = "
					+ in1 + "\nin1 = " + in1, in1.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin3 = "
							+ in3 + "\nin1 = " + in1, !in3.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin1 = "
							+ in1 + "\nin3 = " + in3, !in1.equals(in3));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin2 = "
							+ in2 + "\nin1 = " + in1, !in2.equals(in1));
			assertTrue(
					"les deux objets ServiceCore doivent être différents\nin1 = "
							+ in1 + "\nin2 = " + in2, !in1.equals(in2));

			List<ServiceCore> list1 = new ArrayList<ServiceCore>();
			list1.add(in3);
			list1.add(in);
			list1.add(in1);
			List<ServiceCore> list2 = new ArrayList<ServiceCore>();
			list2.add(in);
			list2.add(in3);
			assertTrue("les deux objets HashSet doivent être égaux",
					new HashSet<ServiceCore>(list1)
							.equals(new HashSet<ServiceCore>(list2)));
			list2.add(in3);
			assertTrue("les deux objets HashSet doivent être égaux",
					new HashSet<ServiceCore>(list1)
							.equals(new HashSet<ServiceCore>(list2)));
			list2.add(in2);
			assertTrue("les deux objets HashSet doivent être différents",
					!new HashSet<ServiceCore>(list1)
							.equals(new HashSet<ServiceCore>(list2)));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	

	@Test
	public void testUserCore1() {
		try {
			LOGGER.info("--------------> @Test : testUserCore1\n");
			UserCore in = UserCore.ANONYMOUS_USER;

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			LOGGER.fine(XmlUtil.prettyXmlString(elem));

			UserCore out = ClientConstants.ADMIN_DATA_XML_READER.getUserCore(elem);
			assertTrue(out!=null);
			assertTrue("in et ou doivent être égaux\nin="+in+"ou=\n"+out,in.equals(out));
		
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testUserCore() {
		try {
			LOGGER.info("--------------> @Test : testUserCore\n");
			UserCore in = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES, true);

			UserCore in2 = new UserCore(CoreTestConstants.MAIL_BENJAMIN, 
					CoreTestConstants.PSEUDO_BENJAMIN, CoreTestConstants.PASSWORD_BENJAMIN,
					true);
			assertTrue(
					"les deux objets UserCore doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets UserCore doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in2.equals(in));

			UserCore in3 = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES, false);
			assertTrue("les deux objets UserCore doivent être différent\nin = "
					+ in + "\nin3 = " + in3, !in.equals(in3));

			UserCore in4 = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES, true);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in.equals(in4));
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in4.equals(in));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			UserCore out = ClientConstants.ADMIN_DATA_XML_READER.getUserCore(elem);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testSite() {
		try {
			LOGGER.info("--------------> @Test : testSite\n");
			Site in = CoreTestConstants.PORTALIS_SITE;

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			Site out = ClientConstants.ADMIN_DATA_XML_READER.getSite(elem);
			assertTrue("les deux objets Site doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
			assertEquals(3, out.getApplications().size());
			assertEquals(CoreTestConstants.APPLI1, out.getApplications().get(CoreTestConstants.APPLI_ID1));
			assertEquals(CoreTestConstants.PLANETS_PLANETS, out.getServiceCore(CoreTestConstants.PLANETS_PLANETS.getFullName()));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testActiveSite() {
		try {
			LOGGER.info("--------------> @Test : testActiveSite\n");
			List<ActiveLisService> activeServices = new ArrayList<ActiveLisService>();
			activeServices.add(CoreTestConstants.cam1);
			activeServices.add(CoreTestConstants.cam2);
			activeServices.add(CoreTestConstants.cam3);
			ActiveSite in = new ActiveSite(CoreTestConstants.PORTALIS_SITE.getApplications(), activeServices);
			

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			ActiveSite out = ClientConstants.ADMIN_DATA_XML_READER.getActiveSite(elem);
			assertTrue("les deux objets ActiveSite doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
			assertEquals(3, out.getApplications().size());
			assertEquals(CoreTestConstants.APPLI1, out.getApplications().get(CoreTestConstants.APPLI_ID1));
			assertEquals(CoreTestConstants.cam1, 
					out.getActiveLisService(CoreTestConstants.cam1.getActiveId()));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testApplication() {
		try {
			LOGGER.info("--------------> @Test : testApplication\n");
			Application in1 = CoreTestConstants.PLANETS;
			in1.add(new ServiceCore(CoreTestConstants.PLANETS_ID, "photo"));
			Application in2 = CoreTestConstants.APPLI1;
			Application in3 = CoreTestConstants.APPLI2;
			Application in4 = new Application(CoreTestConstants.APPLI_ID1);
			in4.add(new ServiceCore(CoreTestConstants.APPLI_ID1, "truc"));
			Application in5 = new Application(CoreTestConstants.APPLI_ID1);
			in5.add(new ServiceCore(CoreTestConstants.APPLI_ID1, CoreTestConstants.SERVICE_NAME1));
			assertTrue(
					"les deux objets Application doivent être différents\nin1 = "
							+ in1 + "\nin2 = " + in2, !in1.equals(in2));
			assertTrue(
					"les deux objets Application doivent être différents\nin1 = "
							+ in1 + "\nin3 = " + in3, !in1.equals(in3));
			assertTrue(
					"les deux objets Application doivent être différents\nin4 = "
							+ in4 + "\nin2 = " + in2, !in4.equals(in2));
			assertTrue("les deux objets UserData doivent être égaux\nin2 = "
					+ in2 + "\nin5 = " + in5, in2.equals(in5));
			
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in1);
			assertNotNull(elem);
			
			Application out = ClientConstants.ADMIN_DATA_XML_READER.getApplication(elem);
			

			assertTrue("les deux objets Application doivent être égaux\nin1 = "
					+ in1 + "\nout = " + out, in1.equals(out));


		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void TestApplication() {
		try {
			LOGGER.info("--------------> @Test : TestApplication\n");

			Application in = CoreTestConstants.PLANETS;
			
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			Application out = ClientConstants.ADMIN_DATA_XML_READER.getApplication(elem);
			

			assertTrue("les deux objets ActiveApplication doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void TestActiveLisService() {
		try {
			LOGGER.info("--------------> @Test : TestActiveLisService\n");
			
			ActiveLisService in =
					new ActiveLisService(CoreTestConstants.SC1.getFullName(),
							CoreTestConstants.MAIL_BENJAMIN,
							CoreTestConstants.HOST,
							CoreTestConstants.PORT, new Date(), new Date(), 1234, 567);
				
			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			ActiveLisService out = ClientConstants.ADMIN_DATA_XML_READER.getActiveLisService(elem);
			
			assertTrue("les deux objets ActiveLisService doivent être égaux\nin1 = "
					+ in + "\nout = " + out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testUserData() {
		try {
			LOGGER.info("--------------> @Test : testUserData\n");
			UserCore in = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES, true);

			UserCore in2 = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_BENJAMIN, CoreTestConstants.PASSWORD_BENJAMIN, true);
			assertTrue(
					"les deux objets UserData doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in.equals(in2));
			assertTrue(
					"les deux objets UserData doivent être différents\nin = "
							+ in + "\nin2 = " + in2, !in2.equals(in));

			UserCore in3 = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES, true);
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin3 = " + in3, in.equals(in3));

			UserCore in4 = new UserCore(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.PSEUDO_YVES, CoreTestConstants.PASSWORD_YVES, true);
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in.equals(in4));
			assertTrue("les deux objets UserData doivent être égaux\nin = "
					+ in + "\nin4 = " + in4, in4.equals(in));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			UserData out = ClientConstants.ADMIN_DATA_XML_READER.getUserCore(elem);
			assertTrue("les deux objets UserCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testRightProperty() {
		try {
			LOGGER.info("--------------> @Test : testUserData\n");
			RightProperty in = new RightProperty(CoreTestConstants.MAIL_YVES, 
					CoreTestConstants.SERVICE_FULL_NAME1, RightValue.NONE);
			
			RightProperty in1 = new RightProperty(CoreTestConstants.MAIL_YVES,
					CoreTestConstants.SERVICE_FULL_NAME2, RightValue.NONE);
			assertTrue(
					"les deux objets RightProperty doivent être différents\nin = "
							+ in + "\nin2 = " + in1, !in.equals(in1));
			
			RightProperty in2 = new RightProperty(CoreTestConstants.MAIL_YVES,
					CoreTestConstants.SERVICE_FULL_NAME1, RightValue.ADMIN);
			assertTrue("les deux objets RightProperty doivent être différents\nin = "
					+ in + "\nout = " + in2, !in.equals(in2));

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			RightProperty out = ClientConstants.ADMIN_DATA_XML_READER.getRightProperty(elem);
			assertTrue("les deux objets RightProperty doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testPortActif() {
		LOGGER.info("--------------> @Test : testPortActif\n");
		try {
			String SERVER = PortalisService.getInstance().getHost();
			int PORT = 8090;
			int PID = 12345;
			PortActif in = new PortActif(SERVER, PORT, PID);

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			PortActif out = ClientConstants.ADMIN_DATA_XML_READER.getPortActif(elem);
			LOGGER.info("\nout = " + out);

			assertTrue("les deux objets ServiceCore doivent être égaux\nin = "
					+ in + "\nout = " + out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testServiceId() {
		try {
			LOGGER.info("--------------> @Test : testServiceId\n");
			ActiveService service = PortalisService.getInstance();
			service.getActiveId();
	
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testSession() {
		try {
			LOGGER.info("--------------> @Test : testSession\n");
			Session in = Session.createPortalisSession(CoreTestConstants.AU1);

			Element elem = ClientConstants.PORTALIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			Session out = ClientConstants.ADMIN_DATA_XML_READER.getSession(elem);
			assertTrue(out!=null);
			assertTrue(out.getActiveService()!=null);
			assertTrue(out.getActiveUser()!=null);
			
			assertTrue("in et ou doivent être égaux\nin="+in+"ou=\n"+out,in.equals(out));
		
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

}
