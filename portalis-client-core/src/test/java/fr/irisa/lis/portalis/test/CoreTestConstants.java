package fr.irisa.lis.portalis.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import fr.irisa.lis.portalis.shared.admin.AdminProprietes;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.ActiveUser;
import fr.irisa.lis.portalis.shared.admin.data.Application;
import fr.irisa.lis.portalis.shared.admin.data.ServiceCore;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.shared.admin.data.UserCore;


public class CoreTestConstants {

	@SuppressWarnings("unused")private static final Logger LOGGER = Logger.getLogger(CoreTestConstants.class.getName());
	
	public static final String ADMIN_KEY = "12AE43BC58F";
    public static final String USER_KEY = "1234567890ABCDEF";

	public static final String MAIL_YVES = "bekkers@irisa.fr";
	public static final String PSEUDO_YVES = "bekkers";
	public static final String PASSWORD_YVES = "yves";
	
    public static final String MAIL_BENJAMIN = "benjamin@dromaludaire.info";
	public static final String PSEUDO_BENJAMIN = "benjamin";
	public static final String PASSWORD_BENJAMIN = "benjamin";

    public static final String BAD_MAIL = "test";

	public static final String SERVICE_PLANETS_PHOTO = "planets:photo";
	public static final String SERVICE_PLANETS_PLANETS = "planets:planets";
	public static final String APPLI_ID1 = "appli1";
	public static final String APPLI_ID2 = "appli2";
	public static final String SERVICE_FULL_NAME1 = "appli:service1";
	public static final String SERVICE_FULL_NAME2 = "appli:service2";
	public static final String SERVICE_NAME1 = "service1";
	public static final String SERVICE_NAME2 = "service2";
	public static final String HOST = "tradewind";
	public static final int PORT = 8090;
	public static final int PORT1 = 8888;
	public static final int PORT2 = 8889;
	public static final Date DATE1 = new Date();
	public static final Date DATE2 = null;
	public static final int PID1 = 14256;
	public static final int PID2 = 99999;
	public static final int NB_OBJECT1 = 32;
	public static final int NB_OBJECT2 = 0;
	public static final String SESS_ID1 = "123321";
	public static final String SESS_ID2 = "ABCCBA";

	public static final ServiceCore SC1 = new ServiceCore(APPLI_ID1,
			SERVICE_NAME1);
	public static final ActiveLisService cam1 = new ActiveLisService(SC1.getFullName(), MAIL_BENJAMIN, HOST, PORT, new Date(), null, PID1, 15);
	public static final ActiveLisService cam2 = new ActiveLisService(SC1.getFullName(), MAIL_YVES, HOST, PORT1, new Date(), null, PID2, 1500);
	
	public static final ServiceCore SC2 = new ServiceCore(APPLI_ID2,
			SERVICE_NAME2);
	public static final ActiveLisService cam3 = new ActiveLisService(SC2.getFullName(), MAIL_YVES, HOST, PORT2, new Date(), null, 4321, 9999);

	public static final UserCore UC1 = new UserCore(MAIL_YVES, PSEUDO_YVES,
			PASSWORD_YVES, true);
	public static final UserCore UC2 = new UserCore(MAIL_BENJAMIN, PSEUDO_BENJAMIN,
			PASSWORD_BENJAMIN, false);

	public static final ActiveUser AU1 = new ActiveUser(UC1, new Date().toString());
	public static final ActiveUser AU2 = new ActiveUser(UC2, new Date().toString());

	public static final String PLANETS_ID = "planets";
	public static final Application PLANETS = new Application(PLANETS_ID);
	public static final Application APPLI1 = new Application(APPLI_ID1);
	public static final Application APPLI2 = new Application(APPLI_ID2);
	public static final ServiceCore PLANETS_PLANETS = new ServiceCore(PLANETS.getId(),
			"planets");
	
	public static final Map<String, Application> applications = new HashMap<String, Application>();

	public static final Site PORTALIS_SITE = new Site(applications);
	public static ActiveSite ACTIVE_SITE = null;
	
	public static final String WEBAPPS_DIR =
			AdminProprietes.portalis.getProperty("admin.webappsDirPath");

	
	static {
		PLANETS.add(PLANETS_PLANETS);
		applications.put(PLANETS.getId(), PLANETS);
		APPLI1.add(SC1);
		APPLI2.add(SC2);
		PORTALIS_SITE.addApplication(APPLI1);
		PORTALIS_SITE.addApplication(APPLI2);
		AU2.getUserCore().addRole(PLANETS_PLANETS.getFullName(), RightValue.ADMIN);
		List<ActiveLisService> activeServices = new ArrayList<ActiveLisService>();
		activeServices.add(CoreTestConstants.cam1);
		activeServices.add(CoreTestConstants.cam2);
		activeServices.add(CoreTestConstants.cam3);
		ACTIVE_SITE = new ActiveSite(CoreTestConstants.PORTALIS_SITE.getApplications(), activeServices);

	}
	
}
