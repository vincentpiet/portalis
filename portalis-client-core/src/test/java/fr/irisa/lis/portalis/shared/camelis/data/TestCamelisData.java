package fr.irisa.lis.portalis.shared.camelis.data;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Date;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.RightValue;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.test.CamelisTestConstants;

public class TestCamelisData {private static final Logger LOGGER = Logger.getLogger(TestCamelisData.class.getName());


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @BeforeClass : TestCamelisCore        |"
				+ "\n            ----------------------------------------------------\n");
		PortalisService.getInstance().init("localhost", 8080, "portalis");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @AfterClass : TestCamelisCore         |"
				+ "\n            ----------------------------------------------------\n");
	}

	@Test
	public void testCamelisUser() {
		LOGGER.info("--------------> @Test : testPortActif\n");
		try {
			String KEY = "123A5DE3F";
			Date Expires = new Date();
			RightValue ROLE = RightValue.COLLABORATOR;
			CamelisUser in = new CamelisUser(KEY, ROLE, Expires);

			Element elem = ClientConstants.CAMELIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			CamelisUser out = ClientConstants.CAMELIS_DATA_XML_READER.getCamelisUser(elem);
			LOGGER.info("\nout = "+out);

			assertTrue("les deux objets CamelisUser doivent être égaux\nin = "
					+in+"\nout = "+out, in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testLisObject() {
		LOGGER.info("--------------> @Test : testLisObject\n");
		try {
            final int IN_ID = 42;
            final String IN_REF = "plop";
			LisObject in = new LisObject(IN_ID, IN_REF);

			Element elem = ClientConstants.CAMELIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			LisObject out = ClientConstants.CAMELIS_DATA_XML_READER.getLisObject(elem);
			LOGGER.info("\nout = "+out);

			assertTrue("Les deux objets LisObject doivent être égaux\nin = " + in + "\nout = " + out,
                       in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testLisExtent() {
		LOGGER.info("--------------> @Test : testLisExtent\n");
		try {		
			LisExtent in = new LisExtent(CamelisTestConstants.OBJS);

			Element elem = ClientConstants.CAMELIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			LisExtent out = ClientConstants.CAMELIS_DATA_XML_READER.getLisExtent(elem);
			LOGGER.info("\nout = "+out);

			assertTrue("Les deux objets LisExtent doivent être égaux\nin = " + in + "\nout = " + out,
                       in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	
	@Test
	public void testLisIntent() {
		LOGGER.info("--------------> @Test : testLisIntent\n");
		try {
			LisIntent in = new LisIntent(CamelisTestConstants.PROPS);
			Element elem = ClientConstants.CAMELIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			LisIntent out = ClientConstants.CAMELIS_DATA_XML_READER.getLisIntent(elem);
			LOGGER.info("\nout = "+out);

			assertTrue("Both LisIntent objects should be equal\nin = " + in + "\nout = " + out,
                       in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	
	@Test
	public void testLisIncrement() {
		LOGGER.info("--------------> @Test : testLisIncrement\n");
		try {
			LisIncrement in = new LisIncrement("fooIncr", 42);
			Element elem = ClientConstants.CAMELIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);

			LisIncrement out = ClientConstants.CAMELIS_DATA_XML_READER.getIncrement(elem);
			LOGGER.info("\nout = "+out);

			assertTrue("Les deux objets LisObject doivent être égaux\nin = " + in + "\nout = " + out,
                       in.equals(out));

		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testLisIncrementSet() {
		LOGGER.info("--------------> @Test : testLisIncrementSet\n");
		try {
			LisIncrementSet in = new LisIncrementSet(CamelisTestConstants.INCRS);
			Element elem = ClientConstants.CAMELIS_DATA_XML_WRITER.visit(in);
			assertNotNull(elem);
			
			LisIncrementSet out = ClientConstants.CAMELIS_DATA_XML_READER.getLisIncrementSet(elem);
			LOGGER.info("\nout = "+out);
			assertTrue("Both LisIncrementSet objects should be equal\nin = " + in + "\nout = " + out,
                       in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
}
