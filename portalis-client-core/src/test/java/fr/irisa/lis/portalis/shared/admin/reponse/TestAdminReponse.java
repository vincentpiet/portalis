package fr.irisa.lis.portalis.shared.admin.reponse;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.logging.Logger;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.w3c.dom.Element;

import fr.irisa.lis.portalis.shared.admin.ClientConstants;
import fr.irisa.lis.portalis.shared.admin.Util;
import fr.irisa.lis.portalis.shared.admin.XmlIdentifier;
import fr.irisa.lis.portalis.shared.admin.XmlUtil;
import fr.irisa.lis.portalis.shared.admin.data.ActiveSite;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.admin.data.Site;
import fr.irisa.lis.portalis.test.CoreTestConstants;

public class TestAdminReponse {private static final Logger LOGGER = Logger.getLogger(TestAdminReponse.class.getName());

		

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @BeforeClass : TestAdminCore        |"
				+ "\n            ----------------------------------------------------\n");
		PortalisService.getInstance().init("localhost", 8080, "portalis");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ----------------------------------------------------"
				+ "\n            |            @AfterClass : TestAdminCore         |"
				+ "\n            ----------------------------------------------------\n");
	}

	@Test
	public void testSiteReponse() {
		try {
			LOGGER.info("--------------> @Test : testSiteReponse\n");
			Site site = CoreTestConstants.PORTALIS_SITE;
			SiteReponse in = new SiteReponse(site);
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK, elem.getAttribute(XmlIdentifier.STATUS()));
			
			SiteReponse out = ClientConstants.ADMIN_REPONSE_XML_READER.getSiteReponse(elem);
			
			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertTrue("les deux objets SiteReponse doivent être égaux", in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testActiveSiteReponse() {
		try {
			LOGGER.info("--------------> @Test : testActiveSiteReponse\n");
			ActiveSite site = CoreTestConstants.ACTIVE_SITE;
			ActiveSiteReponse in = new ActiveSiteReponse(site);
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK, elem.getAttribute(XmlIdentifier.STATUS()));
			
			ActiveSiteReponse out = ClientConstants.ADMIN_REPONSE_XML_READER.getActiveSiteReponse(elem);
			
			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertTrue("les deux objets ActiveSiteReponse doivent être égaux", in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	

			@Test
	public void testPidReponse() {
		try {
			LOGGER.info("--------------> @Test : testPidReponse\n");
			HashSet<Integer> pidSet1 = new HashSet<Integer>();
			pidSet1.add(new Integer(15));
			pidSet1.add(new Integer(16));
			PidReponse in = new PidReponse(pidSet1);
			HashSet<Integer> pidSet2 = new HashSet<Integer>();
			pidSet2.add(new Integer(16));
			pidSet2.add(new Integer(15));
			PidReponse in1 = new PidReponse(pidSet2);
			assertTrue("les deux objets PidReponse doivent être égaux", in.equals(in1));
			assertTrue("les deux objets PidReponse doivent être différents", 
					!in.equals(new PidReponse(new HashSet<Integer>())));
			
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK, elem.getAttribute(XmlIdentifier.STATUS()));
			
			PidReponse out = ClientConstants.ADMIN_REPONSE_XML_READER.getPidReponse(elem);
			LOGGER.info("\nout = "+out);
			
			assertEquals(XmlIdentifier.OK, out.getStatus());
			assertEquals(in, out);

			assertTrue("Les deux hashSet doivent égaux", in.getPids()
					.equals(out.getPids()));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}

	@Test
	public void testVoidReponse() {
		LOGGER.info("--------------> @Test : testVoidReponse\n");
		try {
			VoidReponse in = new VoidReponse();
			LOGGER.info("\nin = "+in);
			VoidReponse in1 = new VoidReponse();
			assertTrue("les deux objets VoidReponse doivent être égaux\nin = "
					+in+"\nin1 = "+in1, in.equals(in1));
			
			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);

			VoidReponse out = ClientConstants.ADMIN_REPONSE_XML_READER.getVoidReponse(elem);
			LOGGER.info("\nout = "+out);
			
			assertTrue("les deux objets VoidReponse doivent être égaux\nin = "
					+in+"\nout = "+out, in.equals(out));
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}


	@Test
	public void testLoginReponseAvecErreurs() {
		try {
			LOGGER.info("--------------> @Test : testLoginReponseAvecErreurs\n");
			
			List<String> messages = new ArrayList<String>();
			String mess1 = "message N°1";
			messages.add(mess1);
			String mess2 = "message N°2";
			messages.add(mess2);
			LoginReponse in = new LoginReponse(XmlIdentifier.ERROR, messages);

			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.ERROR, in.getStatus());
			
			LoginReponse out = ClientConstants.ADMIN_REPONSE_XML_READER.getLoginReponse(elem);
			assertTrue("in et ou doivent être égaux\nin="+in+"ou=\n"+out,in.equals(out));
			assertTrue("le message "+mess1+" doit être présent", out.messagesContains(mess1));
			assertTrue("le message "+mess2+" doit être présent", out.messagesContains(mess2));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
	@Test
	public void testLoginReponse() {
		try {
			LOGGER.info("--------------> @Test : testLoginReponse\n");
			
			LoginReponse in = new LoginReponse(Session.createPortalisSession(CoreTestConstants.AU1));
			LOGGER.info("\nin = "+in);

			Element elem = ClientConstants.ADMIN_REPONSE_XML_WRITER.visit(in);
			assertNotNull(elem);
			assertEquals(XmlIdentifier.OK, in.getStatus());
			LOGGER.info("elem= "+XmlUtil.prettyXmlString(elem));
			
			LoginReponse out = ClientConstants.ADMIN_REPONSE_XML_READER.getLoginReponse(elem);
			LOGGER.info("\nout = "+out);
			assertTrue(out!=null);
			assertTrue("in et ou doivent être égaux\nin="+in+"ou=\n"+out,in.equals(out));
			
		} catch (Exception e) {
			fail("Exception rencontrée lors du test : " + e.getMessage()
					+ "\nStack :\n" + Util.stack2string(e));
		}
	}
	
}
