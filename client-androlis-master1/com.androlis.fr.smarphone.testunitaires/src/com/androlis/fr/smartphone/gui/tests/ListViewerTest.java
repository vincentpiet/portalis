package com.androlis.fr.smartphone.gui.tests;

import android.test.AndroidTestCase;

import com.androlis.fr.smartphone.gui.list.ApplicationView;
import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.modele.data.Application;

public class ListViewerTest extends AndroidTestCase {

	private ListViewer list;
	
	private ApplicationView app1;
	private ApplicationView app2; 
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		list = new ListViewer(getContext());
		app1 = new ApplicationView(new Application("app1"));
		app2 = new ApplicationView(new Application("app2"));
		list.add(app1);
		list.add(app2);
	}

	public void testListContenu() throws Exception {
		assertEquals(list.getCount(), 2);
	}
	
	public void testListOrdre() throws Exception{
		assertEquals(list.getItem(0), app1);
		assertEquals(list.getItem(1), app2);
	}
	
}
