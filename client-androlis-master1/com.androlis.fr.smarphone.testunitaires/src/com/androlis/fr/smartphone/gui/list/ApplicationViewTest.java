package com.androlis.fr.smartphone.gui.list;

import com.androlis.fr.smartphone.modele.data.Application;

import android.test.AndroidTestCase;

public class ApplicationViewTest extends AndroidTestCase {

	private ApplicationView view;
	private Application app;
	private String nomApplication;
	
	@Override
	protected void setUp() throws Exception {
		nomApplication = "App";
		app = new Application(nomApplication);
		view = new ApplicationView(app);
		
		super.setUp();
	}
	
	public void testName() throws Exception {
		assertEquals(app, view.getDataModel());
	}
	
}
