import java.util.ArrayList;
public class Arete {
	public Segment s1;
	public Segment s2;
	public double dist;
	public ArrayList<Segment> chemin;
	public Arete (Segment s1, Segment s2, double dist, ArrayList<Segment> chemin) {
		this.s1 = s1;
		this.s2 = s2;
		this.dist = dist;
		this.chemin = chemin;
	}
	
	public Arete(Arete a) {
		this.s1.p1.x = a.s1.p1.x;
		this.s1.p1.y = a.s1.p1.y;
		this.s1.p2.x = a.s1.p2.x;
		this.s1.p2.y = a.s1.p2.y;
		this.s2.p1.x = a.s2.p1.x;
		this.s2.p1.y = a.s2.p1.y;
		this.s2.p2.x = a.s2.p2.x;
		this.s2.p2.y = a.s2.p2.y;
		this.dist = a.dist;
		this.chemin = new ArrayList();
	}
	
	public Arete maj(Arete a) {
		return new Arete(new Segment(new Point(a.s1.p1.x,a.s1.p1.y),new Point(a.s1.p2.x,a.s1.p2.y)),new Segment(new Point(a.s2.p1.x,a.s2.p1.y),new Point(a.s2.p2.x,a.s2.p2.y)),a.dist,new ArrayList(a.chemin));
	}
	public Boolean egal(Arete a) {
		return ((this.s1.equals(a.s1))&& (this.s2.equals(a.s2)));
	}
	public String toString() {
		String resultat = s1.toString() + " ---- " + dist +  " ----> " + s2.toString() + " [";
		for (int i = 0; i < chemin.size();i++) {
			resultat = resultat + chemin.get(i) + " ";
		}
		return resultat + "]";
	}
}
