
public class Segment {
	public Point p1;
	public Point p2;
	
	public Segment(Point p1, Point p2) {
		this.p1 = p1;
		this.p2 = p2;
	}
	
	public double longueur() {
		return Math.sqrt((this.p1.x-this.p2.x)*(this.p1.x-this.p2.x)+(this.p1.y-this.p2.y)*(this.p1.y-this.p2.y));	
	}
	
	public Boolean equals(Segment s) {
		if (this.p1.equals(s.p1)) {
			return this.p2.equals(s.p2);
		}
		else if (this.p1.equals(s.p2)) {
			return this.p2.equals(s.p1);
		}
		else {
			return false;
		}
	}
	public String toString() {
		return "{ " + p1.toString() + " ; " + p2.toString() + " }";
	}
}
