package com.lis.modele.listAdapter;

import java.util.LinkedList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.lis.activity.R;
import com.lis.modele.data.Service;

public class ListAdapterService extends BaseAdapter{

	private LinkedList<Service> services;
	private LayoutInflater inflater;
	
	public ListAdapterService(Context ctx){
		services = new LinkedList<Service>();
		inflater = LayoutInflater.from(ctx);
	}
	
	public int getCount() {
		return services.size();
	}

	public Object getItem(int position) {
		return services.get(position);
	}

	public void add(Service service){
		services.add(service);
	}
	
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if( convertView == null ){
			convertView = inflater.inflate(R.layout.item_service, null);
			Service s = services.get(position);
			TextView txt = (TextView) convertView.findViewById(R.id.titre_item_service);
			txt.setText(s.getNom());
			ImageView img = (ImageView) convertView.findViewById(R.id.image_item_service);
			img.setImageResource(s.getImage());
		}else{
			Service s = services.get(position);
			TextView txt = (TextView) convertView.findViewById(R.id.titre_item_service);
			txt.setText(s.getNom());
			ImageView img = (ImageView) convertView.findViewById(R.id.image_item_service);
			img.setImageResource(s.getImage());
		}
		return convertView;
	}

}
