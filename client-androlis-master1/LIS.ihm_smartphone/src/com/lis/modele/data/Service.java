package com.lis.modele.data;

public class Service {

	private String nom;
	private int image;
	
	public Service(String nom, int image){
		this.setNom(nom);
		this.setImage(image);
	}

	public int getImage() {
		return image;
	}

	public void setImage(int image) {
		this.image = image;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	
}
