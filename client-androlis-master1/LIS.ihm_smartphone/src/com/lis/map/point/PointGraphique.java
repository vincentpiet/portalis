package com.lis.map.point;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class PointGraphique extends Overlay{

	private final GeoPoint geoPoint;
	private final Context context;
	private final int drawable;

	public PointGraphique(Context context, GeoPoint geoPoint, int drawable) {
		this.context = context;
		this.geoPoint = geoPoint;
		this.drawable = drawable;
	}

	public GeoPoint getPoint(){
		return this.geoPoint;
	}
	
	@Override
	public boolean draw(Canvas canvas, MapView mapView, boolean shadow, long when) {
		super.draw(canvas, mapView, shadow);

		// Converti  le coordonn�es en pixels
		Point screenPoint = new Point();
		mapView.getProjection().toPixels(geoPoint, screenPoint);

		// Lire l'image
		Bitmap markerImage = BitmapFactory.decodeResource(context.getResources(), drawable);

		// Dessine l'image d�cal� sur l'axe x (positionner l'aiguille sur le point) 
		canvas.drawBitmap(markerImage,
				screenPoint.x,
				screenPoint.y - markerImage.getHeight(), null);
		return true;
	}

	@Override
	public boolean onTap(GeoPoint p, MapView mapView) {
		return true;
	}
}