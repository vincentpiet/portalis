package com.lis.map.circle;

import android.R;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class Circle extends Overlay{

	Context context;
	double mLat;
	double mLon;
	float mRadius;


	public Circle(Context context, double mLat, double mLon, float mRadius ){
		super();
		this.context = context;
		this.mLat = mLat;
		this.mLon = mLon;
		this.mRadius = mRadius;
	}

	public boolean draw(Canvas canvas, MapView mapView, 
		    boolean shadow, long when) 
		    {
		        super.draw(canvas, mapView, shadow);                   

		        //---translate the GeoPoint to screen pixels---
		        Point screenPts = new Point();
		        GeoPoint p = new GeoPoint((int)mLat, (int)mLon);
		        mapView.getProjection().toPixels(p, screenPts);
		        //--------------draw circle----------------------            

		        //Point pt=mapView.getProjection().toPixels(p,screenPts);

		        Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		        
		        circlePaint.setStyle(Style.FILL);
		        circlePaint.setAlpha(25);
		        circlePaint.setColor(0x30000055);
		        canvas.drawCircle(screenPts.x, screenPts.y, 50, circlePaint);           

		        
		        
		        //---add the marker---
		       /* Bitmap bmp = BitmapFactory.decodeResource(
		            context.getResources(), R.drawable.checkbox_on_background);            
		        canvas.drawBitmap(bmp, screenPts.x, screenPts.y-bmp.getHeight(), null);              
		        super.draw(canvas,mapView,shadow);
		        */
		        return true;

		    }
}
