package com.androlis.fr.smartphone.modele.data.memento;

import java.util.ArrayList;

public class RequeteMemento implements Memento{

	private ArrayList<String> requetes;
	
	public ArrayList<String> getState(){
		return requetes;
	}
	
	public void setState( ArrayList<String> requete ){
//		for(String s : requete ){
//			this.requetes.add(new String(s));
//			Log.d("setState", s);
//		}
		requetes = new ArrayList<String>(requete);
	}
		
}
