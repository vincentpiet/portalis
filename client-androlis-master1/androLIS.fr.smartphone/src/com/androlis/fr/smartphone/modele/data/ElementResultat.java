package com.androlis.fr.smartphone.modele.data;

import com.androlis.fr.smartphone.cartographie.map.point.Point;

public class ElementResultat implements DataModel {

	public static final int UNKNOWN = -1;
	public static final int COORDONNEES = 0;
	public static final int URL = 1;

	private String donnees;

	public ElementResultat(String donnees)
	{
		this.donnees = donnees;
	}

	public String getDonnees() {
		return donnees;
	}

	public void setDonnees(String donnees) {
		this.donnees = donnees;
	}

	public int getFormat(){
		if( donnees.startsWith("http") || donnees.startsWith("https") || donnees.startsWith("www")) {return URL;}
		else{
			try{
				String[] coords = donnees.split(",");
				Double.parseDouble(coords[0]);
				Double.parseDouble(coords[1]);
				if ( coords.length == 2 ) return COORDONNEES;
			}catch(Exception e){
				return UNKNOWN;
			}
		}
		return UNKNOWN;
	}
	
	public Point getCoordonnee(){
		int start = donnees.indexOf("properties:gps is");
		String Rdonnees = donnees.substring(start+1);
		String[] coords = Rdonnees.split(",");
		double lat = Double.parseDouble(coords[0]);
		double longit = Double.parseDouble(coords[1]);
		return new Point(lat * 1E6, longit * 1E6);
	}

	@Override
	public String getName() {
		return donnees;
	}
}
