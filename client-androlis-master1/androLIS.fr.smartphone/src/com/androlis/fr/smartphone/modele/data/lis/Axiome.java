package com.androlis.fr.smartphone.modele.data.lis;

import com.androlis.fr.smartphone.modele.data.DataModel;

public class Axiome implements DataModel, MyData{

	private Groupe groupe;
	private String nom;
	
	public Axiome(Groupe groupe, String nom){
		this.groupe = groupe;
		this.nom = nom;
	}
		
	public Groupe getGroupe() {
		return groupe;
	}

	public void setGroupe(Groupe groupe) {
		this.groupe = groupe;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String getName() {
		return nom;
	}

	public String toString(){
		return "["+groupe+"] " + nom;
	}
	
	public boolean isObjet(){
		return this.nom.startsWith("objet:");
	}
	
	public boolean isProperties(){
		return nom.startsWith("properties:");
	}
}
