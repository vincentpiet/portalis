package com.androlis.fr.smartphone.modele.data.lis;

import java.util.ArrayList;

import com.androlis.fr.core.Core;
import com.androlis.fr.smartphone.modele.data.Client;

public class ToolsLoader {

	public static ArrayList<String> loadPropertiesFormObject(String objet){
		
		ArrayList<String> proprietes = new ArrayList<String>();
		
		Core c = Client.getInstance().getCore();
		objet = objet.replace("objet:", "");
		int numObjet = 0;
		try{
			numObjet = Integer.parseInt(objet);
		}catch(Exception e){
			return proprietes;
		}
		String[] properties = c.getProperties(numObjet);
		
		for(String s : properties){
			proprietes.add(s);
		}
		return proprietes;
	}
	
}
