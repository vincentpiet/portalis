package com.androlis.fr.smartphone.modele.data;

public class ElementProfil implements DataModel {
	
	private String entete;
	private String contenu;
	
	public ElementProfil(String _entete , String _contenu)
	{
		entete=_entete;
		contenu= _contenu;
		
	}
	public String getEntete() {
		return entete;
	}
	public void setEntete(String entete) {
		this.entete = entete;
	}
	public String getContenu() {
		return contenu;
	}
	public void setContenu(String contenu) {
		this.contenu = contenu;
	}
	@Override
	public String getName() {
		return entete;
	}

}
