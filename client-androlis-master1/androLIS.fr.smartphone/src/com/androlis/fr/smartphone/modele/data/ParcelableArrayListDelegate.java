package com.androlis.fr.smartphone.modele.data;

import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

public class ParcelableArrayListDelegate implements Parcelable {

	private ArrayList<Parcelable> delegate;
	
	public ParcelableArrayListDelegate(ArrayList<Parcelable> delegate){
		this.delegate = delegate;
	}
	
	public ArrayList<Parcelable> getDelegate() {
		return delegate;
	}

	public void setDelegate(ArrayList<Parcelable> delegate) {
		this.delegate = delegate;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		for(Parcelable data : delegate){
			data.writeToParcel(dest, flags);
		}
	}

	public void readFromParcel(Parcel in){
		delegate = in.readParcelable(ParcelableData.class.getClassLoader());
	}
	
}
