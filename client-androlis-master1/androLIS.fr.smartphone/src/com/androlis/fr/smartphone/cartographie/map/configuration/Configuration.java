package com.androlis.fr.smartphone.cartographie.map.configuration;

public class Configuration {

	/*Configuration des couleurs de dessin des zones : */
	public static final int COULEUR_DENSITE_FAIBLE = 0x5555FF15;//Color.GREEN;
	public static final int COULEUR_DENSITE_MOYENNE = 0x65FED855;//Color.YELLOW;
	public static final int COULEUR_DENSITE_FORTE = 0x99F39539; //Couleur orange.
	public static final int COULEUR_DENSITE_TRES_FORTE = 0xAADA1715;//Color.RED;

	public static final int NB_POINTS_DENSITE_MOYENNE = 3;
	public static final int NB_POINTS_DENSITE_FORTE = 2;
	public static final int NB_POINTS_DENSITE_TRES_FORTE = 7;
	
	public static final int RAYON_MIN = 25;
	public static final int RAYON_MAX = 150;
	
	public static final int COEF_AGRANDISSEMENT_CERCLE = 4;
	
	/*Configuration des dimensions des zones :*/
	public static final int MAX_DECOUPAGE_ZONE = 4;
	
	/*Configuration du cadrillage de google map :*/
	/* 
	 * /!\ ZOOM : Valeur donn�es par l'api google, ne pas modifier
	 *  sans conna�tre les valeurs min max du zoom sur google map
	 *  */
	public static final int MAX_ZOOM = 21;
	public static final int MIN_ZOOM = 2;

	/*Nombre de d�coupage min et max sur l'ensemble
	 * de la surface des points visible sur la carte */
	public static final int MAX_DECOUPAGE = 15;
	public static final int MIN_DECOUPAGE = 1;
	
}
