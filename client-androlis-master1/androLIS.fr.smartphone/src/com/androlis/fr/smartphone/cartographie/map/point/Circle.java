package com.androlis.fr.smartphone.cartographie.map.point;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Point;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;

public class Circle extends Overlay{

	Context context;
	double mLat;
	double mLon;
	float mRadius;

	private Zone zone;

	/**
	 * Classe qui dessine un cercle sur la map
	 * @param context
	 * @param zone
	 */
	public Circle(Context context, Zone zone ){
		super();
		this.context = context;
		GeoPoint center = zone.getCenter();
		this.mLat = center.getLatitudeE6();
		this.mLon = center.getLongitudeE6();
		this.mRadius = zone.getRayon();
		this.zone = zone;
	}

	/**
	 * Dessine le cercle sur la map
	 */
	public boolean draw(Canvas canvas, MapView mapView, 
		    boolean shadow, long when) 
		    {
		        super.draw(canvas, mapView, shadow);                   

		        //---translate the GeoPoint to screen pixels---
		        Point screenPts = new Point();
		        GeoPoint p = new GeoPoint((int)mLat, (int)mLon);
		        mapView.getProjection().toPixels(p, screenPts);
		        //--------------draw circle----------------------            

		        //Point pt = mapView.getProjection().toPixels(p,screenPts);

		        Paint circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		        
		        circlePaint.setStyle(Style.FILL);
		        circlePaint.setAlpha(25);
		        circlePaint.setColor(zone.getColorZone());
		        canvas.drawCircle(screenPts.x, screenPts.y, zone.getRayon(), circlePaint);           

		        //---add the marker---
		       /* Bitmap bmp = BitmapFactory.decodeResource(
		            context.getResources(), R.drawable.checkbox_on_background);            
		        canvas.drawBitmap(bmp, screenPts.x, screenPts.y-bmp.getHeight(), null);              
		        super.draw(canvas,mapView,shadow);
		        */
		        return true;

		    }
}
