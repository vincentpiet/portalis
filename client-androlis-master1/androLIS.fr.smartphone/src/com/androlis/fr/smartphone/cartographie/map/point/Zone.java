package com.androlis.fr.smartphone.cartographie.map.point;

import java.util.ArrayList;

import com.androlis.fr.smartphone.cartographie.map.configuration.Configuration;
import com.fr.lis.androlis.smartphone.main.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;

public class Zone {

	private GeoPoint pointHG;
	private GeoPoint pointHD;
	private GeoPoint pointBG;
	private GeoPoint pointBD;

	private ArrayList<PointGraphique> points;

	//private int minDistance;
	/**
	 * Classe representant une zone. Permet d'ajouter des points à une zone,
	 * de calculé la representation de la zone sur la map
	 * @param pointHG
	 * @param latitudeEspace
	 * @param longitudeEspace
	 */
	public Zone( GeoPoint pointHG, double latitudeEspace, double longitudeEspace){
		points = new ArrayList<PointGraphique>();
		//minDistance = Math.min((int) latitudeEspace,(int)longitudeEspace);
		
		this.pointHG = pointHG;
		this.pointHD = new GeoPoint( pointHG.getLatitudeE6(), (int)(pointHG.getLongitudeE6() + longitudeEspace) );
		this.pointBD = new GeoPoint((int) (pointHG.getLatitudeE6() - latitudeEspace), (int)(pointHG.getLongitudeE6() + longitudeEspace));
		this.pointBG = new GeoPoint((int)(pointHG.getLatitudeE6() - latitudeEspace ), pointHG.getLongitudeE6() );
	}

	/**
	 * Renvoie true si le point "point" appartient à la zone sinon false
	 * @param point
	 * @return
	 */
	public boolean pointAppartientZone(GeoPoint point){
		
		double latitudeP = point.getLatitudeE6();
		double longitudeP = point.getLongitudeE6();
		
		double lat1, lat2, lon1, lon2;
		
		lat1 = pointHG.getLatitudeE6();
		lat2 = pointBG.getLatitudeE6();
		
		boolean appartientALatitude = lat1 >= latitudeP && lat2 <= latitudeP;

		lon1 = pointBG.getLongitudeE6();
		lon2 = pointBD.getLongitudeE6();
		
		boolean appartientALongitude = lon1 <= longitudeP && lon2 >= longitudeP;
		return appartientALatitude && appartientALongitude;
	}
	
	/**
	 * Ajoute un point à la zone.
	 * @param point
	 */
	public void addPoint(PointGraphique point){
		points.add(point);
	}
	
	/**
	 * Permet de savoir si la zone sera représentée par un cercle (return true)
	 * ou par un point (return false)
	 * @return
	 */
	public boolean isCercle(){
		return points.size() > 1;
	}

	/**
	 * Détermine la couleur du cercle en fonction du nombre de points dans la zone
	 * @return
	 */
	public int getColorZone(){
		if( points.size() > Configuration.NB_POINTS_DENSITE_TRES_FORTE ){
			return Configuration.COULEUR_DENSITE_TRES_FORTE;
		}else{
			if( points.size() > Configuration.NB_POINTS_DENSITE_FORTE ){
				return Configuration.COULEUR_DENSITE_FORTE;
			}else{
				if( points.size() > Configuration.NB_POINTS_DENSITE_MOYENNE ){
					return Configuration.COULEUR_DENSITE_MOYENNE;
				}else{
					return Configuration.COULEUR_DENSITE_FAIBLE;
				}
			}
		}
	}

	public void testDessineZone(MapView mapView){
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), pointHG, R.drawable.punaise2));
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), pointHD, R.drawable.punaise2));
		
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), pointBD, R.drawable.punaise2));
		mapView.getOverlays().add(new PointGraphique(mapView.getContext(), pointBG, R.drawable.punaise2));
		
		//mapView.getOverlays().add(new PointGraphique(mapView.getContext(), pointHD, R.drawable.punaise2));
		//mapView.getOverlays().add(new PointGraphique(mapView.getContext(), pointBD, R.drawable.punaise2));
	}
	
	/**
	 * retourne le centre de la zone (utilisé pour tracé le cercle)
	 * @return GeoPoint
	 */
	public GeoPoint getCenter(){
		int m_lat = 0;
		int m_long = 0;
		for( PointGraphique p : points  ){
			m_lat += p.getPoint().getLatitudeE6();
			m_long += p.getPoint().getLongitudeE6();
		}
		//int milieuLatitude = (pointHG.getLatitudeE6() + pointBG.getLatitudeE6())/2;
		//int milieuLongitude = (pointHG.getLongitudeE6() + pointHD.getLongitudeE6())/2;
		//return new GeoPoint( milieuLatitude, milieuLongitude);
		return new GeoPoint(m_lat / points.size() , m_long / points.size());
	}
	
	/**
	 * Retourne le premier point de la zone
	 * Utilsé dans le cas ou la zone ne possède qu'un point <=> n'est pas un cercle
	 * @return
	 */
	public PointGraphique getPoint(){
		return points.get(0);
	}
	
	/**
	 * tester si la zone possède des points
	 * @return
	 */
	public boolean isEmpty(){
		return this.points.isEmpty();
	}
	
	/**
	 * Calcul le rayon du cercle en fonction du nombre de points présent dans la zone
	 * Limité par Configuration.RAYON_MIN et Configuration.RAYON_MAX
	 * @return float
	 */
	public float getRayon(){
		float r = (float) (Configuration.RAYON_MIN + points.size()*Configuration.COEF_AGRANDISSEMENT_CERCLE);
		return r%Configuration.RAYON_MAX > 0 && r%Configuration.RAYON_MAX < Configuration.RAYON_MAX ?
						r : Configuration.RAYON_MAX;
	}
	
}
