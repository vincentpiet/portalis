package com.androlis.fr.smartphone.cartographie.map.point;

import android.os.Parcel;
import android.os.Parcelable;

import com.androlis.fr.smartphone.modele.data.ParcelableData;

public class Point implements ParcelableData{

	private  double longitude, latitude;

	public Point(double longitude, double latitude){
		this.longitude = longitude;
		this.latitude = latitude;
	}

	public Point(Parcel in){
		lire(in);
	}

	@SuppressWarnings("rawtypes")
	public static final Parcelable.Creator CREATOR = new Parcelable.Creator()
	{
		public Point createFromParcel(Parcel in)
		{
			return new Point(in);
		}

		@Override
		public Object[] newArray(int size) {
			return null;
		}
	};

	@Override
	public void lire(Parcel in) {
		longitude = in.readDouble();
		latitude = in.readDouble();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeDouble(longitude);
		dest.writeDouble(latitude);
	}

	public double getLongitude() {
		return longitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

}
