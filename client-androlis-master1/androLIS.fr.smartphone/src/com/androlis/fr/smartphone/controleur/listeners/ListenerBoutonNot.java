package com.androlis.fr.smartphone.controleur.listeners;

import android.widget.Button;

import com.androlis.fr.smartphone.gui.ActivityRecherche;

public class ListenerBoutonNot extends ListenerBoutonLogique {

	public ListenerBoutonNot(Button boutonNot, ActivityRecherche activity) {
		super(boutonNot, null, activity);
	}

	@Override
	public void clickAndOrNot() {
		return; //le cas est géré dans la classe parente.
	}

}
