package com.androlis.fr.smartphone.controleur.listeners;

import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;

import com.androlis.fr.core.Core;
import com.androlis.fr.smartphone.gui.ActivityLogin;
import com.androlis.fr.smartphone.modele.data.Client;
import com.fr.lis.androlis.smartphone.main.R;

public class ListenerLogin implements OnClickListener, Runnable{

	private ActivityLogin login;
	
	public ListenerLogin(ActivityLogin login){
		this.login = login;
	}

	@Override
	public void onClick(View v) {
		Thread th = new Thread(this);
		th.start();
	}

	@Override
	public void run() {
		EditText user = (EditText) login.findViewById(R.id.username);
		EditText passwd = (EditText) login.findViewById(R.id.password);
		
		Message msg = new Message();
		msg.setTarget(Client.getInstance().getHandler());
		
		Core core = Client.getInstance().getCore();
		if( core.login(user.getText().toString(), passwd.getEditableText().toString()) ){
			msg.obj = true;
		}else{
			msg.obj = false;
		}
		msg.sendToTarget();
	}

}
