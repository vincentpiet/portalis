package com.androlis.fr.smartphone.controleur.communication;

import android.os.Handler;
import android.os.Message;

public class CommunicationServeur extends Handler{

	private UpdateView view;
	
	public CommunicationServeur(UpdateView view){
		this.view = view;
	}
	
	public void handleMessage(Message msg){
		view.onUpdateReceive(msg);
	}
	
	public void setView(UpdateView view){
		this.view = view;
	}
	
}
