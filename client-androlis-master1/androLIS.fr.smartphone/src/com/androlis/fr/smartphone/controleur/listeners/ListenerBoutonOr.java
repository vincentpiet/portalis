package com.androlis.fr.smartphone.controleur.listeners;

import android.widget.Button;

import com.androlis.fr.smartphone.gui.ActivityRecherche;

public class ListenerBoutonOr extends ListenerBoutonLogique{

	public ListenerBoutonOr(Button boutonListener, Button bouton, ActivityRecherche activity) {
		super(boutonListener, bouton, activity);
	}

	@Override
	public void clickAndOrNot() {
		ActivityRecherche recherche = getRecherche();
		if( recherche.orIsChecked() ){
			uncheckButton(getBoutonListener());
			recherche.checkAnd();
		}else{
			checkButton(getBoutonListener());
			recherche.checkOr();
			uncheckButton(getBouton());
		}
		
	}

	
	
}
