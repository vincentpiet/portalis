package com.androlis.fr.smartphone.controleur.listeners;

import android.widget.Button;

import com.androlis.fr.smartphone.gui.ActivityRecherche;

public class ListenerBoutonAnd extends ListenerBoutonLogique{

	/*
	 * Ici, getBouton() renvoie le bouton "Or"
	 * et getBoutonListener() renvoie le bouton "And"
	 */
	
	public ListenerBoutonAnd(Button boutonAnd, Button boutonOr,  ActivityRecherche activity) {
		super(boutonAnd, boutonOr, activity);
	}

	@Override
	public void clickAndOrNot() {
		ActivityRecherche recherche = super.getRecherche();
		if( recherche.andIsChecked() ){
			uncheckButton(getBoutonListener());
			recherche.checkOr();
			checkButton(getBouton());
		}else{
			checkButton(getBoutonListener());
			uncheckButton(getBouton());
			recherche.checkAnd();
		}
	}
	
}
