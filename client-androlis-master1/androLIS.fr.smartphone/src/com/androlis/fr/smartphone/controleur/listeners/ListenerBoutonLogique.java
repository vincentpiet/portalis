package com.androlis.fr.smartphone.controleur.listeners;

import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import com.androlis.fr.smartphone.gui.ActivityRecherche;
import com.fr.lis.androlis.smartphone.main.R;

public abstract class ListenerBoutonLogique implements OnClickListener {

	private Button boutonListener;
	private Button bouton;

	private ActivityRecherche recherche;
	
	public ListenerBoutonLogique(Button boutonListener, Button bouton, ActivityRecherche activity){
		this.boutonListener = boutonListener;
		this.bouton = bouton;
		//click = false;
		this.recherche = activity;
	}

//	public String getCommandeLogique(){
//		return commandeLogique;
//	}
	
	@Override
	public void onClick(View v){
		if(boutonListener.getText().toString().equals(recherche.getResources().getString(R.string.bouton_not))){
			if( recherche.notIsChecked() ){
				uncheckButton(boutonListener);
				recherche.uncheckNot();
			}else{
				checkButton(boutonListener);
				recherche.checkNot();
			}
		}else{
			clickAndOrNot();
		}
	}
	
	public abstract void clickAndOrNot();
		/*if( click ){
			click = false;
			bouton.setBackgroundResource(R.drawable.bouton_test);
			bouton.setTextColor(Color.WHITE);
		}else{
			click = true;
			bouton.setBackgroundResource(R.drawable.bouton_test_appuye);
			bouton.setTextColor(Color.RED);	
		}*/
		/*if( recherche.andIsChecked() ){
			if( recherche.notIsChecked() ){
				req.andNotAxiome(axiome.getDataModel().getName());
			}else{
				req.andAxiome(axiome.getDataModel().getName());
			}
		}else if (recherche.orIsChecked()){
			if( recherche.notIsChecked() ){
				req.orNotAxiome(axiome.getDataModel().getName());
			}else{
				req.orAxiome(axiome.getDataModel().getName());
			}
		}
		Button bouton = null;
		
		Log.d("",bouton.getText().toString() + " = "+bouton.getText().toString().equals(R.string.bouton_or)+" ");
		if( bouton.getText().toString().equals(recherche.getResources().getString(R.string.bouton_or))){
			if( recherche.orIsChecked() ){
				uncheckButton(bouton);
				recherche.checkAnd();
			}else{
				checkButton(bouton);
				recherche.checkOr();
			}
		}else if(bouton.getText().toString().equals(recherche.getResources().getString(R.string.bouton_and))){
			if( recherche.andIsChecked() ){
				uncheckButton(bouton);
				recherche.checkOr();
			}else{
				checkButton(bouton);
				recherche.checkAnd();
				Log.d("","Bouton and. OK");
			}
		}
		else if(bouton.getText().toString().equals(recherche.getResources().getString(R.string.bouton_not))){
			if( recherche.notIsChecked() ){
				uncheckButton(bouton);
				recherche.uncheckNot();
			}else{
				checkButton(bouton);
				recherche.checkNot();
			}
		}
	}*/
	
	protected void uncheckButton(Button bouton){
		bouton.setBackgroundResource(R.drawable.bouton_test);
		bouton.setTextColor(Color.WHITE);	
	}
	
	protected void checkButton(Button bouton){
		bouton.setBackgroundResource(R.drawable.bouton_test_appuye);
		bouton.setTextColor(Color.RED);
	}

	public Button getBoutonListener() {
		return boutonListener;
	}

	public Button getBouton() {
		return bouton;
	}

	public ActivityRecherche getRecherche() {
		return recherche;
	}

//	public void setCommandeLogique(String commandeLogique) {
//		this.commandeLogique = commandeLogique;
//	}

	public void setBoutonListener(Button boutonListener) {
		this.boutonListener = boutonListener;
	}

	public void setBouton(Button bouton) {
		this.bouton = bouton;
	}

	public void setRecherche(ActivityRecherche recherche) {
		this.recherche = recherche;
	}
	
}
