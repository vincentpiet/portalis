package com.androlis.fr.smartphone.gui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.androlis.fr.smartphone.controleur.communication.CommunicationServeur;
import com.androlis.fr.smartphone.controleur.communication.UpdateView;
import com.androlis.fr.smartphone.gui.expendableList.ExpendableListAdapter;
import com.androlis.fr.smartphone.gui.expendableList.GroupeView;
import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.gui.tools.AlertDialogBuilder;
import com.androlis.fr.smartphone.modele.data.Client;
import com.androlis.fr.smartphone.modele.data.DataModel;
import com.androlis.fr.smartphone.modele.data.ElementProfil;
import com.androlis.fr.smartphone.modele.data.ElementResultat;
import com.androlis.fr.smartphone.modele.data.lis.AxiomeLoader;
import com.androlis.fr.smartphone.modele.data.lis.DataLoader;
import com.androlis.fr.smartphone.modele.data.lis.Groupe;
import com.androlis.fr.smartphone.modele.data.lis.Requete;
import com.androlis.fr.smartphone.modele.data.lis.ResultatLoader;
import com.fr.lis.androlis.smartphone.main.R;

public class ActivityResultat extends Activity implements UpdateView{

	public static boolean estThabor=false;
	public static final int INVISIBLE =4;

	private List<ElementProfil> profil = new ArrayList<ElementProfil>();
	private ListViewer resultatAdapter;
	private ExpendableListAdapter listeExpendableAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resultat);

		resultatAdapter = new ListViewer(getApplicationContext());
		//ListView listProfil = (ListView)findViewById(R.id.activity_resultats);
		//listProfil.setAdapter(resultatAdapter);

		listeExpendableAdapter = new ExpendableListAdapter(this);
		ExpandableListView expv = (ExpandableListView) findViewById(R.id.expandableList);
		expv.setAdapter(listeExpendableAdapter);

		//Bundle bundle = getIntent().getExtras();
		//String pseudo = (String) bundle.get("pseudo");
		tester();
		Button goback = (Button)findViewById(R.id.retoursearch);
		goback.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
		Bundle bundle = getIntent().getExtras();
		if( bundle != null ){
			Requete req = new Requete(bundle.getString("requete"));
			CommunicationServeur cs = Client.getInstance().getHandler();
			cs.setView(this);
			DataLoader axLoader = new ResultatLoader(req);
			axLoader.execute();
		}

	}

	public void tester(){

		//		Groupe g = new Groupe("Test");
		//		ArrayList<Viewer> v = new ArrayList<Viewer>();
		//		
		//		ElementResultat erer = new ElementResultat("https://www.google.com/talk/images/newchatingmail.jpg");
		//		ResultatView rvrv = new ResultatView(erer, this);
		//		
		//		v.add(rvrv);
		//		g.addAxiome(erer);
		//		
		//		resultatAdapter.add(rvrv);
		//
		//		erer = new ElementResultat("https://www.google.com/talk/images/newchatingmail.jpg");
		//		rvrv = new ResultatView(erer, this);
		//
		//		g.addAxiome(erer);
		//		v.add(rvrv);
		//		
		//		resultatAdapter.add(rvrv);
		//		ElementResultat er = new ElementResultat("48.1144561088101,-1.67292038513902");
		//		ResultatView rv = new ResultatView(er, this);
		//
		//		g.addAxiome(er);
		//		v.add(rv);
		//		
		//		GroupeView gv = new GroupeView(g, v);
		//		listeExpendableAdapter.addGroupe(gv);
		//		listeExpendableAdapter.notifyDataSetChanged();
		//		resultatAdapter.add(rv);
		//		resultatAdapter.notifyDataSetChanged();
	}

	@Override
	public void onUpdateReceive(Message msg) {
		if( msg.what == AxiomeLoader.IS_OBJECT ){

		}else{
			@SuppressWarnings("unchecked")
			ArrayList<Groupe> groupes = (ArrayList<Groupe>) msg.obj;
			if( !groupes.isEmpty() ) {
				listeExpendableAdapter.clear();
				for(Groupe gr : groupes){

					ArrayList<DataModel> data = gr.getAxiomes();
					ArrayList<Viewer> vues = new ArrayList<Viewer>();

					GroupeView gv = new GroupeView(gr, vues);

					for( DataModel dt : data ){
						ResultatView rs = new ResultatView(dt, this);
						vues.add(rs);
					}
					listeExpendableAdapter.addGroupe(gv);

					//Log.d("","Ajout Groupe : " + gr.getName() + " nb axiomes : " + gr.getAxiomes().size());
				}
				listeExpendableAdapter.notifyDataSetChanged();
			}else{

				AlertDialog.Builder adb = AlertDialogBuilder.buildAlertDialog("Veuillez effectuer une première recherche.", "Avrtissement", this);
				if( adb != null ){
					adb.setPositiveButton(R.string.ok,new DialogInterface.OnClickListener(){
						public void onClick(DialogInterface arg0, int arg1){
							finish();
						}
					});
					adb.show();
				}
			}
		}
	}

}
