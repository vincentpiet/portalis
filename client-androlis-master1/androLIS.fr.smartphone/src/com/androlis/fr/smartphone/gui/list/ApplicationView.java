package com.androlis.fr.smartphone.gui.list;

import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.TextView;

import com.androlis.fr.smartphone.gui.Viewer;
import com.androlis.fr.smartphone.modele.data.Application;
import com.androlis.fr.smartphone.modele.data.DataModel;
import com.fr.lis.androlis.smartphone.main.R;

public class ApplicationView implements Viewer{

	private static final int LAYOUT = R.layout.item_application;

	private Application application;
	
	public ApplicationView(Application application){
		this.application = application;		
	}
	
	@Override
	public View getView(LayoutInflater inflater, View convertView) {
			
		if( convertView == null ){
			convertView = inflater.inflate(LAYOUT, null);
			  convertView.setBackgroundColor(0xFF202020);  
		}

		TextView name = (TextView) convertView.findViewById(R.id.nom_application);
		name.setText(application.getName());
		name.setTextColor(0xFFFFFFFF);
		ImageView im = (ImageView) convertView.findViewById(R.id.imageApplication);
		if( application.hasPicture() ){
			im.setImageResource(application.getImage());
		}else{
			im.setVisibility(View.INVISIBLE);
			name.setText(application.getName());
		}
		Animation animation = null; 
		animation = new ScaleAnimation((float)1.0, (float)1.0 ,(float)0, (float)1.0); 
		animation.setDuration(750);  
		convertView.startAnimation(animation);  
		animation = null; 
		return convertView;
	}
	
	@Override
	public int getLayout(){
		return LAYOUT;
	}

	@Override
	public DataModel getDataModel() {
		return application;
	}

	@Override
	public void onCreate(ListViewer list) {}
	
}
