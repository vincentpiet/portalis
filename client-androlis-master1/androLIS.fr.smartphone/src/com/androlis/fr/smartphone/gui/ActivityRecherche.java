package com.androlis.fr.smartphone.gui;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androlis.fr.core.Core;
import com.androlis.fr.smartphone.controleur.communication.CommunicationServeur;
import com.androlis.fr.smartphone.controleur.communication.UpdateView;
import com.androlis.fr.smartphone.controleur.listeners.ListenerBoutonAnd;
import com.androlis.fr.smartphone.controleur.listeners.ListenerBoutonGo;
import com.androlis.fr.smartphone.controleur.listeners.ListenerBoutonLogique;
import com.androlis.fr.smartphone.controleur.listeners.ListenerBoutonNot;
import com.androlis.fr.smartphone.controleur.listeners.ListenerBoutonOr;
import com.androlis.fr.smartphone.controleur.listeners.ListenerRequete;
import com.androlis.fr.smartphone.gui.expendableList.ExpendableListAdapter;
import com.androlis.fr.smartphone.gui.expendableList.GroupeView;
import com.androlis.fr.smartphone.modele.data.Client;
import com.androlis.fr.smartphone.modele.data.lis.AxiomeLoader;
import com.androlis.fr.smartphone.modele.data.lis.Groupe;
import com.androlis.fr.smartphone.modele.data.lis.Requete;
import com.androlis.fr.smartphone.modele.data.memento.CareTaker;
import com.androlis.fr.smartphone.modele.data.memento.Memento;
import com.androlis.fr.smartphone.modele.data.memento.Paire;
import com.androlis.fr.smartphone.modele.data.memento.RequeteMemento;
import com.androlis.fr.smartphone.modele.data.observer.Observer;
import com.androlis.fr.smartphone.modele.data.observer.Subject;
import com.fr.lis.androlis.smartphone.main.R;

public class ActivityRecherche extends Activity implements UpdateView, Observer {

	private Requete requete;
	private ExpendableListAdapter listeExpendableAdapter;

	private String application;
	private AxiomeLoader axiomeLoader;

	private boolean andIsChecked;
	private boolean orIsChecked;
	private boolean notIsChecked;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_recherche);

		orIsChecked = true;
		andIsChecked = false;
		notIsChecked = false;

		CommunicationServeur cs = Client.getInstance().getHandler();
		Core core = Client.getInstance().getCore();
		cs.setView(this);

		application = (String)getIntent().getExtras().get("Application");
		String[] dataApp = application.split(":");
		Integer port = Integer.parseInt(dataApp[1]);
		core.setServicePort(port);

		//String applicationToLoad = (String)getIntent().getExtras().get("Application");
		//Toast.makeText(this, "Charger appli " + applicationToLoad, Toast.LENGTH_LONG).show();

		//Envoyer une requete a Camelis pour charger
		//l'application "applicationToLoad

		//Configuration des boutons :
		Button boutonAnd = (Button) findViewById(R.id.button_and);
		Button boutonOr = (Button) findViewById(R.id.button_or);

		ListenerBoutonLogique lstBouton = new ListenerBoutonAnd(boutonAnd, boutonOr, this);
		boutonAnd.setOnClickListener(lstBouton);

		Button boutonNot = (Button) findViewById(R.id.button_not);
		lstBouton = new ListenerBoutonNot(boutonNot, this);
		boutonNot.setOnClickListener(lstBouton);

		lstBouton = new ListenerBoutonOr(boutonOr, boutonAnd, this);
		boutonOr.setOnClickListener(lstBouton);

		//bouton = (Button) findViewById(R.id.carte);
		//bouton.setOnClickListener(new ListenerBoutonCarte(this));

		//Configuration de la expendableListView :
		ExpandableListView expandableList = (ExpandableListView) findViewById(R.id.expandableList);
		listeExpendableAdapter = new ExpendableListAdapter(this);
		expandableList.setAdapter(listeExpendableAdapter);
		//tester();

		Button bouton = (Button) findViewById(R.id.resultats);
		bouton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(getApplicationContext(), ActivityResultat.class);

				Memento mem = CareTaker.getInstance().getLastIndex();
				
				if( mem != null ){
					Requete req = new Requete();
					req.setState(mem);
					intent.putExtra("requete", req.getRequete());
				}
				startActivity(intent);
				
			}
		});

		requete = new Requete();

		requete.register(this);

		CareTaker ct = CareTaker.getInstance();
		ct.clear();

		TextView txt = (TextView) findViewById(R.id.chemin);
		bouton = (Button) findViewById(R.id.go);
		bouton.setOnClickListener(new ListenerBoutonGo(requete, axiomeLoader, txt));

		txt.setOnClickListener(new ListenerRequete(requete, this));

		axiomeLoader = new AxiomeLoader(requete);
		//String applicationToLoad = (String)getIntent().getExtras().get("Application");
		chargeDonnees();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_activity_recherche, menu);
		return true;
	}

	private void chargeDonnees(){
		//		Thread th = new Thread(axiomeLoader);
		//		th.start();
		CareTaker ct = CareTaker.getInstance();
		RequeteMemento rq = new RequeteMemento();
		rq.setState(requete.getSource());
		Paire p = new Paire(axiomeLoader, rq);
		ct.addMemento(p);
		axiomeLoader.execute();
	}

	//	private void tester(){
	//		
	//		Groupe gr = null;
	//		
	//		for(int i = 0 ; i < 10 ; i++){
	//			gr = new Groupe("Groupe " + (i+1));
	//			for( int j = 0 ; j < 10 ; j++ ){
	//				Axiome axiome = new Axiome(gr, "["+gr.getName()+"] Axiome " + (j+1));
	//				gr.addAxiome(axiome);
	//			}
	//			GroupeView groupeView = new GroupeView(gr);
	//			listeExpendableAdapter.addGroupe(groupeView);
	//		}
	//		listeExpendableAdapter.notifyDataSetChanged();
	//	}

	@Override
	public void onUpdateReceive(Message msg) {
		if( msg.what == AxiomeLoader.IS_OBJECT ){

		}else{
			@SuppressWarnings("unchecked")
			ArrayList<Groupe> groupes = (ArrayList<Groupe>) msg.obj;
			if( !groupes.isEmpty() ) {
				listeExpendableAdapter.clear();
				requete.clear();
				for(Groupe gr : groupes){
					listeExpendableAdapter.addGroupe(new GroupeView(gr, this));
					//Log.d("","Ajout Groupe : " + gr.getName() + " nb axiomes : " + gr.getAxiomes().size());
				}
				listeExpendableAdapter.notifyDataSetChanged();
			}else{
				Toast.makeText(this, "Aucune donnée trouvée pour cette recherche.", Toast.LENGTH_LONG).show();
			}
		}
	}

	public void checkAnd(){
		andIsChecked = true;
		orIsChecked = false;
	}

	public void checkOr(){
		andIsChecked = false;
		orIsChecked = true;
	}

	public void checkNot(){
		notIsChecked = true;
	}

	public void uncheckNot(){
		notIsChecked = false;
	}

	public boolean andIsChecked(){
		return andIsChecked;
	}

	public boolean orIsChecked(){
		return orIsChecked;
	}

	public boolean notIsChecked(){
		return notIsChecked;
	}

	@Override
	public void onBackPressed() {
		CareTaker ct = CareTaker.getInstance();
		if( !ct.previous() ){
			super.onBackPressed();
		}else{
			TextView txt = (TextView) findViewById(R.id.chemin);
			txt.setText(requete.getRequete().replace("%20", " "));
		}
	}

	public Requete getRequete() {
		return requete;
	}

	public void setRequete(Requete requete) {
		this.requete = requete;
	}

	public void onResume(){
		Client.getInstance().getHandler().setView(this);
		super.onResume();
	}

	@Override
	public void notifyFromSuject(Subject subject) {
		Requete req = (Requete) subject;
		TextView txt = (TextView) findViewById(R.id.chemin);
		txt.setText(req.getRequete().replace("%20", " "));
	}

}
