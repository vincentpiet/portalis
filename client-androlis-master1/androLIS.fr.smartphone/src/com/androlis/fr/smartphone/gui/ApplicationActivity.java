package com.androlis.fr.smartphone.gui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;

import com.androlis.fr.core.Core;
import com.androlis.fr.smartphone.controleur.communication.UpdateView;
import com.androlis.fr.smartphone.controleur.listeners.ListenerListViewer;
import com.androlis.fr.smartphone.gui.list.ApplicationView;
import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.modele.data.Application;
import com.androlis.fr.smartphone.modele.data.Client;
import com.fr.lis.androlis.smartphone.main.R;

@SuppressWarnings("all")

public class ApplicationActivity extends Activity implements Runnable, UpdateView{

	//Liste des applications disponibles
	private ListViewer listAdapterApplications;

	//Listes des images (des applications) disponible visible dans la gallerie
	private ListViewer vueApplicationGallerie;

	//Liste d'autoCompletion :
	private ArrayAdapter<String> autoCompleteList;

	private static final int PROFIL_SETTINGS = 1;
	private static final int APPLICATION_SETTING = 2;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.accueil_applications_lis);

		Client.getInstance().getHandler().setView(this);

		ListView listeApplication = (ListView) findViewById(R.id.listeApplications);

		listAdapterApplications = new ListViewer(this);
		listeApplication.setAdapter(listAdapterApplications);
		listeApplication.setOnItemClickListener(
				new ListenerListViewer(listAdapterApplications, this));

		/*vueApplicationGallerie = new ListViewer(this);
    	StackView gallerie = (StackView) findViewById(R.id.galerie);
    	gallerie.setAdapter(vueApplicationGallerie);*/

		AutoCompleteTextView autoComplete = (AutoCompleteTextView) findViewById(R.id.autoCompleteTextView1);
		autoCompleteList = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line);
		autoComplete.setAdapter(autoCompleteList);

		//Fonctions de tests :
		//        testeur();
		loadApplication();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_application, menu);
		menu.add(0 , PROFIL_SETTINGS , 1, "Voir le profil utilisateur");
		menu.add(0, APPLICATION_SETTING , 2, "Modifier les applications");
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item){

		switch(item.getItemId()){
		case PROFIL_SETTINGS :
			Intent intent = new Intent(this.getApplicationContext()	, ProfileActivity.class);
			intent.putExtra("pseudo", "Moi :D");
			startActivity(intent);
			break;
		default:
			break;
		}
		return true;
	}

	public void addApplication(Application application){

		//Ajout d'une application au menu déroulant :
		Viewer view = new ApplicationView(application);
		listAdapterApplications.add(view);
		listAdapterApplications.notifyDataSetChanged();

		/*
    	Ajout une image dans la galerie
    	(fonction non disponible sur le serveur)

    	Viewer viewGallerie = new ImageGalerie(application);
    	vueApplicationGallerie.add(viewGallerie);
    	vueApplicationGallerie.notifyDataSetChanged();*/

		//Ajoute la recherche d'une application par son nom
		autoCompleteList.add(application.getName());
		autoCompleteList.notifyDataSetChanged();

	}

	private void testeur(){
		Application app = new Application("PACA", R.drawable.ic_launcher);
		addApplication(app);
		app = new Application("Rennes", R.drawable.rennes);
		addApplication(app);
		app = new Application("Velo star", Application.NO_PICTURE);
		addApplication(app);
		app = new Application("Parc du Thabor", Application.NO_PICTURE);
		addApplication(app);
	}

	private void loadApplication(){

		Thread th = new Thread(this);
		th.start();
	}

	@Override
	public void run() {
		Core core = Client.getInstance().getCore();

		String[] services = core.pingCamelis();
		Message msg = new Message();
		msg.setTarget(Client.getInstance().getHandler());
		String str = "";
		for(String s : services){
			str += s +" - ";
		}
		Log.d("", "SERVICES : " + str);
		msg.obj = services;
		msg.sendToTarget();
	}

	@Override
	public void onUpdateReceive(Message msg) {
		if( msg.obj instanceof String[] ){
			String[] srv = (String[]) msg.obj;
			for(String s : srv){
				Application app = new Application(s);
				addApplication(app);
			}
			Log.d("","Fin d'ajout des services.");
		}
	}

}
