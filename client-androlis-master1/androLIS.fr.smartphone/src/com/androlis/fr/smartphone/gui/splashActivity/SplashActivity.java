package com.androlis.fr.smartphone.gui.splashActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.widget.SeekBar;

import com.androlis.fr.smartphone.gui.ActivityLogin;
import com.androlis.fr.smartphone.gui.splashActivity.LoadingTask.LoadingTaskFinishedListener;
import com.fr.lis.androlis.smartphone.main.R;

public class SplashActivity extends Activity implements LoadingTaskFinishedListener {
	private Thread threadSplash;

	@Override
	public void onCreate (Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splash_activity);

		 //splashImageView.setBackgroundResource(R.drawable.flag);

		 SeekBar progressSeekBar = (SeekBar) findViewById(R.id.seekBarprogress);
	        // Start your loading
	     new LoadingTask(progressSeekBar, (LoadingTaskFinishedListener) this).execute("www.google.co.uk");
		
		
		 		
		threadSplash =  new Thread()
		{
           // @SuppressWarnings("deprecation")
			@Override
            public void run(){
                try {
                    synchronized(this){
                        // Wait given period of time or exit on touch
                        wait(1000);
                    }
                }
                catch(InterruptedException ex)
                {        
                	
                }

                
//                splashImageView.post(new Runnable(){
//     	           
//                  public void run() {
//                       frameAnimation.start();                
//                    }            
//               }
//                ); 
                
//                finish();
//                Intent intent = new Intent();
//                intent.setClass(sPlashScreen, MainActivity.class);
//                startActivity(intent);
//                //stop();                    
            }
        };
        
        threadSplash.start();
        
	}

	    public void onTaskFinished() {
	        completeSplash();
	    }
	 
	    private void completeSplash(){
	        startApp();
	        finish(); // Don't forget to finish this Splash Activity so the user can't return to it!
	    }
	 
	    private void startApp() {
	        Intent intent = new Intent(SplashActivity.this, ActivityLogin.class);
	        startActivity(intent);
	    }
	

        
        @Override
        public boolean onTouchEvent(MotionEvent evt)
        {
            if(evt.getAction() == MotionEvent.ACTION_DOWN)
            {
                synchronized(threadSplash)
                {
                    threadSplash.notifyAll();
                }
            }
            return true;
        }  
	
	
	

}
