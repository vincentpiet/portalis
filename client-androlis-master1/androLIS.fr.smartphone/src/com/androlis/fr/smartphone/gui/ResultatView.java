package com.androlis.fr.smartphone.gui;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.androlis.fr.smartphone.controleur.listeners.ListenerBoutonCarte;
import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.gui.tools.AlertDialogBuilder;
import com.androlis.fr.smartphone.modele.data.DataModel;
import com.androlis.fr.smartphone.modele.data.ElementResultat;
import com.fr.lis.androlis.smartphone.main.R;

public class ResultatView implements Viewer, OnLongClickListener{

	private static final int LAYOUT = R.layout.item_resultat;
	private ElementResultat res;
	private ListViewer list;
	private Context ctx;
	
	public ResultatView (DataModel res, Context ctx)
	{
		this.res= (ElementResultat) res;
		this.ctx = ctx;
	}
	@Override
	public void onCreate(ListViewer listViewer) {
		// TODO Auto-generated method stub
		list = listViewer;
	}

	@Override
	public View getView(LayoutInflater inflater, View convertView) {
		TextView title;
		if(convertView == null) {
			convertView = inflater.inflate(LAYOUT, null);
		}
		title= (TextView) convertView.findViewById(R.id.title);
		title.setText(res.getDonnees());
		
		title.setOnLongClickListener(this);
		
		Button carte = (Button) convertView.findViewById(R.id.carte) ;

		switch (res.getFormat()) {
		case ElementResultat.COORDONNEES:
			
			Log.d("","Coordonnées");
			carte.setText("Carte");
			carte.setVisibility(View.VISIBLE);

			ListenerBoutonCarte lstCarte = new ListenerBoutonCarte(ctx);
			lstCarte.add(res.getCoordonnee());
			carte.setOnClickListener(lstCarte);
			
			break;
			
		case ElementResultat.URL:
			
			Log.d("","URL");
			
			carte.setText("Suivre");
			carte.setVisibility(View.VISIBLE);
			carte.setOnClickListener(new OnClickListener()
			{
				@Override
				public void onClick(View arg0) {
					Context context_pres= list.getContext();
					
					Intent intentRes = new Intent (context_pres,ImageResultat_Activity.class);
					intentRes.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

					//Bundle objetbunble = new Bundle();
					//objetbunble.putString("url", res.getDonnees());
					//intentRes.putExtras(objetbunble);
					intentRes.putExtra("url", res.getDonnees());
					context_pres.startActivity(intentRes);
				}
			});
			break;
		default: //equivaut à : case ElementResultat.UNKNOWN:
			
			Log.d("","Unknown");
			carte.setVisibility(View.GONE);
			break;
		}
		return convertView;
	}

	@Override
	public int getLayout() {
		return 0;
	}

	@Override
	public DataModel getDataModel() {
		return null;
	}
	@Override
	public boolean onLongClick(View v) {
		AlertDialogBuilder.showAlertDialog(res.getDonnees(), "Resultat", ctx);
		return false;
	}

}
