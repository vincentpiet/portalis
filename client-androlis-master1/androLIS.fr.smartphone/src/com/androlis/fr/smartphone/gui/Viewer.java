package com.androlis.fr.smartphone.gui;

import android.view.LayoutInflater;
import android.view.View;

import com.androlis.fr.smartphone.gui.list.ListViewer;
import com.androlis.fr.smartphone.modele.data.DataModel;

//Interface pour generation de liste :
public interface Viewer {

	public void onCreate(ListViewer listViewer);
	public View getView(LayoutInflater inflater, View convertView);
	public int getLayout();
	public DataModel getDataModel();
	
}
