package com.androlis.fr.smartphone.gui.list;

import android.view.LayoutInflater;
import android.view.View;

public interface Viewer {

	public View getView(LayoutInflater inflater, View convertView);
	public int getLayout();
	
}
