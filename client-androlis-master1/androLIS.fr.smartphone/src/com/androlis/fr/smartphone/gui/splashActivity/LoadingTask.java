package com.androlis.fr.smartphone.gui.splashActivity;

import android.os.AsyncTask;
import android.widget.SeekBar;

public class LoadingTask extends AsyncTask<String, Integer, Integer>  {
	 public interface LoadingTaskFinishedListener {
		 void onTaskFinished();
	 }
	 private final SeekBar progressBar;
	 private final LoadingTaskFinishedListener finishedListener;
	 //Load Task qui chargera les donn�es necessaires � l'application
	 public LoadingTask(SeekBar progressBar, LoadingTaskFinishedListener finishedListener) {
	        this.progressBar = progressBar;
	        this.finishedListener = finishedListener;
	    }
	 @Override
	    protected Integer doInBackground(String... params) {
	       // Log.i("Tutorial", "Starting task with url: "+params[0]);
	        if(resourcesDontAlreadyExist()){
	            downloadResources();
	        }
	        // Perhaps you want to return something to your post execute
	        return 1234;
	    }
	 
	    private boolean resourcesDontAlreadyExist() {
	        // Here we query the app's internal state to see if this download had been performed before
	        return true; // returning true so we show the splash every time
	    }
	 
	 
	    private void downloadResources() {
	        // To imitate some process thats takes a bit of time (loading of resources / downloading)
	        int count = 10;
	        for (int i = 0; i < count; i++) {
	 
	            // Update the progress bar after every step
	            int progress = (int) ((i / (float) count) * 100);
	            publishProgress(progress);
	 
	            // Do some long loading things
	            try { Thread.sleep(200); } catch (InterruptedException ignore) {}
	        }
	    }
	    
	    @Override
	    protected void onProgressUpdate(Integer... values) {
	        super.onProgressUpdate(values);
	        progressBar.setProgress(values[0]); // This is ran on the UI thread so it is ok to update the progress bar 
	    }
	 
	    @Override
	    protected void onPostExecute(Integer result) {
	        super.onPostExecute(result);
	        finishedListener.onTaskFinished(); // Tell whoever was listening we have finished
	    }
	 
}
