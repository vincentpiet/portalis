package com.androlis.map;

import android.app.Activity;
import android.os.Bundle;

import com.fr.lis.androlis.smartphone.main.R;

public class LocalisationMap extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map);
    }
}
//extends FragmentActivity implements OnCameraChangeListener {
//
//	private GoogleMap mMap;
//	private float zoom;
//	
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.map);
//        setUpMapIfNeeded();
//        
//        /**/
//       Bundle bundle = getIntent().getExtras();
//       if( bundle != null && bundle.containsKey("points")){
//    	   ArrayList<String> points = bundle.getStringArrayList("points");
//    	   mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)) .getMap();
//    	   for(String str : points ){
//    		   String[] split = str.split(",");
//    		   Double latitude = Double.parseDouble(split[0]);
//    		   Double longitude = Double.parseDouble(split[1]);
//    		   
//    		   MarkerOptions mo = new MarkerOptions();
//    		   mo.position(new LatLng(latitude, longitude));
//    		   mMap.addMarker(mo);
//    	   }
//       }
//        
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        setUpMapIfNeeded();
//    }
//
//    private void setUpMapIfNeeded() {
//        if (mMap == null) {
//            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
//                    .getMap();
//            if (mMap != null) {
//                setUpMap();
//            }
//        }
//    }
//
//    private void setUpMap() {
//        // We will provide our own zoom controls.
//        mMap.getUiSettings().setZoomControlsEnabled(true);
//        mMap.getUiSettings().setZoomGesturesEnabled(true);
//        mMap.setOnCameraChangeListener(this);
//        // Show Sydney
//        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(-33.87365, 151.20689), 10));
//    }
//
//    /**
//     * When the map is not ready the CameraUpdateFactory cannot be used. This should be called on
//     * all entry points that call methods on the Google Maps API.
//     */
//    private boolean checkReady() {
//        if (mMap == null) {
//            Toast.makeText(this, "map pas prete", Toast.LENGTH_SHORT).show();
//            return false;
//        }
//        return true;
//    }
//    
//    public void onZoomIn() {
//        if (!checkReady()) {
//            return;
//        }
//        //TODO quadrillage
//        Log.d("","zoom in");
//        
//    }
//
//    /**
//     * Called when the zoom out button (the one with the -) is clicked.
//     */
//    public void onZoomOut() {
//        if (!checkReady()) {
//            return;
//        }
//        //TODO quadrillage
//        Log.d("","zoom out");
//    }
//
//    @Override
//	public void onCameraChange(CameraPosition position) {
//		if( position.zoom > zoom ){
//			onZoomIn();
//		}else{
//			if( position.zoom < zoom){
//				onZoomOut();
//			}
//		}
//		zoom = position.zoom;
//	}
//}