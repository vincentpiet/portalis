package com.androlis.map;

public interface OnPanAndZoomListener {
	    public void onPan();
	    public void onZoom();
}
