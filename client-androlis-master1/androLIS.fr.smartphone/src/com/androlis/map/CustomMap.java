package com.androlis.map;

import java.util.ArrayList;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.androlis.fr.smartphone.cartographie.map.point.CustomItemizedOverlay;
import com.androlis.fr.smartphone.cartographie.map.point.PointGraphique;
import com.androlis.fr.smartphone.cartographie.map.point.Zone;
import com.fr.lis.androlis.smartphone.main.R;
import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapView;
import com.google.android.maps.OverlayItem;

public class CustomMap extends MapView implements OnPanAndZoomListener{

	private int oldZoomLevel = -1;
	private GeoPoint oldCenterGeoPoint;
	private OnPanAndZoomListener mListener;
	private ArrayList<PointGraphique> points;

	private GeoPoint max;
	private GeoPoint min;

	CustomItemizedOverlay items;

	/**
	 * @param context
	 * @param apiKey
	 */
	public CustomMap(Context context, String apiKey) {
		super(context, apiKey);
		setOnPanListener(this);
		points = new ArrayList<PointGraphique>();
		items = new CustomItemizedOverlay(this.getResources().getDrawable(R.drawable.punaise2));
	}
	/**
	 * @param context
	 * @param attrs
	 */
	public CustomMap(Context context, AttributeSet attrs) {
		super(context, attrs);
		setOnPanListener(this);
		points = new ArrayList<PointGraphique>();
		items = new CustomItemizedOverlay(this.getResources().getDrawable(R.drawable.punaise2));
	}
	
	/**
	 * @param context
	 * @param attrs
	 * @param defStyle
	 */
	public CustomMap(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		setOnPanListener(this);
		points = new ArrayList<PointGraphique>();
		items = new CustomItemizedOverlay(this.getResources().getDrawable( R.drawable.punaise2 ));
	}

	public void setOnPanListener(OnPanAndZoomListener listener) {
		mListener = listener;
	}

	/**
	 * Ajouter un point à la map
	 * @param geoPoint
	 * @param drawable
	 */
	public void addPoint( GeoPoint geoPoint, int drawable){
		PointGraphique point = new PointGraphique(getContext(), geoPoint, drawable);
		this.points.add(point);
		items.addOverlay(new OverlayItem(geoPoint, "", ""));
		getOverlays().add(point);
	}

	/**
	 * Accès au points de la map
	 * @return
	 */
	public ArrayList<PointGraphique> getPoints(){
		return points;
	}

	/**
	 * Détéction des evenements sur la map
	 */
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (ev.getAction() == MotionEvent.ACTION_UP) {
			GeoPoint centerGeoPoint = this.getMapCenter();
			if (oldCenterGeoPoint == null || 
					(oldCenterGeoPoint.getLatitudeE6() != centerGeoPoint.getLatitudeE6()) ||
					(oldCenterGeoPoint.getLongitudeE6() != centerGeoPoint.getLongitudeE6()) ) {
				mListener.onPan();
			}
			oldCenterGeoPoint = this.getMapCenter();
		}
		return super.onTouchEvent(ev);
	}

	/**
	 * Detecte le onZoom
	 */
	@Override
	protected void dispatchDraw(Canvas canvas) {
		super.dispatchDraw(canvas);
		if (getZoomLevel() != oldZoomLevel) {
			mListener.onZoom();
			oldZoomLevel = getZoomLevel();
		}
	}

	/**
	 * do nothing
	 */
	@Override
	public void onPan() {

	}

	/**
	 * Calcul l'espace total occupé par les points ajoutés à la map
	 * Utilisé pour l'encadrement de l'ensemble des zones
	 */
	public void getMinMaxPoint(){

		int maxLong = Integer.MIN_VALUE, maxLat = Integer.MIN_VALUE;
		int minLong = Integer.MAX_VALUE, minLat = Integer.MAX_VALUE;

		for(PointGraphique o : points){
			GeoPoint tmp = o.getPoint();
			if( tmp.getLatitudeE6() < minLat ){minLat = tmp.getLatitudeE6();}
			if( tmp.getLatitudeE6() > maxLat ){maxLat = tmp.getLatitudeE6();}
			if( tmp.getLongitudeE6() < minLong ){minLong = tmp.getLongitudeE6();}
			if( tmp.getLongitudeE6() > maxLong ){maxLong = tmp.getLongitudeE6();}
		}
		setMin(new GeoPoint(minLat, minLong));
		setMax(new GeoPoint(maxLat, maxLong));
	}

	/**
	 * Calcul le centre des tous les points ajoutés à la map
	 * @return
	 */
	public GeoPoint getCenter(){
		if (points.size() == 0) {
			return new GeoPoint(0, 0);
		}

		int minLatitude = Integer.MAX_VALUE;
		int maxLatitude = Integer.MIN_VALUE;
		int minLongitude = Integer.MAX_VALUE;
		int maxLongitude = Integer.MIN_VALUE;

		for (PointGraphique point : points) {
			int lat = point.getPoint().getLatitudeE6();
			int lon = point.getPoint().getLongitudeE6();
			maxLatitude = Math.max(lat, maxLatitude);
			maxLongitude = Math.max(lon, maxLongitude);
			minLatitude = Math.min(lat, minLatitude);
			minLongitude = Math.min(lon, minLongitude);
		}

		return new GeoPoint((maxLatitude + minLatitude) / 2,
				(maxLongitude + minLongitude) / 2);
	}
	/**
	 * Listener, méthode déclenchée lors d'un zoom sur la map
	 */
	@Override
	public void onZoom() {
		pointVisible();
		invalidate();
	}
	/**
	 * Méthode qui calcul les points visible sur l'écran et ceux non visible
	 * Affecte à une zone les points visible et dessine 
	 * les points non visibles directement
	 */
	private void pointVisible(){
		int spanLatitude = getLatitudeSpan();
		int spanLongitude = getLongitudeSpan();
		GeoPoint center = this.getMapCenter();

		double latitudeHG = center.getLatitudeE6() + spanLatitude/2; 
		double longitudeHG = center.getLongitudeE6() - spanLongitude/2;
		GeoPoint hg = new GeoPoint((int)latitudeHG, (int)longitudeHG);

		ArrayList<PointGraphique> pointsIn = new ArrayList<PointGraphique>();
		ArrayList<PointGraphique> pointsOut = new ArrayList<PointGraphique>();

		Zone zoneVisible = new Zone(hg, spanLatitude, spanLongitude);

		for(PointGraphique pg : points){
			if( zoneVisible.pointAppartientZone(pg.getPoint())){
				pointsIn.add(pg);
				Log.d("LIS", "point appartient");
			}else{
				Log.d("LIS", "point n'appartient pas");
				pointsOut.add(pg);
			}
		}
		Cadrillage c = new Cadrillage(this);
		int nbDecoupage = c.getDecoupage(getZoomLevel());

		c.decoupeV2(nbDecoupage, spanLatitude, spanLongitude, center, pointsIn, pointsOut);
	}

	public GeoPoint getMax() {
		return max;
	}

	public void setMax(GeoPoint max) {
		this.max = max;
	}

	public GeoPoint getMin() {
		return min;
	}

	public void setMin(GeoPoint min) {
		this.min = min;
	}
}
