/**
 * @author Laurent ODERMATT
 * 
 * /!\ ne concerne que le thabor /!\
 * classe lisant le fichier kml pass� en param�tre, le transformant et l'�crivant dans le fichier csv sp�cifi� 
 */

import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class KmlToCsv {

	private FileWriter fw;
	private boolean first;
	private String fileI;
	private String fileO;
	private boolean firstLineOK;
	private boolean finished;
	private BufferedWriter output;

	public KmlToCsv(String fileI, String fileO){
		this.fileI = fileI;
		this.fileO = fileO;
		finished = false;
		first = true;
		read();
		
	}
	
	private String read(){
		Scanner scanner = null;
		String str = null;
		try {
			scanner = new Scanner(new FileReader(fileI));
			while (scanner.hasNextLine()) {
				str = scanner.nextLine();
				
				transform(str);

			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return str;
	}

	private void transform(String str) {
		
		//en-t�te
		if(!firstLineOK){
			
			if(str.contains("SimpleField")){
				String[] sstr = str.split("<SimpleField[^.]*name=");
			
				sstr = sstr[1].split("/>");
				sstr[0]+=";";
				writeFile(sstr[0]);
				
			}
			else if(str.contains("</Schema>")){
				firstLineOK = true;
				writeFile("coordinates;\n");
			}
		}
		if(str.contains("SimpleData")){
			String[] sstr = str.split("<SimpleData[^.]*\">");
			sstr = sstr[1].split("<");
			sstr[0]+=";";
			writeFile(sstr[0]);
			
		}
		//fin de lecture des donn�es, lecture des coordonn�es
		if(str.contains("<coordinates>")){
			String tmp = str.split("</*coordinates>")[1];
			
			writeFile(tmp+";\n");
		}
		if(str.contains("</kml>")){			
			//fermeture du fichier

			finished = true;
			try {
				output.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			JOptionPane.showMessageDialog(Converter.getInstance(), "conversion termin�e", "Message", JOptionPane.INFORMATION_MESSAGE);						

		}

	}

	private void writeFile(String text){	
		try
		{

			if(first){
				fw = new FileWriter(fileO, true);
				first = false;
				this.output = new BufferedWriter(fw);
			}
			
			//on marque dans le fichier ou plutot dans le BufferedWriter qui sert comme un tampon(stream)
			output.write(text);
			//on peut utiliser plusieurs fois methode write
			
			output.flush();
			//ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
			
		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}

	}
}
