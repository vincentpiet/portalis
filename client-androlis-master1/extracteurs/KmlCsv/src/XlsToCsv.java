/**
 * @author Tristan 
 * 
 * 
 */
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

import java.io.File;
import java.io.FileWriter;

public class XlsToCsv {

	public void write(String file_name_input,String file_name_output) {
		Workbook workbook = null;
		try {
			workbook = Workbook.getWorkbook(new File(file_name_input));
			/* Un fichier excel est composé de plusieurs feuilles, on y accède de la manière suivante*/
			Sheet sheet = workbook.getSheet(0);
			int n_c = number_columns(sheet);
			int n_r = number_rows(sheet);
			File f = new File (file_name_output);
		    FileWriter fw = new FileWriter (f);
			for (int i=0;i<n_r;i++) {
				for (int j=0; j<(n_c-1);j++) {
					fw.write('"' + sheet.getCell(j,i).getContents() + '"' + ",");	
				}
				fw.write('"' + sheet.getCell(n_c-1,i).getContents() + '"' + "\r\n");		
			}
		    fw.close();
		}
		catch (Exception e)
		{
		}
		
	}

	public static int number_columns(Sheet sheet) {
		int result = 0;
		boolean condition = true;
		Cell a1;
		while (condition) {
			try {
				a1 = sheet.getCell(result,0);
				result++;
				if (a1.getContents() == "") {
					condition = false;
				}
			}
			catch (Exception e) {
				condition = false;
			}
		}
		return (result-1);
	}
	
	public static int number_rows(Sheet sheet) {
		int result = 0;
		boolean condition = true;
		Cell a1;
		while (condition) {
			try {
				a1 = sheet.getCell(0,result);
				result++;
				if (a1.getContents() == "") {
					condition = false;
				}
			}
			catch (Exception e) {
				condition = false;
			}
		}
		return (result-1);
	}
}
