/**
 * @author Laurent ODERMATT
 * Classe lisant le fichier kml g�n�r� par google map.
 * La m�thode taxoSect permet de tester si un point se situe au sein d'un secteur et retourne le nom du secteur concern�.
 */
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;


public class MapReader {

	private String map;
	private String ctx;
	private String csv;
	/**
	 * liste de secteurs, chaque secteur �tant une liste de points
	 */
	private ArrayList<ArrayList<Point>> tabSectors;
	private boolean newSector;
	/**
	 * true si on est en train de lire les coordonn�es d'un secteur
	 */
	private boolean sector = false;
	private boolean test = true;
	private ArrayList<String> nameSect;
	private boolean first;
	private boolean placemark;
	/**
	 * liste contenant les diff�rents chemins gps
	 */
	private ArrayList<ArrayList<Point>> tabPolygones;
	/**
	 * liste contenant les diff�rentes zones gps
	 */
	private ArrayList<ArrayList<Point>> tabSegments;
	private boolean segment= false;
	private boolean polygon= false;
	/**
	 * gps :
	 *	0-> pas gps
	 * 	2-> nouveau segment
	 * 	3-> segment en lecture
	 * 	4-> nouveau polygone
	 * 	5-> polygone en lectures
	 */
	private int gps =0 ;
	/**
	 * boolean indiquant si on est en lecture de donn�es utilis�es par le gps
	 */
	private boolean gpsB = false;
	private boolean newSeg = false;
	private boolean newPol = false; 


	
	/**
	 * constructeur : lit le fichier csv et met les items dans l'une des 4 cat�gories (nord sud est ouest) suivant leurs coordonn�es
	 * @param fileI fichier csv utilis� en entr�e
	 * @param fileO fichier ctx en sortie
	 * @param mapFile fichier kml contenant les coordonn�es des diff�rents secteurs du parc (nord sud est ouest) 
	 */
	public MapReader(String fileI, String fileO, String mapFile) {
		this.map = mapFile;
		this.ctx = fileO;
		this.csv = fileI;
		first = true;
		newSector = true;
		nameSect = new ArrayList<String>();
		tabSectors = new ArrayList<ArrayList<Point>>();
		tabPolygones = new ArrayList<ArrayList<Point>>();
		tabSegments = new ArrayList<ArrayList<Point>>();
		read();
		
	}
	
	/**
	 * lit le fichier kml (map) et appelle transform pour stocker les infos
	 */
	private  void read(){
		Scanner scanner = null;
		String str = null;
		try {
			scanner = new Scanner(new FileReader(map));
			while (scanner.hasNextLine()) {
				str = scanner.nextLine();
				transform(str);
				
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		
		//trace pour tests
		for(int i = 0; i< tabSegments.size(); i++){
			System.out.println(" \n\nSegment" +i+" :");
			for(int j = 0; j< tabSegments.get(i).size(); j++)
				System.out.println(tabSegments.get(i).get(j).x+","+tabSegments.get(i).get(j).y);
		}
		for(int i = 0; i< tabPolygones.size(); i++){
			System.out.println(" \n\nPolygone" +i+" :");
			for(int j = 0; j< tabPolygones.get(i).size(); j++)
				System.out.println(tabPolygones.get(i).get(j).x+","+tabPolygones.get(i).get(j).y);
		}
	}
	
	/**
	 * stoque les infos relatives aux secteurs
	 * @param str la ligne lue (du fichier kml)
	 */
	private void transform(String str) {
		//si on termine ce secteur
		
		if(str.contains("</coordinates>")){
			sector = false;
			polygon = false;
			segment = false;
			gps = 0;
			gpsB = false;
		}
		// si on arrive dans une nouvelle forme utilis�e pour le gps
		else if(gpsB)
			//les valeurs sont mutlipli�es par 2. elles seront redivis�es au commencement de la lecture
			if(str.contains("<Polygon>"))
				gps = -4;
			else if(str.contains("<LineString>"))
				gps = -2;		
		//sinon, si on est en train de lire un secteur
		if(sector){
			if(newSector){
				tabSectors.add(new ArrayList<Point>());
				newSector = false;
			}
			Point nPoint = new Point(Double.parseDouble(str.split(",")[0]),Double.parseDouble(str.split(",")[1]));
			tabSectors.get(tabSectors.size()-1).add(nPoint);	
		}
		//sinon, si on est dans une zone concernee par le gps
		else if(gps>0){
			switch(gps){
			//on commence � lire un segment
			case 2 :
				tabSegments.add(new ArrayList<MapReader.Point>());
				gps = 3;
				newSeg = false;
				//si on �tait d�j� en train de lire un segment
			case 3 :
				Point newPoint = new Point(Double.parseDouble(str.split(",")[0]),Double.parseDouble(str.split(",")[1]));
				tabSegments.get(tabSegments.size()-1).add(newPoint);	
				break;
				//si on commence � lire un polygone
			case 4 :
				tabPolygones.add(new ArrayList<MapReader.Point>());
				gps = 5;
				newPol = false;
				//si on �tait d�j� en train de lire un polygone
			case 5 :
				Point newPoint1 = new Point(Double.parseDouble(str.split(",")[0]),Double.parseDouble(str.split(",")[1]));
				tabPolygones.get(tabPolygones.size()-1).add(newPoint1);	
				break;
			default:
				break;	
			}	
		}
		if(placemark && str.contains("<name>")){

			//dans le cas d'un secteur 
			if(str.split("<[/]?name>")[1].contains("secteur")){
				nameSect.add(str.split("<[/]?name>")[1]);
				placemark = false;
				gpsB = false;
			}
			//sinon, on est sur une chemin / un zone utilis�s par le GPS
			else{
				gpsB = true;
			}
		}
		else if(str.contains("<Placemark>"))
				placemark = true;
		//si on arrive dans un nouveau secteur
		if(str.contains("<coordinates>")){
			if(!gpsB){
				sector = true;
				newSector = true;
			}
			//on remet la valeur de gps � une valeur normale pour pouvoir commencer la lecture
			else if(gps == -2)
				newSeg = true;
			else if (gps == -4)
				newPol = true;
				
			gps*=-1;
			
			

		}

	}
	
	
	/**
	 * ajoute la taxonomie "sectoriale" (suivant le secteur du parc auquel appartient le point)
	 * @param str la ligne du csv lue
	 * @param name le nom de l'arbre
	 * @return la nouvelle ligne du ctx � �crire
	 */
	public String taxoSect(String str) {
		String tmp = str.split(";")[5];
		String ret = "-1";

			Point m = new Point(Double.parseDouble(tmp.split(",")[0]),Double.parseDouble(tmp.split(",")[1]));
			for(int i = 0; i < tabSectors.size();i++){
				if(appartient_polygone(tabSectors.get(i),m))
					ret = nameSect.get(i);

			}
			
			if(ret.equals("-1"))
				ret = "Autre_secteur";
		return ret;
	}
	
	/**
	 * 
	 * @return  la liste d'axiomes sectoriaux
	 */
	String getSectName(){
		String ret = "";
		for(int j = 0 ; j < nameSect.size();j++)
			ret+="axiom "+nameSect.get(j)+",Localisation\n";	
		return ret+"axiom Autre_secteur, Localisation\n";
	}
	
	

	/**
	 * m�thode qui calcule si le point se situe dans le polygone
	 * @author Tristan CHARRIER
	 * @param tab tableau de points du polygone
	 * @param M point recherch�
	 * @return true si le point est situ� dans le polygone
	 */
	public static boolean appartient_polygone(ArrayList<Point> tab,Point M) {
	  int compteur = 0;
	  double y;
	  int n = tab.size();
	  double coefdir;
	  double ordori;
	  for (int i = 0; i < n ;i++) {

	   if (i < (n-1)) {
		   coefdir = ((tab.get(i+1).y-tab.get(i).y)/(tab.get(i+1).x-tab.get(i).x));
		   ordori = tab.get(i).y-coefdir*tab.get(i).x;
		  // System.out.println("Coef directeur" + coefdir + " ordonn�e � l'origine : " +ordori);
	       y =  (coefdir*M.x) + ordori; 
	       //System.out.println(y);
	    
	    if ((M.x < max(tab.get(i+1).x,tab.get(i).x)) && (M.x > min(tab.get(i+1).x,tab.get(i).x)) && (y >= M.y)) {
	     compteur++;
	    }
	   }
	   else {
		   coefdir = ((tab.get(0).y-tab.get(i).y)/(tab.get(0).x-tab.get(i).x));
		   ordori = tab.get(i).y-coefdir*tab.get(i).x;
		  // System.out.println("Coef directeur" + coefdir + " ordonn�e � l'origine : " +ordori);
	       y =  (coefdir*M.x) + ordori; 
	     //  System.out.println(y);
	    if ((M.x < max(tab.get(0).x,tab.get(i).x)) && (M.x > min(tab.get(0).x,tab.get(i).x)) && (y >= M.y)) {
	     compteur++;
	    }
	   }

	  }
	  return ((compteur % 2) == 1);

	 }

	
	public static double min(double a, double b) {
		if (a<b) {
			return a;
		}
		else {
			return b;
		}
	}
	
	public static double max(double a, double b) {
		if (b<a) {
			return a;
		}
		else {
			return b;
		}
	}
	

	
	/* ************************************************CLASSES INTERNES*************************************************************************/
	
	private class Point{
		
		public double y;
		public double x;

		Point(double x,double y){
			
			this.x = x;
			this.y = y;
			
		}
		
	}



	
}
