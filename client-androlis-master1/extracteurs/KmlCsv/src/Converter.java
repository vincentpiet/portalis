/**
 * @author Laurent ODERMATT
 * 
 * interface du transformateur de fichiers 	csv -> ctx
 * 											xls -> csv
 * 											kml -> csv //fichiers internes sp�ciaux :  	- fichier de taxonomie familiale(taxo.csv)
 * 																						- fichier de taxo "sectoriale" 	(map.kml)
 */


import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;


public class Converter extends JFrame{

	private static Converter instance = null;
	private static Converter conv = null ;

	private final String CONFILE = "conf.convert";
	
	private static final long serialVersionUID = 1L;
	/**
	 * fichier de config de l'application. est utilis� pour d�cider de la position par d�faut des explorateurs de fichier.
	 */
	File confFile;
	/**
	 * fichier d'entr�e et de sortie
	 */
	private String fileI, fileO;
	/**
	 * chemins par d�faut pour les fileChoosers
	 */
	private String 	pathI, pathO;

	/**
	 * interface graphique
	 */
	private JTextField inTF, outTF;
	private JButton inB, outB, goB;
	private JCheckBox arbCB;
	/**
	 * filechooser utilis� lors de r�actions aux clics sur les boutons
	 */
	private JFileChooser chooserI,chooserO;
	

	
	public Converter(){
		readConfFile();	
		conv = this;

		confFile = new File(CONFILE);
		
		chooserI = new JFileChooser(pathI);
		chooserO = new JFileChooser(pathO);

	    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    
		this.setLayout(new GridLayout(3,2));
		inTF = new JTextField();
		inTF.setEditable(false);
		
		outTF = new JTextField();
		outTF.setEditable(false);
		
		inB = new JButton("Fichier d'entrée");
		outB = new JButton("Fichier de sortie");
		goB = new JButton("GO!");
		//arbCB = new JCheckBox("arbres",true);

		
		//ajout des r�actions sur les 3 boutons
		inB.addActionListener(new ActionListener(){


			@Override
			public void actionPerformed(ActionEvent arg0) {

			    int returnVal = chooserI.showOpenDialog(getParent());
			    if(returnVal == JFileChooser.APPROVE_OPTION) {
			       fileI =  chooserI.getSelectedFile().getAbsolutePath();
			       inTF.setText(chooserI.getSelectedFile().getName());

			       //sauvegarde du chemin courant pour la prochaine ex�cution
			       pathI = (new File(fileI)).getParent();
			       saveConfFile();

			     
			    }
			}
		});
		
		outB.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				
			    int returnVal = chooserO.showOpenDialog(getParent());
			    
			    if(returnVal == JFileChooser.APPROVE_OPTION) {

			       File f  = new File(chooserO.getSelectedFile().getAbsolutePath());
			       
			       //suppression du fichier d�j� existant
			       if( f.exists() ){
			    	   int reply = JOptionPane.showConfirmDialog(Converter.getInstance(),"Attention, ce fichier sera supprim�. Voulez-vous continuer?","Ecraser ?", JOptionPane.YES_NO_OPTION);
			    	   if(reply == JOptionPane.YES_OPTION){
			    		   	System.out.println("ok"+f.getAbsolutePath());
				    	   	System.out.println(f.delete()); 
				    	   	try {			      
				    	   		//et cr�ation d'un nouveau, vide
								f.createNewFile();
							    fileO =  chooserO.getSelectedFile().getAbsolutePath();
							    outTF.setText(chooserO.getSelectedFile().getName());
							    pathO = f.getParent();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
			    	   }
			       }else{
			    	   try {			      
			    	   		//et cr�ation d'un nouveau, vide
							f.createNewFile();
						    fileO =  chooserO.getSelectedFile().getAbsolutePath();
						    outTF.setText(chooserO.getSelectedFile().getName());
						    pathO = f.getParent();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
			       }
			     
			       //sauvegarde du chemin courant pour la prochaine ex�cution
			       saveConfFile();
			       
			    }
				
			}
			
		});		

		goB.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) {

				if(fileI != null && fileO != null){
					File f  = new File("fileI");
				    if( f.exists() )
				        f.delete();
				    
					if(fileI.split("\\.")[1].equals("xls"))
						new XlsToCsv().write(fileI, fileO);
					else if(fileI.split("\\.")[1].equals("kml"))
						new KmlToCsv(fileI, fileO);	
					else if(fileI.split("\\.")[1].equals("csv"))
						new CsvToCtx2(fileI, fileO, "taxo.csv");	
					else
						System.out.println("erreur : format inconnu");
				}
				
			}
			
		});
		
		this.add(inTF);
		this.add(inB);
		
		this.add(outTF);
		this.add(outB);
	
		this.add(goB);
		//this.add(arbCB);
	
		this.setTitle("Convertisseur");
		//affichage!
		this.setSize(300,150);
		// On r�cup�re la taille de l'�cran (la r�solution)
		Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
		// et on place notre fen�tre au milieu
		this.setLocation((screen.width - this.getSize().width)/2,(screen.height - this.getSize().height)/2);
		this.setResizable(false);

		this.setVisible(true);
	}
	
	/* ************************** config **************************** */
	
	/**
	 * lit le fichier de config et en tire les 2 chemins utilis�s par les fileChoosers
	 */
	private void readConfFile(){
		Scanner scanner = null;
		try {
			File tmp = new File(CONFILE);
			if(tmp.exists()){
				scanner = new Scanner(new FileReader(CONFILE));
				pathI = scanner.nextLine();
				pathO = scanner.nextLine();
				
			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * sauvegarde les valeurs des chemins utilis�s par les fileChoosers dans un fichier de config
	 */
	private void saveConfFile(){
		try {
	    	   FileWriter fwTmp = new FileWriter(confFile, false);
	    	   BufferedWriter output = new BufferedWriter(fwTmp);
	    	   output.write(pathI+"\n"+pathO);
	    	   output.flush();
	    	   output.close();
	       } catch (IOException e) {
	    	   // TODO Auto-generated catch block
	    	   e.printStackTrace();
	       }
	}
	
	/**
	 * launcher
	 * @param args
	 */
	public static void main(String[] args){
		new Converter();
		
		
	}

	public static Converter getInstance() {
		if(conv==null)
			conv = new Converter();
		return conv ;
	}
	
	
}
