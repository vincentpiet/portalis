/**
 * @author Laurent ODERMATT
 * 
 * /!\ ne concerne que le thabor /!\
 * classe lisant le fichier csv pass� en param�tre, le transformant et l'�crivant dans le fichier ctx sp�cifi� 
 */


import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import javax.swing.JOptionPane;


public class CsvToCtx2 {

	private String  mapFile= "zonesThabor.kml";
	
	private FileWriter fw;
	private boolean first;
	private String fileI, fileO;
	private boolean finished;
	private BufferedWriter output;
	private String fileTaxo;
	private Taxonomyer taxo;
	private MapReader mapR;
	
	public CsvToCtx2(String fileI, String fileO,String fileTaxo) {
		this.fileI = fileI;
		this.fileO = fileO;
		finished = false;
		first = true;
		this.fileTaxo = fileTaxo;
		taxo = new Taxonomyer(this,fileTaxo,fileO );
		this.setMapR(new MapReader(fileI,fileO,mapFile)); 
		writeFile(getMapR().getSectName());
		read();	//lecture du fichier csv et �criture du ctx avec la taxo g�n�rale
		taxo.readTaxoFam();// ajout de la taxo familiale
		

	}

	/**
	 * lecture
	 */
	private String read(){
		Scanner scanner = null;
		String str = null, tmp = null;
		try {
			
			scanner = new Scanner(new FileReader(fileI));
			while (scanner.hasNextLine()) {
				str = scanner.nextLine();
				if(!str.contains("coordinates")){
					if(str.contains("'")){
						str = str.replaceAll("'", ""); // ces caract�res entra�nent un conflit lors de la cr�ation du ctx
					}
					transform(str);
					tmp = taxo.taxoGen(str,0);
					writeFile(tmp);
					tmp = taxo.taxoGen(str,1);
					writeFile(tmp);
					
				}

			}
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return str;
	}

	/**
	* transforme la ligne lue ( format csv) en format ctx et l'�crit dans le fichier
	* @param str la ligne lue dans la pr�c�dente �tape
	*/
	private void transform(String str) {
		
		String[] sstr = str.split(";");
		
		
		for(int i = 0; i < sstr.length; i++){
			if(sstr[i].contains("?"))sstr[i] = ""; // suppression de toutes les occurences de "??"

		}
			
		
		if(!sstr[2].equals("")){ // si l'arbre a un identifiant
			
			String name = sstr[1];
			String tmp = "mk \""+name+"\" "; // cr�ation du d�but de la ligne

			tmp+="'"+sstr[2]+"'";// on ajoute le lieu et les coordonn�es
			
			if(!sstr[3].equals("")){ // puis le nom scientifique
				tmp+=","+sstr[3];
			}if(!sstr[4].equals("")){ //puis le nom commun
				tmp+=","+sstr[4];
			}
			tmp += ", gps is \""+sstr[5].split(",")[1]+","+sstr[5].split(",")[0]+"\" , ";// lecture du fichier map (contenant les coordonn�es des diff�rents secteurs) 
			//on ajoute la taxo sectorialle
			tmp += getMapR().taxoSect(str)+"\n";
			writeFile(tmp+"\n");

		}
		
	}
		
	

	/**
	 * �crit text dans le fichier fileO
	 * @param text
	 */
	void writeFile(String text){	
		try
		{

			if(first){
				fw = new FileWriter(fileO, true);
				first = false;
				this.output = new BufferedWriter(fw);
			}
			
			//on marque dans le fichier ou plutot dans le BufferedWriter qui sert comme un tampon(stream)
			output.write(text);
			//on peut utiliser plusieurs fois methode write
			
			output.flush();	//ensuite flush envoie dans le fichier, ne pas oublier cette methode pour le BufferedWriter
			
			if(finished){
				this.output.close();
				JOptionPane.showMessageDialog(Converter.getInstance(), "conversion termin�e", "Message", JOptionPane.INFORMATION_MESSAGE);
			}
			//et on le ferme

		}
		catch(IOException ioe){
			System.out.print("Erreur : ");
			ioe.printStackTrace();
		}

	}

	public void setMapR(MapReader mapR) {
		this.mapR = mapR;
	}

	/**
	 * 
	 * @return le MapReader
	 */
	public MapReader getMapR() {
		return mapR;
	}
}
