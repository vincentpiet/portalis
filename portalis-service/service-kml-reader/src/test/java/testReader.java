import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.logging.Logger;
import fr.irisa.lis.portalois.service.klm.KlmReader;


public class testReader {private static final Logger LOGGER = Logger.getLogger(testReader.class.getName());


	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @BeforeClass : TestCamelisReponse       |"
				+ "\n            ---------------------------------------------------\n");

	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		LOGGER.info(""
				+ "\n            ---------------------------------------------------"
				+ "\n            |         @AfterClass : TestCamelisReponse        |"
				+ "\n            ---------------------------------------------------\n");

	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testKlmReader() {
		final String fileName = "surf_minerales_thabor.kml";
		ClassLoader cl = Thread.currentThread().getContextClassLoader();
		URL url = cl.getResource(fileName);
		try {
			FileWriter writer = new FileWriter(new File("surf_minerales_thabor.ctx"));
			writer.append(KlmReader.read(url));
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
