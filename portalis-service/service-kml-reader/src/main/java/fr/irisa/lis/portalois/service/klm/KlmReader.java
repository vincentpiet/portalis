package fr.irisa.lis.portalois.service.klm;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.jkee.gtree.Tree;

import com.google.common.collect.Lists;

import de.micromata.opengis.kml.v_2_2_0.Boundary;
import de.micromata.opengis.kml.v_2_2_0.Coordinate;
import de.micromata.opengis.kml.v_2_2_0.Document;
import de.micromata.opengis.kml.v_2_2_0.ExtendedData;
import de.micromata.opengis.kml.v_2_2_0.Feature;
import de.micromata.opengis.kml.v_2_2_0.Folder;
import de.micromata.opengis.kml.v_2_2_0.Geometry;
import de.micromata.opengis.kml.v_2_2_0.Kml;
import de.micromata.opengis.kml.v_2_2_0.LinearRing;
import de.micromata.opengis.kml.v_2_2_0.Placemark;
import de.micromata.opengis.kml.v_2_2_0.Point;
import de.micromata.opengis.kml.v_2_2_0.Polygon;
import de.micromata.opengis.kml.v_2_2_0.Schema;
import de.micromata.opengis.kml.v_2_2_0.SchemaData;
import de.micromata.opengis.kml.v_2_2_0.SimpleData;
import de.micromata.opengis.kml.v_2_2_0.SimpleField;

public class KlmReader {

	public static final String AXIOM = "axiom";

	private static TaxonomyTreeBuilder.Funnel urlFunnel = new TaxonomyTreeBuilder.Funnel() {
		public List<String> getPath(String value) {
			List<String> path = Lists.newArrayList(value.split(" "));
			List<String> result = new ArrayList<String>();
			String current = null;
			for (int i = 1; i <= path.size(); i++) {
				String step = path.get(i - 1);
				if (step.length() <= 3
						&& !(step.equals("if") || step.equals("pin"))) {
					current = (current == null ? step : current + " " + step);
				} else {
					result.add((current == null ? step : current + " " + step));
					current = null;
				}
			}
			return result;
		}
	};
	private static TaxonomyTreeBuilder builder = new TaxonomyTreeBuilder(
			urlFunnel);

	public static void main(String[] args) {
		TaxonomyTreeBuilder.Funnel urlFunnel = new TaxonomyTreeBuilder.Funnel() {
			public List<String> getPath(String value) {
				return Lists.newArrayList(value.split("/"));
			}
		};
		List<String> urls = Lists.newArrayList("/home/url1/suburl",
				"/home/url1", "/home", "/home/url2/toto",
				"/home/url1/suburl/more", "/home/url1/suburl/truc",
				"/home/url3/more");
		TaxonomyTreeBuilder builder = new TaxonomyTreeBuilder(urlFunnel);
		Tree<String> build = builder.build(urls, "rootName");

		System.out.println(build.toStringTree());
	}

	/**
	 * Une taxonomie c'est une liste de noms identifi�e par un nom de taxonomie
	 * 
	 * @author bekkers
	 * 
	 */
	private static enum Taxonomie {
		ESPECE(), ESPECE_COM();
		List<String> names = new ArrayList<String>();

		Tree<String> tree = null;

		public Tree<String> getTree() {
			if (tree == null) {
				tree = builder.build(names, this.name());
			}
			return tree;
		}

		public String asCtx() {
			return asCtx(getTree(), this.name());
		}

		public String asCtx(Tree<String> root, String parentName) {
			StringBuffer buff = new StringBuffer();
			for (Tree<String> tree : root.getChildren()) {
				buff.append(AXIOM).append(" ")
						.append(makeTerm(tree.getValue())).append(", ")
						.append(makeTerm(parentName)).append("\n");
				if (tree.getChildren() != null)
					buff.append(asCtx("", tree));
			}
			return buff.toString();
		}

		private String asCtx(String prefixe, Tree<String> root) {
			StringBuffer buff = new StringBuffer();
			String newPrefixe = prefixe.length() == 0 ? root.getValue()
					: prefixe + " " + root.getValue();
			for (Tree<String> tree : root.getChildren()) {
				buff.append(AXIOM).append(" ")
						.append(makeTerm(newPrefixe + " " + tree.getValue()))
						.append(", ").append(makeTerm(newPrefixe)).append("\n");
				if (tree.getChildren() != null)
					buff.append(asCtx(newPrefixe, tree));
			}
			return buff.toString();
		}

		public String toString() {
			return getTree().toStringTree();
		}

		public void addName(String varName) {
			String val = varName;
			if (this.equals(Taxonomie.ESPECE))
				val = val.replaceAll("magnolia", "Magnolia");
			names.add(val);
		}
	}

	private static StringBuffer buff_mk = new StringBuffer();
	private static boolean first = true;

	private static String featureName;

	private static String makeTerm(String value) {
		return String.format("'%s'", value);
	}

	public static String read(URL url) {
		final Kml kml = Kml.unmarshal(new File(url.getFile()));
		final Document doc = (Document) kml.getFeature();

		List<Feature> features = doc.getFeature();

		for (Feature feature : features) {
			featureName = feature.getName();
			System.out.println(featureName);
		}

		List<Schema> schemas = doc.getSchema();

		for (Schema schema : schemas) {
			String name = schema.getName();
			List<SimpleField> fields = schema.getSimpleField();

			for (SimpleField simpleField : fields) {
				System.out.println(name + " " + simpleField.getName());
			}
		}

		Feature feature = kml.getFeature();
		processFeature(null, feature);

		StringBuffer axiomes = new StringBuffer();
		axiomes.append(Taxonomie.ESPECE.asCtx());
		axiomes.append(Taxonomie.ESPECE_COM.asCtx());
		return axiomes.append(buff_mk).toString();
	}

	public static void processFeature(Feature parentFeature, Feature feature) {
		if (feature instanceof Document) {
			processDocument(parentFeature, (Document) feature);
		} else if (feature instanceof Folder) {
			processFolder(parentFeature, (Folder) feature);
		} else if (feature instanceof Placemark) {
			processPlacemark(parentFeature, (Placemark) feature);
		} else {
			System.out
					.println("Feature " + feature.getName() + " : " + feature);
		}
	}

	public static void processDocument(Feature parentFeature, Document doc) {
		List<Feature> features = doc.getFeature();
		System.out.println("Document " + doc.getName());
		for (Feature docFeature : features) {
			processFeature(doc, docFeature);
		}
	}

	public static void processFolder(Feature parentFeature, Folder folder) {
		List<Feature> features = folder.getFeature();
		System.out.println("Folder " + folder.getName());
		for (Feature folderFeature : features) {
			processFeature(folder, folderFeature);
		}
	}

	private static void processPlacemark(Feature parentFeature,
			Placemark placemark) {
		Geometry geometry = placemark.getGeometry();

		// String str=(String)placemark.getDescription();
		// System.out.println(str);

		buff_mk.append("mk \"").append(featureName+"_"+placemark.getName()).append("\" ");
		first = true;

		ExtendedData extendedData = placemark.getExtendedData();
		List<SchemaData> datas = extendedData.getSchemaData();

		for (SchemaData data : datas) {
			List<SimpleData> simpleDatas = data.getSimpleData();
			for (SimpleData simpleData : simpleDatas) {
				if (simpleData.getValue().isEmpty()) { // �limine les valeurs
														// vides
				} else if (simpleData.getName().equals("ID_AORN")
						&& !simpleData.getValue().equals("0")) {
					buff_mk.append(getSimpleData(simpleData));
				} else if (simpleData.getName().equals("OBJECTID")) {
					buff_mk.append(getSimpleData(simpleData));
				} else {
					// rangement des valeurs des deux taxonomies
					for (Taxonomie taxon : Taxonomie.values()) {
						if (simpleData.getName().equals(taxon.name())) {
							processValue(simpleData.getValue(), taxon);
						}
					}
				}
			}
		}

		if (geometry instanceof Point) {

			List<Coordinate> coordinates = ((Point) geometry).getCoordinates();
			for (Coordinate coordinate : coordinates) {
				putComma();
				buff_mk.append(
						"gps is \"" + coordinate.getLongitude() + " ; "
								+ coordinate.getLatitude()).append("\"");
			}
		} else if (geometry instanceof Polygon) {
			Polygon polygon = (Polygon) geometry;
			Boundary outerBoundary = polygon.getOuterBoundaryIs();
			LinearRing linearRing = outerBoundary.getLinearRing();
			List<Coordinate> coordinates = linearRing.getCoordinates();
			putComma();
			buff_mk.append("gpspolygone is \"");
			for (Coordinate coordinate : coordinates) {
				buff_mk.append(
						coordinate.getLongitude() + ","
								+ coordinate.getLatitude()).append(" ");
			}
			buff_mk.append("\"\n");
		}

		buff_mk.append("\n");
	}

	private static void processValue(String value, Taxonomie taxon) {
		// Removes whitespace between a word character and . or ,
		final String patternVar = "^([\\w ]+)'([\\w ]+)'$";
		final String patternSubsp = "^([\\w ]+)subsp ([\\w ]+)$";
		String stringVal = value.trim().replaceAll("_", " ").toLowerCase();

		String varName = null;
		if (stringVal.matches(patternVar)) {
			varName = stringVal.replaceAll(patternVar, "$1var.$2");
		} else if (stringVal.matches(patternSubsp)) {
			varName = stringVal.replaceAll(patternSubsp, "$1subsp.$2");
		} else {
			varName = stringVal.replace("'", " ");
		}
		taxon.addName(varName);
		putComma();
		buff_mk.append(makeTerm(varName));
	}

	private static void putComma() {
		if (first) {
			first = false;
		} else {
			buff_mk.append(", ");
		}
	}

	private static String getSimpleData(SimpleData simpleData) {
		StringBuffer buff = new StringBuffer(", ");
		if (first) {
			buff = new StringBuffer();
			first = false;
		}
		buff.append(simpleData.getName()).append(" is \"")
				
				.append(simpleData.getValue()).append("\"");

		return buff.toString();
	}
}
