package fr.irisa.lis.portalois.service.klm;

import com.flipthebird.gwthashcodeequals.EqualsBuilder;
import com.flipthebird.gwthashcodeequals.HashCodeBuilder;

public class Dependency {
	
	private String leftPart;
	private String rightPart;
	
	public Dependency() {}
	
	public Dependency(String leftPart, String rightPart) {
		this.leftPart = (leftPart.startsWith("'") ? leftPart : String.format("'%s'", leftPart.trim())) ;
		this.rightPart = (rightPart.startsWith("'") ? rightPart : String.format("'%s'", rightPart.trim())) ;
	}
		
	@Override
	public boolean equals(Object aThat){
		  if ( this == aThat ) return true;

		  if ( !(aThat instanceof Dependency) ) return false;
		  //you may prefer this style, but see discussion in Effective Java
		  //if ( aThat == null || aThat.getClass() != this.getClass() ) return false;

		  final Dependency that = (Dependency)aThat;
			return new EqualsBuilder()
			.append(this.leftPart, that.leftPart)
			.append(this.rightPart, that.rightPart)
			.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				//appendSuper(super.hashCode()).
				append(leftPart).
				append(rightPart).
				toHashCode();
	}

	
	public String getLeftPart() {
		return leftPart;
	}
	public void setLeftPart(String leftPart) {
		this.leftPart = leftPart;
	}
	
	public String asCtx() {
		return String.format("axiom %s, %s\n", leftPart, rightPart);
	}
	
	public String toString() {
		return String.format("%s::%s", leftPart, rightPart);
	}

	public String getRightPart() {
		return rightPart;
	}

	public void setRightPart(String rightPart) {
		this.rightPart = rightPart;
	}

}
