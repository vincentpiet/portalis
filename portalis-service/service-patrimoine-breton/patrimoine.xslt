<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:ci="http://www.openarchives.org/OAI/2.0/"
	xmlns:jug="java:fr.irisa.lis.portalis.service.utilities.Eval">
<xsl:output encoding="UTF-8" method="text"/>

	<xsl:template match="/">
			<xsl:apply-templates select="ci:OAI-PMH/ci:ListRecords/ci:record[not(ci:header/@status='deleted')]" />
	</xsl:template>

	<xsl:template match="ci:record">
<xsl:text>mk "</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:metadata/ci:CI/@REF), '&quot;','\\&quot;')"/>
<xsl:text>" id is "</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:header/ci:identifier), '&quot;','\\&quot;')"/>
<xsl:text>", datestamp is "</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:header/ci:datestamp), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
			<xsl:apply-templates select="ci:metadata/ci:CI/ci:DOSARCH/ci:NOTIARCH/ci:DESIARCH | ci:metadata/ci:CI/ci:DOSOBJT/ci:NOTIOBJT/ci:DESIOBJT" />
			<xsl:apply-templates select="ci:metadata/ci:CI/ci:DOSARCH | ci:metadata/ci:CI/ci:DOSOBJT | ci:metadata/ci:CI/ci:DOSCOLL" />
<xsl:text>
</xsl:text>
	</xsl:template>


		<xsl:template match="ci:DOSARCH | ci:DOSOBJT | ci:DOSCOLL">

<xsl:text>, CI_TYPE is "</xsl:text>
<xsl:value-of select="name()"/>
<xsl:text>"</xsl:text>

		
<xsl:for-each select="tokenize(@COPY,';')">
<xsl:text>, COPY is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(@DBOR,';')">
<xsl:text>, DBOR is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(@DENQ,';')">
<xsl:text>, DENQ is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(@DOSS,';')">
<xsl:text>, DOSS is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(@ETUD,';')">
<xsl:text>, ETUD is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(@NOMS,';')">
<xsl:text>, NOMS is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>
            
<xsl:for-each select="tokenize(@RENV,';')">
<xsl:text>, RENV is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(@TYPE,';')">
<xsl:text>, TYPE is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

			<xsl:apply-templates select="ci:NOTIARCH/ci:DESIARCH | ci:NOTIOBJT/ci:DESIOBJT" />
			<xsl:apply-templates select="ci:NOTIARCH/ci:LOCAARCH | ci:NOTIOBJT/ci:LOCAOBJT" />
			<xsl:apply-templates select="ci:NOTIARCH/ci:HISTARCH | ci:NOTIOBJT/ci:HISTOBJT" />
			<xsl:apply-templates select="ci:GEOARCH | ci:GEOOBJT" />

			<xsl:apply-templates select="ci:NOTICOLL" />
			

 	</xsl:template>

 		<xsl:template match="ci:NOTICOLL">
 			<xsl:call-template name="DESI"/>
 			<xsl:call-template name="LOCA"/>
 		</xsl:template>
 		
 		<xsl:template match="ci:DESIARCH | ci:DESIOBJT">
 			<xsl:call-template name="DESI"/>
 		</xsl:template>
 		
 		<xsl:template name="DESI">

<xsl:for-each select="tokenize(ci:COLL,';')">
<xsl:text>, COLL is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:DENO,';')">
<xsl:text>, DENO is "</xsl:text>
<xsl:value-of select="normalize-space(replace(normalize-space(.), '&quot;','\\&quot;'))"/>
<xsl:text>"</xsl:text>
</xsl:for-each>
            
<xsl:for-each select="tokenize(ci:NART,';')">
<xsl:text>, NART is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>
     
     <!--      
<xsl:for-each select="tokenize(ci:TICO,';')">
<xsl:text>, TICO is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>
 		 -->   		

<xsl:for-each select="tokenize(ci:PDEN,';')">
<xsl:text>, PDEN is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

  
<xsl:for-each select="tokenize(ci:VOCA,';')">
<xsl:text>, VOCA is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>


<xsl:for-each select="tokenize(ci:PART,';')">
<xsl:text>, PART is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>
 		</xsl:template>
 		
 		
 		<xsl:template match="ci:LOCAARCH | ci:LOCAOBJT">
 		<xsl:call-template name="LOCA"/>
 		</xsl:template>
 		
 		<xsl:template name="LOCA">
 		<!--  
<xsl:if test="ci:LOCA/ci:DPT">
<xsl:text>, departement = </xsl:text>
<xsl:value-of select="replace(normalize-space(ci:LOCA/ci:DPT), '&quot;','\\&quot;')"/>
</xsl:if>
-->
<xsl:if test="ci:LOCA/ci:COM">
<xsl:text>, '</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:LOCA/ci:COM), '&quot;','\\&quot;')"/>
<xsl:text>'</xsl:text>
</xsl:if>

<xsl:if test="ci:INSEE">
<xsl:text>, insee is "</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:INSEE), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:if>

<xsl:if test="ci:AIRE">
<xsl:text>, aire is "</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:AIRE), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:if>

<xsl:if test="ci:CANT">
<xsl:text>, canton is "</xsl:text>
<xsl:value-of select="replace(normalize-space(ci:CANT), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:if>

<xsl:for-each select="tokenize(ci:IMPL,';')">
<xsl:text>, impl is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

 		</xsl:template>
 		
 		<xsl:template match="ci:HISTARCH | ci:HISTOBJT">
<xsl:for-each select="tokenize(ci:SCLE,';')">
<xsl:text>, scle is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:EXEC,';')">
<xsl:text>, scle is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:SCLD,';')">
<xsl:text>, scld is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:DATE,';')">
<xsl:text>, histarch_date is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:JDAT,';')">
<xsl:text>, jdat is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:AUTR,';')">
<xsl:text>, histarch_autr is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

<xsl:for-each select="tokenize(ci:JATT,';')">
<xsl:text>, attribution is "</xsl:text>
<xsl:value-of select="replace(normalize-space(.), '&quot;','\\&quot;')"/>
<xsl:text>"</xsl:text>
</xsl:for-each>

 		</xsl:template>
 		
 		<xsl:template match="ci:GEOARCH | ci:GEOOBJT">
 		<xsl:if test="ci:ZONE and ci:COOR/ci:Y and ci:COOR/ci:X">
<xsl:text>, gps is "</xsl:text>
<xsl:value-of select="jug:toWGS84(ci:ZONE,ci:COOR/ci:X,ci:COOR/ci:Y)"/>
<xsl:text>"</xsl:text>
 		</xsl:if>
 		</xsl:template>
</xsl:stylesheet>