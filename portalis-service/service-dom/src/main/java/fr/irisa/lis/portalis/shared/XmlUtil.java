package fr.irisa.lis.portalis.shared;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public class XmlUtil {

	public static final String MESSAGE = "message";

	public static void writeFile(Document doc, String fileName)
			throws IOException, TransformerException {
		DOMSource domSource = new DOMSource(doc);
		FileWriter writer = new FileWriter(fileName);
		StreamResult result = new StreamResult(writer);
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer transformer = tf.newTransformer();
		transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		transformer.transform(domSource, result);

	}

	public static String prettyXmlString(Node elem) {
		try {
			DOMSource domSource = new DOMSource(elem);
			StringWriter writer = new StringWriter();
			StreamResult result = new StreamResult(writer);
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.CDATA_SECTION_ELEMENTS,
					MESSAGE);
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
					"yes");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(
					"{http://xml.apache.org/xslt}indent-amount", "3");
			transformer.transform(domSource, result);
			writer.flush();
			return writer.toString();
		} catch (TransformerException ex) {
			ex.printStackTrace();
			return null;
		}
	}

	public static Element parseXMLString(String string) {
		Document doc = null;
		try {
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory
					.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(new ByteArrayInputStream(string
					.getBytes("ISO-8859-1")));
		} catch (IllegalStateException e) {
			// Http connection is impossible
			String errMess = "impossible to connect using http during parseXML()\n"+e.getMessage()+"\nCause = "+
					e.getCause() == null ? "no cause"
							: e.getCause().getMessage();
			return errorElement(errMess);
		} catch (IOException e) {
			// I/O failure such as socket timeout or an socket reset
			String errMess = "I/O failure during http request during parseXML()\n"+e.getMessage()+"\nCause = "+
					 e
						.getCause() == null ? "no cause" : e
						.getCause().getMessage();
			return errorElement(errMess);
		} catch (Exception e) {
			String errMess = "parse XML error";
			return errorElement(errMess);
		}
		return doc.getDocumentElement();
	}

	public static Element errorElement(String mess) {
		Document doc = createDocument();
		Element root = doc.createElement("error");
		root.setAttribute(MESSAGE, mess);
		doc.appendChild(root);
		return root;
	}

	public static Document createDocument() {
		DocumentBuilderFactory docFactory = DocumentBuilderFactory
				.newInstance();
		DocumentBuilder docBuilder;
		try {
			docBuilder = docFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			throw new RuntimeException("impossible de créer l'élément avec DOM",
					e);
		}

		// root elements
		Document doc = docBuilder.newDocument();
		return doc;
	}

	static public Document readFile(File file) throws SAXException,
			IOException, ParserConfigurationException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		return db.parse(file);
	}

}
