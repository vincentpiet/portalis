package fr.irisa.lis.portalis.shared;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class CvsReader {

	private static final String rootName = "rows";
	private static final String rowName = "row";
	private static String cvsSplitBy = ";";
	private String[] headers;

	private static String cvsFile = "/local/bekkers/portalis/portalis-service/service-commune-insee/communes_size.csv";
	private static String outputFileName = "/local/bekkers/portalis/portalis-service/service-commune-insee/communes_size.rows.xml";

	public static void main(String[] args) throws IOException,
			TransformerException {

		CvsReader cvsReader = new CvsReader();

		if (args.length == 2) {
			cvsFile = args[0];
			outputFileName = args[1];
		}
		System.out.println(cvsReader.listHeaders(cvsFile));
		Document doc = cvsReader.readFile(cvsFile);
		XmlUtil.writeFile(doc, outputFileName);

	}

	public String[] getHeaders(String cvsFile) throws IOException {
		BufferedReader br = new BufferedReader(new FileReader(cvsFile));

		headers = br.readLine().split(cvsSplitBy);
		br.close();
		return headers;
	}

	public void setHeaders(String[] newHeaders) {
		headers = newHeaders;
	}

	public String listHeaders(String cvsFile) throws IOException {
		StringBuffer buff = new StringBuffer();
		headers = getHeaders(cvsFile);
		for (String header : headers) {
			buff.append(header).append("\n");
		}
		return buff.toString();
	}

	public Document readFile(String cvsFile) {

		Document doc = XmlUtil.createDocument();
		Element root = doc.createElement(rootName);
		doc.appendChild(root);

		BufferedReader br = null;
		String line = "";

		try {

			br = new BufferedReader(new FileReader(cvsFile));

			String[] tempo = br.readLine().split(cvsSplitBy);
			if (headers == null) {
				headers = tempo;
			}

			while ((line = br.readLine()) != null) {

				// use comma as separator
				String[] colums = line.split(cvsSplitBy);
				Element row = doc.createElement(rowName);
				root.appendChild(row);
				for (int i = 0; i < headers.length; i++) {
					Element property = doc.createElement("property");
					row.appendChild(property);
					if (i >= colums.length) {
						property.setAttribute("name", headers[i]);
						property.setAttribute("value", "null");
					} else {
						property.setAttribute("name", headers[i]);
						property.setAttribute("value", colums[i]);
					}
				}
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return doc;
	}
}
