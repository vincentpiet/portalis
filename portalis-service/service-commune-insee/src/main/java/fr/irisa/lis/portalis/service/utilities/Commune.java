package fr.irisa.lis.portalis.service.utilities;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import fr.irisa.lis.portalis.shared.XmlUtil;

public class Commune {

	static final XPathFactory FACTORY = XPathFactory.newInstance();
	static final XPath xpath = FACTORY.newXPath();
	static XPathExpression exprCommunes;
	
	static String xmlFile = "/local/bekkers/portalis/portalis-service/service-commune-insee/communes.xml";
	static String ctxFile = "/local/bekkers/portalis/portalis-service/service-commune-insee/communes.ctx";

	private static Map<String, Commune> communes = new HashMap<String, Commune>();
	private static Map<String, List<Commune>> homonymes = new HashMap<String, List<Commune>>();

	private String circEU;
	private String regionNom;
	private String regionCode;
	private String dptNom;
	private String dptNum;
	private String nom;
	private String insee;
	private Double longitude;
	private Double latitude;
	private Double eloignement;

	public Commune(Element circEU, Element region, Element dpt, Element commune) {
		this.circEU = circEU.getAttribute("name");
		this.regionNom = region.getAttribute("name");
		this.regionCode = region.getAttribute("code");
		this.dptNom = dpt.getAttribute("name");
		this.dptNum = dpt.getAttribute("num");
		this.nom = commune.getAttribute("name");
		this.insee = commune.getAttribute("code_insee");

		try {
			longitude = Double.parseDouble(commune.getAttribute("longitude"));
			latitude = Double.parseDouble(commune.getAttribute("latitude"));
			eloignement = Double.parseDouble(commune.getAttribute("�loignement"));
		} catch (NumberFormatException e) {
			longitude = null;
			latitude = null;
			eloignement = null;
		}
		
		String key = nom + ":" + dptNum;
		Commune ville = communes.put(key, this);
		if (ville != null) {
			System.out.println("oups ... doublon " + key);
		}

		List<Commune> listHomonymes = homonymes.get(nom);
		if (listHomonymes == null) {
			listHomonymes = new ArrayList<Commune>();
			homonymes.put(nom, listHomonymes);
		}
		listHomonymes.add(this);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		if (args.length == 2) {
			xmlFile = args[0];
			ctxFile = args[1];
		}

		try {
			getCommunes(new File(xmlFile));
			
			Commune.toCtx();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static void toCtx() {
		System.out.println(Commune.getCommunes().size() + " "
				+ Commune.getHomonymes().size());
	}

	public String getCircEU() {
		return circEU;
	}

	public void setCircEU(String circEU) {
		this.circEU = circEU;
	}

	public String getRegionNom() {
		return regionNom;
	}

	public void setRegionNom(String regionNom) {
		this.regionNom = regionNom;
	}

	public String getRegionCode() {
		return regionCode;
	}

	public void setRegionCode(String regionCode) {
		this.regionCode = regionCode;
	}

	public String getDptNom() {
		return dptNom;
	}

	public void setDptNom(String dptNom) {
		this.dptNom = dptNom;
	}

	public String getDptNum() {
		return dptNum;
	}

	public void setDptNum(String dptNum) {
		this.dptNum = dptNum;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getInsee() {
		return insee;
	}

	public void setInsee(String insee) {
		this.insee = insee;
	}

	public static Map<String, Commune> getCommunes() {
		return communes;
	}

	public static Map<String, List<Commune>> getHomonymes() {
		return homonymes;
	}
	
	public String getUniqueName() {
		if (homonymes.get(this.nom).size()==1) {
			return this.nom;
		} else {
			return this.nom+":"+dptNum;
		}
	}

	public static void getCommunes(File file) throws SAXException, IOException,
			ParserConfigurationException, XPathExpressionException {
		Document doc = XmlUtil.readFile(file);
		exprCommunes = xpath
				.compile("/communes/circonscription-europ�enne/r�gion/d�partement/commune");
		NodeList lesCommunes = ((NodeList) exprCommunes.evaluate(doc,
				XPathConstants.NODESET));
		System.out.println("lesCommunes.length = " + lesCommunes.getLength());
		for (int i = 0; i < lesCommunes.getLength(); i++) {
			Element commune = (Element) lesCommunes.item(i);
			Element dpt = (Element) commune.getParentNode();
			Element region = (Element) dpt.getParentNode();
			Element circEU = (Element) region.getParentNode();
			new Commune(circEU, region, dpt, commune);
		}
	}

	public String toString() {
		return null;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getEloignement() {
		return eloignement;
	}

	public void setEloignement(Double eloignement) {
		this.eloignement = eloignement;
	}
}
