<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="2.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output encoding="UTF-8" indent="yes" />
	<xsl:key name="first_EU_circo" match="/rows/row" use="not(@EU_circo=preceding-sibling::row/@EU_circo)"/>
	<xsl:key name="first_région" match="/rows/row[not(@nom_région=preceding-sibling::row/@nom_région)]" use="@EU_circo"/>
	<xsl:key name="first_dpt" match="/rows/row[not(@nom_département=preceding-sibling::row/@nom_département)]" use="@code_région"/>
	<xsl:key name="first_commune" match="/rows/row[not(@nom_commune=preceding-sibling::row/@nom_commune) and not(@nom_commune=following-sibling::row/@nom_commune)]" use="@numéro_département"/>
	<xsl:key name="multiple_commune" match="/rows/row[@nom_commune=preceding-sibling::row/@nom_commune or @nom_commune=following-sibling::row/@nom_commune]" use="@numéro_département"/>

	<xsl:template match="/rows">
		<communes>
			<xsl:for-each select="key('first_EU_circo', true())">
				<circonscription-européenne name="{@EU_circo}">
				<xsl:variable name="EU_circo" select="@EU_circo"/>
					<xsl:for-each
						select="key('first_région', $EU_circo)">
						<région name="{@nom_région}" code="{@code_région}" chef-lieu="{@chef-lieu_région}">
						<xsl:variable name="code_région" select="@code_région"/>

							<xsl:for-each select="key('first_dpt', $code_région)">
								<département name="{@nom_département}" num="{@numéro_département}" préfecture="{@préfecture}">
								<xsl:variable name="numéro_département" select="@numéro_département"/>

									<xsl:for-each
										select="key('first_commune', $numéro_département)">
										<commune name="{@nom_commune}" code_insee="{@code_insee}"/>
									</xsl:for-each>

									<xsl:for-each
										select="key('multiple_commune', $numéro_département)">
										<xsl:variable name="commune" select="." />
										<xsl:if
											test="not(preceding-sibling::row[@nom_commune=$commune/@nom_commune and @nom_département=$commune/@nom_département] | following-sibling::row[@nom_commune=$commune/@nom_commune and @nom_département=$commune/@nom_département])">
											<commune name="{@nom_commune}" code_insee="{@code_insee}" code_postaux="{@code_postaux}"
											longitude="{@longitude}" latatude="{@latitude}" éloignement="{@éloignement}"
											numéro_circonscription="{@numéro_circonscription}"/>
										</xsl:if>
									</xsl:for-each>

								</département>
							</xsl:for-each>

						</région>
					</xsl:for-each>

				</circonscription-européenne>
			</xsl:for-each>
		</communes>
	</xsl:template>
</xsl:stylesheet>