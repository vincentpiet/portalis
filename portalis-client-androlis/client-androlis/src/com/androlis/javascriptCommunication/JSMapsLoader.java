package com.androlis.javascriptCommunication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.webkit.JavascriptInterface;

import com.androlis.JSMaps;
import com.androlis.JSMaps.MarkerData;

public class JSMapsLoader {
	public Context ctx=null;

	public JSMapsLoader(Context ctx) {
		this.ctx=ctx;
	}

	@JavascriptInterface
	public JSONArray getMarkers() {
		JSONArray markers = new JSONArray();

		for(MarkerData markerData : JSMaps.markerArray) {
			JSONObject marker=new JSONObject();
			try {
				marker.put("lat",markerData.lat);
				marker.put("lon",markerData.lon);
				marker.put("title", markerData.title);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			markers.put(marker);
		}

		return markers;
	}

	@JavascriptInterface
	public JSONArray getKmls() {
		JSONArray kmls = new JSONArray();
		kmls.put("https://bitbucket.org/vincentpiet/kmlfiles/raw/f9d3875d31cb1b2976aeab0614e4c104933a58e4/kmlFiles/massifs_arbustifs_thabor.kml");
		kmls.put("https://bitbucket.org/vincentpiet/kmlfiles/raw/f9d3875d31cb1b2976aeab0614e4c104933a58e4/kmlFiles/pelouses_thabor.kml");
		kmls.put("https://bitbucket.org/vincentpiet/kmlfiles/raw/f9d3875d31cb1b2976aeab0614e4c104933a58e4/kmlFiles/surf_minerales_thabor.kml");
		kmls.put("https://bitbucket.org/vincentpiet/kmlfiles/raw/f9d3875d31cb1b2976aeab0614e4c104933a58e4/kmlFiles/massifs_floraux_thabor.kml");
		kmls.put("https://bitbucket.org/vincentpiet/kmlfiles/raw/f9d3875d31cb1b2976aeab0614e4c104933a58e4/kmlFiles/rosiers_thabor.kml");
		return kmls;
	}

}
