package com.androlis;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.androlis.javascriptCommunication.JSMapsLoader;
import com.androlis.list.Doublet;
import com.androlis.list.List_MyCustomAdapter;
import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.AttributesGetZoom;
import com.androlis.networkCommunication.ObjectsExtent;
import com.androlis.slidingMenu.FlyOutContainer;

import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

@SuppressLint("SetJavaScriptEnabled")
public class JSMaps extends Activity{
	private WebView webView=null;
	public static ArrayList<MarkerData> markerArray = null;
	private ListView listItems;
	private List_MyCustomAdapter adapter;
	FlyOutContainer root;
	private int menuMargin = 0;
	ListView listObjects = null;
	List_MyCustomAdapter adapterObjects = null;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_jsmaps);
		
		root = (FlyOutContainer) findViewById(R.id.JSMaps_flyout);
		
		menuMargin = (int) (getWindowManager().getDefaultDisplay().getWidth()-400);
		if(menuMargin<=0)
			menuMargin = 0;
		else
			menuMargin = (int) (menuMargin*0.7);

		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());
		findViewById(R.id.header_arrow).setVisibility(View.VISIBLE);


		//Create a WebView which is able to display JavaScript
		webView = (WebView)this.findViewById(R.id.jsmaps__webView); 
		webView.getSettings().setJavaScriptEnabled(true);

		//Display timeline.html in this view
		webView.loadUrl("file:///android_asset/www/jsmaps.html");
		webView.addJavascriptInterface(new JSMapsLoader(this), "mapData");

		listItems = (ListView) findViewById(R.id.JSMaps_menu_list);
		adapter= new List_MyCustomAdapter(getApplicationContext());
		listItems.setAdapter(adapter);

		adapter.addSeparatorItem("Calques KML");
		adapter.addItem("massifs arbustifs");
		adapter.addItem("pelouses");
		adapter.addItem("surfaces minerales");
		adapter.addItem("massifs floraux");
		adapter.addItem("rosiers");
		
		listObjects = (ListView) findViewById(R.id.JSMaps_menu_objects_list);
		adapterObjects = new List_MyCustomAdapter(getApplicationContext());
		listObjects.setAdapter(adapterObjects);

		addMarkers();
		
		adapterObjects.addSeparatorItem("Mode navigation");

		listItems.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				int pos = 0; 
				String name = adapter.getItem(position).getItem();
				
				Doublet[] names = Memory.getKmlsNames();
				
				while(pos<name.length() && name != names[pos].arg0)
					pos++;
				
				if((Boolean) names[pos].arg1) {
					parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.blue_start));
					webView.loadUrl("javascript:displayKML("+(pos)+")");
					names[pos].arg1 = false;
				} else {
					parent.getChildAt(position).setBackgroundColor(getResources().getColor(R.color.white));
					webView.loadUrl("javascript:hideKML("+(pos)+")");
					names[pos].arg1 = true;
				}
				
			}
		});
		
		
//		Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://maps.google.com/maps?f=d&daddr=51.448,-0.972"));
//		intent.setComponent(new ComponentName("com.google.android.apps.maps", "com.google.android.maps.MapsActivity"));
//		startActivity(intent);	
		
		listObjects.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				
			}
		});
		
		

		findViewById(R.id.header_logo).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				root.toggleMenu();
			}
		});
		
		findViewById(R.id.header_arrow).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				root.toggleMenu();
			}
		});
	}

	/**
	 * Add markers from Objects which have à gps Property, from the current request
	 */
	private void addMarkers() {
		//Retrieve all the gps positions in properties thanks to "gps ?"
		AttributesGetZoom attributesGetZoom = new AttributesGetZoom();
		ZoomReponse zoomReponse=null;
		String[] params = {Memory.getRequestMemorizator().getState(), "gps ?"};
		try {
			zoomReponse = attributesGetZoom.execute(params).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LisIncrementSet receivedIncrs = zoomReponse.getIncrements();
		LisIncrement[] increments = receivedIncrs.toArray();

		markerArray = new ArrayList<JSMaps.MarkerData>();

		//Retrieve all the Objects located at the gps position and add them to the map
		for(LisIncrement lisIncrement : increments) {
			addMarkers(lisIncrement);
		}
		
		Memory.getRequestMemorizator().clean();
		listObjects.setAdapter(adapterObjects);
	}


	/**
	 * Add Markers for each Object which is located to the position contains in lisIncrement 
	 * @param lisIncrement
	 */
	private void addMarkers(LisIncrement lisIncrement) {
		//Create the new request to find all the Objects to the position contains in lisIncrement
		String request = Memory.getRequestMemorizator().getState();
		if(!(request.length()==0)) request = "("+request+") and ";
		request += lisIncrement.getName();
		Memory.getRequestMemorizator().add(request);

		//Retrieve all the Objects located to the gps position thanks to the request
		ObjectsExtent listExtent = new ObjectsExtent();
		ExtentReponse extentReponse=null;
		try {
			extentReponse = listExtent.execute().get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		LisExtent receivedExtent = extentReponse.getExtent();
		LisObject[] arrayExtent = receivedExtent.toArray();
		
		Double[] pos = parseLocation(lisIncrement.getName());
		for(LisObject objectExtent : arrayExtent){
			markerArray.add(new MarkerData(pos[0], pos[1], objectExtent.getRef()));
			adapterObjects.addCheckableItem(objectExtent.getRef());
		}

		//Undo the last request to be able to use it for the next position
		Memory.getRequestMemorizator().undo();
	}

	private Double[] parseLocation(String name) {
		String delims = "[ ;,\"]+";
		String[] tokens = name.split(delims);
		Double[] result = {Double.parseDouble(tokens[2]), Double.parseDouble(tokens[3])};
		return result;
	}

	public class MarkerData{
		public double lat, lon;
		public String title;

		public MarkerData(double pos, double pos2, String title) {
			this.lon=pos;
			this.lat=pos2;
			this.title=title;
		}

	}
}