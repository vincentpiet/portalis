package com.androlis;

import java.util.concurrent.ExecutionException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.androlis.javascriptCommunication.ChartDataLoader;
import com.androlis.memorizator.Memory;
import com.androlis.memorizator.TextViewState;
import com.androlis.networkCommunication.AttributesGetZoom;

import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

public class WordCloud extends Activity {
	private WebView webView=null;
	private final TextViewState attributeMemorizator = Memory.getAttributesMemorizator();
	private ImageButton previous;
	private ImageButton next;
	private Request request = null;
	private Button javascriptButton;


	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_word_cloud);
		
		request = (Request) findViewById(R.id.request);

		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());

		attributeMemorizator.clear();

		updateIncrements("all");

		//Create a WebView which is able to display JavaScript
		webView = (WebView)this.findViewById(R.id.word_cloud_webView); 
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);

		//Display timeline.html in this view
		webView.loadUrl("file:///android_asset/www/wordcloud.html");

		//Add a reference to a ChartDataLoader accessible from timeline.html
		webView.addJavascriptInterface(new ChartDataLoader(this, request), "chartData");

		javascriptButton = new Button(this);
		webView.addJavascriptInterface(javascriptButton, "javascriptButton");

		//click listener from the compound component
		//(need to be in an Activity)
		findViewById(R.id.request_text).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(WordCloud.this, RequestEdition.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.request_reload).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				cleanUpdate();				
			}
		});

		//If the path contain at least one element, remove the last element and update the list
		previous = (ImageButton) findViewById(R.id.word_cloud_left);
		previous.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//Erase the last element in attribute memorizator
				if(attributeMemorizator.size()>0) 
					attributeMemorizator.undo();

				String attribute=attributeMemorizator.getState();
				if(attribute.equals(""))
					attribute="all";

				updateIncrements(attribute);
				webView.reload();

			}
		});

		//Add the last attribute to the path and update the list
		next = (ImageButton) findViewById(R.id.word_cloud_right);
		next.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(attributeMemorizator.hasNext()){
					attributeMemorizator.redo();

					updateIncrements(attributeMemorizator.getState());
					webView.reload();
				}
			}
		});

		javascriptButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				runOnUiThread(new Runnable() {
					public void run() {
						request.changeRequest(Memory.getParam());
						cleanUpdate();
					}
				});
			}
		});
	}

	@Override
	protected void onResume() {
		super.onResume();
		request.reload();
		cleanUpdate();
	}

	private void cleanUpdate() {
		request.updateCurrentRequest();
		attributeMemorizator.clear();
		updateIncrements("all");
		((WebView) findViewById(R.id.word_cloud_webView)).reload();
	}
	/**
	 * Update the adapter through the request memorizator and the last attribute in the attribute memorizator
	 * if the last attribute has at least one child
	 */
	public boolean updateIncrements(String attribute) {
		AttributesGetZoom attributesGetZoom = new AttributesGetZoom();
		ZoomReponse zoomReponse=null;
		String[] params = {request.getCurrentRequest(),attribute};

		try {
			zoomReponse = attributesGetZoom.execute(params).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LisIncrementSet receivedIncrs = zoomReponse.getIncrements();
		LisIncrement[] increments = receivedIncrs.toArray();

		Memory.setIncrements(increments);
		return true;
	}
}
