package com.androlis.memorizator;

import java.util.ArrayList;
import java.util.Stack;

public class TextViewState {
	private Stack<String> memory = new Stack<String>();
	private Stack<String> bufferMemory = new Stack<String>();

	public TextViewState () {}

	/**
	 * Give the current state of the memorizator
	 */
	public String getState() {
		if(!memory.isEmpty())
			return memory.peek();

		return "";
	}

	/**
	 * Add a new state in the memorizator
	 */
	public void add(String state){
		memory.push(state);
		bufferMemory.clear();
	}

	/**
	 * Go to the previous state
	 */
	public void undo() {
		if(!memory.isEmpty()) {
			String temp = memory.pop();
			bufferMemory.push(temp);
		}
	}

	/**
	 * Go to the next state
	 */
	public void redo() {
		if(hasNext()) {
			String temp = bufferMemory.pop();
			memory.push(temp);
		}
	}

	/**
	 * Give all strings memorized in an array list
	 */
	public ArrayList<String> getAll() {
		return new ArrayList<String>(memory);
	}
	
	public int size(){
		return memory.size();
	}
	
	/**
	 * Clean all states saved
	 */
	public void clear() {
		memory.clear();
		bufferMemory.clear();
	}
	
	/**
	 * Clean only states next to the current state
	 */
	public void clean() {
		bufferMemory.clear();
	}
	
	public boolean hasNext() {
		return !bufferMemory.isEmpty();
	}
	
	public boolean hasPrevious() {
		return size()>0;
	}
}
