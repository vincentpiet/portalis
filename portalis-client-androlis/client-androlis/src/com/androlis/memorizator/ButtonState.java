package com.androlis.memorizator;

public class ButtonState {
	private boolean state;
	
	public ButtonState() {
		state=false;
	}
	
	public boolean getState() {
		return state;
	}
	
	public void changeState() {
		state=!state;
	}
}
