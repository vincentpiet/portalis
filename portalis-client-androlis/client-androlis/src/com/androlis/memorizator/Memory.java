package com.androlis.memorizator;

import java.util.HashMap;
import java.util.List;

import com.androlis.list.Doublet;

import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;

public class Memory {
	
	private static TextViewState requestMemorizator =  new TextViewState();;
	private static TextViewState attributesMemorizator = null;
	
	//Boolean which must be true if the current CamelisService contains GPS positions
	//Allow the user to use the GoogleMap
	private static Boolean isMap = null;
	
	
	private static Session portalisSession=null;
	private static Session camelisSession=null;
	
	//Contain the array of the previous objects load on the Objects activity
	private static LisObject[] arrayObjects=null;
	
	//Contain camelis services
	//(key : shared camelis services name, value : list of camelis services)
	public static HashMap<String, List<ActiveLisService>> activeLisServices = new HashMap<String, List<ActiveLisService>>();
	
	//Contain attributes data
	public static LisIncrement[] increments = null;
	
	//Contain the focused Object for details
	public static LisObject focusedObject = null;
	
	//Contain the current camelis name
	public static String camelisName=null;
	
	//Contain the kmls names
	public static Doublet[] kmlsNames = {new Doublet("massifs arbustifs",true),new Doublet("pelouses",true),new Doublet("surfaces minerales",true),new Doublet("massifs floraux",true),new Doublet("rosiers",true)};
	
	public static String[] param = {};
	
	private Memory() {}
	
	public static TextViewState getRequestMemorizator() {
		return requestMemorizator;
	}
	
	public static TextViewState getAttributesMemorizator() {
		return attributesMemorizator;
	}

	public static Boolean getIsMap() {
		return isMap;
	}

	public static void setIsMap(Boolean isMap) {
		Memory.isMap = isMap;
	}

	public static Session getPortalisSession() {
		return portalisSession;
	}

	public static void setPortalisSession(Session portalisSession) {
		Memory.portalisSession = portalisSession;
	}

	public static HashMap<String, List<ActiveLisService>> getActiveLisServices() {
		return activeLisServices;
	}

	public static void setActiveLisServices(HashMap<String, List<ActiveLisService>> camelisServices) {
		Memory.activeLisServices = camelisServices;
	}

	public static Session getCamelisSession() {
		return camelisSession;
	}

	public static void setCamelisSession(Session camelisSession) {
		Memory.camelisSession = camelisSession;
	}

	public static LisObject[] getArrayObjects() {
		return arrayObjects;
	}

	public static void setArrayObjects(LisObject[] arrayObjects) {
		Memory.arrayObjects = arrayObjects;
	}

	public static LisIncrement[] getIncrements() {
		return increments;
	}

	public static void setIncrements(LisIncrement[] increments) {
		Memory.increments = increments;
	}

	public static LisObject getFocusedObject() {
		return focusedObject;
	}

	public static void setFocusedObject(LisObject lisObject) {
		Memory.focusedObject = lisObject;
	}
	
	public static String getCamelisName() {
		return camelisName;
	}
	
	public static void setCamelisName(String camelisName) {
		Memory.camelisName = camelisName;
	}
	
	public static Doublet[] getKmlsNames() {
		return kmlsNames;
	}
	
	public static void setKmlsNames(Doublet[] kmlsNames) {
		Memory.kmlsNames = kmlsNames;
	}
	
	public static void initKmls() {
		Doublet[] kmlsArray = {new Doublet("massifs arbustifs",true),new Doublet("pelouses",true),new Doublet("surfaces minerales",true),new Doublet("massifs floraux",true),new Doublet("rosiers",true)};
		Memory.kmlsNames = kmlsArray;
	}
	
	public static void setParam(String[] param) {
		Memory.param = param;
	}
	
	public static String[] getParam() {
		return param;
	}
	
	public static void serviceInitFactory(){
		requestMemorizator = new TextViewState();
		attributesMemorizator = new TextViewState();
		isMap = Boolean.valueOf(true);
		arrayObjects=null;
		increments = null;
		focusedObject = null;
		camelisName=null;
		//TODO : initialiser kmlsNames
		
	}

}
