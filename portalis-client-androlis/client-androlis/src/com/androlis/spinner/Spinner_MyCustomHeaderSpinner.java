package com.androlis.spinner;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.androlis.Attributes;
import com.androlis.MenuService;
import com.androlis.Objects;
import com.androlis.R;
import com.androlis.Services;
import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.log.LoggerProvider;
//import org.apache.log4j.Logger;

public class Spinner_MyCustomHeaderSpinner {
	private final static LoggerProvider logger = LoggerProvider.getDefault(Spinner_MyCustomHeaderSpinner.class);
	
	@SuppressWarnings("serial")
	private final static List<String> spinnerItems = new ArrayList<String>() {{add("services"); add("menu"); add("request"); add("attributes"); add("objects");}};

	public Spinner_MyCustomHeaderSpinner() {}

	static public void initSpinner(Spinner spinner, final Context context, final String ident) {
		logger.info("function initSpinner called.");
		
		spinner.setVisibility(View.VISIBLE);

		//Search the position of the name of the current view inside the spinner
		final int positionToHide=spinnerItems.indexOf(ident);

		int indexOfMap;
		if(Memory.getIsMap())
			indexOfMap = -1;
		else
			indexOfMap = spinnerItems.indexOf("map");

		//Need to be final to be able to use it inside an inner class
		final int map = indexOfMap;

		//Create an adapter which is able to hide spinner items
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,android.R.layout.simple_dropdown_item_1line, context.getResources().getStringArray(R.array.spinner_string_array)) {
			@Override
			public View getDropDownView(int position, View convertView, ViewGroup parent)
			{
				View v = null;

				//Hide the item if it is in a position to hide
				if (position == positionToHide || position == map) {
					TextView tv = new TextView(getContext());
					tv.setHeight(0);
					tv.setVisibility(View.GONE);
					v = tv;
				}
				else
					v = super.getDropDownView(position, null, parent);

				// Hide scroll bar because it appears sometimes unnecessarily
				parent.setVerticalScrollBarEnabled(false);
				return v;
			}
		};

		spinner.setAdapter(adapter);

		//Set the item show 
		spinner.setSelection(positionToHide);

		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
				Intent intent;

				//Change the view if the position selected in the spinner is not the current one
				if(position != positionToHide) {
					switch (position) {
					case 0:
						intent = new Intent(context, Services.class);
						context.startActivity(intent);
						break;
						
					case 1:
						intent = new Intent(context, MenuService.class);
						context.startActivity(intent);
						break;
						
					case 3:
						intent = new Intent(context, Attributes.class);
						context.startActivity(intent);
						break;

					case 4:
						intent = new Intent(context, Objects.class);
						context.startActivity(intent);
						break;
					}
				}
			}

			public void onNothingSelected(AdapterView<?> parentView) {
			}
		});
	}
}

