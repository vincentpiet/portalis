package com.androlis.list;

public class List_Item {
	private String item;
	private int itemType;
	private int id;
	
	public List_Item(String item, int itemType, int id) {
		this.item=item;
		this.itemType=itemType;
		this.id=id;
	}

	public String getItem() {
		return item;
	}

	public int getItemType() {
		return itemType;
	}
	
	public int getId() {
		return id;
	}
}
