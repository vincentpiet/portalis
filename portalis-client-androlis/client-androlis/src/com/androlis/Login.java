package com.androlis;

import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.LoginPortalis;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.PortalisService;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;

public class Login extends Activity {
	private final LoggerProvider logger = LoggerProvider.getDefault(Login.class);
	private Button connectButton;
	private TextView error;
	private String userId;
	private String userPwd;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("Login activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		((TextView) findViewById(R.id.header_title)).setText("Login");
		((TextView) findViewById(R.id.header_subtitle)).setVisibility(View.GONE);

		PortalisService.getInstance().init("lisfs2008.irisa.fr", 8080, "portalis");

		//Check if the user can access to camlis on click on connectButton
		error = (TextView) findViewById(R.id.login_error); 
		connectButton = (Button) findViewById(R.id.login_connectButton);
		connectButton.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//Hides the soft keyboard when connect button is clicked
				InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
				inputManager.toggleSoftInput(0, 0);

				EditText editTextId=(EditText) findViewById(R.id.login_userId); 
				userId=editTextId.getText().toString();

				EditText editTextPwd=(EditText) findViewById(R.id.login_passwd); 
				userPwd=editTextPwd.getText().toString();

				logger.info("Connection tried with userID :"+userId+", pwd :"+userPwd);

				//Ask portalis if informations entered by user refer to an allowed account
				LoginPortalis loginTask = new LoginPortalis();
				LoginReponse loginReponse = null;
				try {
					String [] att = {userId, userPwd};
					loginReponse = loginTask.execute(att).get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}


				if(loginReponse.isOk()) {
					logger.info("Connection succeeded");

					error.setVisibility(View.INVISIBLE);

					//Retrive the new portalis session
					try {
						Memory.setPortalisSession(loginReponse.getSession());
					} catch (PortalisException e1) {
						e1.printStackTrace();
					}

					Intent intent = new Intent(Login.this, Services.class);
					startActivity(intent);
				} 

				else {
					logger.info("Connection failed");
					error.setVisibility(View.VISIBLE);
					error.setText(loginReponse.getMessagesAsString());
				}
			}
		});
	}
}