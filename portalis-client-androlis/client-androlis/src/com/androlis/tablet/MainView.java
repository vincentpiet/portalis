package com.androlis.tablet;

import android.app.TabActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TabHost;
import android.widget.TextView;

import com.androlis.JSMaps;
import com.androlis.R;

@SuppressWarnings("deprecation")
public class MainView extends TabActivity {

	private TabHost tabHost;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main_view);
		
		tabHost = (TabHost) findViewById(android.R.id.tabhost);
		 setupTab("Glis", "tab1", new Intent().setClass(this, Glis.class));
		 setupTab("Maps", "tab2", new Intent().setClass(this, JSMaps.class));
		 setupTab("Chart", "tab3", new Intent().setClass(this, ChartView.class));
	}
	
    private void setupTab(String name, String tag, Intent intent) {
		tabHost.addTab(tabHost.newTabSpec(tag).setIndicator(createTabView(tabHost.getContext(), name)).setContent(intent));
	}

    private static View createTabView(final Context context, final String text) {
		View view = LayoutInflater.from(context).inflate(R.layout.tab_item, null);
		TextView tv = (TextView) view.findViewById(R.id.tabsText);
		tv.setText(text);

		return view;
	}
}
