package com.androlis.tablet;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.TextView;

import com.androlis.R;
import com.androlis.memorizator.Memory;

public class Glis extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_glis);

		TextView headerTitle = (TextView) findViewById(R.id.header_title);
		headerTitle.append(" : "+Memory.getCamelisName());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.glis, menu);
		return true;
	}

}
