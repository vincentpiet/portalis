package com.androlis;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.androlis.list.List_MyCustomAdapter;
import com.androlis.memorizator.Memory;
import com.androlis.memorizator.TextViewState;
import com.androlis.networkCommunication.AttributesGetZoom;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrement;
import fr.irisa.lis.portalis.shared.camelis.data.LisIncrementSet;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;

public class Attributes extends Activity {
	private final LoggerProvider logger = LoggerProvider.getDefault(Attributes.class);
	private ListView listItems;
	private EditText searchBox;
	private List_MyCustomAdapter adapter;
	private TextView location;
	private HorizontalScrollView scrollView;
	private final TextViewState attributesMemorizator = Memory.getAttributesMemorizator();
	private ImageButton previous;
	private ImageButton next;
	private Request request = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("Attributes activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_attributes);

		request = (Request) findViewById(R.id.request);

		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());

		//reset attribute path
		location= (TextView) findViewById(R.id.attributes_location);
		attributesMemorizator.clear();

		//Add attributes from request to the list
		listItems = (ListView) findViewById(R.id.attribute_list_view);
		updateAdapter("all");

		//click listener from the compound component
		//(need to be in an Activity)
		findViewById(R.id.request_text).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Attributes.this, RequestEdition.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.request_reload).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				request.updateCurrentRequest();
				((ImageButton) findViewById(R.id.request_reload)).setImageResource(R.drawable.reload);
				cleanUpdate();			
			}
		});

		//If the path contain at least one element, remove the last element and update the list
		previous = (ImageButton) findViewById(R.id.attribute_left);
		previous.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				//Erase the last element in attribute memorizator
				if(attributesMemorizator.size()>0)
					attributesMemorizator.undo();

				//Retrieve all elements contains by attribute memorizator
				ArrayList<String> path = attributesMemorizator.getAll();

				//Add all these elements to the displayed path
				location.setText("/");
				for(String str : path)
					location.append(" > "+str);

				//Update the list displayed through the last element of that path
				String attribute="all";
				if(path.size()>0)
					attribute=path.get(path.size()-1);

				updateAdapter(attribute);
			}
		});

		//Add the last attribute to the path and update the list
		next = (ImageButton) findViewById(R.id.attribute_right);
		next.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				if(attributesMemorizator.hasNext()){
					attributesMemorizator.redo();

					//Add the last attribute to the path
					location.append(" > "+attributesMemorizator.getState());

					updateAdapter(attributesMemorizator.getState());
				}
			}
		});

		//OnItemClick, add the item click to the path and update the list
		scrollView = (HorizontalScrollView) findViewById(R.id.attributes_location_scroll_view);
		listItems.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String item = adapter.getItem(position).getItem();

				//Update displayed attributes
				Boolean isUpdate=updateAdapter(item);

				//If the attribute clicked has at least one child
				if (isUpdate) {
					//Update the path displayed
					attributesMemorizator.add(item);
					location.append(" > "+item);

					//Add a needed delay between the modification of the path and the scoll.
					//Without it, the scoll can be made before the modification of the path
					scrollView.postDelayed(new Runnable() {
						public void run() {
							scrollView.fullScroll(HorizontalScrollView.FOCUS_RIGHT);
						}
					}, 300L);
				}
			}
		});

		//Use the filter function on the adapter when the searchBox text is changed
		searchBox = (EditText) findViewById(R.id.attribute_input_search);
		searchBox.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				adapter.getFilter().filter(cs);  
			}

			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}

			public void afterTextChanged(Editable arg0) {
			}
		});

		//Registering context menu for the listview
		registerForContextMenu(listItems);
	}

	@Override
	protected void onResume() {
		super.onResume();
		request.reload();
		cleanUpdate();
	}

	/** This will be invoked when an item in the listview is long pressed
	 * Make the contextual menu appears */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.contextual_menu_attributes , menu);

		//Retrieve the position of the pressed item
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) menuInfo;
		int position = info.position;

		//Get the item name from the position
		//(this is why we need a custom adapter)
		String itemName = adapter.getItem(position).getItem();

		menu.setHeaderTitle(getString(R.string.attribute)+" : "+itemName);
	}

	/** This will be invoked when a menu item is selected
	 * Use to assign actions to the items from the contextual menu */
	@Override
	public boolean onContextItemSelected(MenuItem item) {

		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		int position = info.position;
		String itemName = adapter.getItem(position).getItem();
		String[] params = {itemName};

		switch(item.getItemId()){
		case R.id.contextual_menu_attributes_selection:
			request.changeRequest(params);
			cleanUpdate();
			((ImageButton) findViewById(R.id.request_reload)).setImageResource(R.drawable.reload);
			request.updateCurrentRequest();
			break;
		}
		return true;
	}

	private void cleanUpdate() {
		attributesMemorizator.clear();
		location.setText("/");
		updateAdapter("all");
	}

	/**
	 * Hide the soft keyboard when it is clicked outside an EditText component
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) { 
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	/**
	 * Update the adapter through the request memorizator and the last attribute in the attribute memorizator
	 * if the last attribute has at least one child
	 */
	public boolean updateAdapter(String attribute) {

		AttributesGetZoom attributesGetZoom = new AttributesGetZoom();
		ZoomReponse zoomReponse=null;
		String[] params = {request.getCurrentRequest(),attribute};

		try {
			zoomReponse = attributesGetZoom.execute(params).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LisIncrementSet receivedIncrs = zoomReponse.getIncrements();
		LisIncrement[] increments = receivedIncrs.toArray();

		if(increments.length!=0) {
			adapter= new List_MyCustomAdapter(getApplicationContext());
			for(LisIncrement increment : increments)
				adapter.addItem(increment.getName());

			listItems.setAdapter(adapter);
			return true;
		}
		return false;
	}
}

