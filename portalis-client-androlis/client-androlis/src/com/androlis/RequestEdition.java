package com.androlis;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;

import com.androlis.memorizator.Memory;
import com.androlis.memorizator.TextViewState;

import fr.irisa.lis.portalis.log.LoggerProvider;

public class RequestEdition extends Activity {
	private final LoggerProvider logger = LoggerProvider.getDefault(RequestEdition.class);
	private EditText text;
	private ImageButton undo;
	private ImageButton confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("RequestEdition activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_request_edition);
		
		final TextViewState requestMemorizator = Memory.getRequestMemorizator();
		
		text = (EditText) findViewById(R.id.requestedition_text);
		text.append(requestMemorizator.getState());
		
		//Buttons Listener
		undo = (ImageButton) findViewById(R.id.request_edition_undo);
		undo.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				text.setText(requestMemorizator.getState());
			}
		});
		
		confirm = (ImageButton) findViewById(R.id.request_edition_confirm);
		confirm.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				requestMemorizator.add(text.getText().toString());
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
				finish();
			}
		});
	}
	@Override  
	public void onBackPressed() {
		InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
		finish();
	}
}
