package com.androlis.networkCommunication;

import android.os.AsyncTask;
import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;
import fr.irisa.lis.portalis.shared.http.admin.AdminHttp;

public class ServicesGetCamelisSessions extends AsyncTask<Session, Void, PingReponse> {
	private final LoggerProvider logger = LoggerProvider.getDefault(ServicesGetCamelisSessions.class);


	@Override
	protected PingReponse doInBackground(Session... params) {
		logger.info("Start ServicesGetCamelisSessions Task");
		PingReponse pingReponse = AdminHttp.getActiveLisServicesOnPortalis(params[0]);
		return pingReponse;
	}
	
	@Override
	protected void onPostExecute(PingReponse result) {
		super.onPostExecute(result);
	}
}
