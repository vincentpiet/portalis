package com.androlis.networkCommunication;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp;

public class ObjectsExtent extends AsyncTask<Void, Void, ExtentReponse> {
	private final LoggerProvider logger = LoggerProvider.getDefault(ObjectsExtent.class);


	@Override
	protected ExtentReponse doInBackground(Void... params) {
		logger.info("Start ObjectsExtent Task");
		String request = Memory.getRequestMemorizator().getState();
		if(request==null || request.length()==0)
			request="all";
		
		ExtentReponse extentReponse = CamelisHttp.extent(Memory.getCamelisSession(),request);
		return extentReponse;
	}
	
	@Override
	protected void onPostExecute(ExtentReponse extentReponse) {
		super.onPostExecute(extentReponse);
	}
}
