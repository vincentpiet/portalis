package com.androlis.networkCommunication;

import android.os.AsyncTask;
import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.admin.reponse.LoginReponse;
import fr.irisa.lis.portalis.shared.http.admin.AdminHttp;

public class LoginPortalis extends AsyncTask<String, Void, LoginReponse> {
	private final LoggerProvider logger = LoggerProvider.getDefault(LoginPortalis.class);


	@Override
	protected LoginReponse doInBackground(String... params) {
		logger.info("Start LoginPortalis Task");
		LoginReponse loginReponse = AdminHttp.login(params[0], params[1]);
		return loginReponse;
	}
	
	@Override
	protected void onPostExecute(LoginReponse result) {
		super.onPostExecute(result);
	}
}
