package com.androlis.networkCommunication;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.camelis.reponse.ZoomReponse;
import fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp;

public class AttributesGetZoom extends AsyncTask<String, Void, ZoomReponse> {
	private final LoggerProvider logger = LoggerProvider.getDefault(AttributesGetZoom.class);


	@Override
	protected ZoomReponse doInBackground(String... params) {
		logger.info("Start AttributesGetZoom Task");
		ZoomReponse zoomReponse = null;
		
		if(params.length==2) {
			String request = params[0];
			if(request==null || request.length()==0)
				request="all";

			zoomReponse = CamelisHttp.zoom(Memory.getCamelisSession(), request, params[1]);
		}
		return zoomReponse;
	}

	@Override
	protected void onPostExecute(ZoomReponse zoomReponse) {
		super.onPostExecute(zoomReponse);
	}
}
