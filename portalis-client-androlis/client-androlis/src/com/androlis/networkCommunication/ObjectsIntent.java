package com.androlis.networkCommunication;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;
import fr.irisa.lis.portalis.shared.http.camelis.CamelisHttp;

public class ObjectsIntent extends AsyncTask<String, Void, IntentReponse> {
	private final LoggerProvider logger = LoggerProvider.getDefault(ObjectsIntent.class);


	@Override
	protected IntentReponse doInBackground(String... params) {
		logger.info("Start ObjectsIntent Task");
		IntentReponse intentReponse = CamelisHttp.intent(Memory.getCamelisSession(), params);
		return intentReponse;
	}
	
	@Override
	protected void onPostExecute(IntentReponse intentReponse) {
		super.onPostExecute(intentReponse);
	}
}
