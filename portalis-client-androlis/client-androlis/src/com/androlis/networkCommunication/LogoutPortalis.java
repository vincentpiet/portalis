package com.androlis.networkCommunication;

import android.os.AsyncTask;

import com.androlis.memorizator.Memory;

import fr.irisa.lis.portalis.shared.http.admin.AdminHttp;

public class LogoutPortalis extends AsyncTask<Void, Void, Void> {

	@Override
	protected Void doInBackground(Void... params) {
		AdminHttp.logout(Memory.getPortalisSession());
		return null;
	}
}
