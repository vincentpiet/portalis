package com.androlis;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.androlis.list.List_MyCustomAdapter;
import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.ObjectsExtent;
import com.androlis.networkCommunication.ObjectsIntent;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.camelis.data.LisExtent;
import fr.irisa.lis.portalis.shared.camelis.data.LisIntent;
import fr.irisa.lis.portalis.shared.camelis.data.LisObject;
import fr.irisa.lis.portalis.shared.camelis.data.Property;
import fr.irisa.lis.portalis.shared.camelis.reponse.ExtentReponse;
import fr.irisa.lis.portalis.shared.camelis.reponse.IntentReponse;

public class Objects extends Activity {
	private final LoggerProvider logger = LoggerProvider.getDefault(Objects.class);
	private ListView listItems;
	private ImageButton configuration;
	private EditText searchBox;
	private List_MyCustomAdapter list_adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		logger.info("Objects activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_objects);

		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());

		//Link the adapter to the list
		listItems = (ListView) findViewById(R.id.object_list_view);
		listItems.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);

		updateAdapter();

		((Request) findViewById(R.id.request)).hideButtons();
		//click listener from the compound componant
		//(need to be in an Activity)
		findViewById(R.id.request_text).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(Objects.this, RequestEdition.class);
				startActivity(intent);
			}
		});

		findViewById(R.id.request_reload).setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				((Request) findViewById(R.id.request)).updateCurrentRequest();				
				((ImageButton) findViewById(R.id.request_reload)).setImageResource(R.drawable.reload);
				updateAdapter();				
			}
		});

		//Use the filter function on the adapter when the searchBox text is changed
		searchBox = (EditText) findViewById(R.id.object_input_search);
		searchBox.addTextChangedListener(new TextWatcher() {
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				list_adapter.getFilter().filter(cs);  
			}

			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}

			public void afterTextChanged(Editable arg0) {
			}
		});

		//Check the checkbox if the user click on the ListItem (not only on the checkbox)
		listItems.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				CheckBox checkbox = (CheckBox) view.findViewById(R.id.list_checkable_item_checkbox);
				checkbox.setChecked(!checkbox.isChecked());
			}
		});

		//Display object clicked details
		listItems.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				//Retrieve the position of the selected item
				int pos=list_adapter.getItem(position).getId();
				Memory.setFocusedObject(Memory.getArrayObjects()[pos]);

				Intent intent = new Intent(Objects.this, ObjectDetail.class);
				startActivity(intent);
				return false;
			}
		});

		//Button listener
		configuration = (ImageButton) findViewById(R.id.objects_configuration_button);
		configuration.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				v.showContextMenu();
			}
		});

		//Registering context menu for the listview
		registerForContextMenu(configuration);
	}

	/** This will be invoked when an item in the listview is long pressed
	 * Make the contextual menu appears */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);    	
		getMenuInflater().inflate(R.menu.contextual_menu_objects , menu);

		menu.setHeaderTitle(getString(R.string.objects));
	}


	/** This will be invoked when a menu item is selected
	 * Use to assign actions to the items from the contextual menu */
	@Override
	public boolean onContextItemSelected(MenuItem item) {

		switch(item.getItemId()){
		case R.id.contextual_menu_objects_selection:
			//Retrive Oid of checked items
			ArrayList<Integer> itemChecked=list_adapter.getChecked();
			if(itemChecked.size()!=0){
				ArrayList<String> itemsOid=new ArrayList<String>();
				for(Integer position : itemChecked)
					itemsOid.add(String.valueOf(Memory.getArrayObjects()[position].getOid()));

				String[] itemOidArray = itemsOid.toArray(new String[itemsOid.size()]);

				//Get IntentReponse
				ObjectsIntent objectsIntent = new ObjectsIntent();
				IntentReponse intentReponse=null;
				try {
					intentReponse = objectsIntent.execute(itemOidArray).get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}

				LisIntent receivedIntent = intentReponse.getIntent();
				Property[] arrayIntent = receivedIntent.toArray();

				//Create a string array from a property array
				String[] arrayProperty = new String[arrayIntent.length];
				for(int i=0; i<arrayIntent.length; i++)
					arrayProperty[i]=arrayIntent[i].getName();

				((com.androlis.Request) findViewById(R.id.request)).changeRequest(arrayProperty);
				((Request) findViewById(R.id.request)).updateCurrentRequest();
				((ImageButton) findViewById(R.id.request_reload)).setImageResource(R.drawable.reload);

				updateAdapter();
			}
			break;
		case R.id.contextual_menu_objects_uncheck:
			list_adapter.unCheck();
			listItems.invalidate();
			break;
		}
		return true;
	}

	@Override
	protected void onResume() {
		super.onResume();
		((Request) findViewById(R.id.request)).reload();
		updateAdapter();
		
	}

	/**
	 * Hide the soft keyboard when it is clicked outside an EditText component
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {

		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) { 
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	/**
	 * Retrieve objects to display through the request
	 */
	public void updateAdapter() {
		ObjectsExtent listExtent = new ObjectsExtent();
		ExtentReponse extentReponse=null;
		try {
			extentReponse = listExtent.execute().get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}

		LisExtent receivedExtent = extentReponse.getExtent();
		LisObject[] arrayExtent = receivedExtent.toArray();
		Memory.setArrayObjects(arrayExtent);

		list_adapter = new List_MyCustomAdapter(this);

		for(LisObject objectExtent : arrayExtent)
			list_adapter.addCheckableItem(objectExtent.getRef());

		listItems.setAdapter(list_adapter);
	}
}
