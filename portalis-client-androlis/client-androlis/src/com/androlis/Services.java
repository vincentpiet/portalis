package com.androlis;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.concurrent.ExecutionException;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.androlis.list.List_Item;
import com.androlis.list.List_MyCustomAdapter;
import com.androlis.memorizator.Memory;
import com.androlis.networkCommunication.LogoutPortalis;
import com.androlis.networkCommunication.ServicesGetCamelisSessions;

import fr.irisa.lis.portalis.log.LoggerProvider;
import fr.irisa.lis.portalis.shared.admin.PortalisException;
import fr.irisa.lis.portalis.shared.admin.data.ActiveLisService;
import fr.irisa.lis.portalis.shared.admin.data.Session;
import fr.irisa.lis.portalis.shared.camelis.reponse.PingReponse;

public class Services extends Activity {
	private final LoggerProvider logger = LoggerProvider.getDefault(Services.class);
	private ListView listItems;
	private EditText searchBox;
	private List_MyCustomAdapter adapter;
	private ImageButton services_refresh;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		logger.info("Services activity start");

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_services);
		
		((TextView) findViewById(R.id.header_title)).setText("Services");
		((TextView) findViewById(R.id.header_subtitle)).setVisibility(View.GONE);

		adapter = new List_MyCustomAdapter(this);
		activeLisServicesUpdate();
		displayCamelisServices(this);
			
		listItems = (ListView) findViewById(R.id.list_view);
		listItems.setAdapter(adapter);

		//Use the filter function on the adapter when the searchBox text is changed
		searchBox = (EditText) findViewById(R.id.inputSearch);
		searchBox.addTextChangedListener(new TextWatcher() {
			@Override
			public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
				adapter.getFilter().filter(cs);  
			}

			@Override
			public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
					int arg3) {
			}

			@Override
			public void afterTextChanged(Editable arg0) {
			}
		});

		//OnItemClick launch the good CamelisService
		listItems.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//Retrieve the item selected
				List_Item item = (List_Item) listItems.getItemAtPosition(position);

				Memory.setCamelisName(item.getItem());

				//Parse the item name
				LinkedList<String> serviceName = new LinkedList<String>();

				StringTokenizer st = new StringTokenizer(item.getItem()," ");
				while ( st.hasMoreTokens() ) {
					serviceName.add(st.nextToken());
				}

				//Retrieve the list which contains the camelisService linked to the selected item
				List<ActiveLisService> listServices = Memory.getActiveLisServices().get(serviceName.get(0));

				//Retrieve the camelis service through the port
				ActiveLisService activeLisService = null;
				for(ActiveLisService service : listServices)
					if(String.valueOf(service.getPort()).equals(serviceName.get(1)))
						activeLisService=service;

				//Launch a camelis session
				Session camelisSession=null;
				
				try {
					camelisSession = Session.createCamelisSession(Memory.getPortalisSession(), activeLisService);
				} catch (PortalisException e) {
					e.printStackTrace();
				}
				Memory.setCamelisSession(camelisSession);

				Intent intent = new Intent(Services.this, MenuService.class);
				startActivity(intent);
			}
		});
		
		services_refresh = (ImageButton) findViewById(R.id.services_refresh);
		services_refresh.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				activeLisServicesUpdate();
				displayCamelisServices(Services.this);
			}
		});
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		Memory.serviceInitFactory();
	}

	/**
	 * Hide the soft keyboard when it is clicked outside an EditText component
	 */
	@Override
	public boolean dispatchTouchEvent(MotionEvent event) {
		View v = getCurrentFocus();
		boolean ret = super.dispatchTouchEvent(event);

		if (v instanceof EditText) {
			View w = getCurrentFocus();
			int scrcoords[] = new int[2];
			w.getLocationOnScreen(scrcoords);
			float x = event.getRawX() + w.getLeft() - scrcoords[0];
			float y = event.getRawY() + w.getTop() - scrcoords[1];

			if (event.getAction() == MotionEvent.ACTION_UP && (x < w.getLeft() || x >= w.getRight() || y < w.getTop() || y > w.getBottom()) ) { 
				InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
				imm.hideSoftInputFromWindow(getWindow().getCurrentFocus().getWindowToken(), 0);
			}
		}
		return ret;
	}

	/**
	 * Ping Portalis
	 */
	public PingReponse pingPortalisSession() {
		logger.info("Ping Portalis");
		ServicesGetCamelisSessions listServicesTask = new ServicesGetCamelisSessions();
		PingReponse pingReponse=null;
		try {
			pingReponse = listServicesTask.execute(Memory.getPortalisSession()).get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return pingReponse;
	}

	/**
	 * Update the HashMap which contains camelis services
	 */
	public void activeLisServicesUpdate() {
		logger.info("Update services known");

		Memory.getActiveLisServices().clear();

		PingReponse pingReponse = pingPortalisSession();
		if(!pingReponse.isOk()) logger.error("PingReponse is not OK");

		else {
			for(ActiveLisService activeLisService : pingReponse.getLesServices()) {
				//TODO : bug sur les servies IDs
				activeLisService.getPid();
				List<ActiveLisService> similarServices = Memory.getActiveLisServices().get(activeLisService.getFullName());

				if(similarServices == null) {
					similarServices = new ArrayList<ActiveLisService>();
					similarServices.add(activeLisService);
					Memory.getActiveLisServices().put(activeLisService.getFullName(), similarServices);
				} else {
					similarServices.add(activeLisService);
				}

			}
		}
		
		registerForContextMenu(findViewById(R.id.services_view_menu));
	}


	/**
	 * Add Camelis Services from the HashMap to the adapter, to display them
	 * @param ctx 
	 */
	public void displayCamelisServices(Context ctx) {
		logger.info("Add camelis services to the adapter");
		Collection<List<ActiveLisService>> activeLisServices = Memory.getActiveLisServices().values();
		adapter.clean();
		for(List<ActiveLisService> similarServices : activeLisServices){
			adapter.addSeparatorItem(similarServices.get(0).getFullName());

			for(ActiveLisService activeLisService : similarServices)
				adapter.addItem(activeLisService.getFullName()+" "+activeLisService.getPort()+" ("+activeLisService.getNbObject()+")");
		}
	}
	
	@Override
	public void onBackPressed() {
		openContextMenu(findViewById(R.id.services_view_menu));
	}
	
	
	/** This will be invoked when an item in the listview is long pressed
	 * Make the contextual menu appears */
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		getMenuInflater().inflate(R.menu.session_quit , menu);

		menu.setHeaderTitle(getString(R.string.services_quit_session));
	}

	/** This will be invoked when a menu item is selected
	 * Use to assign actions to the items from the contextual menu */
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch(item.getItemId()){
		case R.id.session_quit_yes:
			LogoutPortalis logoutPortalis = new LogoutPortalis();
			logoutPortalis.execute();
			finish();
			break;
		case R.id.session_quit_no:
			break;
		}
		return true;
	}
}