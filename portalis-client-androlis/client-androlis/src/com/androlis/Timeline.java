package com.androlis;

import java.util.HashMap;
import java.util.Set;

import org.json.JSONArray;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.androlis.memorizator.Memory;
import com.androlis.spinner.Spinner_MyCustomSpinner;

public class Timeline extends Activity {
	private WebView webView=null;
	private Spinner vertical=null;
	private Spinner horizontal=null;
	private HashMap<String, String[]> tables = null;
	private Button ok=null;

	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_timeline);
		
		((TextView) findViewById(R.id.header_title)).setText("Visu | ");
		((TextView) findViewById(R.id.header_subtitle)).setText(Memory.getCamelisName());

		createTables();
		Set<String> itemsSet=tables.keySet();
		String[] items=itemsSet.toArray(new String[itemsSet.size()]);

		final ChartDataLoader chartDataLoader = new ChartDataLoader();
		chartDataLoader.tables=tables;

		//Initialize spinners
		vertical=(Spinner) findViewById(R.id.timeline_choices_spinner_vertical);
		Spinner_MyCustomSpinner customVerticalSpinner = new Spinner_MyCustomSpinner();
		customVerticalSpinner.initSpinner(vertical, this, 0, items);

		horizontal=(Spinner) findViewById(R.id.timeline_choices_spinner_horizontal);
		Spinner_MyCustomSpinner customHorizontalSpinner = new Spinner_MyCustomSpinner();
		customHorizontalSpinner.initSpinner(horizontal, this, 1, items);

		//Create a WebView which is able to display JavaScript
		webView = (WebView)this.findViewById(R.id.timeline_webView); 
		webView.getSettings().setBuiltInZoomControls(true);
		webView.getSettings().setJavaScriptEnabled(true);
		
		//Display timeline.html in this view
		webView.loadUrl("file:///android_asset/www/timeline.html");

		//Add a reference to a ChartDataLoader accessible from timeline.html
		webView.addJavascriptInterface(chartDataLoader, "chartData");

		//OnClick on ok button, reload the webView with the new selected axis
		ok=(Button)findViewById(R.id.timeline_choices_ok);
		ok.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				webView.reload();				
			}
		});
	}

	private void createTables() {
		tables=new HashMap<String, String[]>();
		String[] titres = {"President","President","President","Vice President","Vice President","Vice President","Vice President","Secretary of State","Secretary of State","Secretary of State","Secretary of State","Secretary of State","Secretary of State","Secretary of State","Secretary of State"};
		String[] personnes = {"George Washington","John Adams","Thomas Jefferson","John Adams","Thomas Jefferson","Aaron Burr","George Clinton","John Jay","Thomas Jefferson","Edmund Randolph","Timothy Pickering","Charles Lee","John Marshall","Levi Lincoln","James Madison"};

		tables.put("titres", titres);
		tables.put("personnes", personnes);

	}

	/**
	 * Create data readable from Javascript
	 */
	final class ChartDataLoader{
		HashMap<String, String[]> tables=null;
		int[][] startLabel = {{1789, 3, 29}, {1797, 2, 3}, {1801, 2, 3}, {1789, 3, 20}, {1797, 2, 3}, {1801, 2, 3}, {1805, 2, 3}, {1789, 8, 25}, {1790, 2, 21}, {1794, 0, 1}, {1795, 7, 19}, {1800, 4, 12}, {1800, 5, 12}, {1801, 2, 4}, {1801, 4, 1}};
		int[][] endLabel = {{1797, 2, 3}, {1801, 2, 3}, {1809, 2, 3}, {1797, 2, 3}, {1801, 2, 3}, {1805, 2, 3}, {1812, 3, 19}, {1790, 2, 21}, {1793, 11, 30}, {1795, 7, 19}, {1800, 4, 11}, {1800, 5, 4}, {1801, 2, 3}, {1801, 4, 0}, {1809, 2, 2}};



		/**
		 * Create a JSONArray which contains timeline data
		 * [["rowLabel","barLabel",[yyyy,mm,dd],[yyyy,mm,dd]],[...],...]
		 */
		@JavascriptInterface
		public JSONArray getArray() {
			JSONArray table = new JSONArray();

			//Retrieve names of selected items
			String[] vertically=getTable(vertical.getSelectedItem().toString());
			String[] horizontally=getTable(horizontal.getSelectedItem().toString());

			int sizeMax=Math.min(vertically.length, horizontally.length);

			for(int i=0; i<sizeMax; i++) {
				JSONArray timelineBar = new JSONArray();
				timelineBar.put(vertically[i%sizeMax]);
				timelineBar.put(horizontally[i%sizeMax]);

				JSONArray start = new JSONArray();
				start.put(startLabel[i%sizeMax][0]);
				start.put(startLabel[i%sizeMax][1]);
				start.put(startLabel[i%sizeMax][2]);
				timelineBar.put(start);

				JSONArray end = new JSONArray();
				end.put(endLabel[i%sizeMax][0]);
				end.put(endLabel[i%sizeMax][1]);
				end.put(endLabel[i%sizeMax][2]);
				timelineBar.put(end);

				table.put(timelineBar);
			}
			return table;
		}

		private String[] getTable(String name) {
			return tables.get(name);
		}


	}
}
